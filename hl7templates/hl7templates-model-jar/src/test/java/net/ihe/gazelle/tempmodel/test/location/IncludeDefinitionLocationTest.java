package net.ihe.gazelle.tempmodel.test.location;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.location.util.IncludeDefinitionLocation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class IncludeDefinitionLocationTest {

	@Test
	public void testGetLocation1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(rdfirst, "participantRole");
		ChoiceDefinition cd = RuleDefinitionUtil.getChoices(participantRole).get(0);
		IncludeDefinition inc = ChoiceDefinitionUtil.getIncludes(cd).get(0);
		String loc = IncludeDefinitionLocation.getLocation(inc);
		assertTrue(loc.equals("//rules/template[1]/element[1]/element[3]/choice[1]/include[1]"));
	}
	
	@Test
	public void testGetLocation2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/template1.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		IncludeDefinition inc = RuleDefinitionUtil.getIncludes(rdfirst).get(0);
		String loc = IncludeDefinitionLocation.getLocation(inc);
		assertTrue(loc.equals("//rules/template[1]/element[1]/include[1]"));
	}
	
	@Test
	public void testGetLocation3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/template3.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		IncludeDefinition inc = TemplateDefinitionUtil.getIncludes(td).get(0);
		String loc = IncludeDefinitionLocation.getLocation(inc);
		assertTrue(loc.equals("//rules/template[1]/include[1]"));
	}
	
	@Test
	public void testGetLocation4() throws FileNotFoundException, JAXBException {
		assertTrue(IncludeDefinitionLocation.getLocation(null).equals(""));
		assertTrue(IncludeDefinitionLocation.getLocation(new IncludeDefinition()).equals("/include"));
	}

}
