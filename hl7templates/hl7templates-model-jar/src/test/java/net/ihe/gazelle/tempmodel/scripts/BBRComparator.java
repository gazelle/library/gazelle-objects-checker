package net.ihe.gazelle.tempmodel.scripts;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

public class BBRComparator {
	
	public static void main1(String[] args) throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/tmp/ccda-R1.1.xml");
		dec.setProject(null);
		dec.setTerminology(null);
		dec.setDatasets(null);
		dec.setScenarios(null);
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/tmp/ccda-R1.1-marsh.xml"));
		dec = DecorMarshaller.loadDecor("/home/aboufahj/tmp/ccda-R1.1-notfixed.xml");
		dec.setProject(null);
		dec.setTerminology(null);
		dec.setDatasets(null);
		dec.setScenarios(null);
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/tmp/ccda-R1.1-notfixed-marsh.xml"));
	}

}
