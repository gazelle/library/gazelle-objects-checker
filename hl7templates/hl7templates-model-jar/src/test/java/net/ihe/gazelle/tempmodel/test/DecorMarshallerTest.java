package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DecorMarshallerTest {

	@Test
	public void testLoadDecor1() throws FileNotFoundException, JAXBException {
		Decor decorObs = DecorMarshaller.loadDecor(new FileInputStream("src/test/resources/decor_obs.xml"));
		assertTrue(decorObs != null);
	}
	
	@Test
	public void testLoadDecor2() throws FileNotFoundException, JAXBException {
		Decor decorObs = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
		assertTrue(decorObs != null);
	}

	@Test
	public void testMarshallDecor() throws JAXBException {
		Decor dec = new Decor();
		dec.setLanguage("fr");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DecorMarshaller.marshallDecor(dec, baos);
		String decStr = baos.toString();
		assertTrue(decStr.contains("fr"));
	}

}
