package net.ihe.gazelle.tempmodel.test;

import org.apache.commons.validator.routines.UrlValidator;
import org.junit.Assert;
import org.junit.Test;

public class URLValidatorTest {
	
	@Test
	public void testIsValid() {
		String url = "http://art-decor.org/decor/services/modules/RetrieveProject.xquery?prefix=ccda-&mode=compiled&language=en-US&format=xml";
		Assert.assertTrue(UrlValidator.getInstance().isValid(url));
	}

}