package net.ihe.gazelle.tempmodel.test.location;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.location.util.RulesLocation;

public class RulesLocationTest {

	@Test
	public void testGetLocation() {
		assertTrue(RulesLocation.getLocation(null).equals("//rules"));
	}

}
