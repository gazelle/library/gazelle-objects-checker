package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.HTMLUtils;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class HTMLUtilsTest {

	@Test
	public void testExtractTextByteArray() {
		String res = HTMLUtils.extractText("<b>test</b>".getBytes());
		assertTrue(res.equals("test"));
		res = HTMLUtils.extractText("<p class='bold'>test</p>".getBytes());
		assertTrue(res.equals("test"));
	}

}
