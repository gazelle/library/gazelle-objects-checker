package net.ihe.gazelle.tempmodel.test.choice;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.choices.model.ChoiceElement;

public class ChoiceDefaultTest {

	@Test
	public void testGetChoiceElement() {
		ChoiceDefault cd = new ChoiceDefault();
		cd.getChoiceElement().add(new ChoiceElement());
		assertTrue(cd.getChoiceElement().size() == 1);
	}

	@Test
	public void testGetMax() {
		ChoiceDefault cd = new ChoiceDefault();
		cd.setMax(2);
		assertTrue(cd.getMax() == 2);
	}

	@Test
	public void testGetMin() {
		ChoiceDefault cd = new ChoiceDefault();
		cd.setMin(3);
		assertTrue(cd.getMin() == 3);
	}

	@Test
	public void testGetType() {
		ChoiceDefault cd = new ChoiceDefault();
		cd.setType("aa");
		assertTrue(cd.getType().equals("aa"));
	}

}
