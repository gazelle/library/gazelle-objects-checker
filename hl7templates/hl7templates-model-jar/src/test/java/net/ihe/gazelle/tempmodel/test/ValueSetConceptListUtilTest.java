package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptListUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptListUtilTest {

	@Test
	public void testGetConcepts() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_terma.xml");
		ValueSetConceptList vsc = decorTemplates.getTerminology().getValueSet().get(1).getConceptList();
		assertTrue(ValueSetConceptListUtil.getConcepts(vsc).size()==6);
	}

	@Test
	public void testGetIncludes() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_terma.xml");
		ValueSetConceptList vsc = decorTemplates.getTerminology().getValueSet().get(1).getConceptList();
		assertTrue(ValueSetConceptListUtil.getIncludes(vsc).size()==1);
	}

}
