package net.ihe.gazelle.tempmodel.test.choice;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.choices.model.ChoicesDefault;

public class ChoicesDefaultTest {

	@Test
	public void testGetChoiceDefault() {
		ChoicesDefault cd = new ChoicesDefault();
		assertTrue(cd.getChoiceDefault().size() == 0);
		cd.getChoiceDefault().add(new ChoiceDefault());
		assertTrue(cd.getChoiceDefault().size() == 1);
	}

}
