package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.goc.common.utils.OperationKind;
import net.ihe.gazelle.goc.common.utils.XPATHOCLGroup;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AssertUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

import java.util.List;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AssertUtilTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
	}

	@Test
	public void testGetParentTemplateDefinition() {
		Assert currentAssert = new Assert();
		currentAssert.setTest("hl7:id/@root='1.2.3'");
		currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		currentAssert.setParentObject(firstRD);
		TemplateDefinition td = AssertUtil.getParentTemplateDefinition(currentAssert);
		assertTrue(td.getId().equals("1.3.6.1.4.1.19376.1.5.3.1.4.5.2"));
	}

	@Test
	public void testGetStringValue() {
		Assert currentAssert = new Assert();
		currentAssert.setTest("hl7:id/@root='1.2.3'");
		currentAssert.setRole(AssertRole.ERROR);
		currentAssert.getContent().add("Hello !");
		assertTrue(AssertUtil.getStringValue(currentAssert).equals("Hello !"));
	}

	@Test
	public void testExtractGroupsFromXPATH(){
		List<XPATHOCLGroup> groups = AssertUtil.extractGroupsFromXPATH("not(@xsi:type=\\u0027CD\\u0027) or @codeSystem=\\u00272.16.840.1.113883.6.96\\u0027");
		assertEquals(2,groups.size());
		assertEquals(OperationKind.OR,groups.get(0).getOperation());
		assertEquals(OperationKind.EMPTY,groups.get(1).getOperation());
		assertEquals(1,groups.get(0).getRules().size());
		assertEquals(1,groups.get(1).getRules().size());
		assertEquals("not(@xsi:type=\\u0027CD\\u0027)",groups.get(0).getRules().get(0).getContent());
		assertEquals(OperationKind.EMPTY,groups.get(0).getRules().get(0).getOperation());
	}

	@Test
	public void testEncodeInnerParenthesis(){
		String xpath = "@xsi:type='ED' and (@codeSystem='1.2.3.4.5' or not(@xsi:type='CD'))";
		String encodedXpath = AssertUtil.encodeInnerParenthesis(xpath);
		String expected = "@xsi:type='ED' and (@codeSystem='1.2.3.4.5' or not[ASCI:40]@xsi:type='CD'[ASCI:41])";
		assertEquals(expected,encodedXpath);
	}

	@Test
	public void testDecodeInnerParenthesis(){
		String encodedXpath = "@xsi:type='ED' and (@codeSystem='1.2.3.4.5' or not[ASCI:40]@xsi:type='CD'[ASCI:41])";
		String xpath = AssertUtil.decodeASCIIString(encodedXpath);
		String expected = "@xsi:type='ED' and (@codeSystem='1.2.3.4.5' or not(@xsi:type='CD'))";
		assertEquals(expected,xpath);
	}

	@Test
	public void testExtractAssertFromName(){
		String name = "hl7:effectiveTime[@xsi:type='CD']";
		String extracted = AssertUtil.extractAssertFromName(name);
		assertEquals("@xsi:type='CD'",extracted);
	}

}
