package net.ihe.gazelle.tempmodel.scripts;

import java.io.File;

public class GenerateSchematronsInclude {
	
	public static void main(String[] args) {
		File file = new File("/home/aboufahj/Documents/art-decor-workspace/ccda-runtime-20170501T113800/include");
		StringBuilder res = new StringBuilder();
		for (File fis : file.listFiles()) {
			if (fis.getName().indexOf("2.") == 0 || fis.getName().indexOf("1.") == 0) {
				if (!fis.getName().contains("2013")) {
					res.append("<include href=\"include/").append(fis.getName()).append("\"/>\n");
				}
			}
		}
		System.out.println(res);
	}
}


