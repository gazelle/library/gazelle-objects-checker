package net.ihe.gazelle.tempmodel.test.location;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.location.util.ExtractLocation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class ExtractLocationTest {

	@Test
	public void testGetLocation1() {
		assertTrue(ExtractLocation.getLocation(null).equals(""));
		assertTrue(ExtractLocation.getLocation("toto").equals(""));
	}
	
	@Test
	public void testGetLocation2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		String loc = ExtractLocation.getLocation(rdfirst);
		assertTrue(loc.equals("//rules/template[1]/element[1]"));
	}
	
	@Test
	public void testGetLocation3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		String loc = ExtractLocation.getLocation(td);
		assertTrue(loc.equals("//rules/template[1]"));
		loc = ExtractLocation.getLocation(dec.getRules());
		assertTrue(loc.equals("//rules"));
	}

}
