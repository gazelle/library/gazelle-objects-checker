package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TerminologyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TerminologyUtilTest {

	@Test
	public void testGetValueSetById() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_terma.xml");
		Terminology terma = decorTemplates.getTerminology();
		ValueSet aa = TerminologyUtil.getValueSetById(terma, "2.3.5");
		assertTrue(aa != null);
		assertTrue(aa.getId().equals("2.3.5"));
		assertTrue(aa.getConceptList().getConceptOrInclude().size()==3);
	}

}
