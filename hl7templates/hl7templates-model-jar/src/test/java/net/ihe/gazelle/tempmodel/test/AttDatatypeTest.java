package net.ihe.gazelle.tempmodel.test;

import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AttDatatypeTest {

	@Test
	public void testGetOperationName() {
		assertTrue(AttDatatype.INT.getOperationName() == null);
		assertTrue(AttDatatype.CS.getOperationName().equals("isCSType"));
	}

	@Test
	public void testGetRegex() {
        assertTrue(AttDatatype.INT.getRegex().equals("\\d+(\\.0)?"));
    }

    @Test
    public void testGetRegexForJavaFile() {
        Assert.assertEquals("\\\\d+(\\\\.0)?", AttDatatype.INT.getRegexForJavafile());
    }

	@Test
	public void testIsTestable() {
		assertTrue(AttDatatype.INT.isTestable());
		assertFalse(AttDatatype.ST.isTestable());
	}

	@Test
	public void testGetName() {
		assertTrue(AttDatatype.INT.getName().equals("int"));
	}

	@Test
	public void testGetPrimitiveType() {
		assertTrue(AttDatatype.INT.getPrimitiveType() == UMLPrimitiveType.INTEGER);
	}

	@Test
	public void testGetAttDatatypeByName() {
		assertTrue(AttDatatype.getAttDatatypeByName("int") == AttDatatype.INT);
		assertNull(AttDatatype.getAttDatatypeByName("aaaa"));
	}

}
