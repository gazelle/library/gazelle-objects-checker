package net.ihe.gazelle.tempmodel.test;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;

import static org.junit.Assert.*;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DPathExtractorTest extends DPathExtractor {

	@Test
	public void testExtractDElementFromDPath1() {
		DParent par = DPathExtractor.extractDElementFromDPath("/hl7:observation[hl7:templateId/@root='1.2.3']/hl7:templateId/@root");
		assertTrue(par.getName().equals("observation"));
		assertTrue(par instanceof DElement);
		DElement parent = (DElement)par;
		assertTrue(parent.getName().equals("observation"));
		assertTrue(parent.getFollowingAttributeOrElement() instanceof DElement);
		assertTrue(parent.getDistinguisherAttributeOrElement() instanceof DElement);
		DElement templateId = (DElement)parent.getFollowingAttributeOrElement();
		assertTrue(templateId.getFollowingAttributeOrElement() instanceof DAttribute);
		assertTrue(templateId.getDistinguisherAttributeOrElement()== null);
		assertTrue(templateId.getName().equals("templateId"));
		DElement dist = (DElement)parent.getDistinguisherAttributeOrElement();
		assertTrue(dist.getName().equals("templateId"));
		assertTrue(dist.getFollowingAttributeOrElement() instanceof DAttribute);
		assertTrue(dist.getDistinguisherAttributeOrElement() == null);
		DAttribute root = (DAttribute) dist.getFollowingAttributeOrElement(); 
		assertTrue(root.getName().equals("root"));
		assertTrue(root.getValue().equals("1.2.3"));
		
	}
	
	@Test
	public void testExtractDElementFromDPath2() {
		DParent par = DPathExtractor.extractDElementFromDPath("/hl7:templateId/@root");
		assertTrue(par instanceof DElement);
		String path = DPathExtractor.createPathFromDParent(par);
		assertEquals("/hl7:templateId/@root", path);
	}
	
	@Test
	public void testExtractDElementFromDPath3() {
		DParent par = DPathExtractor.extractDElementFromDPath("/hl7:templateId/@root/@root");
		assertTrue(par== null);
		String path = DPathExtractor.createPathFromDParent(par);
		System.out.println("'" + path + "'");
		assertTrue(path.equals(""));
	}
	
	@Test
	public void testExtractDElementFromDPath4() {
		DParent par = DPathExtractor.extractDElementFromDPath(null);
		assertTrue(par ==null);
	}
	
	@Test
	public void testExtractDElementFromDPath5() {
		DParent par = DPathExtractor.extractDElementFromDPath("");
		assertTrue(par ==null);
	}
	
	@Test
	public void testExtractDElementFromDPath6() {
		DParent par = DPathExtractor.extractDElementFromDPath("azeazeazeaz");
		assertTrue(par ==null);
	}
	
	@Test
	public void testExtractDElementFromDPath7() {
		DParent par = DPathExtractor.extractDElementFromDPath("/azaaza[[[[[azeaze]]]]]");
		assertTrue(par ==null);
	}
	
	@Test
	public void testExtractDElementFromDPath8() {
		DParent par = DPathExtractor.extractDElementFromDPath("/hl7:azaaza[2]");
		assertTrue(par ==null);
	}
	
	@Test
	public void testExtractDElementFromDPath9() {
		DParent par = DPathExtractor.extractDElementFromDPath("/hl7:azaaza/@test/aaa");
		assertTrue(par ==null);
	}
	
	@Test
	public void testExtractDElementFromDPath10() {
		DParent par = DPathExtractor.extractDElementFromDPath("/hl7:azaaza/@test/hl7:aaa");
		String pathBack = DPathExtractor.createPathFromDParent(par);
		assertTrue(pathBack.equals(""));
		assertTrue(par ==null);
	}
	
	@Test
	public void testCreatePathFromDParent(){
		String path = "/hl7:observation[hl7:templateId/@root='1.2.3']/hl7:templateId/@root";
		DParent par = DPathExtractor.extractDElementFromDPath(path);
		String pathBack = DPathExtractor.createPathFromDParent(par);
		assertTrue(pathBack.equals(path));
	}
	
	@Test
	public void testPathIsValidDPath1() throws Exception {
		String path = "/hl7:observation[hl7:templateId/@root='1.2.3']/hl7:templateId/@root";
		assertTrue(DPathExtractor.pathIsValidDPath(path));
		path = "/@root";
		assertTrue(DPathExtractor.pathIsValidDPath(path));
		path = "/@root='aaa'";
		assertTrue(DPathExtractor.pathIsValidDPath(path));
		path = "/hl7:addr/@root";
		assertTrue(DPathExtractor.pathIsValidDPath(path));
		path = "/hl7:observation[hl7:code/@code]/hl7:addr/@root";
		assertTrue(DPathExtractor.pathIsValidDPath(path));
	}
	
	@Test
	public void testPathIsValidDPath2() throws Exception {
		String path = "/hl7:addr/@root/@extension/@@@\\///";
		assertFalse(DPathExtractor.pathIsValidDPath(path));
		path = "/hl7:addr[[[";
		assertFalse(DPathExtractor.pathIsValidDPath(path));
	}
	
	@Test
	public void testPathIsValidDPath3() throws Exception {
		assertFalse(DPathExtractor.pathIsValidDPath(null));
		assertFalse(DPathExtractor.pathIsValidDPath(""));
		assertFalse(DPathExtractor.pathIsValidDPath("  "));
	}
	
	@Test
	public void testCreateElementFromPathElement1() throws Exception {
		DElement del = createElementFromPathElement("/hl7:templateId");
		assertTrue(del.getName().equals("templateId"));
		del = createElementFromPathElement("/hl7:templateId/hl7:ext");
		assertTrue(del.getName().equals("templateId"));
		del = createElementFromPathElement("/hl7:templateId[@root='1.2.3'");
		assertTrue(del.getName().equals("templateId"));
	}
	
	@Test
	public void testCreateElementFromPathElement2() throws Exception {
		DElement del = createElementFromPathElement("");
		assertTrue(del == null);
		del = createElementFromPathElement(null);
		assertTrue(del == null);
		del = createElementFromPathElement("/hl7:templateId[");
		String path = DPathExtractor.createPathFromDParent(del);
		assertEquals("/hl7:templateId", path);
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute1() throws Exception {
		DAttribute dattr = createAttributeFromPathAttribute("/@root");
		assertTrue(dattr != null);
		assertTrue(dattr.getName().equals("root"));
		assertTrue(dattr.getValue() == null);
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute2() throws Exception {
		DAttribute dattr = createAttributeFromPathAttribute("/@ext='aaa'");
		assertTrue(dattr != null);
		assertTrue(dattr.getName().equals("ext"));
		assertTrue(dattr.getValue().equals("aaa"));
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute3() throws Exception {
		DAttribute dattr = createAttributeFromPathAttribute("/aaaa");
		assertTrue(dattr == null);
		dattr = createAttributeFromPathAttribute("");
		assertTrue(dattr == null);
		dattr = createAttributeFromPathAttribute(null);
		assertTrue(dattr == null);
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute4() throws Exception {
		DAttribute dattr = createAttributeFromPathAttribute("aaaa");
		assertTrue(dattr == null);
		dattr = createAttributeFromPathAttribute("@ext='aaa'");
		assertTrue(dattr == null);
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute5() throws Exception {
		DParent dp = extractDElementFromDPath("/hl7:aa/hl7:bb/@tt");
		assertTrue(dp instanceof DElement);
		DElement de = (DElement)dp;
		DElement dd = (DElement) de.getFollowingAttributeOrElement();
		DAttribute dattr = (DAttribute) dd.getFollowingAttributeOrElement();
		assertTrue(dattr != null);
		assertTrue(createPathFromDParent(dattr).equals("/@tt"));
		
		assertTrue(dattr.getTail() == dattr);
		assertTrue(dattr.getValue() == null);
		assertTrue(dattr.get(0) == dattr);
		
		assertTrue(de.getTail() == dattr);
		assertTrue(de.getValue() == null);
		assertTrue(de.get(0) == de);
		assertTrue(de.get(1) == dd);
		assertTrue(de.get(2) == dattr);
		
		assertTrue(dd.get(0) == dd);
		assertTrue(dd.get(1) == dattr);
		assertTrue(dd.getValue() == null);
		assertTrue(createPathFromDParent(de).equals("/hl7:aa/hl7:bb/@tt"));
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute6() throws Exception {
		DParent dp = extractDElementFromDPath("/hl7:aa/epsos:bb/@ee");
		DElement de = (DElement)dp;
		DElement dd = (DElement) de.getFollowingAttributeOrElement();
		DAttribute dattr = (DAttribute) dd.getFollowingAttributeOrElement();
		
		assertTrue(dattr != null);
		assertTrue(createPathFromDParent(dattr).equals("/@ee"));
		
		assertTrue(dattr.getTail() == dattr);
		assertTrue(dattr.getValue() == null);
		assertTrue(dattr.get(0) == dattr);
		
		assertTrue(de.getTail() == dattr);
		assertTrue(de.getValue() == null);
		assertTrue(de.get(0) == de);
		assertTrue(de.get(1) == dd);
		assertTrue(de.get(2) == dattr);
		
		assertTrue(dd.get(0) == dd);
		assertTrue(dd.get(1) == dattr);
		assertTrue(dd.getValue() == null);
	}
	
	@Test
	public void testCreateAttributeFromPathAttribute7() throws Exception {
		DParent dp = extractDElementFromDPath("/hl7:aa/epsos:bb/@");
		assertNull(dp);
	}

}
