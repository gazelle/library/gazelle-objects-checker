package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.IncludeDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RulesUtilTest {
	
	Decor decorTemplates = null;
	
	Decor decorCHTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/template1.xml");
		decorCHTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
	}

	@Test
	public void testGetTemplateDefinitions() {
		List<TemplateDefinition> aa = RulesUtil.getTemplates(decorTemplates.getRules());
		assertTrue(aa.size()>0 && aa.get(0) != null);
	}
	
	@Test
	public void testGetReferenceTemplateIdentifier() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorCHTemplates.getRules()).get(0));
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
		ChoiceDefinition choice = RuleDefinitionUtil.getChoices(participantRole).get(0);
		IncludeDefinition inc = ChoiceDefinitionUtil.getIncludes(choice).get(0);
		String ident = IncludeDefinitionUtil.getReferenceTemplateIdentifier(inc, decorCHTemplates.getRules());
		assertTrue(ident.equals("1.2.3.4"));
	}
	
	@Test
	public void testGetTemplateDefinitionByIdOrName() {
		TemplateDefinition aa = RulesUtil.getTemplateDefinitionByIdOrName(decorCHTemplates.getRules(), "1.2.3.4");
		assertTrue(aa.getName().equals("playingEntity"));
		aa = RulesUtil.getTemplateDefinitionByIdOrName(decorCHTemplates.getRules(), "playingEntity");
		assertTrue(aa.getId().equals("1.2.3.4"));
		aa = RulesUtil.getTemplateDefinitionByIdOrName(decorCHTemplates.getRules(), "aaaaaaaaaa");
		assertTrue(aa == null);
	}
	
	@Test
	public void testGetTemplateDefinitionsById() {
		Decor dec3 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled2.xml");
		List<TemplateDefinition> aa = RulesUtil.getTemplateDefinitionsById(dec3.getRules(), "1.3.6.1.4.1.19376.1.5.3.1.3.16.1");
		assertTrue(aa.size() == 2);
		aa = RulesUtil.getTemplateDefinitionsById(dec3.getRules(), "2.16.840.1.113883.10.20.22.4.17");
		assertTrue(aa.size() == 1);
		aa = RulesUtil.getTemplateDefinitionsById(dec3.getRules(), "1.1.1.1");
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplateDefinitionsById(dec3.getRules(), "");
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplateDefinitionsById(dec3.getRules(), null);
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplateDefinitionsById(null, "2.16.840.1.113883.10.20.22.4.17");
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplateDefinitionsById(null, null);
		assertTrue(aa.size() == 0);
	}
	
	@Test
	public void testGetTemplatesByVersionLabel1() {
		Decor dec3 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled2.xml");
		List<TemplateDefinition> aa = RulesUtil.getTemplatesByVersionLabel(dec3.getRules(), "1.0");
		assertTrue(aa.size() == 2);
		aa = RulesUtil.getTemplatesByVersionLabel(dec3.getRules(), "1.1");
		assertTrue(aa.size() == 1);
	}
	
	@Test
	public void testGetTemplatesByVersionLabel2() {
		Decor dec3 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled3.xml");
		List<TemplateDefinition> aa = RulesUtil.getTemplatesByVersionLabel(dec3.getRules(), "1.0");
		assertTrue(aa.size() == 2);
		aa = RulesUtil.getTemplatesByVersionLabel(dec3.getRules(), "2.32.2.2.2.1");
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplatesByVersionLabel(dec3.getRules(), "");
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplatesByVersionLabel(dec3.getRules(), null);
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplatesByVersionLabel(null, null);
		assertTrue(aa.size() == 0);
		aa = RulesUtil.getTemplatesByVersionLabel(null, "eee");
		assertTrue(aa.size() == 0);
	}
	
}
