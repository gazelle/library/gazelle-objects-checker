package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class CloneUtilTest {

	@Test
	public void testCloneHL7TemplateEl1() {
		RuleDefinition e = new RuleDefinition();
		e.setContains("test");
		e.setName("hl7:id");
		e.getAttribute().add(new Attribute());
		RuleDefinition clone = CloneUtil.cloneHL7TemplateEl(e);
		assertTrue(clone != e);
		assertTrue(clone.getName().equals("hl7:id"));
	}
	
	@Test
	public void testCloneHL7TemplateEl2() {
		Assert e = new Assert();
		e.setFlag("test");
		e.setSee("aa");
		Assert clone = CloneUtil.cloneHL7TemplateEl(e);
		assertTrue(clone != e);
		assertTrue(clone.getFlag().equals("test"));
	}

}
