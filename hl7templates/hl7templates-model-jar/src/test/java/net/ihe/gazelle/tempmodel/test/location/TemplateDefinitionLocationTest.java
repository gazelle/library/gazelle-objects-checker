package net.ihe.gazelle.tempmodel.test.location;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.location.util.TemplateDefinitionLocation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class TemplateDefinitionLocationTest {

	@Test
	public void testGetLocation1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		String loc = TemplateDefinitionLocation.getLocation(td);
		assertTrue(loc.equals("//rules/template[1]"));
		td = RulesUtil.getTemplates(dec.getRules()).get(1);
		loc = TemplateDefinitionLocation.getLocation(td);
		assertTrue(loc.equals("//rules/template[2]"));
	}
	
	@Test
	public void testGetLocation2() throws Exception {
		TemplateDefinition td = new TemplateDefinition();
		assertTrue(TemplateDefinitionLocation.getLocation(td).equals("/template"));
		assertTrue(TemplateDefinitionLocation.getLocation(null).equals(""));
	}

}
