package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateFormats;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateTypes;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplatePropertiesUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TemplatePropertiesUtilTest {

	@Test
	public void testContainsTemplateType() {
		List<TemplateProperties> ltp = new ArrayList<TemplateProperties>();
		ltp.add(new TemplateProperties());
		ltp.add(new TemplateProperties());
		ltp.get(0).setType(TemplateTypes.CDAENTRYLEVEL);
		ltp.get(0).setFormat(TemplateFormats.HL_7_V_3_XML_1);
		ltp.get(1).setType(TemplateTypes.CDAHEADERLEVEL);
		assertTrue(TemplatePropertiesUtil.containsTemplateType(ltp, TemplateTypes.CDAENTRYLEVEL));
		assertFalse(TemplatePropertiesUtil.containsTemplateType(ltp, TemplateTypes.CDADOCUMENTLEVEL));
	}

}
