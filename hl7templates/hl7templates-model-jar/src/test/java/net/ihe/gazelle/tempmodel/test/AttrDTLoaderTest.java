package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.dtdefault.model.AttrDTs;
import net.ihe.gazelle.tempmodel.dtdefault.utils.AttrDTLoader;

public class AttrDTLoaderTest {

	@Test
	public void testLoadDT() throws FileNotFoundException, JAXBException {
		String pathAttrDTs = "src/main/resources/attrDTs.xml";
		AttrDTs attrDTs = AttrDTLoader.loadDT(new FileInputStream(pathAttrDTs));
		assertTrue(attrDTs != null);
		assertTrue(attrDTs.getPE().size()>0);
	}

}
