package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplateSpecification;
import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplatesTypeMapping;
import net.ihe.gazelle.tempmodel.temp.mapping.utils.TemplatesTypeMappingUtil;

public class TemplatesTypeMappingUtilIT extends TemplatesTypeMappingUtil{

	@Test
	public void testExtractTemplatesTypeMapping() throws JAXBException {
		TemplatesTypeMapping ttm = TemplatesTypeMappingUtil.extractTemplatesTypeMapping("src/test/resources/ttm/ttm1.xml");
		assertTrue(ttm != null);
		assertTrue(ttm.getTemplateSpecification().size()>0);
	}

	@Test
	public void testPrintTemplatesTypeMapping() throws JAXBException {
		TemplatesTypeMapping ttm = new TemplatesTypeMapping();
		ttm.getTemplateSpecification().add(new TemplateSpecification());
		ttm.getTemplateSpecification().get(0).setTemplateType("XDW");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		TemplatesTypeMappingUtil.printTemplatesTypeMapping(ttm, baos);
		assertTrue(baos.toString().contains("XDW"));
	}

	@Test
	public void testTemplateTypeCanBeHL7Template() {
		assertTrue(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("POCDMT000040ClinicalDocument"));
		assertTrue(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("POCDMT000040Observation"));
		assertTrue(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("POCDMT000040Participant1"));
		assertTrue(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("POCDMT000040Component1"));
		assertFalse(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("POCDMT000040Component2"));
		assertFalse(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("AD"));
		assertFalse(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template("ZAAAA"));
	}
	
	@Test
	public void testTemplateTypeCanBeHL7TemplateString() throws JAXBException {
		TemplatesTypeMapping ttm = TemplatesTypeMappingUtil.extractTemplatesTypeMapping("src/test/resources/ttm/ttm2.xml");
		assertTrue(templateTypeCanBeHL7Template(ttm, "ExtrinsicObjectType"));
		assertFalse(TemplatesTypeMappingUtil.templateTypeCanBeHL7Template(ttm, "ZAAAA"));
	}
	
	@Test
	public void testExtractTemplatePath() throws Exception {
		String tp = TemplatesTypeMappingUtil.extractTemplatePath("POCDMT000040ClinicalDocument");
		assertTrue(tp.equals("templateId.root"));
	}
	
	@Test
	public void testExtractListTemplateSpecificationForElement() throws Exception {
		List<TemplateSpecification> aa = TemplatesTypeMappingUtil.extractListTemplateSpecificationForElement("ClinicalDocument");
		assertTrue(aa.size() == 1);
		assertTrue(aa.get(0).getTemplateType().equals("POCDMT000040ClinicalDocument"));
		aa = TemplatesTypeMappingUtil.extractListTemplateSpecificationForElement("participant");
		assertTrue(aa.size() == 2);
		aa = TemplatesTypeMappingUtil.extractListTemplateSpecificationForElement("component");
		assertTrue(aa.isEmpty());
		aa = TemplatesTypeMappingUtil.extractListTemplateSpecificationForElement(null);
		assertTrue(aa.isEmpty());
	}
	
	@Test
	public void testExtractTemplateTypeFromElementNameIfExists() throws Exception {
		String aa = TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists("ClinicalDocument", null);
		assertTrue(aa.equals("POCDMT000040ClinicalDocument"));
		aa = TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists("observation", null);
		assertTrue(aa.equals("POCDMT000040Observation"));
		aa = TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists(null, null);
		assertTrue(aa == null);
		aa = TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists("participant", null);
		assertTrue(aa == null);
		aa = TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists("participant", Arrays.asList(new String[] {"cdaheaderlevel"}));
		assertTrue(aa.equals("POCDMT000040Participant1"));
	}
	
	@Test
	public void testExtractListTemplateSpecificationForElementString() throws Exception {
		TemplatesTypeMapping ttm = TemplatesTypeMappingUtil.extractTemplatesTypeMapping("src/test/resources/ttm/ttm1.xml");
		List<TemplateSpecification> aa = extractListTemplateSpecificationForElement(ttm, "ClinicalDocument");
		assertTrue(aa.size() == 1);
		assertTrue(aa.get(0).getTemplateType().equals("POCDMT000040ClinicalDocument"));
		aa = extractListTemplateSpecificationForElement(ttm, "component");
		assertTrue(aa.isEmpty());
		aa = extractListTemplateSpecificationForElement(ttm, null);
		assertTrue(aa.isEmpty());
		aa = extractListTemplateSpecificationForElement(null, null);
		assertTrue(aa.isEmpty());
		aa = extractListTemplateSpecificationForElement(null, "aa");
		assertTrue(aa.isEmpty());
	}

}
