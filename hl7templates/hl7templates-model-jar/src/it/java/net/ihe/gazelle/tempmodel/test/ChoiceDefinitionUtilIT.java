package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceDefinitionUtilIT {
	
	Decor decorTemplates = null;
	ChoiceDefinition selectedChoice = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
		selectedChoice = RuleDefinitionUtil.getChoices(participantRole).get(0);
	}

	@Test
	public void testGetConstraints() {
		List<FreeFormMarkupWithLanguage> aa = ChoiceDefinitionUtil.getConstraints(selectedChoice);
		assertTrue(aa.size()==1);
	}

	@Test
	public void testGetElements() {
		List<RuleDefinition> aa = ChoiceDefinitionUtil.getElements(selectedChoice);
		assertTrue(aa.size()==1);
	}

	@Test
	public void testGetIncludes() {
		List<IncludeDefinition> aa = ChoiceDefinitionUtil.getIncludes(selectedChoice);
		assertTrue(aa.size()==1);
	}

	@Test
	public void testGetParentTemplateDefinition() {
		TemplateDefinition td = ChoiceDefinitionUtil.getParentTemplateDefinition(selectedChoice);
		assertTrue(td.getId().equals("2.16.840.1.113883.2.4.3.11.60.22.10.134"));
	}
	
	@Test
	public void testGetDParentOfChoiceDefinition() throws Exception {
		DParent dp = ChoiceDefinitionUtil.getDParentOfChoiceDefinition(selectedChoice);
		String dps = DPathExtractor.createPathFromDParent(dp);
		assertTrue(dps.equals("/hl7:participant/hl7:participantRole"));
	}
	
	@Test
	public void testGetElementsByName() throws Exception {
		List<RuleDefinition> rd = ChoiceDefinitionUtil.getElementsByName(selectedChoice, "hl7:playingDevice");
		assertTrue(rd != null);
		assertTrue(rd.size()==1);
	}
	
	@Test
	public void testGetContains() throws Exception {
		List<ContainDefinition> lcd = ChoiceDefinitionUtil.getContains(selectedChoice);
		assertTrue(lcd != null);
		assertTrue(lcd.size()==1);
	}

}
