package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.VocabularyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VocabularyUtilTI {
	
	Decor decorTemplates = null;
	
	Decor decorValueSets = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/template1.xml");
		decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_valueset.xml");
	}

	@Test
	public void testIsVocabularyListUsefulListOfVocabularyDecor1() {
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition code = RuleDefinitionUtil.getElementByName(rd, "hl7:code");
		assertTrue(VocabularyUtil.isVocabularyListUseful(code.getVocabulary(), decorTemplates));
	}
	
	@Test
	public void testIsVocabularyListUsefulListOfVocabularyDecor2() {
		List<Vocabulary> lv = new ArrayList<Vocabulary>();
		lv.add(new Vocabulary());
		lv.get(0).setValueSet("1.2.3");
		lv.get(0).setCode(null);
		lv.get(0).setCodeSystem(null);
		lv.get(0).setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(lv, null));
		lv.get(0).setValueSet(null);
		lv.get(0).setCode("aaa");
		lv.get(0).setCodeSystem(null);
		lv.get(0).setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(lv, null));
		lv.get(0).setValueSet(null);
		lv.get(0).setCode(null);
		lv.get(0).setCodeSystem("bbbb");
		lv.get(0).setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(lv, null));
		lv.get(0).setValueSet(null);
		lv.get(0).setCode("aaa");
		lv.get(0).setCodeSystem("bbbb");
		lv.get(0).setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(lv, null));
		lv.get(0).setValueSet(null);
		lv.get(0).setCode(null);
		lv.get(0).setCodeSystem(null);
		lv.get(0).setFlexibility("123");
		assertFalse(VocabularyUtil.isVocabularyListUseful(lv, null));
		lv.get(0).setValueSet("eeee");
		lv.get(0).setCode(null);
		lv.get(0).setCodeSystem(null);
		lv.get(0).setFlexibility("123");
		assertFalse(VocabularyUtil.isVocabularyListUseful(lv, null));
	}
	
	@Test
	public void testIsVocabularyListUsefulListOfVocabularyDecor3() {
		List<Vocabulary> lv = new ArrayList<Vocabulary>();
		lv.add(new Vocabulary());
		lv.get(0).setValueSet("1.2.3");
		lv.get(0).setCode(null);
		lv.get(0).setCodeSystem(null);
		lv.get(0).setFlexibility("123");
		lv.add(new Vocabulary());
		lv.get(1).setValueSet("aaaaa");
		lv.get(1).setCode(null);
		lv.get(1).setCodeSystem(null);
		lv.get(1).setFlexibility("124");
		assertTrue(VocabularyUtil.isVocabularyListUseful(lv, null));
		lv.get(0).setValueSet("bbbb");
		assertFalse(VocabularyUtil.isVocabularyListUseful(lv, null));
	}

	@Test
	public void testIsVocabularyListUsefulVocabularyDecor1() {
		Vocabulary vocab = new Vocabulary();
		vocab.setValueSet("1.2.3");
		vocab.setCode(null);
		vocab.setCodeSystem(null);
		vocab.setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(vocab, null));
	}
	
	/**
	 * test for search in terminology/valuesets
	 */
	@Test
	public void testIsVocabularyListUsefulVocabularyDecor2() {
		Vocabulary vocab = new Vocabulary();
		vocab.setValueSet("ObservationMethod");
		vocab.setCode(null);
		vocab.setCodeSystem(null);
		vocab.setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(vocab, decorValueSets));
	}
	
	/**
	 * test for search in terminology/valuesets
	 */
	@Test
	public void testIsVocabularyListUsefulVocabularyDecor3() {
		Vocabulary vocab = new Vocabulary();
		vocab.setValueSet("epSOSPregnancyInformation");
		vocab.setCode(null);
		vocab.setCodeSystem(null);
		vocab.setFlexibility("123");
		assertTrue(VocabularyUtil.isVocabularyListUseful(vocab, decorValueSets));
	}
	
	/**
	 * test for flexibility in valueset
	 */
	@Test
	public void testIsVocabularyListUsefulVocabularyDecor4() {
		Vocabulary vocab = new Vocabulary();
		vocab.setValueSet("ObservationMethod");
		vocab.setCode(null);
		vocab.setCodeSystem(null);
		assertTrue(VocabularyUtil.isVocabularyListUseful(vocab, decorValueSets));
	}
	
	/**
	 * test for flexibility in valueset
	 */
	@Test
	public void testIsVocabularyListUsefulVocabularyDecor5() {
		Vocabulary vocab = new Vocabulary();
		vocab.setValueSet("ObservationMethod");
		vocab.setCode(null);
		vocab.setCodeSystem(null);
		vocab.setFlexibility("");
		assertTrue(VocabularyUtil.isVocabularyListUseful(vocab, decorValueSets));
	}
	
	/**
	 * test for flexibility in valueset
	 */
	@Test
	public void testIsVocabularyListUsefulVocabularyDecor6() {
		Vocabulary vocab = new Vocabulary();
		vocab.setValueSet("ObservationMethod");
		vocab.setCode(null);
		vocab.setCodeSystem(null);
		vocab.setFlexibility("dynamic");
		assertTrue(VocabularyUtil.isVocabularyListUseful(vocab, decorValueSets));
	}

	@Test
	public void testGetRealValueSetOid() {
		assertTrue(VocabularyUtil.getRealValueSetOid("epSOSPregnancyInformation", decorValueSets).equals("1.3.6.1.4.1.12559.11.10.1.3.1.42.9"));
		assertTrue(VocabularyUtil.getRealValueSetOid("ObservationMethod", decorValueSets).equals("2.16.840.1.113883.1.11.14079"));
		assertTrue(VocabularyUtil.getRealValueSetOid("1.2.3", decorValueSets).equals("1.2.3"));
		assertNull(VocabularyUtil.getRealValueSetOid("aaaaa", decorValueSets));
	}
	
	@Test
	public void testHasExceptionValues() {
		Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_exception2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
		RuleDefinition selectedRuleDefinitionParent = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinitionParent, "hl7:participantRole");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
		assertTrue(VocabularyUtil.hasExceptionValues(selectedRuleDefinition.getVocabulary(), decorValueSets));
		assertFalse(VocabularyUtil.hasExceptionValues(selectedRuleDefinitionParent.getVocabulary(), decorValueSets));
		assertFalse(VocabularyUtil.hasExceptionValues(selectedRuleDefinitionParent.getVocabulary(), null));
		assertFalse(VocabularyUtil.hasExceptionValues(null, decorValueSets));
		assertFalse(VocabularyUtil.hasExceptionValues(null, null));
	}
	
	@Test
	public void testExtractListRelatedException() {
		Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_exception2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
		RuleDefinition selectedRuleDefinitionParent = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinitionParent, "hl7:participantRole");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
		assertTrue(VocabularyUtil.extractListRelatedException(selectedRuleDefinition.getVocabulary(), decorValueSets).size()>0);
		assertTrue(VocabularyUtil.extractListRelatedException(selectedRuleDefinitionParent.getVocabulary(), decorValueSets).size()==0);
		assertTrue(VocabularyUtil.extractListRelatedException(selectedRuleDefinitionParent.getVocabulary(), null).size()==0);
		assertTrue(VocabularyUtil.extractListRelatedException(null, decorValueSets).size()==0);
		assertTrue(VocabularyUtil.extractListRelatedException(null, null).size()==0);
	}
	
	@Test
	public void testVocabularyTrimmedCodeIsNotEmpty() throws Exception {
		assertFalse(VocabularyUtil.vocabularyTrimmedCodeIsNotEmpty(null));
		Vocabulary voc = new Vocabulary();
		assertFalse(VocabularyUtil.vocabularyTrimmedCodeIsNotEmpty(voc));
		voc.setCode("");
		assertFalse(VocabularyUtil.vocabularyTrimmedCodeIsNotEmpty(voc));
		voc.setCode("   ");
		assertFalse(VocabularyUtil.vocabularyTrimmedCodeIsNotEmpty(voc));
		voc.setCode("AA");
		assertTrue(VocabularyUtil.vocabularyTrimmedCodeIsNotEmpty(voc));
	}
	
	@Test
	public void testIsVocabularyListUseful() throws Exception {
		assertFalse(VocabularyUtil.isVocabularyListUseful(new ArrayList<Vocabulary>(), null));
	}
	
}
