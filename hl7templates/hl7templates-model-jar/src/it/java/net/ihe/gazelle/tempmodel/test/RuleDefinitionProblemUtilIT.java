package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.NegativeDistinguisherList;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RuleDefinitionProblemUtilIT extends RuleDefinitionProblemUtil {
	
	Decor decorVocab1 = null;
	
	Decor decorDisting = null;
	
	@Before
	public void setUp(){
		decorVocab1 = DecorMarshaller.loadDecor("src/test/resources/decor_vocab1.xml");
		decorDisting = DecorMarshaller.loadDecor("src/test/resources/decor_disting.xml");
	}
	
	@Test
	public void testIsStringDatatype() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition templateId = RuleDefinitionUtil.getElementByName(firstRD, "hl7:templateId");
		assertFalse(RuleDefinitionProblemUtil.isStringDatatype(templateId));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		assertFalse(RuleDefinitionProblemUtil.isStringDatatype(code));
		RuleDefinition text = new RuleDefinition();
		text.setParentObject(firstRD);
		text.setName("hl7:text");
		assertTrue(RuleDefinitionProblemUtil.isStringDatatype(text));
		RuleDefinition title = new RuleDefinition();
		title.setParentObject(firstRD);
		title.setName("hl7:title");
		assertTrue(RuleDefinitionProblemUtil.isStringDatatype(title));
	}

	@Test
	public void testVerifyIfRuleDefinitionIsTreatable1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorDisting.getRules()).get(0));
		RuleDefinition consent = RuleDefinitionUtil.getElementByName(firstRD, "epsos:consent");
		assertTrue(consent != null);
		assertFalse(verifyIfRuleDefinitionIsTreatable(consent));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		assertTrue(verifyIfRuleDefinitionIsTreatable(code));
	}
	
	@Test
	public void testVerifyIfRuleDefinitionIsTreatable2() {
		assertFalse(verifyIfRuleDefinitionIsTreatable(null));
		assertFalse(verifyIfRuleDefinitionIsTreatable(new RuleDefinition()));
	}
	
	@Test
	public void testVerifyThatRuleDefinitionIsUnique1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		assertTrue(verifyThatRuleDefinitionIsUnique(code));
	}
	
	@Test
	public void testVerifyThatRuleDefinitionIsUnique2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:templateId");
		assertFalse(verifyThatRuleDefinitionIsUnique(code));
	}
	
	@Test
	public void testverifyThatRuleDefinitionHasUniqueDistinguisher1() throws Exception {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(firstRD));
		RuleDefinition templateId = RuleDefinitionUtil.getElementByName(firstRD, "hl7:templateId");
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(templateId));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		assertFalse(verifyThatRuleDefinitionHasUniqueDistinguisher(code));
	}
	
	@Test
	public void testverifyThatRuleDefinitionHasUniqueDistinguisher2() throws Exception {
		RuleDefinition firstRDist = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorDisting.getRules()).get(0));
		RuleDefinition entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entryRelationship").get(0);
		assertFalse(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entryRelationship").get(1);
		assertFalse(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
		RuleDefinition templateId = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:templateId").get(1);
		assertFalse(verifyThatRuleDefinitionHasUniqueDistinguisher(templateId));
	}
	
	@Test
	public void testverifyThatRuleDefinitionHasUniqueDistinguisher3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting2.xml");
		RuleDefinition firstRDist = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(0);
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(1);
		assertFalse(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
	}
	
	@Test
	public void testverifyThatRuleDefinitionHasUniqueDistinguisher4() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting3.xml");
		RuleDefinition firstRDist = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(0);
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(1);
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
	}
	
	@Test
	public void testverifyThatRuleDefinitionHasUniqueDistinguisher5() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting4.xml");
		RuleDefinition firstRDist = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(0);
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(1);
		assertFalse(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(2);
		assertTrue(verifyThatRuleDefinitionHasUniqueDistinguisher(entryRelationship));
	}
	
	@Test
	public void testVerifyThatRuleDefinitionCanHaveNegativeDistinguisher() {
		NegativeDistinguisherList.updateNegativeDistinguisherVerifier(null);
		NegativeDistinguisherList.cleanMapNegativeDistinguisher();
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting4.xml");
		RuleDefinition firstRDist = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(0);
		assertFalse(verifyThatRuleDefinitionCanHaveNegativeDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(1);
		assertTrue(verifyThatRuleDefinitionCanHaveNegativeDistinguisher(entryRelationship));
		entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entry").get(2);
		assertFalse(verifyThatRuleDefinitionCanHaveNegativeDistinguisher(entryRelationship));
	}
	
	@Test
	public void testRuleDefinitionNeedsDistinguisher() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
		RuleDefinition section = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition tid2 = RuleDefinitionUtil.getElementsByName(section, "hl7:templateId").get(2);
		RuleDefinition tid1 = RuleDefinitionUtil.getElementsByName(section, "hl7:templateId").get(1);
		RuleDefinition tid0 = RuleDefinitionUtil.getElementsByName(section, "hl7:templateId").get(0);
		assertFalse(RuleDefinitionProblemUtil.ruleDefinitionNeedsDistinguisher(tid2));
		assertTrue(RuleDefinitionProblemUtil.ruleDefinitionNeedsDistinguisher(tid1));
		assertTrue(RuleDefinitionProblemUtil.ruleDefinitionNeedsDistinguisher(tid0));
	}
	
	@Test
	public void testRuleDefinitionHasKnownType() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
		RuleDefinition section = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		assertTrue(ruleDefinitionHasKnownType(section));
		assertFalse(ruleDefinitionHasKnownType(null));
		assertFalse(ruleDefinitionHasKnownType(new RuleDefinition()));
	}
	
	@Test
	public void testRuleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
		RuleDefinition section = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition tid2 = RuleDefinitionUtil.getElementsByName(section, "hl7:templateId").get(2);
		RuleDefinition tid1 = RuleDefinitionUtil.getElementsByName(section, "hl7:templateId").get(1);
		RuleDefinition tid0 = RuleDefinitionUtil.getElementsByName(section, "hl7:templateId").get(0);
		assertTrue(RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(tid2));
		assertTrue(RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(tid1));
		assertTrue(RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(tid0));
		assertFalse(RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(section));
	}
	
	@Test
	public void testVerifyThatRuleDefinitionNeedAlwaysADistinguisher() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
		RuleDefinition firstRDist = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entryRelationship = RuleDefinitionUtil.getElementsByName(firstRDist, "hl7:entryRelationship").get(0);
		assertTrue(verifyThatRuleDefinitionNeedAlwaysADistinguisher(entryRelationship));
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entryRelationship, "hl7:observation");
		assertFalse(verifyThatRuleDefinitionNeedAlwaysADistinguisher(obs));
		assertFalse(verifyThatRuleDefinitionNeedAlwaysADistinguisher(null));
		assertFalse(verifyThatRuleDefinitionNeedAlwaysADistinguisher(new RuleDefinition()));
	}

}
