package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.handler.util.ChoiceDefinitionProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceDefinitionProblemUtilIT {
	
	Decor decorTemplates = null;
	ChoiceDefinition selectedChoice = null;
	ChoiceDefinition selectedChoice2 = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choicex.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
		selectedChoice = RuleDefinitionUtil.getChoices(participantRole).get(0);
		selectedChoice2 = RuleDefinitionUtil.getChoices(participantRole).get(1);
	}

	@Test
	public void testChoiceContainsValidsubElements1() {
		assertTrue(ChoiceDefinitionProblemUtil.choiceContainsValidsubElements(selectedChoice));
	}
	
	@Test
	public void testChoiceContainsValidsubElements2() {
		assertFalse(ChoiceDefinitionProblemUtil.choiceContainsValidsubElements(selectedChoice2));
		assertFalse(ChoiceDefinitionProblemUtil.choiceContainsValidsubElements(null));
	}

}
