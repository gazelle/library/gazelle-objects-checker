package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TemplateDefinitionUtilIT extends TemplateDefinitionUtil {
	
	Decor decorTemplates = null;
	
	Decor decorTemplatesWierd = null;
	
	Decor decorC = null;
	
	Decor decorCust = null;
	
	@Before
	public void setUp() {
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/template1.xml");
		decorTemplatesWierd = DecorMarshaller.loadDecor("src/test/resources/weird.xml");
		decorCust = DecorMarshaller.loadDecor("src/test/resources/decor_custodian.xml");
		decorC = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		
	}

	@Test
	public void testGetElements() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		List<RuleDefinition> lel = TemplateDefinitionUtil.getElements(aa);
		assertTrue(lel.size()>0 && lel.get(0)!= null);
	}

	@Test
	public void testGetFirstElement() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		RuleDefinition el = TemplateDefinitionUtil.getFirstElement(aa);
		assertTrue(el != null);
		assertTrue(el.getName().equals("hl7:section"));
	}

	@Test
	public void testGetAttributes() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		aa.getAttributeOrChoiceOrElement().add(new Attribute());
		aa.getAttributeOrChoiceOrElement().add(new Attribute());
		assertTrue(TemplateDefinitionUtil.getAttributes(aa).size()==2);
	}

	@Test
	public void testGetLets() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		aa.getAttributeOrChoiceOrElement().add(new Let());
		aa.getAttributeOrChoiceOrElement().add(new Let());
		assertTrue(TemplateDefinitionUtil.getLets(aa).size()==2);
	}

	@Test
	public void testGetAsserts() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		aa.getAttributeOrChoiceOrElement().add(new Assert());
		aa.getAttributeOrChoiceOrElement().add(new Assert());
		assertTrue(TemplateDefinitionUtil.getAsserts(aa).size()==2);
	}

	@Test
	public void testGetReports() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		aa.getAttributeOrChoiceOrElement().add(new Report());
		aa.getAttributeOrChoiceOrElement().add(new Report());
		assertTrue(TemplateDefinitionUtil.getReports(aa).size()==2);
	}

	@Test
	public void testGetDefineVariables() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		aa.getAttributeOrChoiceOrElement().add(new DefineVariable());
		aa.getAttributeOrChoiceOrElement().add(new DefineVariable());
		assertTrue(TemplateDefinitionUtil.getDefineVariables(aa).size()==2);
	}

	@Test
	public void testExtractListCDATemplate1() {
		List<TemplateDefinition> aa = RulesUtil.getTemplates(decorTemplates.getRules());
		List<TemplateDefinition> ltd = TemplateDefinitionUtil.extractListCDATemplate(aa);
		assertTrue(ltd.size()==1);
	}
	
	@Test
	public void testExtractListCDATemplate2() {
		List<TemplateDefinition> aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules());
		List<TemplateDefinition> ltd = TemplateDefinitionUtil.extractListCDATemplate(aa);
		System.out.println(ltd.size());
		assertTrue(ltd.size()==4);
	}
	
	@Test
	public void testExtractListCDATemplate3() throws FileNotFoundException, JAXBException {
		Decor decorTemplatesWierd2 = DecorMarshaller.loadDecor("src/test/resources/weird2.xml");
		List<TemplateDefinition> aa = RulesUtil.getTemplates(decorTemplatesWierd2.getRules());
		List<TemplateDefinition> ltd = TemplateDefinitionUtil.extractListCDATemplate(aa);
		System.out.println(ltd.size());
		assertTrue(ltd.size()==1);
	}
	
	@Test
	public void testGetElementsByName() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		List<RuleDefinition> lrd = TemplateDefinitionUtil.getElementsByName(aa, "hl7:section");
		assertTrue(lrd.size()==1);
		lrd = TemplateDefinitionUtil.getElementsByName(aa, "hl7:entry");
		assertTrue(lrd.size()==0);
	}
	
	@Test
	public void testReferencedTemplateIsCDA1() {
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorC.getRules());
		List<TemplateDefinition> listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		TemplateDefinition first = ltd.get(0);
		assertFalse(TemplateDefinitionUtil.referencedTemplateIsCDA(first.getId(), listCDATemplate));
	}
	
	@Test
	public void testReferencedTemplateIsCDA2() {
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorCust.getRules());
		List<TemplateDefinition> listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		TemplateDefinition first = ltd.get(0);
		assertTrue(TemplateDefinitionUtil.referencedTemplateIsCDA(first.getId(), listCDATemplate));
	}
	
	@Test
	public void testTemplateDefinitionIsCDATemplate() throws Exception {
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorC.getRules());
		assertFalse(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(0)));
		assertTrue(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(1)));
		assertTrue(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(2)));
		assertTrue(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(3)));
		assertFalse(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(4)));
		assertFalse(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(5)));
		assertTrue(TemplateDefinitionUtil.templateDefinitionIsCDATemplate(ltd.get(6)));
	}
	
	@Test
	public void testGetRealStatusCode() throws Exception {
		TemplateDefinition first = new TemplateDefinition();
		assertTrue(TemplateDefinitionUtil.getRealStatusCode(first) == TemplateStatusCodeLifeCycle.ACTIVE);
		first.setStatusCode(TemplateStatusCodeLifeCycle.CANCELLED);
		assertTrue(TemplateDefinitionUtil.getRealStatusCode(first) == TemplateStatusCodeLifeCycle.CANCELLED);
	}
	
	@Test
	public void testExtractListTemplateDefinitionById() throws Exception {
		assertTrue(TemplateDefinitionUtil.extractListTemplateDefinitionById(RulesUtil.getTemplates(decorC.getRules()), 
				"2.16.840.1.113883.2.4.3.11.60.22.10.134").size()==1);
		assertTrue(TemplateDefinitionUtil.extractListTemplateDefinitionById(RulesUtil.getTemplates(decorC.getRules()), 
				"1.2.3").size()==0);
	}
	
	@Test
	public void testExtractListCDATemplateFromDecorTemplateRepository() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
		List<TemplateDefinition> aaa = TemplateDefinitionUtil.extractListCDATemplateFromDecorTemplateRepository(dec);
		List<TemplateDefinition> lid = TemplateDefinitionUtil.extractListTemplateDefinitionById(aaa, "2.16.840.1.113883.10.20.21.2.1");
		assertTrue(lid.size()>0);
	}
	
	@Test
	public void testReferencedTemplateIsCDADecor1() throws Exception {
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorC.getRules());
		TemplateDefinition first = ltd.get(0);
		assertFalse(TemplateDefinitionUtil.referencedTemplateIsCDA(first.getId(), decorC));
	}
	
	@Test
	public void testReferencedTemplateIsCDADecor2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
		assertTrue(TemplateDefinitionUtil.referencedTemplateIsCDA("1.3.6.1.4.1.19376.1.12.1.1.2.10.1", dec));
		assertTrue(TemplateDefinitionUtil.referencedTemplateIsCDA("2.16.840.1.113883.10.20.21.2.1", dec));
		assertFalse(TemplateDefinitionUtil.referencedTemplateIsCDA("aaaaaaaaaaa", dec));
	}
	
	@Test
	public void testExtractListTemplateIdForSpecialization1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListTemplateIdForSpecialization(aa);
		assertTrue(lt.contains("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
		assertTrue(lt.size()==1);
	}
	
	@Test
	public void testExtractListTemplateIdForSpecialization2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListTemplateIdForSpecialization(aa);
		assertTrue(lt.size()==1);
		assertTrue(lt.contains("2.2.2.2.2"));
	}
	
	@Test
	public void testExtractListTemplateIdForSpecialization3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid3.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListTemplateIdForSpecialization(aa);
		System.out.println(lt);
		assertTrue(lt.size()==1);
		assertTrue(lt.contains("1.3.6.1.4.1.19376.1.12.1.3.1"));
	}
	
	@Test
	public void testExtractListTemplateIdForSpecialization4() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid4.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListTemplateIdForSpecialization(aa);
		System.out.println(lt);
		assertTrue(lt.size()==1);
		assertTrue(lt.contains("2.16.840.1.113883.10.20.22.4.27"));
	}
	
	@Test
	public void testExtractListCurrentTemplateId1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid4.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListCurrentTemplateId(aa);
		assertTrue(lt.size()==1);
		assertTrue(lt.contains("2.16.840.1.113883.10.20.22.4.27"));
	}
	
	@Test
	public void testExtractListCurrentTemplateId2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListCurrentTemplateId(aa);
		assertTrue(lt.size()==2);
		assertTrue(lt.contains("1.2.3.4.5"));
		assertTrue(lt.contains("2.2.2.2.2"));
	}
	
	@Test
	public void testExtractListCurrentTemplateId3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid5.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> lt = TemplateDefinitionUtil.extractListCurrentTemplateId(aa);
		assertTrue(lt.size()==2);
		assertTrue(lt.contains("1.2.3.4.5"));
		assertTrue(lt.contains("2.2.2.2.2"));
	}
	
	@Test
	public void testExtractListSpecialization1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(dec);
		List<TemplateDefinition> lt = TemplateDefinitionUtil.extractListSpecialization(aa, tr);
		assertTrue(lt.size()==1);
		assertTrue(lt.get(0).getId().equals("2.2.2.2"));
	}

	@Test
	@Ignore("This test is not working, depends on the environment to be fixed later")
	public void testExtractListSpecialization2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid4.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(dec);
		List<TemplateDefinition> lt = TemplateDefinitionUtil.extractListSpecialization(aa, tr);
		assertTrue(lt.size()==1);
		assertTrue(lt.get(0).getId().equals("2.16.840.1.113883.10.20.22.4.27"));
	}
	
	@Test
	public void testGetTypeNameOfTemplateDefinition1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid4.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Observation"));
	}
	
	@Test
	public void testGetTypeNameOfTemplateDefinition2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Section"));
	}
	
	@Test
	public void testGetTypeNameOfTemplateDefinition3() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules()).get(0);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa) == null);
		aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules()).get(1);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Section"));
		aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules()).get(1);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Section"));
		aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules()).get(2);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Section"));
		aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules()).get(3);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Participant1"));
		aa = RulesUtil.getTemplates(decorTemplatesWierd.getRules()).get(4);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Observation"));
	}
	
	@Test
	public void testGetTypeNameOfTemplateDefinition4() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/weird3.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa).equals("POCDMT000040Participant2"));
		aa = RulesUtil.getTemplates(dec.getRules()).get(1);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa) == null);
		aa = RulesUtil.getTemplates(dec.getRules()).get(2);
		assertTrue(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(aa) == null);
	}
	
	@Test
	public void testGetListSupportedClassificationLevel() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/weird3.xml");
		TemplateDefinition aa = RulesUtil.getTemplates(dec.getRules()).get(0);
		List<String> bb = TemplateDefinitionUtil.getListSupportedClassificationLevel(aa);
		assertTrue(bb.size()==1);
		assertTrue(bb.contains("cdaentrylevel"));
		aa = RulesUtil.getTemplates(dec.getRules()).get(1);
		bb = TemplateDefinitionUtil.getListSupportedClassificationLevel(aa);
		assertTrue(bb.size()==0);
	}

}
