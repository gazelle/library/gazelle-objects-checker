package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.distinguisher.CDAElementsDistinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.Distinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.distinguisher.utils.DistinguisherUtil;
import net.ihe.gazelle.tempmodel.distinguisher.utils.ElementDiscriberUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DistinguisherUtilIT extends  DistinguisherUtil {
	
	private static CDAElementsDistinguisher cdaDistinguisers = null;
	
	@Before
	public void before(){
		try {
			cdaDistinguisers = DistinguisherUtil.loadCDAElementsDistinguisher(
					new FileInputStream("src/main/resources/distinguishers.xml"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testFindElementDiscriber1() {
		ElementDiscriber el = findElementDiscriber("POCDMT000040Section.templateId");
		assertTrue(el != null);
		assertTrue(el.getName().equals("templateId"));
	}
	
	@Test
	public void testFindElementDiscriber2() {
		ElementDiscriber el = findElementDiscriber("POCDMT000040Observation");
		assertTrue(el != null);
		assertTrue(el.getType().value().equals("POCDMT000040Observation"));
	}
	
	@Test
	public void testFindElementDiscriber3() {
		assertNull(findElementDiscriber(new RuleDefinition()));
	}
	
	@Test
	public void testGetRealDistinguisher1(){
		ElementDiscriber ed = ElementDiscriberUtil.extractElementDiscriberById("POCDMT000040Consumable", cdaDistinguisers);
		List<Distinguisher> ld = DistinguisherUtil.getRealDistinguisher(ed);
		for (Distinguisher distinguisher : ld) {
			assertNull(distinguisher.getRef());
		}
		assertTrue(ld.size()==6);
	}
	
	@Test
	public void testGetRealDistinguisher2(){
		assertTrue(DistinguisherUtil.getRealDistinguisher(null).isEmpty());
	}

}
