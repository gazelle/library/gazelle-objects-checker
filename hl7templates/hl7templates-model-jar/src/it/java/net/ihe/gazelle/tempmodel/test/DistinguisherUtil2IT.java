package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.distinguisher.utils.DistinguisherUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DistinguisherUtil2IT extends DistinguisherUtil{
	
	@Test
	public void testFindElementDiscriber1() {
		ElementDiscriber el = DistinguisherUtil.findElementDiscriber("POCDMT000040Section.templateId");
		assertTrue(el != null);
		assertTrue(el.getName().equals("templateId"));
	}
	
	@Test
	public void testFindElementDiscriber2() {
		ElementDiscriber el = findElementDiscriber("POCDMT000040Observation");
		assertTrue(el != null);
		assertTrue(el.getType().value().equals("POCDMT000040Observation"));
	}
	
}
