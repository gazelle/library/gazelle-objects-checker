package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttributeUtilIT {
	
	Decor decorObs = null;
	
	@Before
	public void setUp(){
		decorObs = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
	}

	@Test
	public void testGetAttributeValue() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		assertTrue(AttributeUtil.getAttributeValue(attr, "root").equals("1.3.6.1.4.1.19376.1.5.3.1.4.5.2"));
	}

	@Test
	public void testGetDParentOfTheParent() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		DParent dp = AttributeUtil.getDParentOfTheParent(attr);
		String dpstring = DPathExtractor.createPathFromDParent(dp);
		assertTrue(dpstring.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
	}

	@Test
	public void testGetParentTemplateDefinition() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		TemplateDefinition tem = AttributeUtil.getParentTemplateDefinition(attr);
		assertTrue(tem == aa);
	}

	@Test
	public void testGetDParentOfAttibute() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		DParent dp = AttributeUtil.getDParentOfAttibute(attr, "root", "1.3.6.1.4.1.19376.1.5.3.1.4.5.2");
		String dpstring = DPathExtractor.createPathFromDParent(dp);
		assertTrue(dpstring.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/@root"));
		dp = AttributeUtil.getDParentOfAttibute(attr, "extension", null);
		dpstring = DPathExtractor.createPathFromDParent(dp);
		assertTrue(dpstring.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/@extension"));
	}
	
	@Test
	public void testGetDParentOfAttibute2() {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		DParent dp = AttributeUtil.getDParentOfAttibute(attr);
		String dpstring = DPathExtractor.createPathFromDParent(dp);
		assertTrue(dpstring.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/"
				+ "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/@root"));
	}
	
	@Test
	public void testGetUMLTypeName() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		String tn = AttributeUtil.getUMLTypeName(attr, "root");
		assertTrue(tn.equals(UMLPrimitiveType.STRING.getValue()));
	}
	
	@Test
	public void testAssertDatatypeDifferent() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		assertTrue(AttributeUtil.assertDatatypeDifferent(attr, "root", AttDatatype.BL));
		assertFalse(AttributeUtil.assertDatatypeDifferent(attr, "root", AttDatatype.UID));
	}
	
	@Test
	public void testAssertAttributeIsNotRequiredByDefault1() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(rd, "hl7:templateId");
		Attribute attr = templ.getAttribute().get(0);
		assertTrue(AttributeUtil.assertAttributeIsNotRequiredByDefault(attr, "root"));
	}
	
	@Test
	public void testAssertAttributeIsNotRequiredByDefault2() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		Attribute attr = rd.getAttribute().get(0);
		assertFalse(AttributeUtil.assertAttributeIsNotRequiredByDefault(attr, "classCode"));
	}
	
	@Test
	public void testGetUMLMaxAttribute1() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		Attribute attr = rd.getAttribute().get(0);
		assertTrue(AttributeUtil.getUMLMaxAttribute(attr) == 1);
		assertTrue(AttributeUtil.getUMLMaxAttribute(null) == null);
	}
	
	@Test
	public void testGetUMLMaxAttribute2() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		Attribute attr = rd.getAttribute().get(0);
		assertTrue(AttributeUtil.getUMLMaxAttribute(attr, "classCode") == 1);
		assertTrue(AttributeUtil.getUMLMaxAttribute(attr, null) == 1);
		assertTrue(AttributeUtil.getUMLMaxAttribute(attr, "typeCode") == 0);
		assertTrue(AttributeUtil.getUMLMaxAttribute(null, null) == null);
	}
	
	@Test
	public void testGetUMLMaxAttribute3() throws Exception {
		Attribute attr = new Attribute();
		assertTrue(AttributeUtil.getUMLMaxAttribute(attr, "classCode") == null);
	}
	
	@Test
	public void testGetJavaPath() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		Attribute attr = rd.getAttribute().get(0);
		assertTrue(AttributeUtil.getJavaPath(attr, null).equals("POCDMT000040Act.classCode"));
		assertTrue(AttributeUtil.getJavaPath(attr, "classCode").equals("POCDMT000040Act.classCode"));
		assertTrue(AttributeUtil.getJavaPath(attr, "typeCode").equals("POCDMT000040Act.typeCode"));
	}
	
	@Test
	public void testGetListAttributesNames() throws Exception {
		Attribute attr = new Attribute();
		attr.setClassCode("aa");
		attr.setName("typeCode");
		assertTrue(AttributeUtil.getListAttributesNames(attr).size() == 2);
		assertTrue(AttributeUtil.getListAttributesNames(attr).contains("typeCode"));
		assertTrue(AttributeUtil.getListAttributesNames(attr).contains("classCode"));
	}
	
	@Test
	public void testGetBetterMatchingName() {
		assertTrue(AttributeUtil.getBetterMatchingName(null) == null);
		Attribute attr = new Attribute();
		assertTrue(AttributeUtil.getBetterMatchingName(attr) == null);
		attr.setName("aa");
		assertTrue(AttributeUtil.getBetterMatchingName(attr) == "aa");
		attr.setRoot("zz");
		assertTrue(AttributeUtil.getBetterMatchingName(attr) == "aa");
	}
	
	@Test
	public void testGetBetterMatchingValue() {
		assertTrue(AttributeUtil.getBetterMatchingValue(null) == null);
		Attribute attr = new Attribute();
		assertTrue(AttributeUtil.getBetterMatchingValue(attr) == null);
		attr.setName("aa");
		assertTrue(AttributeUtil.getBetterMatchingValue(attr) == null);
		attr.setValue("zz");
		assertTrue(AttributeUtil.getBetterMatchingValue(attr) == "zz");
	}
	
}
