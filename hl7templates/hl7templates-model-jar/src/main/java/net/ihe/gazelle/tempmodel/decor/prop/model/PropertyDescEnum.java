package net.ihe.gazelle.tempmodel.decor.prop.model;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.PropertyUtil;

public enum PropertyDescEnum {

	PQ_UNIT("PQ_unit", new PropertyAttr[] { PropertyAttr.UNIT, PropertyAttr.MIN_INCLUDE, PropertyAttr.MAX_INCLUDE,
			PropertyAttr.FRACTION_DIGITS }) {
		@Override
		public boolean containsDT(String datatype) {
			String path = datatype + ".unit";
			String type = UMLLoader.getPropertyTypeName(path);
			return type != null;
		}
	},

	PQ_MO("PQ_MO", new PropertyAttr[] { PropertyAttr.CURRENCY, PropertyAttr.MIN_INCLUDE, PropertyAttr.MAX_INCLUDE,
			PropertyAttr.FRACTION_DIGITS }) {
		@Override
		public boolean containsDT(String datatype) {
			String path = datatype + ".currency";
			String type = UMLLoader.getPropertyTypeName(path);
			return type != null;
		}
	},

	STR("STR", new PropertyAttr[] { PropertyAttr.MIN_LENGTH, PropertyAttr.MAX_LENGTH }) {
		@Override
		public boolean containsDT(String datatype) {
			return UMLLoader.datatypeHasMixedElements(datatype);
		}
	},

	VAL("VAL", new PropertyAttr[] { PropertyAttr.VALUE }) {
		@Override
		public boolean containsDT(String datatype) {
			String path = datatype + ".value";
			String type = UMLLoader.getPropertyTypeName(path);
			return type != null;
		}
	};

	private PropertyDescriber propertyDescriber;

	private PropertyDescEnum(String identifier, PropertyAttr[] listProperties) {
		this.propertyDescriber = new PropertyDescriber(identifier, listProperties);
	}

	public PropertyDescriber getPropertyDescriber() {
		return propertyDescriber;
	}

	public abstract boolean containsDT(String datatype);

	public static PropertyDescEnum extractPropertyDescEnum(Property prop) {
		if (prop != null) {
			List<PropertyAttr> lpd = new ArrayList<>();
			if (PropertyUtil.isUnitAttributePresent(prop)) {
				lpd.add(PropertyAttr.UNIT);
			}
			if (PropertyUtil.isMinIncludeAttributePresent(prop)) {
				lpd.add(PropertyAttr.MIN_INCLUDE);
			}
			if (PropertyUtil.isMaxIncludeAttributePresent(prop)) {
				lpd.add(PropertyAttr.MAX_INCLUDE);
			}
			if (PropertyUtil.isFractionDigitsAttributePresent(prop)) {
				lpd.add(PropertyAttr.FRACTION_DIGITS);
			}
			if (PropertyUtil.isCurrencyAttributePresent(prop)) {
				lpd.add(PropertyAttr.CURRENCY);
			}
			if (PropertyUtil.isMinLengthAttributePresent(prop)) {
				lpd.add(PropertyAttr.MIN_LENGTH);
			}
			if (PropertyUtil.isMaxLengthAttributePresent(prop)) {
				lpd.add(PropertyAttr.MAX_LENGTH);
			}
			if (PropertyUtil.isValueAttributePresent(prop)) {
				lpd.add(PropertyAttr.VALUE);
			}
			if (lpd.isEmpty()) {
				return null;
			}
			for (PropertyDescEnum pde : PropertyDescEnum.values()) {
				if (pde.propertyDescriber.getListProperties().containsAll(lpd)) {
					return pde;
				}
			}
		}
		return null;
	}

}
