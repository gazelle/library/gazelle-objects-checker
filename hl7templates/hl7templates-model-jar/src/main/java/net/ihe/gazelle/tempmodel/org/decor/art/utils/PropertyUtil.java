package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class PropertyUtil {
	
	private static final Pattern PAT = Pattern.compile("(\\d+)!?");
	
	private PropertyUtil() {}
	
	public static Integer extractDigitNumber(String fractionDigits) {
		if (fractionDigits == null) {
			return null;
		}
		Matcher mat = PAT.matcher(fractionDigits);
		if (mat.find()){
			return Integer.valueOf(mat.group(1));
		}
		return null;
	}
	
	public static boolean isUnitAttributePresent(Property prop) {
		return prop!=null && prop.getUnit() != null && !prop.getUnit().isEmpty();
	}
	
	public static boolean isMinIncludeAttributePresent(Property prop) {
		return prop!=null && prop.getMinInclude() != null && !prop.getMinInclude().isEmpty();
	}
	
	public static boolean isMaxIncludeAttributePresent(Property prop) {
		return prop!=null && prop.getMaxInclude() != null && !prop.getMaxInclude().isEmpty();
	}
	
	public static boolean isFractionDigitsAttributePresent(Property prop) {
		boolean bl = prop!=null && prop.getFractionDigits() != null && !prop.getFractionDigits().isEmpty();
		if (!bl) {
			return false;  
		}
		Integer ii = PropertyUtil.extractDigitNumber(prop.getFractionDigits());
		boolean digistStrict = prop.getFractionDigits().contains("!");
		return ii != null && ii>=0 && !(!digistStrict && ii==0);
	}
	
	public static boolean isCurrencyAttributePresent(Property prop) {
		return prop!=null && prop.getCurrency() != null && !prop.getCurrency().isEmpty();
	}
	
	public static boolean isMinLengthAttributePresent(Property prop) {
		return prop!=null && prop.getMinLength() != null;
	}
	
	public static boolean isMaxLengthAttributePresent(Property prop) {
		return prop!=null && prop.getMaxLength() != null;
	}
	
	public static boolean isValueAttributePresent(Property prop) {
		return prop!=null && prop.getValue() != null;
	}
	


}
