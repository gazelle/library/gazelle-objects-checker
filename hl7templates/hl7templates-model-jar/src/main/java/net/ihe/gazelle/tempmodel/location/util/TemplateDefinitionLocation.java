package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public final class TemplateDefinitionLocation {
	
	private TemplateDefinitionLocation() {}
	
	public static String getLocation(TemplateDefinition templateDefinition) {
		if (templateDefinition != null) {
			if (templateDefinition.getParentObject() != null) {
				Rules aa = templateDefinition.getParentObject();
				String locationParent = RulesLocation.getLocation(aa);
				return locationParent + "/template[" + 
						(RulesUtil.getTemplates(aa).indexOf(templateDefinition) + 1) + "]";
			}
			return "/template";
		}
		return "";
	}

}
