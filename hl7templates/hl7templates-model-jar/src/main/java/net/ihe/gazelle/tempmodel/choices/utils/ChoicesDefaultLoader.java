package net.ihe.gazelle.tempmodel.choices.utils;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.tempmodel.choices.model.ChoicesDefault;

/**
 *
 * @author Abderrazek Boufahja
 *
 */
public final class ChoicesDefaultLoader {
	
	private ChoicesDefaultLoader() {}

	public static ChoicesDefault loadChoicesDefault(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(ChoicesDefault.class);
		Unmarshaller u = jc.createUnmarshaller();
		return (ChoicesDefault) u.unmarshal(is);
	}
	
}
