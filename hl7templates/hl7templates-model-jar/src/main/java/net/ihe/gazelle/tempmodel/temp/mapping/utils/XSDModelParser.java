package net.ihe.gazelle.tempmodel.temp.mapping.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplateSpecification;
import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplatesTypeMapping;

public final class XSDModelParser {
	
	
	private static Logger log = LoggerFactory.getLogger(XSDModelParser.class);
	
	private XSDModelParser() {}
	
	public static void main1(String[] args) throws JAXBException, IOException {
		
		TemplatesTypeMapping map = TemplatesTypeMappingUtil.extractTemplatesTypeMapping(
				"/home/aboufahj/workspaceTopcased/hl7templates-resources/cdabasic/templatesTypeMapping.xml");
		String content = readDoc("/home/aboufahj/Dropbox/Standards/HL7/"
				+ "Clinical Document Architecture CDA/Normative/infrastructure/cda/POCD_MT000040.xsd");
		for (TemplateSpecification ts : map.getTemplateSpecification()) {
			Pattern pat = Pattern.compile("<xs:element name=\"(.*?)\" type=\"" + ts.getTemplateType().replace("POCDMT000040", "POCD_MT000040.") + "\".*?>");
			Matcher mat = pat.matcher(content);
			if (mat.find()) {
				ts.getTemplateElement().clear();
				ts.getTemplateElement().add(mat.group(1));
			}
			else {
				ts.getTemplateElement().clear();
				ts.getTemplateElement().add("");
			}
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		TemplatesTypeMappingUtil.printTemplatesTypeMapping(map, baos);
		log.info("{}", baos);
	}
	
	public static void main(String[] args) {
		TemplatesTypeMapping map = TemplatesTypeMappingUtil.extractTemplatesTypeMapping(
				"/home/aboufahj/workspaceTopcased/hl7templates-resources/cdabasic/templatesTypeMapping.xml");
		for (TemplateSpecification ts : map.getTemplateSpecification()) {
			processTemplateSpecification(map, ts);
		}
	}

	private static void processTemplateSpecification(TemplatesTypeMapping map, TemplateSpecification ts) {
		for (String te : ts.getTemplateElement()) {
			if (!te.isEmpty()) {
				for (TemplateSpecification ts2 : map.getTemplateSpecification()) {
					if (ts2 != ts && ts2.getTemplateElement().contains(te)) {
						log.info("Problem : " + ts.getTemplateType() + "," + ts2.getTemplateType());
					}
				}
			}
		}
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name), "ISO-8859-15"));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line).append("\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
