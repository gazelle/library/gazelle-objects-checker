package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class RulesUtil {

	private RulesUtil() {}

	public static List<TemplateDefinition> getTemplates(Rules rules){
		List<TemplateDefinition> templates = new ArrayList<>();
		if (rules != null){
			for(Object obj : rules.getTemplateAssociationOrTemplate()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition){
						templates.add( (TemplateDefinition) obj );
				}
			} 
		}
		return templates;
	}
	
	public static List<TemplateDefinition> getTemplateDefinitionsById(Rules rules, String templateId){
		List<TemplateDefinition> ltd = getTemplates(rules);
		List<TemplateDefinition> res = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if (templateDefinition.getId() != null && templateDefinition.getId().equals(templateId)){
				res.add(templateDefinition);
			}
		}
		return res;
	}
	
	public static TemplateDefinition getTemplateDefinitionById(Rules rules, String templateId){
		List<TemplateDefinition> ltd = getTemplates(rules);
		for (TemplateDefinition templateDefinition : ltd) {
			if (templateDefinition.getId() != null && templateDefinition.getId().equals(templateId)){
				return templateDefinition;
			}
		}
		return null;
	}
	
	public static TemplateDefinition getTemplateDefinitionByIdOrName(Rules rules, String templateIdOrName){
		List<TemplateDefinition> ltd = getTemplates(rules);
		if (templateIdOrName!= null) {
			for (TemplateDefinition templateDefinition : ltd) {
				if (templateIdOrName.equals(templateDefinition.getId()) || templateIdOrName.equals(templateDefinition.getName())){
					return templateDefinition;
				}
			}
		}
		return null;
	}

	public static List<TemplateAssociationDefinition> getTemplateAssociations(Rules rules) {
		List<TemplateAssociationDefinition> tempass = new ArrayList<>();
		if (rules != null){
			for(Object obj : rules.getTemplateAssociationOrTemplate()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition){
						tempass.add( (TemplateAssociationDefinition) obj );
				}
			} 
		}
		return tempass;
	}
	
	public static String getReferenceTemplateIdentifier(Rules rules, String ref){
		if (ref != null && rules != null && rules.getParentObject() != null) {
			TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(rules.getParentObject());
			return TemplateRepositoryUtil.getReferenceTemplateIdentifierFromTemplateRepository(tr, ref);
		}
		return null;
	}

	public static List<TemplateDefinition> getTemplatesByVersionLabel(Rules rules, String versionLabel) {
		List<TemplateDefinition> res = new ArrayList<>();
		List<TemplateDefinition> listAll = RulesUtil.getTemplates(rules);
		for (TemplateDefinition templateDefinition : listAll) {
			if (templateDefinition.getVersionLabel() != null && templateDefinition.getVersionLabel().equals(versionLabel)) {
				res.add(templateDefinition);
			}
		}
		return res;
	}

	public static List<TemplateDefinition> getTemplateDefinitionsByIdOrName(Rules rules, String templateId, String templateName){
		List<TemplateDefinition> ltd = getTemplates(rules);
		List<TemplateDefinition> res = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ((templateId != null && templateId.equals(templateDefinition.getId())) || (templateName != null && templateName.equals(templateDefinition.getName()))){
				res.add(templateDefinition);
			}
		}
		return res;
	}

}
