package net.ihe.gazelle.tempmodel.templates.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.tempmodel.templates.model.TNoeud;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class TNoeudUtil {
	
	public static Set<String> extractListTemplateIdForIdentificationFromTD(TemplateDefinition templateDefinition) {
		if (templateDefinition != null) {
			TNoeud tn = extractTNoeud(templateDefinition);
			return extractListTemplateIdForIdentification(tn);
		}
		return new HashSet<>();
	}
	
	protected static TNoeud extractTNoeud(TemplateDefinition templateDefinition) {
		if (templateDefinition.getParentObject() == null) {
			return null;
		}
		Decor dec = templateDefinition.getParentObject().getParentObject();
		TemplateRepository templateRepository = TemplateRepositoryUtil.extractTemplateRepositoryIfAlreadyProcessed(dec);
		if (templateRepository == null){
			templateRepository = TemplateRepositoryUtil.generateTemplateRepository(dec);
		}
		return extractTNoeud(templateDefinition, templateRepository);
	}
	
	protected static TNoeud extractTNoeud(TemplateDefinition templateDefinition, TemplateRepository templateRepository) {
		if (templateDefinition != null && templateDefinition.getId() != null) {
			TNoeud tn = new TNoeud();
			Set<String> ltid = TemplateDefinitionUtil.extractListCurrentTemplateId(templateDefinition);
			tn.setIdentifier(templateDefinition.getId());
			tn.setListTemplateId(new ArrayList<String>(ltid));
			List<TemplateDefinition> ltd = TemplateDefinitionUtil.extractListSpecialization(templateDefinition, templateRepository);
			for (TemplateDefinition templateDefinition2 : ltd) {
				TNoeud parent = extractTNoeud(templateDefinition2, templateRepository);
				tn.getListParents().add(parent);
			}
			return tn;
		}
		return null;
	}
	
	protected static Set<String> extractListTemplateIdForIdentification(TNoeud tnoeud) {
		if (tnoeud != null && tnoeud.getListTemplateId() != null) {
			List<String> listParentTemplates = extractListParentTemplateIds(tnoeud);
			Set<String> listTemplateIdsNotIncludedInParents = extractListTemplateIdsNotIncludedInParents(tnoeud.getListTemplateId(), listParentTemplates);
			if (!listTemplateIdsNotIncludedInParents.isEmpty()) {
				return listTemplateIdsNotIncludedInParents;
			}
			else if (listParentTemplates.containsAll(tnoeud.getListTemplateId()) && tnoeud.getListParents() != null) {
				for (TNoeud tparent : tnoeud.getListParents()) {
					if (tparent.getListTemplateId().containsAll(tnoeud.getListTemplateId()) && 
							tparent.getListTemplateId().size() == tnoeud.getListTemplateId().size()) {
						return extractListTemplateIdForIdentification(tparent);
					}
				}
			}
		}
		return new HashSet<>();
	}

	private static Set<String> extractListTemplateIdsNotIncludedInParents(List<String> listTemplateId, List<String> listParentTemplates) {
		if (listTemplateId != null) {
			List<String> res = new ArrayList<>(listTemplateId.size());
			res.addAll(listTemplateId);
			if (listParentTemplates != null) {
				res.removeAll(listParentTemplates);
			}
			return new TreeSet<>(res);
		}
		return new HashSet<>();
	}

	private static List<String> extractListParentTemplateIds(TNoeud tnoeud) {
		List<String> res = new ArrayList<>();
		if (tnoeud != null) {
			for (TNoeud tparent : tnoeud.getListParents()) {
				res.addAll(tparent.getListTemplateId());
			}
		}
		return res;
	}

}
