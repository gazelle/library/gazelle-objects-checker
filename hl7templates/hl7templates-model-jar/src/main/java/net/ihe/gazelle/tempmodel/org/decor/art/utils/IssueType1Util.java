package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Issue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueTracking;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class IssueType1Util {
	
	private IssueType1Util() {}

	/**
	 * method not needed by goc
	 * @param t
	 * @return
	 */
	public static List<IssueAssignment> getAssignments(Issue t) {
		return new ArrayList<>();
	}

	/**
	 * method not needed by goc
	 * @param t
	 * @return
	 */
	public static List<IssueTracking> getTrackings(Issue t) {
		return new ArrayList<>();
	}

}
