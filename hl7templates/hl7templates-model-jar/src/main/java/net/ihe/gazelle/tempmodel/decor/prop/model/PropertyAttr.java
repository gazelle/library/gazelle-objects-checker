package net.ihe.gazelle.tempmodel.decor.prop.model;

public enum PropertyAttr {
	
	UNIT("unit"), CURRENCY("currency"), MIN_INCLUDE("minInclude"), MAX_INCLUDE("maxInclude"),
	FRACTION_DIGITS("fractionDigits"), MIN_LENGTH("minLength"),
	MAX_LENGTH("maxLength"), VALUE("value");
	
	private String name;

	private PropertyAttr(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
