package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public final class ExtractLocation {
	
	private ExtractLocation() {} 
	
	public static String getLocation(Object obj) {
		if (obj instanceof RuleDefinition) {
			RuleDefinition ruleDefinition = (RuleDefinition)obj;
			return RuleDefinitionLocation.getLocation(ruleDefinition );
		}
		else if (obj instanceof ChoiceDefinition) {
			ChoiceDefinition cd = (ChoiceDefinition)obj;
			return ChoiceDefinitionLocation.getLocation(cd);
		}
		else if (obj instanceof TemplateDefinition) {
			TemplateDefinition td = (TemplateDefinition)obj;
			return TemplateDefinitionLocation.getLocation(td);
		}
		else if (obj instanceof Rules) {
			return RulesLocation.getLocation((Rules)obj);
		}
		else if (obj instanceof Attribute) {
			return AttributeLocation.getLocation((Attribute)obj);
		}
		else if (obj instanceof Vocabulary) {
			Vocabulary voc = (Vocabulary)obj;
			return ExtractLocation.getLocation(voc.getParentObject());
		}
		else if (obj instanceof Property) {
			Property prop = (Property)obj;
			return ExtractLocation.getLocation(prop.getParentObject());
		}
		else if (obj instanceof IncludeDefinition) {
			return IncludeDefinitionLocation.getLocation((IncludeDefinition)obj);
		}
		return "";
	}
	

}
