package net.ihe.gazelle.tempmodel.decor.prop.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PropertyDescriber {

	private String identifier;

	private Set<PropertyAttr> listProperties;

	public PropertyDescriber(String identifier, PropertyAttr[] listProperties) {
		super();
		this.identifier = identifier;
		this.listProperties = new HashSet<>(Arrays.asList(listProperties));
	}

	public PropertyDescriber(String identifier,	Set<PropertyAttr> listProperties) {
		super();
		this.identifier = identifier;
		this.listProperties = listProperties;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Set<PropertyAttr> getListProperties() {
		return listProperties;
	}

	public void setListProperties(Set<PropertyAttr> listProperties) {
		this.listProperties = listProperties;
	}



}
