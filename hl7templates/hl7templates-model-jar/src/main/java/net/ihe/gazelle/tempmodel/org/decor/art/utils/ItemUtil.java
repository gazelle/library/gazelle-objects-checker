package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasItem;
import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasParent;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ItemUtil {
	
	private ItemUtil() {}
	
	public static Set<String> extractSetItems(Item item) {
		if (item != null && item.getLabel() != null && !item.getLabel().trim().equals("")) {
			String[] ss = item.getLabel().trim().split(";|/");
			Set<String> res = new TreeSet<>();
			for (String string : ss) {
				res.add(string.trim());
			}
			return res;
		}
		return new HashSet<>();
	}
	
	public static <T extends HasParent> Set<String> extractRelatedItems(T hasItemElement) {
		Set<String> res = new TreeSet<>();
		if (hasItemElement != null) {
			if (hasItemElement instanceof HasItem) {
				extractListInternItems((HasItem)hasItemElement, res);
			}
			Set<String> setParentItems = null;
			if (hasItemElement.getParentObject() instanceof HasParent) {
				setParentItems = ItemUtil.extractRelatedItems((HasParent)hasItemElement.getParentObject());
			}
			if (setParentItems != null) {
				res.addAll(setParentItems);
			}
		}
		return res;
	}

	private static <T extends HasItem> void extractListInternItems(T hasItemElement, Set<String> res) {
		if (hasItemElement.getItem() != null && hasItemElement.getItem().getLabel() != null) {
			res.addAll(ItemUtil.extractSetItems(hasItemElement.getItem()));
		}
	}

}
