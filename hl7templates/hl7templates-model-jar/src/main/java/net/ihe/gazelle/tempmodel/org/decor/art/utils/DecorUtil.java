package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import gnu.trove.map.hash.THashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;

import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class DecorUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(DecorUtil.class);

	private static Map<String, XMLGregorianCalendar> removedTemplatesLog = new THashMap<>();


	private DecorUtil() {}

	public static boolean containValueSet(String valueSet, Decor decor) {
		return getValueSetOid(valueSet, decor) != null;
	}
	
	public static String getValueSetOid(String valueSet, Decor decor){
		if (decor == null){
			return null;
		}
		TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(decor);
		return getValueSetOid(valueSet, tr);
	}
	
	private static String getValueSetOid(String valueSet, TemplateRepository tr){
		List<TemplateRepository> ltrvisited = new ArrayList<>();
		return getValueSetOid(valueSet, tr, ltrvisited);
	}

	private static String getValueSetOid(String valueSet, TemplateRepository tr, List<TemplateRepository> ltrvisited){
		if (tr == null){
			return null;
		}
		if (ltrvisited.contains(tr)) {
			return null;
		}
		ltrvisited.add(tr);
		
		Decor dec = tr.getCurrentDecor();
		if (dec.getTerminology() != null){
			for (ValueSet vs : dec.getTerminology().getValueSet()) {
				if ((vs.getName() != null && vs.getName().equals(valueSet)) || 
						(vs.getDisplayName() != null && vs.getDisplayName().equals(valueSet))){
					if (vs.getId() != null) {
						return vs.getId();
					}
					if (vs.getRef() != null) {
						return vs.getRef();
					}
				}
				else if (vs.getId() != null && vs.getId().equals(valueSet)){
					return valueSet;
				}
			}
		}
		for (TemplateRepository subtr : tr.getListParentRepository()) {
			String id =  getValueSetOid(valueSet, subtr);
			if (id != null){
				return id;
			}
		}
		return null;
	}

	public static Decor getDecorFromRD(RuleDefinition ruleDefinition) {
		if (ruleDefinition != null) {
			TemplateDefinition td = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition);
			if (td != null && td.getParentObject() != null) {
				return td.getParentObject().getParentObject();
			}
		}
		return null;
	}
	
	public static String extractPrefixFromDecor(Decor decor) {
		if (decor!= null && decor.getProject() != null && decor.getProject().getPrefix() != null){
			return decor.getProject().getPrefix().substring(0, decor.getProject().getPrefix().length()- 1);
		}
		return "";
	}

	public static boolean listDecorContainsDecorWithTheSamePrefix(List<Decor> ldec, Decor currentDecor) {
		if (ldec == null || currentDecor == null || currentDecor.getProject() == null || currentDecor.getProject().getPrefix() == null) {
			return false;
		}
		for (Decor decor : ldec) {
			if (decor.getProject() != null && decor.getProject().getPrefix() != null && 
					decor.getProject().getPrefix().equals(currentDecor.getProject().getPrefix())) {
				return true;
			}
		}
		return false;
	}

	public static Decor cleanupForVersionLabel(Decor decorParam, String versionLabel) {
		Decor decor = decorParam;
		if (decor != null && decor.getRules() != null && !StringUtils.isBlank(versionLabel)) {
			List<TemplateDefinition> listAllTemplates = RulesUtil.getTemplates(decor.getRules());
			log.info("total number of templates to clean on Version Label : {}", listAllTemplates.size());
			List<TemplateDefinition> listTemplatesByVersionLabel = RulesUtil.getTemplatesByVersionLabel(decor.getRules(), versionLabel);
			for (TemplateDefinition tdv : listTemplatesByVersionLabel) {
				cleanupListAllTemplatesBasedOnTDV(decor.getRules(), tdv);
			}
			log.warn("final number of cleaned templates : {}", RulesUtil.getTemplates(decor.getRules()).size());
		}
		return decor;
	}

	private static void cleanupListAllTemplatesBasedOnTDV(Rules rules, TemplateDefinition tdv) {
		if (tdv.getVersionLabel() == null) {
			return;
		}
		List<TemplateDefinition> listAllSameTD = RulesUtil.getTemplateDefinitionsById(rules, tdv.getId());
		for (TemplateDefinition templateDefinition : listAllSameTD) {
			if (!tdv.getVersionLabel().equals(templateDefinition.getVersionLabel())) {
				rules.getTemplateAssociationOrTemplate().remove(templateDefinition);
			}
		}
	}


	public static Decor cleanupForEffectiveDate(Decor decorParam) {
		Decor decor = decorParam;
		if (decor != null && decor.getRules() != null) {
			List<TemplateDefinition> listAllTemplates = RulesUtil.getTemplates(decor.getRules());
			log.info("total number of templates to clean on effectiveDate : {}", listAllTemplates.size());
			for (TemplateDefinition tdv : listAllTemplates) {
				cleanupListAllTemplatesBasedOnEffectiveDate(decor.getRules(), tdv);
			}
			log.info("final number of cleaned templates : {}", RulesUtil.getTemplates(decor.getRules()).size());
			StringBuilder stringToLog = new StringBuilder("[ ");
			for(Map.Entry<String,XMLGregorianCalendar> entry:removedTemplatesLog.entrySet()){
				stringToLog.append("(").append(entry.getKey()).append(", ").append(entry.getValue()).append(");");
			}
			stringToLog.append(" ]");
			log.info("All removed templates figure in the following list: "+ stringToLog);
			log.warn("Removed "+removedTemplatesLog.size()+" templates based on effective list, the full list is in the detailed log");
		}
		return decor;
	}

	private static void cleanupListAllTemplatesBasedOnEffectiveDate(Rules rules, TemplateDefinition po_templateDefinition) {
		TemplateDefinition lo_templateToKeep = po_templateDefinition;
		List<TemplateDefinition> listAllSameTD = RulesUtil.getTemplateDefinitionsByIdOrName(rules, lo_templateToKeep.getId(), lo_templateToKeep.getName());
		for (TemplateDefinition templateDefinition : listAllSameTD) {
			if (lo_templateToKeep.getEffectiveDate() != null && templateDefinition.getEffectiveDate() != null){
				if (lo_templateToKeep.getEffectiveDate().compare(templateDefinition.getEffectiveDate())>0) {
					removedTemplatesLog.put(templateDefinition.getId(),templateDefinition.getEffectiveDate());
					rules.getTemplateAssociationOrTemplate().remove(templateDefinition);
				} else if (lo_templateToKeep.getEffectiveDate().compare(templateDefinition.getEffectiveDate())<0){
					removedTemplatesLog.put(lo_templateToKeep.getId(), lo_templateToKeep.getEffectiveDate());
					rules.getTemplateAssociationOrTemplate().remove(lo_templateToKeep);
					lo_templateToKeep = templateDefinition;
				}
			} else {
				log.error("Unable to clean list of templates on EffectiveDate for template with id {} because some do not have effectiveDate", templateDefinition.getId() );
				return;

			}
		}


	}

}
