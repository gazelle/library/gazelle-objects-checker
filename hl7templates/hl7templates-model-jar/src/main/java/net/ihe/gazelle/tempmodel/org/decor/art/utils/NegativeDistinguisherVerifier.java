package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * used to verify if a distinguisher is accepted
 * @author Abderrazek Boufahja
 *
 */
public interface NegativeDistinguisherVerifier {
	
	boolean acceptNegativeDistinguisher(RuleDefinition ruleDefinition);

}
