package net.ihe.gazelle.tempmodel.decor.dt.model;

import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;

public enum AttDatatype {
	
	ST("st", UMLPrimitiveType.STRING, false, null),
	BIN("st", UMLPrimitiveType.STRING, false, null),
	BL("bl", UMLPrimitiveType.BOOLEAN, true, "(true|false)?"),
	BN("bn", UMLPrimitiveType.BOOLEAN, true, "(true|false)"),
	CS("cs", UMLPrimitiveType.STRING, true, "[^\\s]+", "isCSType"),
	INT("int", UMLPrimitiveType.INTEGER, true, "\\d+(\\.0)?"),
	OID("oid", UMLPrimitiveType.STRING, true, "^(0|[1-9]\\d*)\\.((0|[1-9]\\d*)\\.)*(0|[1-9]\\d*)$", "isOID"),
	REAL("real", UMLPrimitiveType.DOUBLE, true, "^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$"),
	RUID("ruid", UMLPrimitiveType.STRING, true, "^[A-Za-z][A-Za-z0-9\\-]*$", "isRUID"),
	TS("ts", UMLPrimitiveType.STRING, true, "[0-9]{1,8}|([0-9]{9,14}|[0-9]{14,14}\\.[0-9]+)([+\\-][0-9]{1,4})?", "isTSType"),
	UID("uid", UMLPrimitiveType.STRING, true, "isUID"),
	UUID("uuid", UMLPrimitiveType.STRING, true, "^(urn:uuid:)?[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}$", "isUUID");
	
	
	private String name;
	
	private UMLPrimitiveType primitiveType;
	
	private boolean testable;
	
	private String regex;
	
	private String operationName;
	
	private AttDatatype(String name, UMLPrimitiveType primitiveType, boolean testable, String regex) {
		this.name = name;
		this.primitiveType = primitiveType;
		this.testable = testable;
		this.regex = regex;
	}
	
	private AttDatatype(String name, UMLPrimitiveType primitiveType, boolean testable, String regex, String operationName) {
		this.name = name;
		this.primitiveType = primitiveType;
		this.testable = testable;
		this.regex = regex;
		this.operationName = operationName;
	}
	
	public String getOperationName() {
		return operationName;
	}

	public String getRegex() {
		return regex;
	}

    public String getRegexForJavafile() {
        return escapeForJavaFile(getRegex());
    }

    private String escapeForJavaFile(String value) {
        // replace actual slash by double slashes as it will be printed in java source code file.
        // See : https://www.xkcd.com/1638/
        return value.replaceAll("\\\\", "\\\\\\\\");
    }

	public boolean isTestable() {
		return testable;
	}

	public String getName() {
		return name;
	}

	public UMLPrimitiveType getPrimitiveType() {
		return primitiveType;
	}
	
	public static AttDatatype getAttDatatypeByName(String name){
		for (AttDatatype dt : AttDatatype.values()) {
			if (dt.getName().equals(name)){
				return dt;
			}
		}
		return null;
	}

}
