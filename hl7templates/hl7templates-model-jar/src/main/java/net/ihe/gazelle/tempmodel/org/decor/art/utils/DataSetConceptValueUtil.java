package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptValue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueProperty;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class DataSetConceptValueUtil {
	
	private DataSetConceptValueUtil() {}

	public static List<DataSetConceptList> getConceptLists(DataSetConceptValue t) {
		List<DataSetConceptList> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getPropertyOrConceptListOrExample()){
				if (obj instanceof DataSetConceptList){
					elements.add( (DataSetConceptList) obj );
				}
			} 
		}
		return elements;
	}

	public static List<Example> getExamples(DataSetConceptValue t) {
		List<Example> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getPropertyOrConceptListOrExample()){
				if (obj instanceof Example){
					elements.add( (Example) obj );
				}
			} 
		}
		return elements;
	}

	public static List<DataSetValueProperty> getPropertys(DataSetConceptValue t) {
		List<DataSetValueProperty> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getPropertyOrConceptListOrExample()){
				if (obj instanceof DataSetValueProperty){
					elements.add( (DataSetValueProperty) obj );
				}
			} 
		}
		return elements;
	}

}
