package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class DecorMarshaller {
	
	
	private static Logger log = LoggerFactory.getLogger(DecorMarshaller.class);

	private static HttpClient httpClient = HttpClient.newBuilder()
			.connectTimeout(java.time.Duration.ofSeconds(10))
			.followRedirects(HttpClient.Redirect.ALWAYS)
			.build();
	
	private DecorMarshaller() {}

	public static Decor loadDecor(BBRResource bbrResource){
		if(bbrResource instanceof PathBBRResource){
			String bbrpath = (String) bbrResource.getResource();
			return loadDecor(bbrpath);
		}
		else if(bbrResource instanceof StreamBBRResource) {
			URL bbrurl = (URL) bbrResource.getResource();
			try {
				return loadDecor(bbrurl.openStream());
			} catch (IOException e) {
				log.error("Cannot open stream from URL",e);
			}
		}
		return null;
	}

	public static Decor loadDecor(InputStream is) {
		try {
			JAXBContext jc = JAXBContext.newInstance(Decor.class);
			Unmarshaller u = jc.createUnmarshaller();
			Decor res = (Decor) u.unmarshal(is);
			updateDecorContent(res);
			return res;
		}
		catch (JAXBException e){
			log.error("Error to parse decor",e);
			return null;
		}
	}
	
	public static Decor loadDecor(String path) {
		log.info("--decor path : {}", path);
		InputStream is = null;
		try {
			if (UrlValidator.getInstance().isValid(path)) {
				is = getInputStreamFromURL(path);
			}
			else {
				is = new FileInputStream(path);
			}
			return loadDecor(is);
		} catch (IOException | InterruptedException e) {
			log.error("Error to load decor from url : " + path, e);
		}finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error("Error to close stream related to path : " + path, e);
				}
			}
		}
		return null;
	}
	
	private static void updateDecorContent(Decor res) {
		if (res != null) {
			res.setDatasets(null);
			res.setIds(null);
			res.setIssues(null);
			res.setScenarios(null);
		}
	}

	public static void marshallDecor(Decor decor, OutputStream os) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(Decor.class);
		Marshaller mar = jc.createMarshaller();
		mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		mar.marshal(decor, os);
	}

	private static InputStream getInputStreamFromURL(String url) throws IOException, InterruptedException {
		HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.uri(java.net.URI.create(url))
				.build();
		HttpResponse<InputStream> response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
		return response.body();
	}

}
