package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class TerminologyUtil {
	
	private TerminologyUtil() {}
	
	public static ValueSet getValueSetById(Terminology terma, String id) {
		if (terma != null && terma.getValueSet() != null && id != null) {
			return ValueSetRefUtil.extractReferencedValueSet(id, terma.getValueSet());
		}
		return null;
	}

}
