package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.goc.uml.utils.PathWithDT;
import net.ihe.gazelle.goc.uml.utils.PathWithDTUtil;
import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTUtils;
import net.ihe.gazelle.tempmodel.distinguisher.Distinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.distinguisher.utils.DistinguisherUtil;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;
import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplateSpecification;
import net.ihe.gazelle.tempmodel.temp.mapping.utils.TemplatesTypeMappingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 */
public class RuleDefinitionUtil {


    private static final String POSSIBLE_NAMESPACE = "(.*:)?";
    private static Logger log = LoggerFactory.getLogger(RuleDefinitionUtil.class);

    public static List<RuleDefinition> getElements(RuleDefinition ruleDefinition) {
        List<RuleDefinition> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition) {
                    elements.add((RuleDefinition) obj);
                }
            }
        }
        return elements;
    }

    /**
     * Flatten the rule definition and extract choices, to get all possible elements
     * @param ruleDefinition the rule definition to flatten
     * @return list of ruledefinitions
     */
    public static List<RuleDefinition> getAllEffectiveElements(RuleDefinition ruleDefinition){
        List<RuleDefinition> elements = getElements(ruleDefinition);
        List<ChoiceDefinition> choices = getChoices(ruleDefinition);
        for(ChoiceDefinition choice : choices){
            elements.addAll(ChoiceDefinitionUtil.getElements(choice));
        }
        return elements;
    }

    public static List<ChoiceDefinition> getChoices(RuleDefinition ruleDefinition) {
        List<ChoiceDefinition> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition) {
                    elements.add((ChoiceDefinition) obj);
                }
            }
        }
        return elements;
    }

    public static List<Let> getLets(RuleDefinition ruleDefinition) {
        List<Let> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Let) {
                    elements.add((Let) obj);
                }
            }
        }
        return elements;
    }

    public static List<Assert> getAsserts(RuleDefinition ruleDefinition) {
        List<Assert> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Assert) {
                    elements.add((Assert) obj);
                }
            }
        }
        return elements;
    }

    public static List<Report> getReports(RuleDefinition ruleDefinition) {
        List<Report> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Report) {
                    elements.add((Report) obj);
                }
            }
        }
        return elements;
    }

    public static List<DefineVariable> getDefineVariables(RuleDefinition ruleDefinition) {
        List<DefineVariable> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable) {
                    elements.add((DefineVariable) obj);
                }
            }
        }
        return elements;
    }

    public static List<IncludeDefinition> getIncludes(RuleDefinition ruleDefinition) {
        List<IncludeDefinition> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition) {
                    elements.add((IncludeDefinition) obj);
                }
            }
        }
        return elements;
    }

    public static List<FreeFormMarkupWithLanguage> getConstraints(RuleDefinition ruleDefinition) {
        List<FreeFormMarkupWithLanguage> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage) {
                    elements.add((FreeFormMarkupWithLanguage) obj);
                }
            }
            return elements;
        }
        return elements;
    }

    public static TemplateDefinition getParentTemplateDefinition(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null && ruleDefinition.getParentObject() != null) {
            if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
                return (TemplateDefinition) ruleDefinition.getParentObject();
            } else if (ruleDefinition.getParentObject() instanceof RuleDefinition) {
                return RuleDefinitionUtil.getParentTemplateDefinition((RuleDefinition) ruleDefinition.getParentObject());
            } else if (ruleDefinition.getParentObject() instanceof ChoiceDefinition) {
                return ChoiceDefinitionUtil.getParentTemplateDefinition((ChoiceDefinition) ruleDefinition.getParentObject());
            } else {
                log.error("problem processing ruleDefinition : " + ruleDefinition.getName() + " (RuleDefinitionUtil::getParentTemplateDefinition");
            }
        }
        return null;
    }

    public static DParent getDParentOfRuleDefinition(RuleDefinition ruleDefinition) {
        DParent res = null;
        if (ruleDefinition == null) {
            return null;
        }
        if (ruleDefinition.getParentObject() instanceof RuleDefinition) {
            res = getDParentOfRuleDefinition((RuleDefinition) ruleDefinition.getParentObject());
            updateTailFollowingElement(ruleDefinition, res);
        } else if (ruleDefinition.getParentObject() instanceof ChoiceDefinition) {
            ChoiceDefinition cd = (ChoiceDefinition) ruleDefinition.getParentObject();
            if (cd.getParentObject() instanceof RuleDefinition) {
                res = getDParentOfRuleDefinition((RuleDefinition) cd.getParentObject());
                updateTailFollowingElement(ruleDefinition, res);
            } else if (cd.getParentObject() instanceof TemplateDefinition) {
                TemplateDefinition parentTD = (TemplateDefinition) cd.getParentObject();
                String nameRD = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
                res = treatTemplateDefinitionToGetDParent(parentTD, nameRD, ruleDefinition.getName().split(":")[0]);
            }
        } else if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
            TemplateDefinition parentTD = (TemplateDefinition) ruleDefinition.getParentObject();
            String nameRD = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
            res = treatTemplateDefinitionToGetDParent(parentTD, nameRD, ruleDefinition.getName().split(":")[0]);
        } else if (ruleDefinition.getParentObject() instanceof ContainDefinition) {
            ContainDefinition cd = (ContainDefinition) ruleDefinition.getParentObject();
            if (cd.getParentObject() instanceof RuleDefinition) {
                res = getDParentOfRuleDefinition((RuleDefinition) cd.getParentObject());
                updateTailFollowingElement(ruleDefinition,res);
            }
        }
        return res;
    }


    private static void updateTailFollowingElement(RuleDefinition ruleDefinition, DParent res) {
        DElement el = (DElement) res;
        if (el.get(el.size() - 1) instanceof DElement) {
            DElement lastDE = new DElement();
            String realName = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
            lastDE.setName(realName);
            lastDE.setNameSpace(ruleDefinition.getName().split(":")[0]);
            if (RuleDefinitionProblemUtil.ruleDefinitionNeedsDistinguisher(ruleDefinition)) {
                // TODO still need to process when the ruleDefinition has a negative distinguisher
                lastDE.setDistinguisherAttributeOrElement(RuleDefinitionDistinguisherUtil.generateDistinguisherForRuleDefinition(ruleDefinition));
            }
            if (RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition) != RuleDefinitionUtil.getUMLTypeName(ruleDefinition)) {
                String cons = RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition);
                String original = RuleDefinitionUtil.getUMLTypeName(ruleDefinition);
                if (UMLLoader.verifyTheParentOfType1IsType2(cons, original)) {
                    lastDE.setExtendedType(cons);
                }
            }
            ((DElement) el.get(el.size() - 1)).setFollowingAttributeOrElement(lastDE);
        } else {
            log.info("there are a problem in rule definition parent");
        }
    }

    private static DParent treatTemplateDefinitionToGetDParent(TemplateDefinition parentTD, String nameRD, String namespace) {
        DParent res = null;
        DElement el = new DElement();
        el.setName(nameRD);
        el.setNameSpace(namespace);
        String templateIdValue = TemplateDefinitionUtil.extractTemplateId(parentTD);
        if (templateIdValue != null) {
            el.setDistinguisherAttributeOrElement(new DElement());
            DElement dis = (DElement) el.getDistinguisherAttributeOrElement();
            dis.setName("templateId");
            dis.setNameSpace(namespace);
            dis.setFollowingAttributeOrElement(new DAttribute());
            ((DAttribute) dis.getFollowingAttributeOrElement()).setName("root");
            ((DAttribute) dis.getFollowingAttributeOrElement()).setValue(templateIdValue);
        }
        String nameTDElement = nameRD.split(":")[nameRD.split(":").length - 1];
        List<TemplateSpecification> lts = TemplatesTypeMappingUtil.extractListTemplateSpecificationForElement(nameTDElement);
        if (lts != null && lts.size() > 1) {
            for (TemplateSpecification templateSpecification : lts) {
                if (TemplateDefinitionUtil.getListSupportedClassificationLevel(parentTD).contains(templateSpecification.getLevel())) {
                    el.setExtendedType(templateSpecification.getTemplateType());
                }
            }
        }
        res = el;
        return res;
    }

    /**
     * return the value specified in the RuleDefinition children if there are one
     * Example : @root -&gt; return the value of the attribute root from RuleDefinition
     *
     * @param dParent parent
     * @param ruleDefinition rule
     * @return string
     */
    public static String extractValueDPathFromRuleDefinition(DParent dParent, RuleDefinition ruleDefinition) {
        if (dParent instanceof DElement) {
            DElement dElement = (DElement) dParent;
            RuleDefinition val = RuleDefinitionUtil.getElementByNameFromElemntsOrContains(ruleDefinition, dElement.getName());
            if (dElement.getFollowingAttributeOrElement() != null && val != null) {
                return extractValueDPathFromRuleDefinition(dElement.getFollowingAttributeOrElement(), val);
            }
        } else if (dParent instanceof DAttribute) {
            DAttribute dattr = (DAttribute) dParent;
            if (RuleDefinitionUtil.getAttributeByName(ruleDefinition, dattr.getName()) != null) {
                Attribute attr = RuleDefinitionUtil.getAttributeByName(ruleDefinition, dattr.getName());
                return AttributeUtil.getAttributeValue(attr, dattr.getName());
            }
        }
        return null;
    }

    public static Object extractRuleDefinitionOrAttributeFromRuleDefinition(DParent dParent, RuleDefinition ruleDefinition) {
        if (dParent instanceof DElement) {
            DElement dElement = (DElement) dParent;
            return extractRuleDefinitionOrAttributeFromRuleDefinitionAndDElement(ruleDefinition, dElement);
        } else if (dParent instanceof DAttribute) {
            DAttribute dattr = (DAttribute) dParent;
            if (RuleDefinitionUtil.getAttributeByName(ruleDefinition, dattr.getName()) != null) {
                Attribute attr = RuleDefinitionUtil.getAttributeByName(ruleDefinition, dattr.getName());
                if (attr != null) {
                    if (dattr.getValue() == null) {
                        return attr;
                    }
                    String valAttr = AttributeUtil.getAttributeValue(attr, dattr.getName());
                    if (dattr.getValue().equals(valAttr)) {
                        return attr;
                    }
                }
            }
        }
        return null;
    }

    private static Object extractRuleDefinitionOrAttributeFromRuleDefinitionAndDElement(RuleDefinition ruleDefinition, DElement dElement) {
        RuleDefinition val = RuleDefinitionUtil.getElementByNameFromElemntsOrContains(ruleDefinition, dElement.getName());
        DParent dist = dElement.getDistinguisherAttributeOrElement();
        if (dist != null) {
            Object distVal = extractRuleDefinitionOrAttributeFromRuleDefinition(dist, val);
            if (distVal == null) {
                val = null;
            }
        }
        if (val == null) {
            return null;
        }
        if (dElement.getFollowingAttributeOrElement() != null) {
            return extractRuleDefinitionOrAttributeFromRuleDefinition(dElement.getFollowingAttributeOrElement(), val);
        } else {
            return val;
        }
    }

    /**
     * return the value specified in the RuleDefinition children if there are one
     * The tool will look into the distinguisher.xml and search for a match
     * Then for this match it will search in the RuleDefinition is there are a value that match the distinguisher
     *
     * @param ruleDefinition rule
     * @return the value specified in the RuleDefinition children
     */
    public static List<Pair<String, String>> extractValuesDistinguishersFromRuleDefinition(RuleDefinition ruleDefinition) {
        List<Pair<String, String>> res = new ArrayList<>();
        ElementDiscriber eldisc = DistinguisherUtil.findElementDiscriber(ruleDefinition);
        List<Distinguisher> ldis = DistinguisherUtil.getRealDistinguisher(eldisc);
        if (eldisc != null && ldis != null && !ldis.isEmpty()) {
            for (Distinguisher distinguisher : ldis) {
                DParent dElement = DPathExtractor.extractDElementFromDPathWithNS(distinguisher.getXpath(), ruleDefinition.getName().split(":")[0]);
                String valueDPath = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dElement, ruleDefinition);
                if (valueDPath != null) {
                    Pair<String, String> pr = new Pair<>(distinguisher.getXpath(), valueDPath);
                    res.add(pr);
                }
            }
        }
        return res;
    }

    /**
     * return the Java path based on a ruleDefinition
     * Example : POCDMT000040Observation.templateId
     * Example : POCDMT000040Act.entryRelationship.observation.value
     *
     * @param ruleDefinition rule
     * @return the Java path based on a ruleDefinition
     */
    public static List<PathWithDT> getJavaPath(RuleDefinition ruleDefinition) {
        List<PathWithDT> res = new ArrayList<>();
        if (ruleDefinition != null) {
            if (ruleDefinition.getParentObject() instanceof RuleDefinition) {
                res = getJavaPath((RuleDefinition) ruleDefinition.getParentObject());
                String name = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
                String type = RuleDefinitionUtil.getExtendedUMLTypeNameOrNull(ruleDefinition, res);
                res.add(new PathWithDT(name, type));
            } else if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
                TemplateDefinition td = (TemplateDefinition) ruleDefinition.getParentObject();
                res.add(new PathWithDT(TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(td), null));
            } else if (ruleDefinition.getParentObject() instanceof ChoiceDefinition) {
                ChoiceDefinition cd = (ChoiceDefinition) ruleDefinition.getParentObject();
                if (cd.getParentObject() instanceof RuleDefinition) {
                    RuleDefinition parent = (RuleDefinition) cd.getParentObject();
                    res = getJavaPath(parent);
                    String name = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
                    String type = RuleDefinitionUtil.getExtendedUMLTypeNameOrNull(ruleDefinition, res);
                    res.add(new PathWithDT(name, type));
                } else {
                    log.error("problem to handle choice in a templateDefinition RuleDefinitionUtil::getJavaPath");
                }
            }
        }
        return res;
    }

    public static DParent getDParentOfTheParent(RuleDefinition ruleDefinition) {
        DParent parentPath = null;
        if (ruleDefinition.getParentObject() instanceof RuleDefinition) {
            parentPath = RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition) ruleDefinition.getParentObject());
        } else if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
            parentPath = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
        } else if (ruleDefinition.getParentObject() instanceof ChoiceDefinition) {
            parentPath = ChoiceDefinitionUtil.getDParentOfChoiceDefinition((ChoiceDefinition) ruleDefinition.getParentObject());
        }
        return parentPath;
    }

    /**
     * get the CDA name of a RuleDefinition
     * Example : hl7:templateId -&gt; templateId
     *
     * @param ruleDefinition rule
     * @return the CDA name of a RuleDefinition
     */
    public static String getRealNameOfRuleDefinition(RuleDefinition ruleDefinition) {
        String name = ruleDefinition.getName();
        return StringUtil.getRealNameOfRuleDefinition(name);
    }

    /**
     * return the first attribute that have the same name of the param name
     * some attribute are not common : exmplae : classCode, moodCode, etc
     *
     * @param ruleDefinition rule
     * @param name           -&gt; example : hl7:templateId
     * @return the first attribute that have the same name of the param name
     */
    public static Attribute getAttributeByName(RuleDefinition ruleDefinition, String name) {
        List<Attribute> res = getAttributesByName(ruleDefinition, name);
        if (res != null && !res.isEmpty()) {
            return res.get(0);
        }
        return null;
    }

    public static List<Attribute> getAttributesByName(RuleDefinition ruleDefinition, String name) {
        if (name == null || ruleDefinition == null) {
            return new ArrayList<>();
        }
        List<Attribute> la = ruleDefinition.getAttribute();
        List<Attribute> res = new ArrayList<>();
        for (Attribute attribute : la) {
            if (attribute.getName() != null && attribute.getName().equals(name)) {
                res.add(attribute);
            } else if (name.equals("classCode") && attribute.getClassCode() != null) {
                res.add(attribute);
            } else if (name.equals("contextConductionInd") && attribute.isContextConductionInd() != null) {
                res.add(attribute);
            } else if (name.equals("contextControlCode") && attribute.getContextControlCode() != null) {
                res.add(attribute);
            } else if (name.equals("determinerCode") && attribute.getDeterminerCode() != null) {
                res.add(attribute);
            } else if (name.equals("extension") && attribute.getExtension() != null) {
                res.add(attribute);
            } else if (name.equals("independentInd") && attribute.isIndependentInd() != null) {
                res.add(attribute);
            } else if (name.equals("institutionSpecified") && attribute.isInstitutionSpecified() != null) {
                res.add(attribute);
            } else if (name.equals("inversionInd") && attribute.isInversionInd() != null) {
                res.add(attribute);
            } else if (name.equals("mediaType") && attribute.getMediaType() != null) {
                res.add(attribute);
            } else if (name.equals("moodCode") && attribute.getMoodCode() != null) {
                res.add(attribute);
            } else if (name.equals("negationInd") && attribute.getNegationInd() != null) {
                res.add(attribute);
            } else if (name.equals("nullFlavor") && attribute.getNullFlavor() != null) {
                res.add(attribute);
            } else if (name.equals("operator") && attribute.getOperator() != null) {
                res.add(attribute);
            } else if (name.equals("qualifier") && attribute.getQualifier() != null) {
                res.add(attribute);
            } else if (name.equals("representation") && attribute.getRepresentation() != null) {
                res.add(attribute);
            } else if (name.equals("root") && attribute.getRoot() != null) {
                res.add(attribute);
            } else if (name.equals("typeCode") && attribute.getTypeCode() != null) {
                res.add(attribute);
            } else if (name.equals("unit") && attribute.getUnit() != null) {
                res.add(attribute);
            } else if (name.equals("use") && attribute.getUse() != null) {
                res.add(attribute);
            }
        }
        return res;
    }


    /**
     * return the first element that have the same name of the param name
     *
     * @param ruleDefinition rule
     * @param name           -&gt; example : hl7:templateId
     * @return the first element that have the same name of the param name
     */
    public static RuleDefinition getElementByName(RuleDefinition ruleDefinition, String name) {
        List<RuleDefinition> la = RuleDefinitionUtil.getElements(ruleDefinition);
        for (RuleDefinition rul : la) {
            if (rul.getName().matches(POSSIBLE_NAMESPACE + name)) {
                return rul;
            }
        }
        return null;
    }

    /**
     * get the element that have the corresponding name,event if it is on a contain
     *
     * @param ruleDefinition rule
     * @param name string
     * @return the element that have the corresponding name,event if it is on a contain
     */
    public static RuleDefinition getElementByNameFromElemntsOrContains(RuleDefinition ruleDefinition, String name) {
        List<RuleDefinition> la = RuleDefinitionUtil.getElements(ruleDefinition);
        for (RuleDefinition rul : la) {
            if (rul.getName().matches(POSSIBLE_NAMESPACE + name)) {
                return rul;
            }
        }
        List<ContainDefinition> lc = RuleDefinitionUtil.getContains(ruleDefinition);
        for (ContainDefinition containDefinition : lc) {
            TemplateDefinition referencedTemplateDefinition = ContainDefinitionUtil.referencedTemplateDefinitionFromParentBBR(containDefinition);
            RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(referencedTemplateDefinition);
            if (rd != null && rd.getName().matches(POSSIBLE_NAMESPACE + name)) {
                return rd;
            }
        }
        return null;
    }

    /**
     * return the all elements that have the same name of the param name
     *
     * @param ruleDefinition rule
     * @param name           -&gt; example : {hl7:templateId, hl7:templateId}
     * @return the all elements that have the same name of the param name
     */
    public static List<RuleDefinition> getElementsByName(RuleDefinition ruleDefinition, String name) {
        List<RuleDefinition> res = new ArrayList<>();
        List<RuleDefinition> la = RuleDefinitionUtil.getElements(ruleDefinition);
        for (RuleDefinition rul : la) {
            if (rul.getName().matches("^([^(:|/|\\[)]*:)?" + name + "(\\[.*)?")) {
                res.add(rul);
            }
        }
        List<ChoiceDefinition> lcd = RuleDefinitionUtil.getChoices(ruleDefinition);
        for (ChoiceDefinition choiceDefinition : lcd) {
            List<RuleDefinition> lelFromChoices = ChoiceDefinitionUtil.getElementsByName(choiceDefinition, name);
            res.addAll(lelFromChoices);
        }
        return res;
    }

    public static String getUMLTypeName(RuleDefinition ruleDefinition) {
        List<PathWithDT> lpathWithDT = RuleDefinitionUtil.getJavaPath(ruleDefinition);
        String path = PathWithDTUtil.extractTypedPathFromListPathWithDTWithoutLastDTChecking(lpathWithDT);
        return UMLLoader.getPropertyTypeName(path);
    }

    public static String getConstrainedUMLTypeName(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            String type = RuleDefinitionUtil.getUMLTypeName(ruleDefinition);
            String dt = ruleDefinition.getDatatype() != null ? ruleDefinition.getDatatype().getLocalPart() : null;
            String dtProposed = DTUtils.getUMLDatatype(dt);
            if (dtProposedISARealExtension(type, dtProposed)) {
                return dtProposed;
            }
            return type;
        }
        return null;
    }

    protected static boolean dtProposedISARealExtension(String type,
                                                        String dtProposed) {
        return dtProposed != null && !dtProposed.equals(type) && UMLLoader.verifyTheParentOfType1IsType2(dtProposed, type);
    }

    public static String getExtendedUMLTypeNameOrNull(RuleDefinition ruleDefinition, List<PathWithDT> lpathWithDT) {
        List<PathWithDT> copy = new ArrayList<>();
        if (lpathWithDT != null) {
            copy.addAll(lpathWithDT);
            copy.add(new PathWithDT(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)));
        }
        String path = PathWithDTUtil.extractTypedPathFromListPathWithDT(copy);
        String realType = UMLLoader.getPropertyTypeName(path);
        String dt = ruleDefinition.getDatatype() != null ? ruleDefinition.getDatatype().getLocalPart() : null;
        if(dt == null) {
            return null;
        }
        String dtProposed = DTUtils.getUMLDatatype(dt);
        if (dtProposedISARealExtension(realType, dtProposed)) {
            return dtProposed;
        }
        return null;
    }

    /**
     * contains both elements and choice/elements
     *
     * @param selectedRuleDefinition rule
     * @return both elements and choice/elements
     */
    public static Set<String> getListAllowedSubElements(RuleDefinition selectedRuleDefinition) {
        Set<String> hashset = new TreeSet<>();
        if (selectedRuleDefinition != null) {
            List<RuleDefinition> lrd = RuleDefinitionUtil.getAbsoluteSubElements(selectedRuleDefinition);
            for (RuleDefinition ruleDefinition : lrd) {
                hashset.add(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
            }
        }
        return hashset;
    }

    public static List<RuleDefinition> getAbsoluteSubElements(RuleDefinition ruleDefinition) {
        List<RuleDefinition> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition) {
                    elements.add((RuleDefinition) obj);
                }
            }
            List<ChoiceDefinition> lcd = getChoices(ruleDefinition);
            for (ChoiceDefinition choiceDefinition : lcd) {
                List<RuleDefinition> choiceElements = ChoiceDefinitionUtil.getElements(choiceDefinition);
                if (choiceElements != null) {
                    elements.addAll(choiceElements);
                }
            }
        }
        return elements;
    }

    /**
     * the attributes are ignored, only elements are token
     *
     * @param selectedRuleDefinition rule
     * @return elements list
     */
    public static Set<String> getListCompleteSubElements(RuleDefinition selectedRuleDefinition) {
        String type = RuleDefinitionUtil.getConstrainedUMLTypeName(selectedRuleDefinition);
        if (type != null) {
            return UMLLoader.getListPropertiesNamesWithoutEAttributes(type);
        }
        return new HashSet<>();
    }

    public static boolean ruleDefinitionIsCompletelyClosed(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            if (ruleDefinition.getIsClosed() != null) {
                return ruleDefinition.getIsClosed();
            } else {
                if (ruleDefinition.getParentObject() instanceof RuleDefinition) {
                    return ruleDefinitionIsCompletelyClosed((RuleDefinition) ruleDefinition.getParentObject());
                } else if (ruleDefinition.getParentObject() instanceof ChoiceDefinition) {
                    ChoiceDefinition choiceDefinition = (ChoiceDefinition) ruleDefinition.getParentObject();
                    if (choiceDefinition.getParentObject() instanceof RuleDefinition) {
                        return ruleDefinitionIsCompletelyClosed((RuleDefinition) choiceDefinition.getParentObject());
                    } else if (choiceDefinition.getParentObject() instanceof TemplateDefinition) {
                        return ((TemplateDefinition) choiceDefinition.getParentObject()).isIsClosed();
                    }
                } else if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
                    return ((TemplateDefinition) ruleDefinition.getParentObject()).isIsClosed();
                }
            }
        }
        return false;
    }

    public static List<ContainDefinition> getContains(RuleDefinition ruleDefinition) {
        List<ContainDefinition> elements = new ArrayList<>();
        if (ruleDefinition != null) {
            for (Object obj : ruleDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition) {
                    elements.add((ContainDefinition) obj);
                }
            }
        }
        return elements;
    }

    /**
     * verify if a DElement already exists in the RuleDefinition
     */
    public static Boolean verifyIfDElementConvertible(DElement dElement, RuleDefinition ruleDefinition) {
        if (dElement == null || ruleDefinition == null) {
            return false;
        }
        if (dElement.getName() == null ||
                !dElement.getName().equals(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition))) {
            return false;
        }
        DParent distinguisher = dElement.getDistinguisherAttributeOrElement();
        DParent following = dElement.getFollowingAttributeOrElement();
        if (following != null) {
            // the dElement does not represent the surrent element but a sub element, other treatment needed
            return false;
        }
        return RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(distinguisher, ruleDefinition) != null;
    }

    public static void includeDParentInRuleDefinition(DParent distinguisherAttributeOrElement,
                                                      RuleDefinition currentRuleDefinition) {
        if (distinguisherAttributeOrElement != null && currentRuleDefinition != null) {
            if (distinguisherAttributeOrElement instanceof DAttribute) {
                DAttribute disAttr = (DAttribute) distinguisherAttributeOrElement;
                Attribute attr = new Attribute();
                attr.setName(disAttr.getName());
                attr.setValue(disAttr.getValue());
                attr.setIsOptional(true);
                attr.setParentObject(currentRuleDefinition);
                currentRuleDefinition.getAttribute().add(attr);
            } else if (distinguisherAttributeOrElement instanceof DElement) {
                DElement disEl = (DElement) distinguisherAttributeOrElement;
                RuleDefinition fils = new RuleDefinition();
                currentRuleDefinition.getLetOrAssertOrReport().add(fils);
                fils.setName(disEl.getName());
                if (disEl.getDistinguisherAttributeOrElement() != null) {
                    includeDParentInRuleDefinition(disEl.getDistinguisherAttributeOrElement(), fils);
                }
                if (disEl.getFollowingAttributeOrElement() != null) {
                    includeDParentInRuleDefinition(disEl.getFollowingAttributeOrElement(), fils);
                }
            }
        }

    }

    public static boolean includingDParentInRuleDefinitionIsPossible(DParent distinguisherAttributeOrElement,
                                                                     RuleDefinition currentRuleDefinition) {
        if (distinguisherAttributeOrElement != null
                && distinguisherAttributeOrElement.getName() != null
                && currentRuleDefinition != null) {
            if (distinguisherAttributeOrElement instanceof DAttribute) {
                Attribute attr = RuleDefinitionUtil.getAttributeByName(currentRuleDefinition,
                        distinguisherAttributeOrElement.getName());
                return attr == null;
            } else if (distinguisherAttributeOrElement instanceof DElement) {
                DElement delement = (DElement) distinguisherAttributeOrElement;
                List<RuleDefinition> listEls = RuleDefinitionUtil.getElementsByName(currentRuleDefinition,
                        delement.getName());
                if (!listEls.isEmpty()) {
                    return false;
                }
                if (delement.getFollowingAttributeOrElement() != null) {
                    return includingDParentInRuleDefinitionIsPossible(delement.getFollowingAttributeOrElement(),
                            currentRuleDefinition);
                }
                return true;
            }
        }
        return false;
    }

    public static Set<ValueSetConcept> extractListRelatedException(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            List<Vocabulary> lv = ruleDefinition.getVocabulary();
            Decor dec = DecorUtil.getDecorFromRD(ruleDefinition);
            return VocabularyUtil.extractListRelatedException(lv, dec);
        }
        return new HashSet<>();
    }

    public static boolean verifyIfRDIsACodeDT(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            String type = RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition);
            if (type != null) {
                String typeCode = UMLLoader.getPropertyTypeName(type + ".code");
                Boolean isAnAttribute = UMLLoader.isAnAttribute(type + ".code");
                return typeCode != null && isAnAttribute;
            }
        }
        return false;
    }

}
