package net.ihe.gazelle.tempmodel.dpath.model;

public class DElement  extends DParent{
	
	private DParent followingAttributeOrElement;
	
	private DParent distinguisherAttributeOrElement;
	
	private String extendedType;

	private String nameSpace;

	public String getExtendedType() {
		return extendedType;
	}

	public void setExtendedType(String extendedType) {
		this.extendedType = extendedType;
	}

	public DParent getFollowingAttributeOrElement() {
		return followingAttributeOrElement;
	}

	public void setFollowingAttributeOrElement(DParent followingAttributeOrElement) {
		this.followingAttributeOrElement = followingAttributeOrElement;
	}

	public DParent getDistinguisherAttributeOrElement() {
		return distinguisherAttributeOrElement;
	}

	public void setDistinguisherAttributeOrElement(
			DParent distinguisherAttributeOrElement) {
		this.distinguisherAttributeOrElement = distinguisherAttributeOrElement;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public DElement setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
		return this;
	}
}
