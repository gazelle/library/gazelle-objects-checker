package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class StringUtil {
	
	private static final Pattern PAT = Pattern.compile(".*?:([^\\[]*)(\\[.*)?");
	
	private StringUtil() {}
	
	/**
	 * get the CDA name
	 * Example : hl7:templateId -&gt; templateId
	 * @param nameWithNamespace nameWithNamespace
	 * @return string CDA name
	 */
	public static String getRealNameOfRuleDefinition(String nameWithNamespace){
		String name = nameWithNamespace;
		Matcher mat = PAT.matcher(name);
		if (mat.find()){
			name = mat.group(1);
		}
		return name;
	}
	
	public static String cap1stChar(String userIdea) {
		if (userIdea != null && userIdea.length()>0){
			return Character.toUpperCase(userIdea.charAt(0)) + userIdea.substring(1);
		}
		return userIdea;
	}

	public static boolean isOID(String valueSet) {
		if (valueSet == null || valueSet.trim().isEmpty()){
			return false;
		}
		return valueSet.matches("\\d+(\\.\\d+)*");
	}
	

}
