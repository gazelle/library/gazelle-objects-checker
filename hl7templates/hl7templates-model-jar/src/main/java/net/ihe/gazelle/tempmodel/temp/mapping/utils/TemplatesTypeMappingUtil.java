package net.ihe.gazelle.tempmodel.temp.mapping.utils;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import gnu.trove.map.hash.THashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplateSpecification;
import net.ihe.gazelle.tempmodel.temp.mapping.model.TemplatesTypeMapping;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class TemplatesTypeMappingUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(TemplatesTypeMappingUtil.class);
	
	private static String ttmPath = System.getProperty("HL7TEMP_RESOURCES_PATH", "../../hl7templates-resources") + File.separator +
			System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") + 
			"/templatesTypeMapping.xml";
	
	private static Map<String, TemplatesTypeMapping> templatesTypeMappings = new THashMap<>();
	
	public static TemplatesTypeMapping extractTemplatesTypeMapping(String path) {
		if (templatesTypeMappings.containsKey(path)) {
			return templatesTypeMappings.get(path);
		}
		try {
			Unmarshaller um = JAXBContext.newInstance(TemplatesTypeMapping.class).createUnmarshaller();
			TemplatesTypeMapping ttm = (TemplatesTypeMapping) um.unmarshal(new File(path));
			templatesTypeMappings.put(path, ttm);
			return ttm;
		} catch (JAXBException e) {
			log.error("error to interpret templatesTypeMapping.xml", e);
		}
		return null;
	}
	
	public static void printTemplatesTypeMapping(TemplatesTypeMapping ttm, OutputStream os) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(TemplatesTypeMapping.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(ttm, os);
	}
	
	public static String extractTemplatePath(String templateType) {
		TemplatesTypeMapping ttm = extractTemplatesTypeMapping(ttmPath);
		if (ttm != null && templateType != null && !templateType.isEmpty()) {
			for (TemplateSpecification ts : ttm.getTemplateSpecification()) {
				if (ts.getTemplateType().equals(templateType)) {
					return ts.getTemplatePath();
				}
			}
		}
		return null;
	}
	
	public static boolean templateTypeCanBeHL7Template(String templateType) {
		TemplatesTypeMapping ttm = extractTemplatesTypeMapping(
				System.getProperty("HL7TEMP_RESOURCES_PATH", "../../hl7templates-resources//") + "/" +
						System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") + 
						"/templatesTypeMapping.xml");
		return templateTypeCanBeHL7Template(ttm, templateType);
	}
	
	protected static boolean templateTypeCanBeHL7Template(TemplatesTypeMapping ttm, String templateType) {
		if (ttm != null && templateType != null && !templateType.equals("")) {
			for (TemplateSpecification ts : ttm.getTemplateSpecification()) {
				if (ts.getTemplateType().equals(templateType)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static List<TemplateSpecification> extractListTemplateSpecificationForElement(String element) {
		TemplatesTypeMapping ttm = extractTemplatesTypeMapping(ttmPath);
		return extractListTemplateSpecificationForElement(ttm, element);
	}
	
	public static String extractTemplateTypeFromElementNameIfExists(String element, List<String> listLevels) {
		List<TemplateSpecification> lts = TemplatesTypeMappingUtil.extractListTemplateSpecificationForElement(element);
		if (lts.size()>1) {
			for (TemplateSpecification templateSpecification : lts) {
				if (listLevels != null && listLevels.contains(templateSpecification.getLevel())) {
					return templateSpecification.getTemplateType();
				}
			}
		}
		else if (lts.size()==1) {
			return lts.get(0).getTemplateType();
		}
		return null;
	}
	
	protected static List<TemplateSpecification> extractListTemplateSpecificationForElement(TemplatesTypeMapping ttm, String element) {
		List<TemplateSpecification> res = new ArrayList<>();
		if (ttm != null && element != null && !element.isEmpty()) {
			for (TemplateSpecification ts : ttm.getTemplateSpecification()) {
				if (ts.getTemplateElement().contains(element)) {
					res.add(ts);
				}
			}
		}
		return res;
	}

}
