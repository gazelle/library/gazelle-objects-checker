package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ChoiceDefinitionUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(ChoiceDefinitionUtil.class);
	
	private ChoiceDefinitionUtil() {}

	public static List<FreeFormMarkupWithLanguage> getConstraints(ChoiceDefinition t) {
		List<FreeFormMarkupWithLanguage> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getIncludeOrElementOrConstraint()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage){
					elements.add( (FreeFormMarkupWithLanguage) obj );
				}
			} 
		}
		return elements;
	}

	public static List<RuleDefinition> getElements(ChoiceDefinition t) {
		List<RuleDefinition> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getIncludeOrElementOrConstraint()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition){
					elements.add( (RuleDefinition) obj );
				}
			} 
		}
		return elements;
	}

	public static List<IncludeDefinition> getIncludes(ChoiceDefinition t) {
		List<IncludeDefinition> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getIncludeOrElementOrConstraint()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition){
					elements.add( (IncludeDefinition) obj );
				}
			} 
		}
		return elements;
	}
	
	public static TemplateDefinition getParentTemplateDefinition(ChoiceDefinition cd) {
		if (cd != null && cd.getParentObject() != null) {
			if (cd.getParentObject() instanceof TemplateDefinition) {
				return (TemplateDefinition)cd.getParentObject();
			}
			else if (cd.getParentObject() instanceof RuleDefinition){
				return RuleDefinitionUtil.getParentTemplateDefinition((RuleDefinition)cd.getParentObject());
			}
		}
		return null;
	}

	public static DParent getDParentOfChoiceDefinition(ChoiceDefinition choiceDefinition) {
		if (choiceDefinition != null) {
			if (choiceDefinition.getParentObject() instanceof RuleDefinition) {
				return RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition)choiceDefinition.getParentObject());
			}
			else {
				log.error("Problem to handle parent of a choice definition in a templateDefinition");
				return null;
			}
		}
		return null;
	}
	
	public static List<RuleDefinition> getElementsByName(ChoiceDefinition choiceDefinition, String name){
		List<RuleDefinition> res = new ArrayList<>();
		List<RuleDefinition> la = ChoiceDefinitionUtil.getElements(choiceDefinition);
		for (RuleDefinition rul : la) {
			if (rul.getName().matches("(.*:)?" + name + "(\\[.*)?")){
				res.add(rul);
			}
		}
		return res;
	}

	public static List<ContainDefinition> getContains(ChoiceDefinition t) {
		List<ContainDefinition> elements = new ArrayList<>();
		if (t != null){
			for(Object obj : t.getIncludeOrElementOrConstraint()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition){
					elements.add( (ContainDefinition) obj );
				}
			} 
		}
		return elements;
	}
	
}
