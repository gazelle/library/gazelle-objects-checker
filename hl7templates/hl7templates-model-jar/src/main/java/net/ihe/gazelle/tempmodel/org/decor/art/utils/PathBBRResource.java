package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;

public class PathBBRResource implements BBRResource {

    private String bbrPath;

    public PathBBRResource(String bbrPath) {
        this.bbrPath = bbrPath;
    }


    @Override
    public Object getResource() {
        return bbrPath;
    }
}
