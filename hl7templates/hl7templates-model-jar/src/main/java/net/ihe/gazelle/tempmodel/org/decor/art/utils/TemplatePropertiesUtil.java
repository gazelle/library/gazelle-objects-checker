package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateTypes;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class TemplatePropertiesUtil {
	
	private TemplatePropertiesUtil() {}
	
	public static boolean containsTemplateType(List<TemplateProperties> ltp, TemplateTypes tt) {
		if (tt != null && ltp != null) {
			for (TemplateProperties templateProperties : ltp) {
				if (templateProperties.getType() != null && templateProperties.getType() == tt) {
					return true;
				}
			}
		}
		return false;
	}

}
