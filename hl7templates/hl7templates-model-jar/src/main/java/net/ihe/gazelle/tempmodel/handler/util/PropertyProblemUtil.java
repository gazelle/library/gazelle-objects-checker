package net.ihe.gazelle.tempmodel.handler.util;

import net.ihe.gazelle.tempmodel.decor.prop.model.PropertyDescEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class PropertyProblemUtil {
	
	private PropertyProblemUtil() {}
	
	//TODO process contain attribute !!
	public static boolean verifyIfPropertyContainValidCombination(Property prop) {
		PropertyDescEnum pde = PropertyDescEnum.extractPropertyDescEnum(prop);
		return pde != null;
	}
	
	public static boolean verifyIfPropertyDeclaredForTheRightElement(Property prop) {
		if (prop == null) {
			return false;
		}
		PropertyDescEnum pde = PropertyDescEnum.extractPropertyDescEnum(prop);
		RuleDefinition constrainedRD = prop.getParentObject();
		if (constrainedRD == null) {
			return false;
		}
		String type = RuleDefinitionUtil.getConstrainedUMLTypeName(constrainedRD);
		if (verifyIfPropertyContainValidCombination(prop)) {
			return pde.containsDT(type);
		}
		return false;
	}
	
}
