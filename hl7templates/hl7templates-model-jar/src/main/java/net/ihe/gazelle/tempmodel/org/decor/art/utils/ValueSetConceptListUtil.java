package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ValueSetConceptListUtil {
	
	private ValueSetConceptListUtil() {}

	public static List<ValueSetConcept> getConcepts(ValueSetConceptList valueSetConceptList) {
		List<ValueSetConcept> templates = new ArrayList<>();
		if (valueSetConceptList != null){
			for(Object obj : valueSetConceptList.getConceptOrInclude()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept){
						templates.add( (ValueSetConcept) obj );
				}
			} 
		}
		return templates;
	}

	public static List<ValueSetRef> getIncludes(ValueSetConceptList valueSetConceptList) {
		List<ValueSetRef> templates = new ArrayList<>();
		if (valueSetConceptList != null){
			for(Object obj : valueSetConceptList.getConceptOrInclude()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef){
						templates.add( (ValueSetRef) obj );
				}
			} 
		}
		return templates;
	}

}
