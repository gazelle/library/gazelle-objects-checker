//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.09 at 11:11:31 AM CEST 
//


package net.ihe.gazelle.tempmodel.org.decor.art.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DesignationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="DesignationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="preferred"/&gt;
 *     &lt;enumeration value="synonym"/&gt;
 *     &lt;enumeration value="abbreviation"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DesignationType")
@XmlEnum
public enum DesignationType {


    /**
     * preferred
     * 
     */
    @XmlEnumValue("preferred")
    PREFERRED("preferred"),

    /**
     * synonym
     * 
     */
    @XmlEnumValue("synonym")
    SYNONYM("synonym"),

    /**
     * abbreviation
     * 
     */
    @XmlEnumValue("abbreviation")
    ABBREVIATION("abbreviation");
    private final String value;

    DesignationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DesignationType fromValue(String v) {
        for (DesignationType c: DesignationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
