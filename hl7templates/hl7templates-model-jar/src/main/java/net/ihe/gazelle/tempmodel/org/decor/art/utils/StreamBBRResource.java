package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;

import java.net.URL;

public class StreamBBRResource implements BBRResource {

    private URL url;

    public StreamBBRResource(URL url) {
        this.url = url;
    }

    @Override
    public Object getResource() {
        return url;
    }
}
