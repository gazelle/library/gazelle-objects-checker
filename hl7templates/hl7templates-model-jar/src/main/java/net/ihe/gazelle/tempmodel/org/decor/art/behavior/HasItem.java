package net.ihe.gazelle.tempmodel.org.decor.art.behavior;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public interface HasItem {
	
	Item getItem();
	
	void setItem(Item item);

}
