package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AuthorityType;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class AuthorityTypeUtil {
	
	private AuthorityTypeUtil() {}
	
	/**
	 * 
	 * @param at
	 * @return the content is mixed as string
	 */
	public static List<AddrLine> getAddrLines(AuthorityType at){
		return new ArrayList<>();
	}

}
