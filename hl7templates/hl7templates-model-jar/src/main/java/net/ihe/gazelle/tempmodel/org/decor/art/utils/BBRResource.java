package net.ihe.gazelle.tempmodel.org.decor.art.utils;

public interface BBRResource {

    Object getResource();
}
