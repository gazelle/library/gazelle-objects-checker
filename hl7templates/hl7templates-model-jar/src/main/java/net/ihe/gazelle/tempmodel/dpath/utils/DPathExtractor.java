package net.ihe.gazelle.tempmodel.dpath.utils;

import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class DPathExtractor {
	
	
	private static Logger log = LoggerFactory.getLogger(DPathExtractor.class);
	
	public static final String PATH_REGEX = "(/([^/\\@]+?:|\\@)([^/\\[]+)(\\[(.+)\\])?)";
	
	public static final String PATH_REGEX_COMPLETE = "^(/(.+?:|\\@)([^/\\[]+)(\\[(.+)\\])?)+$";
	
	public static final String PATH_ATTR = "/\\@([^=]+)(=\\'(.*)?\\')?";
	
	public static final String PATH_ELEM = "(/([^/\\@]+?:)([^/\\[]+)(\\[(.+)\\])?)";
	
	private static final Pattern PATT_PATH_REGEX = Pattern.compile(PATH_REGEX);
	
	private static final Pattern PATT_PATH_REGEX_COMPLETE = Pattern.compile(PATH_REGEX_COMPLETE);
	
	private static final Pattern PATT_PATH_ATTR = Pattern.compile(PATH_ATTR);
	
	private static final Pattern PATT_PATH_ELEM = Pattern.compile(PATH_ELEM);



	public static DParent extractDElementFromDPath(String dpath){
		return extractDElementFromDPathWithNS(dpath, "hl7");
	}

	public static DParent extractDElementFromDPathWithNS(String dpath, String namespace){
		if (!DPathExtractor.pathIsValidDPath(dpath)) {
			log.error("The path specified is not a valid dpath : {}", dpath);
			return null;
		}
		Matcher mat = PATT_PATH_REGEX.matcher(dpath);
		DParent del = null;
		DParent pointer = null;
		while (mat.find()){
			if ("@".equals(mat.group(2))){
				DAttribute attr = createAttributeFromPathAttribute(mat.group());
				if (attr == null) {
					// the tool not able to process
					return null;
				}
				if (pointer == null){
					del = attr;
					pointer = del;
				}
				else if (pointer instanceof DElement){
					((DElement)pointer).setFollowingAttributeOrElement(attr);
					pointer = attr;
				}
				else {
					log.error("pointer is not a ruleDefinition element! The path could not be processed "
							+ "completely because it may contains a double following attributes: {}", dpath);
					return null;
				}
			}
			else {
				DElement ell = createElementFromPathElement(mat.group(), namespace);
				if (ell == null) {
					// the tool not able to process
					return null;
				}
				if (pointer == null){
					del = ell;
					pointer = del;
				}
				else if (pointer instanceof DElement){
					((DElement)pointer).setFollowingAttributeOrElement(ell);
					((DElement)pointer).setNameSpace(namespace);
					pointer = ell;
				}
				else {
					log.error("pointer is not a ruleDefinition element! The path could not be processed "
							+ "completely because it may contains an attribute followed by an element path: {}", dpath);
					return null;
				}
			}
		}
		return del;
	}
	
	public static boolean pathIsValidDPath(String dpath) {
		if (StringUtils.isBlank(dpath)) {
			return false;
		}
		return PATT_PATH_REGEX_COMPLETE.matcher(dpath).matches();
	}
	
	public static String createPathFromDParent(DParent attrOrElement){
		StringBuilder sb = new StringBuilder("");
		createPathFromDParentByStringBuilder(attrOrElement, sb);
		return sb.toString();
	}

	public static void createPathFromDParentByStringBuilder(DParent attrOrElement, StringBuilder res){
		if (attrOrElement == null) {
			return;
		}
		res.append("/");
		createCorePathFromDParentByStringBuilder(attrOrElement, res);
	}

	private static void createCorePathFromDParentByStringBuilder(DParent attrOrElement, StringBuilder res) {
		if (attrOrElement instanceof DAttribute){
			res.append("@").append(attrOrElement.getName());
			if (attrOrElement.getValue() != null){
				res.append("='").append(attrOrElement.getValue()).append("'");
			}
		}
		else if(attrOrElement instanceof DElement){
			DElement el = (DElement)attrOrElement;
			res.append(el.getNameSpace()).append(":").append(el.getName());
			if (el.getDistinguisherAttributeOrElement() != null){
				res.append("["); 
				createCorePathFromDParentByStringBuilder(el.getDistinguisherAttributeOrElement(), res);
				res.append("]");
			}
			if (el.getFollowingAttributeOrElement() != null){
				createPathFromDParentByStringBuilder(el.getFollowingAttributeOrElement(), res);
			}
		}
	}

	protected static DElement createElementFromPathElement(String group){
		return createElementFromPathElement(group, "hl7");
	}

	protected static DElement createElementFromPathElement(String group, String namespace) {
		if (group == null) {
			return null;
		}
		Matcher mat = PATT_PATH_ELEM.matcher(group);
		if (mat.find()){
			DElement elem = new DElement();
			elem.setName(mat.group(3));
			elem.setNameSpace(namespace);
			if (mat.group(5) != null){
				if (!StringUtils.isNumeric(mat.group(5))) {
					DParent dist = extractDElementFromDPath("/" + mat.group(5));
					elem.setDistinguisherAttributeOrElement(dist);
				}
				else {
					log.error("The distinguisher is an integer, the tool is not able to process it : {}s", group);
					return null;
				}
			}
			return elem;
		}
		return null;
	}

	protected static DAttribute createAttributeFromPathAttribute(String group) {
		if (group == null) {
			return null;
		}
		Matcher mat = PATT_PATH_ATTR.matcher(group);
		if (mat.find()){
			DAttribute attr = new DAttribute();
			attr.setName(mat.group(1));
			attr.setValue(mat.group(3));
			return attr;
		}
		return null;
	}

}
