package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Abderrazek Boufahja
 */
public final class ContainDefinitionUtil {

    private static final String POSSIBLE_NAMESPACE = "(.*:)?";

    private ContainDefinitionUtil() {
    }

    /**
     * extract referenced templateDefinition
     *
     * @param containDefinition
     * @return
     */
    public static TemplateDefinition referencedTemplateDefinition(ContainDefinition containDefinition) {
        if (containDefinition != null && containDefinition.getRef() != null) {
            TemplateDefinition parentTD = ContainDefinitionUtil.getParentTemplateDefinition(containDefinition);
            if (parentTD != null) {
                String identifier = RulesUtil.getReferenceTemplateIdentifier(parentTD.getParentObject(), containDefinition.getRef());
                for (TemplateDefinition childTD : RulesUtil.getTemplates(parentTD.getParentObject())) {
                    if (childTD.getId() != null && childTD.getId().equals(identifier)) {
                        return childTD;
                    }
                }
            }
        }
        return null;
    }

    public static TemplateDefinition referencedTemplateDefinitionFromParentBBR(ContainDefinition containDefinition) {
        if (containDefinition != null && containDefinition.getRef() != null) {
            TemplateDefinition parentTD = ContainDefinitionUtil.getParentTemplateDefinition(containDefinition);
            if (parentTD != null) {
                TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(parentTD.getParentObject().getParentObject());
                String identifier = TemplateRepositoryUtil.getReferenceTemplateIdentifierFromTemplateRepository(tr, containDefinition.getRef());
                return TemplateRepositoryUtil.getTemplateDefinitionByIdentifier(tr, identifier);
            }
        }
        return null;
    }

    public static TemplateDefinition getParentTemplateDefinition(ContainDefinition containDefinition) {
        if (containDefinition != null) {
            if (containDefinition.getParentObject() instanceof TemplateDefinition) {
                return (TemplateDefinition) containDefinition.getParentObject();
            } else if (containDefinition.getParentObject() instanceof RuleDefinition) {
                RuleDefinition rd = (RuleDefinition) containDefinition.getParentObject();
                return RuleDefinitionUtil.getParentTemplateDefinition(rd);
            } else if (containDefinition.getParentObject() instanceof ChoiceDefinition) {
                ChoiceDefinition cd = (ChoiceDefinition) containDefinition.getParentObject();
                return ChoiceDefinitionUtil.getParentTemplateDefinition(cd);
            }
        }
        return null;
    }

    public static RuleDefinition getElementByName(ContainDefinition containDefinition, String name) {
        List<RuleDefinition> la = ContainDefinitionUtil.getElements(containDefinition);
        for (RuleDefinition rul : la) {
            if (rul.getName().matches(POSSIBLE_NAMESPACE + name)) {
                return rul;
            }
        }
        return null;
    }

    public static List<RuleDefinition> getElements(ContainDefinition containDefinition) {
        List<RuleDefinition> elements = new ArrayList<>();
        if (containDefinition != null) {
            for (Object obj : containDefinition.getLetOrAssertOrReport()) {
                if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition) {
                    elements.add((RuleDefinition) obj);
                }
            }
        }
        return elements;
    }


    public static RuleDefinition getTemplateIdElement(ContainDefinition cd) {
        if (getElements(cd) != null) {
            RuleDefinition rd = getElements(cd).get(0);
            if (!rd.getName().matches(POSSIBLE_NAMESPACE + "templateId")) {
                rd = getTemplateIdElement(rd);
            }
            return rd;
        }
        return null;
    }

    private static RuleDefinition getTemplateIdElement(RuleDefinition rd) {
        if (RuleDefinitionUtil.getElements(rd) != null) {
            rd = RuleDefinitionUtil.getElements(rd).get(0);
            if (!rd.getName().matches(POSSIBLE_NAMESPACE + "templateId")) {
                rd = getTemplateIdElement(rd);
            }
            return rd;
        }
        return null;
    }

}
