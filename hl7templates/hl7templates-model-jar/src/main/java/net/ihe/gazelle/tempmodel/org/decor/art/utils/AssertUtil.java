package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.goc.common.utils.OperationKind;
import net.ihe.gazelle.goc.common.utils.XPATHOCLGroup;
import net.ihe.gazelle.goc.common.utils.XPATHOCLRule;
import net.ihe.gazelle.goc.common.utils.XSITypeOCL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class AssertUtil {


	private static Logger log = LoggerFactory.getLogger(AssertUtil.class);

	private static final String VS_REG = "\\\\u0027include/voc-((\\b\\d*\\.\\d*\\b)*)";
	private static final Pattern VS_REG_PATT = Pattern.compile(VS_REG);

	private static final String XSI_TYPE_REG = "((not\\()?(@xsi:type=\\\\u0027([A-Za-z_-]*)\\\\u0027)\\)?)";
	private static final Pattern XSI_TYPE_PATT = Pattern.compile(XSI_TYPE_REG);

	private static final String INNER_REG = "(\\((.*)\\))";
	private static final Pattern INNER_PATT = Pattern.compile(INNER_REG);

	private static final String PATH_ELEM = "(([^/@]+?:)([^/\\[]+)(\\[(.+)])?)";
	private static final Pattern PATT_PATH_ELEM = Pattern.compile(PATH_ELEM);

	
	private AssertUtil() {}
	
	public static TemplateDefinition getParentTemplateDefinition(Assert assert1){
		if (assert1 != null && assert1.getParentObject() != null){
			if (assert1.getParentObject() instanceof TemplateDefinition){
				return (TemplateDefinition) assert1.getParentObject();
			}
			else if (assert1.getParentObject() instanceof RuleDefinition){
				return RuleDefinitionUtil.getParentTemplateDefinition((RuleDefinition)assert1.getParentObject());
			}
			else {
				log.error("problem processing assert1 : " + assert1.getTest() + " (AssertUtil::getParentTemplateDefinition");
			}
		}
		return null;
	}
	
	public static String getStringValue(Assert assert1){
		StringBuilder res = new StringBuilder();
		int i = 0;
		for(Object obj : assert1.getContent()){
			if (obj instanceof String){
				if (i++>0) {
					res.append(", ");
				}
				res.append((String)obj);
			}
		} 
		return res.toString();
	}

	public static String extractVSFromIncludeRule(String ocl){
		Matcher matcher = VS_REG_PATT.matcher(ocl);
		if(matcher.find() && matcher.group(1) != null){
			return matcher.group(1);
		}
		else {
			return null;
		}

	}

	public static List<XPATHOCLGroup>  extractGroupsFromXPATH(String xpath){
		if(xpath.contains("[")){
			return null;
		}
		List<XPATHOCLGroup> groups = new ArrayList<>();
		String encodedXpath = encodeInnerParenthesis(xpath);
		String[] stringGroups = encodedXpath.split(" (?![^(]*\\))");
		for(int i=0;i<stringGroups.length;i+=2){
			stringGroups[i] = decodeASCIIString(stringGroups[i]);
			String groupOperationString = i!=stringGroups.length-1?stringGroups[i+1]:"";
			if(!stringGroups[i].startsWith("(")){
				XPATHOCLRule rule = null;
				if(stringGroups[i].contains("@xsi:type")){
					Matcher matcher = XSI_TYPE_PATT.matcher(stringGroups[i]);
					if(matcher.find()){
						boolean isNot = !matcher.group(1).equals(matcher.group(3));
						rule = new XSITypeOCL(OperationKind.EMPTY,isNot,matcher.group(4),stringGroups[i]);
					}
					else {
						log.error("no  match found for xsi type");
						return null;
					}
				}
				else{
					rule = new XPATHOCLRule(OperationKind.EMPTY,stringGroups[i]);
				}
				XPATHOCLGroup group = new XPATHOCLGroup();
				group.getRules().add(rule);
				group.setOperation(OperationKind.getOperationKind(groupOperationString));
				groups.add(group);
			}
			else{
				Matcher matcher = INNER_PATT.matcher(stringGroups[i]);
				if(matcher.find()){
					String innerXPATH = matcher.group(2);
					String[] splitInnerXPATH = innerXPATH.split(" ");
					XPATHOCLGroup group = new XPATHOCLGroup();
					for(int j=0;j<splitInnerXPATH.length;j+=2){
						splitInnerXPATH[j] = splitInnerXPATH[j].replaceAll("\\(?\\)?","");
						XPATHOCLRule rule;
						String operationString = j!=splitInnerXPATH.length-1?splitInnerXPATH[j+1]:"";
						if(splitInnerXPATH[j].contains("@xsi:type")){
							Matcher innerMatcher = XSI_TYPE_PATT.matcher(splitInnerXPATH[j]);
							if(innerMatcher.find()){
								boolean isNot = !innerMatcher.group(1).equals(innerMatcher.group(3));
								rule = new XSITypeOCL(OperationKind.getOperationKind(operationString),isNot,innerMatcher.group(4),splitInnerXPATH[j]);
							}
							else{
								log.error("no match found for xsi type in group");
								return null;
							}
						}
						else{
							rule = new XPATHOCLRule(OperationKind.getOperationKind(operationString),splitInnerXPATH[j]);
						}
						group.setOperation(OperationKind.getOperationKind(groupOperationString));
						group.getRules().add(rule);
					}
					groups.add(group);

				}
				else {
					log.error("no match found for inner rule");
					return null;
				}
			}
		}
		return groups;
	}

	public static String encodeInnerParenthesis(String xpath){
		Pattern pattern = Pattern.compile("(\\(([^(]*\\([^(]*)\\))");
		Matcher matcher = pattern.matcher(xpath);
		if(matcher.find()){
			String res = "";
			String openingParenthesisASCII = "[ASCI:" + ((int) '(') + "]";
			String closingParenthesisASCII = "[ASCI:" + ((int) ')') + "]";

			//replace '(' with its ASCII Code
			res = matcher.group(2).replaceAll("\\(",openingParenthesisASCII);
			res = res.replaceAll("\\)",closingParenthesisASCII);
			xpath = xpath.replace(matcher.group(2), res);
		}
		return xpath;
	}

	public static String decodeASCIIString(String xpath){
		String ASCII_REG = "(\\[ASCI:(\\d*)\\])";
		Pattern ASCII_PATT = Pattern.compile(ASCII_REG);
		Matcher matcher = ASCII_PATT.matcher(xpath);
		while (matcher.find()){
			String c = ((char)(Integer.parseInt(matcher.group(2)))+"");
			xpath = xpath.replace(matcher.group(1),c);
		}
		return xpath;
	}


	public static String extractAssertFromName(String name){
		Matcher matcher = PATT_PATH_ELEM.matcher(name);
		if (matcher.find()) {
			if(matcher.group(5) != null){
				return matcher.group(5);
			}
			else{
				//TODO: find a functional way to handle elements without assertions
				log.warn("couldn't find the assertion in element name: {}, ignoring assertion part for now",name);
				return name;
			}
		}
		else{
			log.error("wrong provided String {}",name);
			return null;
		}
	}

}
