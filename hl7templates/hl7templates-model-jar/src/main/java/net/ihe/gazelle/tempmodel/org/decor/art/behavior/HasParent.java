package net.ihe.gazelle.tempmodel.org.decor.art.behavior;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public interface HasParent {
	
	<T> T getParentObject();

}
