#!/bin/bash

skipUT="-DskipTests=true"

#Add commands to build archetype projects
cd ../../archetype/generated-validator/ && mvn clean install

#Add commands to build all model jars, to execute in model folder
cd ../../models/model-resources/ && mvn clean install
cd ../common-models/ && mvn clean install
cd ../voc-model/ && mvn clean install
cd ../datatypes-model/ && mvn clean install
cd ../nblock-model/ && mvn clean install
cd ../cda-model/ && mvn clean install
cd ../infr-model/ && mvn clean install
cd ../cdaepsos-model/ && mvn clean install

#Add commands to build all hl7templates projects, to execute in hl7templates folder
cd ../../hl7templates/gocmodel-jar && mvn clean install $skipUT
cd ../hl7templates-model-jar && mvn clean install $skipUT
cd ../hl7templates-api && mvn clean install $skipUT
cd ../hl7templates-generator-jar && mvn clean install $skipUT
cd ../hl7templates-packager-jar && mvn clean install $skipUT