package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.EnvironementException;
import org.junit.Test;
import static org.junit.Assert.*;

public class EnvironementCheckerTest {

    @Test
    public void checkJavaVersionTest()  {
        EnvironementChecker environementChecker = new EnvironementCheckerImpl();
        try {
            environementChecker.checkJavaVersion();
        } catch (EnvironementException e) {
            e.printStackTrace();
            fail();
        }

    }
}
