package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class FileWriterImplTest {

    private FileWriterImpl fileWriter;

    @Before
    public void initForTests(){
        this.fileWriter = new FileWriterImpl();
    }

    @Test (expected = IOException.class)
    public void wrongURLWriteTest() throws IOException {
        URL url;
        try {
            url = new URL("file:/test/goc/url/is/wrong");
        } catch (MalformedURLException e){
            Assert.fail("This url should not be malformed !");
            return;
        }
        fileWriter.writeContentInFile(url, "test.txt");
    }

    @Test
    public void copyResourceTest(){
        URL url = Thread.currentThread().getContextClassLoader().getResource("testFileToWrite.txt");
        try{
            fileWriter.writeContentInFile(url, "src/test/resources/resultFile.txt");
        } catch (IOException e){
            Assert.fail();
        }
        File outputFile = new File("src/test/resources/resultFile.txt");
        Assert.assertTrue(outputFile.exists());
        try{
            String outputContent = FileUtils.readFileToString(outputFile, StandardCharsets.UTF_8);
            Assert.assertEquals("Test", outputContent);
        } catch (IOException e){
            Assert.fail();
        }

        if (!outputFile.delete()){
            Assert.fail("Cannot delete generated file !");
        }
    }
}