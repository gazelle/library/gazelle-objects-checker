package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.ArchetypeInstantiatorMaven;
import net.ihe.gazelle.lib.hl7templatespackager.application.GOCLogger;
import net.ihe.gazelle.lib.hl7templatespackager.application.ValueSetExtractor;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.GOCLoggerImpl;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.ProcessExecutorBash;
import org.junit.*;
import static org.junit.Assert.*;

public class ArchetypeInstantiatorMavenTest extends ArchetypeInstantiatorMaven {


    /**
     * Creates an instance of the class with a defined root path to create archetypes instances.
     *
     * @param workspacesRoot    : root path where workspaces will be created.
     * @param mvnExecutionPath
     * @param gocLogger
     * @param processExecutor
     * @param valueSetExtractor
     */
    public ArchetypeInstantiatorMavenTest() {
        super("/opt/apache-maven-3.5.4/bin/mvn",new ProcessExecutorBash(new GOCLoggerImpl()));
    }

    /**
     * Creates an instance of the class with a defined root path to create archetypes instances.
     *
     * @param workspacesRoot    : root path where workspaces will be created.
     * @param mvnExecutionPath
     * @param gocLogger
     * @param processExecutor
     * @param valueSetExtractor
     */


    //This test is ignored because we can not run a mvn cmd in the pipeline
    @Ignore
    @Test
    public void getMavenRepoTest(){
        String output = getMavenRepo();
        assertTrue(output.contains(".m2/repository"));
    }
}
