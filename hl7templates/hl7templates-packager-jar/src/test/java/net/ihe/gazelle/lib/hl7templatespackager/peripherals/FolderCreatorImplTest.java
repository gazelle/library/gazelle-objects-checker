package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FolderCreatorImplTest {

    private FolderCreatorImpl folderCreator;

    @Before
    public void initForTests(){
        this.folderCreator = new FolderCreatorImpl();
    }

    @Test
    public void testCreateFolder(){
        try {
            folderCreator.createFolder("src/test/resources/test/test");
            Assert.assertTrue(new File("src/test/resources/test/test").exists());
        } catch (IOException e){
            Assert.fail();
        }
    }

    @Test
    public void testResetFolder(){
        try {
            folderCreator.resetFolder("src/test/resources/test");
            File resetFolder = new File("src/test/resources/test");
            Assert.assertTrue(resetFolder.exists());
            Assert.assertEquals(0, resetFolder.listFiles().length);
        } catch (IOException e){
            Assert.fail();
        }
    }

    @After
    public void cleanUp(){
        File folderToDelete = new File("src/test/resources/test/test");
        if (folderToDelete.exists()){
            folderToDelete.delete();
        }
    }
}
