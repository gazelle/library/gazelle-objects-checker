package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class UpdateModelsDefinitionTest {

	@Test
	public void testUpdateWorkspacePath() {
		UpdateModelsDefinition updateModelsDefinition = new UpdateModelsDefinition();
		try {
			String newWorkspacePath = updateModelsDefinition.updateWorkspacePath("<modelUMLDesc>"
					+ "<path>/path/on/my/machine/voc-model/models/voc.uml</path>"
					+ "aa", "ee");
			assertEquals("<modelUMLDesc>"
					+ "<path>ee/voc-model/models/voc.uml</path>"
					+ "aa", newWorkspacePath);
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Test
	public void testExtractWorkspace() {
		UpdateModelsDefinition updateModelsDefinition = new UpdateModelsDefinition();
		try {
			String workspacePath = updateModelsDefinition.extractWorkspace("/path/on/my/machine/voc-model/models/voc.uml");
			assertEquals("/path/on/my/machine",workspacePath);
		} catch (IOException e) {
			Assert.fail();
		}
	}


}
