package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class ProcessExecutorBashTest {

    private ProcessExecutorBash processExecutor;

    @Before
    public void initForTests(){
        this.processExecutor = new ProcessExecutorBash(new GOCLoggerImpl());
    }

    @Test
    public void executeProcessTest(){
        try {
            processExecutor.executeProcess("echo turtle");
        } catch (IOException e){
            Assert.fail();
        }
    }

    @Test(expected = IOException.class)
    public void executeProcessUnknownCommandTest() throws IOException {
        processExecutor.executeProcess("troubadour");
    }

    @Test(expected = IOException.class)
    public void executeProcessWrongMavenGoalTest() throws IOException {
        processExecutor.executeProcess("mvn zegsbdf");
    }
}
