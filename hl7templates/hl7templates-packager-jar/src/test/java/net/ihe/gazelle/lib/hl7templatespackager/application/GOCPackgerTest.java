package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.PathBBRResource;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.GOCLoggerImpl;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.GOCPackagerFactory;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.ProcessExecutorBash;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GOCPackgerTest {

    @Test
    @Ignore
    public void packageBBRTest() throws Exception{
        //TODO This contains home dir links and should not be pushed as it is !
        //TODO Eventually this could become end to end test Ignored for now as we do not want to generate ever time a validator
        //Also put files in test resources
        String mavnExecPath ="mvn";
        String workspaceRoot = "/home/aai@kereval.com/IdeaProjects/GOCFomGit/hl7templates/hl7templates-packager-jar/test-packaging/";
        String generationDate = "noDate";
        GOCLogger gocLogger = new GOCLoggerImpl(workspaceRoot,generationDate);
        GOCPackager packager = new GOCPackagerFactory().createGOCPackager(workspaceRoot, mavnExecPath,generationDate, gocLogger, new ProcessExecutorBash(gocLogger));
//        String bbrPath = "/home/aai@kereval.com/IdeaProjects/GOCFomGit/bbr-eHDSI-4.1.2.xml";
        String bbrPath = "/home/aai@kereval.com/IdeaProjects/GOCFomGit/hl7templates/hl7templates-packager-jar/src/test/resources/decor_assertionChoices3.xml";
        String folderOutput = "output-folder";
        System.setProperty("HL7TEMP_RESOURCES_PATH", "target/hl7templates-resources");
        System.setProperty("HL7TEMP_CDACONFFOLDERNAME", "cdaepsos");//"cdabasic");
        String serviceName = "HL7 - C-CDA R2.1 - Meaningful Use Stage 3";
        Path path = Paths.get(workspaceRoot+"/logs/log-"+generationDate);
        Files.createDirectories(path);

        //notOverride set to false because new resources are retrieved with an anonymous workspace so we need to replace it
        packager.packageBBRToGOCValidationTool(new Workspace(folderOutput), new ValidatorConfiguration(new PathBBRResource(bbrPath), "2.1", false,false, serviceName));
    }
}
