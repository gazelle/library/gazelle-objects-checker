package net.ihe.gazelle.db;

import net.ihe.gazelle.gen.common.ConceptProviderImpl;

public class ConceptProviderDBImpl extends ConceptProviderImpl {
	
	@Override
	protected String getSVSRepositoryUrl() {
		return "https://gazellecontent.sequoiaproject.org/SVSSimulator/rest/RetrieveValueSetForSimulator";
	}

}
