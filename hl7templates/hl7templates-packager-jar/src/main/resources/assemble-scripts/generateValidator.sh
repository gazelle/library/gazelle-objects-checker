#!/bin/bash

#JAVA_HOME="/usr/local/jdk1.7.0_79"
MVN_EXEC="/opt/apache-maven-3.5.4/bin/mvn"
CURRENT_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $CURRENT_FOLDER

printinfo() {
	echo "INFO: $@"	
}

printerror() {
	echo "ERROR: $@"	
}

printcmd() {
	echo "Executed CMD: $@"	
}

printparagraph() {
	line='----------------'
	echo "$line $@ $line"
}

function cecho {
  while [ "$1" ]; do
    case "$1" in
    -normal)        color="\033[00m" ;;
    -black)         color="\033[30;01m" ;;
    -red)           color="\033[31;01m" ;;
    -green)         color="\033[32;01m" ;;
    -yellow)        color="\033[33;01m" ;;
    -blue)          color="\033[34;01m" ;;
    -magenta)       color="\033[35;01m" ;;
    -cyan)          color="\033[36;01m" ;;
    -white)         color="\033[37;01m" ;;
    -gray)          color="\033[90;01m" ;;
    -n)             one_line=1;   shift ; continue ;;
    *)              echo -n "$1"; shift ; continue ;;
    esac

    shift
    echo -en "$color"
    echo -en "$1"
    echo -en "\033[00m"
    shift

  done
  if [ ! $one_line ]; then
    echo
  fi
}

function initiate_logs {
   cecho -green "initiate log files"
   _currDate=`date +%Y-%m-%d-%H-%M-%S`
   _logfolder="`pwd`/logs/log-$_currDate"
   mkdir -p $_logfolder
   if ! [[ -d "$_logfolder" ]]; then 
       cecho -red "not able to create log folder : $_logfolder"
       exit 1
   fi
}

function check_java {
    cecho -green "check the java version used"
    if [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    	cecho -normal "found java executable in JAVA_HOME"     
    	_java="$JAVA_HOME/bin/java"
    elif type -p java; then
        cecho -normal "found java executable in PATH"
        _java=java
    else
    	cecho -red "no java found, please reconfigure the parameter JAVA_HOME"
	exit 1
    fi

    if [[ "$_java" ]]; then
    	version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    	cecho -normal "java version shall be more or equal to 11"
    	if [[ "$version" > "11" ]]; then
    	    cecho -normal "java version found : $version"
    	else         
           cecho -red "java version found : $version, please provide a JVM with a version <= 11"
	   exit 1
   	 fi
    fi
}

function check_mvn {
    cecho -green "check the mvn used"
    if [[ -x "$MVN_EXEC" ]];  then
        cecho -normal "found mvn executable in $MVN_EXEC"
    else
        cecho -red "no mvn found in the specified path : $MVN_EXEC, please reconfigure the parameter MVN_EXEC"
        exit 1
    fi
}

function check_bbr {
    cecho -green "check bbr url (or path)"
    _bbr=$1
    if [[ -r "$1" ]]; then 
        cecho -normal "bbr found in : $_bbr"
    else
		cecho -normal "download of the BBR : $_bbr"
        _bbr=$_logfolder/bbr-`date +"%Y-%m-%d-%H-%M-%S"`.xml
		wget -v --no-check-certificate $1 --output-document=$_bbr 
        if [[ -r "$_bbr" ]]; then 
            cecho -normal "bbr downloaded in the output : $_bbr"
	else 
	    cecho -red "the script is not able to download the bbr : $1"
	    exit 1
        fi
    fi
}

function check_outputFolder {
    _outputFolder=$1
    cecho -green "check about the output folder"
    if [ "x$1" = "x" ]; then
		cecho -red "the parameter 2 is not provided to the script"
		exit 1
    fi
    if [[ -d "$1" ]]; then 
	echo `pwd`
    	cecho -normal "outputFolder $1 found"
        _outputFolder=`realpath $_outputFolder`
    else
	cecho -normal "outputFolder $1 not found"
    	mkdir -p $1
        if [[ -d "$1" ]]; then
	    cecho -normal "the output folder is created"
	    _outputFolder=`realpath $_outputFolder`
		else 
		    cecho -red "the script is not able to create the outputfolder : $1"
		    exit 1
		fi
    fi
    cecho -normal "output folder is : $_outputFolder"
}

function generateValidator {
    cecho -green "generate the validator in folder "
    cd packager/bin/
    if [[ "x$3" = "x" ]]; then
      cmd="-bbr $1 -out $2 -mvn $MVN_EXEC $4  2>&1"
      echo "1- generate with cmd: $cmd"
#    	./generator.sh  -bbr $1 -out $2 -mvn $MVN_EXEC $4  2>&1
    else
    	#$5 SHALL be -versionLabel and $6 is the versionLabel
    	cmd="-bbr "$1" -out "$2" -mvn "$MVN_EXEC" -serviceName "$3" $4 $5 $6 $7 $8  2>&1"
    	echo "2- generate with cmd: $cmd"
#		  ./generator.sh  -bbr "$1" -out "$2" -mvn "$MVN_EXEC" -serviceName "$3" $4 $5 $6 $7 $8  2>&1
    fi
}

function check_ignoreTemplateIdRequirements {
	cecho -green "check templatesId requirements"
	ignoreTemplateIdReq=""
	if [[ "x$1" = "xtrue" ]]; then
		ignoreTemplateIdReq="-ignoreTemplateIdRequirements"
	fi
}

function check_ignoreCdaBasic {
	cecho -green "check CDA Basic requirements"
	ignoreCdaBasicReq=""
	if [[ "x$1" = "xtrue" ]]; then
		ignoreCdaBasicReq="-ignoreCdaBasicRequirements"
	fi
	cecho -green $ignoreCdaBasicReq
}

function check_rootClassName {
	cecho -green "check rootClassName"
	rootClassName=""

	if [[ "x$1" = "x" ]]; then
		echo "no rootClassName specified"
	else
		rootClassName="-rootClassName $1"
		echo "rootClassName $1"
	fi
}

function check_hl7temp_cdaconffoldername {
	cecho -green "check hl7temp_cdaconffoldername"
	hl7temp_cdaconffoldername=""
	if [[ "x$1" = "x" ]]; then
		echo "no HL7TEMP_CDACONFFOLDERNAME specified"
	else
		hl7temp_cdaconffoldername="-HL7TEMP_CDACONFFOLDERNAME $1"
		echo "HL7TEMP_CDACONFFOLDERNAME $1"
	fi
}

function check_versionLabel {
	cecho -green "check versionLabel"
	versionLabel=""
	if [[ "x$1" = "x" ]]; then
		echo "no versionlabel specified"
	else
		versionLabel="-versionLabel $1"
		echo "versionLabel $1"
	fi
}


for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)   
    if ! [ $KEY = "$ARGUMENT" ]; then
	    case "$KEY" in
	            --BBR_LINK)              
	            	BBR_LINK=${VALUE} 
	            	;;
	            --OUTPUT_FOLDER)    
	            	OUTPUT_FOLDER=${VALUE} 
	            	;;
	            --VALIDATOR_NAME)    
	            	VALIDATOR_NAME="${VALUE}" 
	            	;;
	            --IGNORE_TEMPLATEID_REQUIREMENTS)    
	            	IGNORE_TEMPLATEID_REQUIREMENTS=${VALUE} 
	            	;;
	            --IGNORE_CDA_BASIC)
	                IGNORE_CDA_BASIC=${VALUE}
	                ;;
	            --VERSION_LABEL)    
	            	VERSION_LABEL=${VALUE} 
	            	;;
	            --ROOT_CLASS_NAME)
	            	ROOT_CLASS_NAME=${VALUE}
	            	;;
	            --HL7TEMP_CDACONFFOLDERNAME)
	            	HL7TEMP_CDACONFFOLDERNAME=${VALUE}
	            	;;
	            *)  
	            	printerror "Not supported argument: $KEY"
	            	printerror "Supported argument are: --BBR_LINK, --OUTPUT_FOLDER, --VALIDATOR_NAME, --IGNORE_TEMPLATEID_REQUIREMENTS, --IGNORE_CDA_BASIC, --VERSION_LABEL, --ROOT_CLASS_NAME, --HL7TEMP_CDACONFFOLDERNAME"
	            	exit 1
	            	;;	 
	    esac    
	elif [ $ARGUMENT = "--help" ]; then
		echo ""
		echo "Generate the validation tool based on the link to the BBR from ART-DECOR"
		echo ""
		echo "Usage: ./generateValidator.sh [options]"
		echo "List of options:"
		printf "%-40s"  "  --BBR_LINK"
		echo "Value of url to bbr, example: /home/gazelle/projects/bbr.xml"
		printf "%-40s"  "  --OUTPUT_FOLDER"
		echo "folder that will be used as workspace to compile and generate the validation tool. Example: --OUTPUT_FOLDER=output-10"
		
		printf "%-40s"  "  --VALIDATOR_NAME"
		echo "The name of the generated validator. Example: HL7 - C-CDA"
		
		printf "%-40s"  "  --IGNORE_TEMPLATEID_REQUIREMENTS"
		echo "boolean value that can be true of false, and describes if we checks the constraints related to the tempalteId"

		printf "%-40s"  "  --IGNORE_CDA_BASIC"
		echo "boolean value that can be true of false, and describes if we checks the constraints related to the CDA Basic interpretation"
		
		printf "%-40s"  "  --VERSION_LABEL"
		echo "The version that shall be extracted from the BBR to create the validation tool. Example: for C-CDA it can be 1.1 or 2.1"

		printf "%-40s"  "  --ROOT_CLASS_NAME"
		echo "The Full qualified class name (eg:net.ihe.gazelle.cdaepsos3.POCDMT000040ClinicalDocument)"

		printf "%-40s"  "  --HL7TEMP_CDACONFFOLDERNAME"
		echo "The name of the folder containing HL7 Template CDA model to build with.(eg:cdaepsos)"
		
		exit 0
	else
		printerror "argument not understood, please use --help option for documentation"
		printerror "Documentation: ./generateValidator.sh --help" 
		exit 0
	fi
done

#initiate_logs
check_java
check_mvn
check_bbr "$BBR_LINK"
check_outputFolder "$OUTPUT_FOLDER"
check_ignoreTemplateIdRequirements $IGNORE_TEMPLATEID_REQUIREMENTS
check_ignoreCdaBasic $IGNORE_CDA_BASIC
check_versionLabel $VERSION_LABEL
check_rootClassName $ROOT_CLASS_NAME
check_hl7temp_cdaconffoldername $HL7TEMP_CDACONFFOLDERNAME
generateValidator "$_bbr" "$_outputFolder" "$VALIDATOR_NAME" "$ignoreTemplateIdReq" "$ignoreCdaBasicReq" $versionLabel "$rootClassName" $hl7temp_cdaconffoldername
cecho -green "generation ended"
