#!/bin/bash

####################################
# SEQUOIA Generation & Adaptation Script
# Created by Achraf Achkari - 09 Nov 2021
# Last revision - 09 Nov 2021
# Version 1.0.0
#####################################

#Colors definitions for terminal
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'

#SEQUOIA Validators
MU3="HL7 - C-CDA R2.1 - Meaningful Use Stage 3"
USCDI="HL7 - C-CDA R2.1 - USCDI"
CCDA_R_1="HL7 - C-CDA R1.1"

#UNCOMENT THIS TO OVERRIDE DEFAULT JAVA VERSION USED BY SYSTEM

#JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"

######################################################################

##### Checking JAVA Version

if [ -z "$JAVA_HOME" ]; then
    echo -e "${RED}Please set your JAVA_HOME before running the script${NC}"
    exit 1
fi;

javaVersion=$($JAVA_HOME/bin/java -version 2>&1 | awk -F '"' '/version/ {print $2}' | grep -e "11\.[0-9]*.*")
if [ -z "$javaVersion" ]; then
  echo -e "${RED}Invalid java version, needed 11${NC}"
  exit 1
fi

# Try to get the jar of GOC automatically
jarPath=$(ls -l | grep -oh  -e  "[^ ]*-jar-with-dependencies\.jar")

if [ -z "$jarPath" ]; then
  echo -e "${ORANGE}Could not detect the JAR of GOC, please provide the path manually${NC}"
  read -r jarPath
else
  absJarPath=$(realpath "$jarPath")
  echo -e "${GREEN}A GOC JAR has been detected at: (${jarPath}) want you to use it?${NC}"
  printf "hit enter for yes, enter an other path to change it: "
  read -r newJarPath
  if [ ! -z "$newJarPath" ]; then
    isAbsPath=$(echo "${newJarPath}" | grep -c "^/.*")
    if [ "$isAbsPath" -eq 0 ]; then
        echo -e "${RED}[ ${newJarPath} ] is not an absolute path${NC}"
        exit 1
    fi
    absJarPath=$newJarPath
  fi
fi


#Try to use maven wrapper
if [ -f ./classes/maven-wrapper/mvnw  ];then
#  base_dir=$(realpath ./classes/maven-wrapper)
  mavenPath=$(realpath "./classes/maven-wrapper/mvnw")
  testMaven=$($mavenPath -v)
  if [ ! -z "$testMaven" ]; then
    echo -e "${GREEN}Using maven wrapper with version 5.3.4${NC}"
  else
    mavenPath=""
  fi;
else
  mavenPath=""
fi;

if [ -z "$mavenPath" ];then
  #Try to get maven path automatically
  echo -e "${ORANGE}Could not find Maven Wrapper, trying to detect local installation automatically...${NC}"
  mavenPath=$(mvn -v | sed -n 2p | grep -oh -e "/.*")
  mvnVersion=$(echo "$mavenPath" |grep -oh -e "3\.5\..*")
  if [ -z "$mavenPath" ] || [ -z "$mvnVersion" ];then
    echo -e "${ORANGE}Could not detect the required maven installation [5.3.x], please enter the absolute to the binary file of maven: ${NC}"
    read -r mavenPath
    testMaven2=$($mavenPath -v)
    if [ -z "$testMaven2" ];then
      echo -e "${RED}Wrong maven executable...${NC}"
      exit 1
    fi;
    mvnVersion=$($mavenPath -v | sed -n 2p | grep -oh -e "3\.5\..*")
    if [ -z "$mvnVersion" ];then
      echo -e "${RED}Wrong maven version, please provide a [3.5.x] version...${NC}"
      exit 1
    fi
  else
    echo -e "${GREEN}A valid maven version detect ${mvnVersion}, do you want to use it?${NC}"
    printf "Press enter for yes, enter absolute path for maven binary to change it: "
    read -r newMaven
    if [ -z newMaven ]; then
      mavenPath="${mavenPath}/bin/mvn"
    else
      if [ -f "$newMaven" ];then
        testMaven3=$($newMaven -v)
        if [ ! -z "$testMaven3"]; then
          mavenVersion2=$($newMaven -v | sed -n 2p | grep -oh -e "3\.5\..*")
          if [ ! -z "$mavenVersion2" ]; then
            mavenPath="$newMaven"
          else
            echo "${RED}Wrong maven version, needed [3.5.x], try to re-install GOC, and use the wrapper${NC}"
            exit 1
          fi;
        else
          echo "${RED}Wrong maven path${NC}"
        fi;
      else
        echo "${RED}File Not found!${NC}"
      fi;
    fi;
  fi
fi;


echo -e "${GREEN}Using maven at: ${mavenPath}${NC}"


printf "Please enter a path to your BBR (Absolute Path) : "
read -r bbrPath
isAbsPath=$(echo "${bbrPath}" | grep ^/.* | wc -l)
if [ "$isAbsPath" -eq 0 ]; then
    echo -e "${RED}[ ${bbrPath} ] is not an absolute path${NC}"
    exit 1
fi
if [ ! -f "$bbrPath" ]; then
    echo -e "${RED}[ ${bbrPath} ] is not a file or doesn't exist !${NC}"
    exit 1
fi

printf "Please choose the kind of validator you want to generate: \n"
printf "\t 1- $MU3\n"
printf "\t 2- $USCDI\n"
printf "\t 3- $CCDA_R_1\n"
printf "your choice: "
read -r choice

if [ "$choice" -gt 3 -o "$choice" -lt 1 ]; then
  echo "${RED}Wrong choice, please repeat and choose between 1 and 3${NC}"
  exit 1
fi

versionLabel=""
case $choice in

  1)
    echo -e "${GREEN}generating ${MU3// /}${NC}"
    validator=${MU3// /}
    versionLabel="2.1"
    ;;
  2)
    echo -e "${GREEN}generation ${USCDI// /}${NC}"
     validator=${USCDI// /}
     versionLabel="2.1"
    ;;
  3)
    echo -e "${GREEN}generation ${CCDA_R_1// /}${NC}"
    validator=${CCDA_R_1// /}
    versionLabel="1.1"
    ;;
esac

printf "Please enter workspace path (where you want to generate the validator): "
read -r workspaceDir
if [ ! -d "$workspaceDir" ]; then
  echo "${RED}The provided workspace is not a valid directory"
  exit 1
fi

isAbsPath=$(echo "${workspaceDir}" | grep -c "^/.*")
if [ "$isAbsPath" -eq 0 ]; then
    echo -e "${RED}[ ${workspaceDir} ] is not an absolute path"
    exit 1
fi

_currDate=$(date +%Y-%m-%d-%H-%M-%S)
printf "Please enter the folder name of your validator: (output-${validator}-${_currDate})?: "
read -r folderName
if [ -z "$folderName" ]; then
  folderName="output-${validator}-${_currDate}"
fi;

echo -e "${GREEN}your validator will be generated in: ${workspaceDir}/${folderName}${NC}"

######## GENERATION COMMAND ########
$JAVA_HOME/bin/java -jar "$absJarPath" -bbr "$bbrPath" -out "${workspaceDir}/${folderName}" -mvn "$mavenPath" -serviceName "$validator" -versionLabel "$versionLabel" -ignoreTemplateIdRequirements -ignoreCdaBasicRequirements
####################################

if [ $? != 0 ]; then
  echo "${RED}Generation Failed${NC}"
  exit 1
fi

echo -e "${GREEN}Validator generated successfully${NC}"

echo -e "${NC}Start SVS Adaptations..."

if [ ! -f ./SEQUOIASVSAdaptation.sh ]; then
  echo -e "${RED}Could not find the SEQUOIASVSAdaptation script, please recompile GOC and rerun this script"
  exit 1;
fi;

# adapt only if version is higher than 1.1
if [ "$versionLabel" != "1.1" ]; then
  pathToResources=''

  if [ ! -d ./classes/sequoia-db-hack ]; then
    echo -e "${RED}Resources for adaptations not found, please provide a path manually"
    read -r $pathToResources
  else
    pathToResources=$(realpath ./classes/sequoia-db-hack)
  fi

  if [ ! -x ./SEQUOIASVSAdaptation.sh ]; then
    chmod +x SEQUOIASVSAdaptation.sh
  fi;

  env JAVA_HOME=$JAVA_HOME M2_BIN=$mavenPath ./SEQUOIASVSAdaptation.sh "$workspaceDir/$folderName" $pathToResources

  if [ $? != 0 ]; then
    echo "${RED}Adaptations failed"
    exit 1
  fi
fi;

#Zip validator
 #move to bin path
appassemblerPath=$(realpath $(find $workspaceDir/$folderName -type d -wholename */appassembler))
cd $appassemblerPath

 #Create bbr folder
mkdir "bbr" || echo "File already exist, that won't affect next steps"
cp $bbrPath bbr

 #create documentation folder
documentationPath=$(realpath $(find $workspaceDir/$folderName -type d -wholename "*/documentation"))
cp -r $documentationPath  $appassemblerPath

 #Zipping
zip -r "validator-$_currDate.zip" bbr/ documentation/ repo/ bin/