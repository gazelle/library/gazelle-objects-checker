#!/bin/bash

####################################
# DGSANTE Generation  Script
# Created by Achraf Achkari - 09 Nov 2021
# Last revision - 21 Feb 2022
# Version 1.1.0
#####################################


#Colors definitions for terminal
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'


## FUNCTIONS DEFINITION

function readBBR {
    read -e -r bbrPath
    isAbsPath=`echo "${bbrPath}" | grep ^/.* | wc -l`
    if [ $isAbsPath -eq 0 ]; then
        echo -e "${RED}[ ${bbrPath} ] is not an absolute path${NC}"
        exit 1
    fi
    if [ ! -f $bbrPath ]; then
        echo -e "${RED}[ ${bbrPath} ] is not a file or doesn't exist !${NC}"
        exit 1
    fi
}

function bbr_transform {
  echo $1
  bbrOutput=`echo "$bbrPath" | sed --expression="s/.xml/-$1.xml/g"`
  echo $bbrOutput
  packagerPath=`realpath ..`
  packagerFolder=${packagerPath##*/}
  if [ $packagerFolder == "hl7templates-packager-jar" ]; then
    $mavenPath -f "$packagerPath/pom.xml" compile org.codehaus.mojo:exec-maven-plugin:3.0.0:java -Dexec.mainClass="net.ihe.gazelle.lib.hl7templatespackager.adapters.scripts.EHDSITransformers" -Dvalidator="$1" -DbbrPath="$bbrPath" -DbbrOutput="$bbrOutput"
    if [ $? == 0 ]; then
      bbrPath=$bbrOutput
      echo -e "${GREEN}BBR Transformed to $1 at: ${bbrOutput}${NC}"
    else
      echo -e "${ORANGE}Transformation to $1 BBR Failed! Please provide $1 manually, or contact support${NC}"
      readBBR
    fi
  else
    echo -e "${ORANGE}Could not found 'packager module' at $packagerPath, cannot perform transformation, please provide $1 BBR manually${NC}"
    readBBR
  fi
}

#UNCOMENT THIS TO OVERRIDE DEFAULT JAVA VERSION USED BY SYSTEM

#JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"

######################################################################

##### Checking JAVA Version

if [ -z "$JAVA_HOME" ]; then
    echo -e "${RED}Please set your JAVA_HOME before running the script${NC}"
    exit 1
fi;

javaVersion=$($JAVA_HOME/bin/java -version 2>&1 | awk -F '"' '/version/ {print $2}' | grep -e "11\.[0-9]*.*")
if [ -z "$javaVersion" ]; then
  echo -e "${RED}Invalid java version, needed 11${NC}"
  exit 1
fi

## JAR DETECTION

# Try to get the jar of GOC automatically
jarPath=`ls -l | grep -oh  -e  "[^ ]*-jar-with-dependencies\.jar"`

if [ -z $jarPath ]; then
  echo -e "${ORANGE}Could not detect the JAR of GOC, please provide the path manually${NC}"
  read -e -r jarPath
else
  absJarPath=`realpath $jarPath`
  echo -e "${GREEN}A GOC JAR has been detected at: (${jarPath}) want you to use it?${NC}"
  printf "hit enter for yes, provide an other path to change it: "
  read -e -r newJarPath
  if [ ! -z $newJarPath ]; then
    isAbsPath=`echo "${newJarPath}" | grep ^/.* | wc -l`
    if [ $isAbsPath -eq 0 ]; then
        echo -e "${RED}[ ${newJarPath} ] is not an absolute path${NC}"
        exit 1
    fi
    absJarPath=$newJarPath
  fi
fi

## MAVEN VERIFICATION

#Try to use maven wrapper
if [ -f ./classes/maven-wrapper/mvnw  ];then
  mavenPath=$(realpath "./classes/maven-wrapper/mvnw")
  testMaven=$($mavenPath -v)
  if [ ! -z "$testMaven" ]; then
    echo -e "${GREEN}Using maven wrapper with version 5.3.4${NC}"
  else
    mavenPath=""
  fi;
else
  mavenPath=""
fi;

if [ -z "$mavenPath" ];then
  #Try to get maven path automatically
  echo -e "${ORANGE}Could not find Maven Wrapper, trying to detect local installation automatically...${NC}"
  mavenPath=$(mvn -v | sed -n 2p | grep -oh -e "/.*")
  mvnVersion=$(echo "$mavenPath" |grep -oh -e "3\.5\..*")
  if [ -z "$mavenPath" ] || [ -z "$mvnVersion" ];then
    echo -e "${ORANGE}Could not detect the required maven installation [5.3.x], please enter the absolute to the binary file of maven: ${NC}"
    read -e -r mavenPath
    testMaven2=$($mavenPath -v)
    if [ -z "$testMaven2" ];then
      echo -e "${RED}Wrong maven executable...${NC}"
      exit 1
    fi;
    mvnVersion=$($mavenPath -v | sed -n 2p | grep -oh -e "3\.5\..*")
    if [ -z "$mvnVersion" ];then
      echo -e "${RED}Wrong maven version, please provide a [3.5.x] version...${NC}"
      exit 1
    fi
  else
    echo -e "${GREEN}A valid maven version detect ${mvnVersion}, do you want to use it?${NC}"
    printf "Press enter for yes, enter absolute path for maven binary to change it: "
    read -e -r newMaven
    if [ -z newMaven ]; then
      mavenPath="${mavenPath}/bin/mvn"
    else
      if [ -f "$newMaven" ];then
        testMaven3=$($newMaven -v)
        if [ ! -z "$testMaven3"]; then
          mavenVersion2=$($newMaven -v | sed -n 2p | grep -oh -e "3\.5\..*")
          if [ ! -z "$mavenVersion2" ]; then
            mavenPath="$newMaven"
          else
            echo "${RED}Wrong maven version, needed [3.5.x], try to re-install GOC, and use the wrapper${NC}"
            exit 1
          fi;
        else
          echo "${RED}Wrong maven path${NC}"
        fi;
      else
        echo "${RED}File Not found!${NC}"
      fi;
    fi;
  fi
fi;


echo -e "${GREEN}Using maven at: ${mavenPath}${NC}"

## BBR READING

printf "Please enter a path to your BBR (Absolute Path) : "
readBBR

printf "Please choose the kind of validator you want to generate: \n"
printf "\t 1- Pivot (L3)\n"
printf "\t 2- Scanned (L1)\n"
printf "\t 3- Friendly\n"
printf "your choice: "
read -r choice

if [ $choice -gt 3 -o $choice -lt 1 ]; then
  echo "${RED}Wrong choice, please repeat and choose between 1 and 3${NC}"
  exit 1
fi

case $choice in

  1)
    echo -e "${GREEN}generating pivot${NC}"
    validator="pivot"
    ;;
  2)
    echo -e "${GREEN}generation scanned${NC}"
     validator="scanned"
    ;;
  3)
    echo -e "${GREEN}generation friendly${NC}"
    validator="friendly"
    echo -e "Friendly BBR Will be used for generation"
    ;;
esac

bbr_transform $validator

printf "Please enter workspace path (where you want to generate the validator): "
read -e -r workspaceDir
if [ ! -d $workspaceDir ]; then
  echo "${RED}The provided workspace is not a valid directory"
  exit 0
fi

isAbsPath=`echo "${workspaceDir}" | grep ^/.* | wc -l`
if [ $isAbsPath -eq 0 ]; then
    echo -e "${RED}[ ${workspaceDir} ] is not an absolute path"
    exit 0
fi

_currDate=`date +%Y-%m-%d-%H-%M-%S`
printf "Please enter the folder name of your validator: (output-${validator}-${_currDate})?"
read -r folderName
if [ -z $folderName ]; then
  folderName="output-${validator}-${_currDate}"
fi;

echo -e "${GREEN}your validator will be generated in: ${workspaceDir}/${folderName}${NC}"

######## GENERATION COMMAND ########
$JAVA_HOME/bin/java -jar $jarPath -bbr $bbrPath -out "${workspaceDir}/${folderName}" -mvn $mavenPath -serviceName $validator -HL7TEMP_CDACONFFOLDERNAME cdaepsos
####################################

#Replacing XSD
pathToResources=""
if [ -d ./classes/eHDSI-resources/xsd ]; then
  pathToResources=$(realpath "./classes/eHDSI-resources/xsd")
  echo -e "${GREEN}XSD Resources found at $pathToResources${NC}"
else
  echo -e "${ORANGE}Could not find resources, please provide the path to the XSD folder manually${NC}"
  echo -e "${ORANGE}be careful, the directory should contain correct XSD files for the validator to run. This is not checked by the script!${NC}"
  read -e -r pathToResources
  if [ ! -d $pathToResources ]; then
    echo -e "${RED} invalid path to directory"
    exit 1;
  fi;
fi;

pathToXSD=$(realpath $(find "${workspaceDir}/${folderName}" -type d -name "xsd" | grep -e ".*-app/.*"))
echo "$pathToXSD"
XSDDirname=$(dirname "$pathToXSD")
echo "$XSDDirname"
rm -r "$pathToXSD"
cp -r "$pathToResources" "$XSDDirname"


#Zip validator
 #move to app path
appPath=$(realpath $(find $workspaceDir/$folderName -type d -wholename *validator-app))
cd $appPath

 #Create bbr folder
mkdir "bbr" || echo "File already exist, that won't affect next steps"
cp $bbrPath bbr

 #create documentation folder
documentationPath=$(realpath $(find $workspaceDir/$folderName -type d -wholename "*/documentation"))
cp -r $documentationPath  $appPath

 #Zipping
zip -r "validator-$_currDate.zip" bbr/ documentation/ repo/ bin/
