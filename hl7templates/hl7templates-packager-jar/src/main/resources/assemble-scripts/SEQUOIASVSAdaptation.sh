#!/bin/bash

#Colors definitions for terminal
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m'

USAGE_STRING="${RED}./sequoiaHack.sh <path_to_validator> [<path_to_resources>]${NC}"
pathToValidator=""
pathToResources=""

## Check if M2_HOME and JAVA_HOEM are set
if [ -z $JAVA_HOME ] || [ -z $M2_BIN ]; then
  echo -e "${RED}JAVA_HOME or/and M2_BIN are not set properly${NC}"
  exit 1
fi;

# Check number & types of parameters
if [ $# -gt 2 ] || [ $# -lt 1 ]; then
  echo -e "${RED} Wrong number of parameters, expected 1 or 2${NC}"
  echo -e $USAGE_STRING
  exit 1
fi;

if [ ! -d $1 ]; then
  echo -e "${RED} The provided path: $1 is not a valid directory${NC}"
  echo -e $USAGE_STRING
  exit 1
else
  pathToValidator=$1
fi

if [ $# -eq 2 ]; then
    if [ ! -d $2 ]; then
      echo -e "${RED} The provided path: $2 for resources is not a valid path${NC}"
      echo -e $USAGE_STRING
      exit 1
    else
      pathToResources=$2
    fi;
else
  pathToResources='../src/main/resources/sequoia-db-hack'
  if [ ! -d $pathToResources ]; then
    echo -e "${RED} Could not find the resources automatically, please re-run the script and provide the path manually${NC}"
    exit 1
  fi;
fi;


# Start working (replacing)
numberOfFiles=$(find $pathToResources -regex  '.*\(java\|xml\)$' -type f | wc -l)

if [ "$numberOfFiles" -ne 7 ]; then
  echo -e "${ORANGE}The number of found files is ${numberOfFiles}, expected 7! Probably something went wrong!${NC}"
  echo -e "${ORANGE}Adaptation is not suspended${NC}"
fi;

# Creating missing directory
utilFile=$(dirname $(find "$pathToValidator" -type f -name "XMLValidation.java" ))
echo $utilFile
cd "$utilFile"
mkdir ../db

# Replacing & Adding java files and Pom.xml
for x in $(find $pathToResources  -regex  '.*\(java\|xml\)$' -type f); do
  fileName=$(basename "$x")
  fileToBeReplaced=$(find "$pathToValidator" -name "$fileName")
  echo "file to be replaced: $fileToBeReplaced"
  if [ -z "$fileToBeReplaced" ]; then
    echo -e "${NC}Need to be created"
    resourceDirName=$(dirname "$x")
    resourceBaseName=$(basename "$resourceDirName")
    outputDir=$(find "$pathToValidator" -type d -name "$resourceBaseName" | grep -v  ".*\/target.*")
    echo "$outputDir"
    cp "$x" "$outputDir"
  else
    echo -e "${NC}Need to be replaced"
    targetDirName=$(dirname "$fileToBeReplaced")
    cp "$x" "$targetDirName"
  fi;

done;

# Package validator
pathToValidatorJar=$(find $pathToValidator -type d -name "*-validator-jar" | grep -v ".*\/net\/.*")
cd $pathToValidatorJar
env JAVA_HOME=$JAVA_HOME $M2_BIN clean package

# Replacing XSD

pathToXSD=$(find "$pathToValidator" -type d -name "xsd" | grep -e ".*\/target\/appassembler.*")
echo "$pathToXSD"
XSDDirname=$(dirname "$pathToXSD")
echo "$XSDDirname"
rm -r "$pathToXSD"
cp -r "$pathToResources"/xsd "$XSDDirname"

