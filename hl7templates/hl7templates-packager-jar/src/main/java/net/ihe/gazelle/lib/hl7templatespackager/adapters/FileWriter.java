package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import java.io.IOException;
import java.net.URL;

public interface FileWriter {

    void writeContentInFile(URL resourceURL, String fileOutputPath) throws IOException;
}
