package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class EnvironementException extends Exception {

    public EnvironementException() {
    }

    public EnvironementException(String message) {
        super(message);
    }

    public EnvironementException(String message, Throwable cause) {
        super(message, cause);
    }
}
