package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;
import net.ihe.gazelle.lib.hl7templatespackager.application.ValidatorConfiguration;
import net.ihe.gazelle.lib.hl7templatespackager.application.ValueSetExtractor;
import net.ihe.gazelle.lib.hl7templatespackager.application.Workspace;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ValueSetExtractionException;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.PathBBRResource;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * This class extracts the value-sets from a ValidatorConfiguration into a target generated validator project.
 * @author wbars
 */
public class ValueSetExtractorImpl implements ValueSetExtractor {

    private String workspaceRoot;

    /**
     * Base constructor for the class
     */
    private ValueSetExtractorImpl(){
    }

    /**
     * Public constructor for the class
     * @param workspaceRoot : root of the workspace where value-sets will be extracted to.
     */
    public ValueSetExtractorImpl(String workspaceRoot){
        setWorkspaceRoot(workspaceRoot);
    }

    /**
     * Getter for workspaceRoot property.
     * @return workspace root
     */
    public String getWorkspaceRoot() {
        if (workspaceRoot != null){
            return workspaceRoot;
        }
        return "";
    }

    /**
     * Setter for workspaceRoot property.
     * @param workspaceRoot : value to set.
     */
    public void setWorkspaceRoot(String workspaceRoot) {
        if (workspaceRoot != null){
            this.workspaceRoot = workspaceRoot;
        } else {
            this.workspaceRoot = "";
        }
    }

    /**
     * Extract Value-Sets from ValidatorConfiguration as resource files in the generated validator.
     * @param workspace : workspace where the value-sets will be extracted will be created.
     * @param validatorConfiguration : ValidatorConfiguration used for the generation. Contains value-sets that will be extracted.
     * @throws ValueSetExtractionException when Value-sets cannot be correctly extracted from ValidatorConfiguration.
     */
    public void extractValueSets(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValueSetExtractionException{
        BBRResource bbrResource = validatorConfiguration.getBbrResource();
        Decor decor = DecorMarshaller.loadDecor(bbrResource);
        File outputFile = new File(getWorkspaceRoot() + workspace.getName() + File.separator +
                validatorConfiguration.getProjectNaming().getProjectName() + "-validator-jar/src/main/resources/valresources/valueSets/");
        try {
            net.ihe.gazelle.tempgen.valueset.action.ValueSetExtractor.flattenAndExtractValueSetsFromDecor(decor, outputFile);
        } catch (IOException e) {
            throw new ValueSetExtractionException("Cannot extract value-sets from validatorConfiguration !", e);
        }
    }
}
