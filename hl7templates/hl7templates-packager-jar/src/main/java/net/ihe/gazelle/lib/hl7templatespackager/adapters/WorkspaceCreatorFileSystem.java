package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import net.ihe.gazelle.lib.hl7templatespackager.application.WorkspaceCreator;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.WorkspaceCreationException;
import net.ihe.gazelle.lib.hl7templatespackager.application.Workspace;

import java.io.IOException;

/**
 * This class is responsible for creating the workspace in file system.
 * @author wbars
 */
public class WorkspaceCreatorFileSystem implements WorkspaceCreator {

    private String workspacesRoot;
    private FolderCreator folderCreator;
    private FileWriter fileWriter;

    /**
     * Base constructor for the class
     */
    private WorkspaceCreatorFileSystem(){
    }

    /**
     * Creates an instance of the class with a defined root path to create workspaces.
     * @param workspacesRoot : root path where workspaces will be created.
     * @param folderCreator : to create folders
     * @param fileWriter : to write resources content in files
     */
    public WorkspaceCreatorFileSystem(String workspacesRoot, FolderCreator folderCreator, FileWriter fileWriter){
        setWorkspacesRoot(workspacesRoot);
        this.folderCreator = folderCreator;
        this.fileWriter = fileWriter;
    }

    /**
     * Getter for the workspaceRoot property.
     */
    private String getWorkspacesRoot() {
        if (this.workspacesRoot != null){
            return this.workspacesRoot;
        }
        return "";
    }

    /**
     * Setter for the workspaceRoot property
     * @param workspacesRoot : the path we want to use as root.
     */
    private void setWorkspacesRoot(String workspacesRoot) {
        if (workspacesRoot != null){
            this.workspacesRoot = workspacesRoot;
        } else {
            this.workspacesRoot = "";
        }
    }

    /**
     * Reset the workspace based on his name.
     * @param workspace : the workspace to be reset.
     */
    @Override
    public void resetWorkspace(Workspace workspace) throws WorkspaceCreationException {
        String workspacePath = getWorkspacesRoot() + workspace.getName();
        try {
            folderCreator.resetFolder(workspacePath);
            WorkspaceResourceCreator workspaceResourceCreator = new WorkspaceResourceCreator(folderCreator, fileWriter);
            workspaceResourceCreator.retrieveNeededResources(workspacePath);

        } catch (IOException e) {
            throw new WorkspaceCreationException("Cannot create workspace for validator generation !", e);
        }
    }
}
