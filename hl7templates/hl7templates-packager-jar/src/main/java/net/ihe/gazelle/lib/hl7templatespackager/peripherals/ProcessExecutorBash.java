package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.ProcessExecutor;
import net.ihe.gazelle.lib.hl7templatespackager.application.GOCLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * This class allows to execute a process from a command line.
 * @author wbars
 */
public class ProcessExecutorBash implements ProcessExecutor {

    private GOCLogger gocLogger;

    private Logger LOG = LoggerFactory.getLogger(ProcessExecutorBash.class);

    /**
     * Base constructor for the class
     */
    private ProcessExecutorBash(){
    }

    /**
     * Create an instance of the class with a logger instance.
     * @param gocLogger : logger used by the class.
     */
    public ProcessExecutorBash(GOCLogger gocLogger){
        this.gocLogger = gocLogger;
    }

    /**
     * Execute a given command line.
     * @param command : command line to execute.
     */
    @Override
    public void executeProcess(String command) throws IOException {
        Process pr = Runtime.getRuntime().exec(new String[]{"bash","-c", command});
        gocLogger.logExecutionOutput(pr.getInputStream(), pr.getErrorStream(), command);
        try {
            int result = pr.waitFor();
            if (result != 0){
                throw new IOException("Cannot execute process with code :" + result);
            }
        } catch (InterruptedException e) {
            LOG.error("Couldn't execute command {}",e);
        }
    }

    @Override
    public List<String> executeProcessAndReturn(String command) throws IOException {
        Process pr = Runtime.getRuntime().exec(new String[]{"bash","-c", command});
        LOG.info("executing command: {}",command);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        try {
            int result = pr.waitFor();
            if (result != 0){
                throw new IOException("Cannot execute process with code :" + result);
            }
        } catch (InterruptedException e) {
            LOG.error("Couldn't execute command {}",e);
            return null;
        }
        List<String> output = new ArrayList<>();
        String line = null ;
        while((line = bufferedReader.readLine()) != null){
            output.add(line);
        }
        LOG.info("executed command: {}",command);
        return output;
    }
}
