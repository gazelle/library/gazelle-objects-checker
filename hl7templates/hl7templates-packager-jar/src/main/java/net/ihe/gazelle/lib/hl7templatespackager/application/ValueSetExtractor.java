package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ValueSetExtractionException;

public interface ValueSetExtractor {

    void extractValueSets(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValueSetExtractionException;
}
