package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ModelGenerationException;

public interface ModelGenerator {

    void generateNewModel(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ModelGenerationException;
}
