package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.FolderCreator;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;

/**
 * This class allows to create folders in the file system.
 * @author wbars
 */
public class FolderCreatorImpl implements FolderCreator {

    private static Logger logger = LoggerFactory.getLogger(FolderCreatorImpl.class);

    /**
     * Base constructor for the class.
     */
    public FolderCreatorImpl(){
    }

    /**
     * Reset the folder at path given in parameter. This means if it exist it will delete it and create it again. If it does not already
     * exists, it will simply create it.
     * @param folderToResetPath : path to the folder to reset.
     * @throws IOException throw IOException if the reset failed
     */
    public void resetFolder(String folderToResetPath) throws IOException {
        File folderToReset = new File(folderToResetPath);
        if (folderToReset.isFile()) {
            throw new FileAlreadyExistsException(folderToResetPath + " is a file !");
        }
        if (folderToReset.exists()) {
            FileUtils.forceDelete(folderToReset);
        }
        if (folderToReset.mkdirs()) {
            logger.info("Folder successfully created.");
        } else {
            logger.error("Error creating folder !");
            throw new IOException("Folder " + folderToReset.getAbsolutePath() + " has not been created !");
        }
    }

    /**
     * Create a folder at given path. Does nothing if folder already exists.
     * @param folderToCreatePath : path to the folder to create.
     * @throws IOException throws and IOException if the creation failed
     */
    public void createFolder(String folderToCreatePath) throws  IOException {
        File folderToCreate = new File(folderToCreatePath);
        if (folderToCreate.exists()){
            logger.info("Folder " + folderToCreate.getAbsolutePath() + " already exists.");
        } else if (!folderToCreate.mkdirs()){
            throw new IOException("Cannot create folder " + folderToCreate.getAbsolutePath());
        }
    }
}
