package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class ValidatorGenerationException extends Exception{

    public ValidatorGenerationException(String message) {
        super(message);
    }

    public ValidatorGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}