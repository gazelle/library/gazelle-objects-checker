package net.ihe.gazelle.lib.hl7templatespackager.application;


import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;

/**
 * This class holds the information about the ValidatorConfiguration used for the generation as well as all options for the generation.
 *
 * @author wbars
 */
public class ValidatorConfiguration {

    private BBRResource bbrResource;
    private String templateVersionFilter;
    private Boolean ignoreTemplateIdRequirements;
    private Boolean ignoreCdaBasicRequirements;
    private String serviceName;
    private ProjectNaming projectNaming;

    @Deprecated
    private String bbrPath;

    /**
     * Base constructor for the class.
     */
    private ValidatorConfiguration() {
        //Emtpy.
    }

    /**
     * Create an instance of ValidatorConfiguration with all configurations as argument.
     *
     * @param bbrResource                      : path to th BBR that will be used for the generator
     * @param templateVersionFilter        : filter on the version of templates available in the BBR
     * @param ignoreTemplateIdRequirements : if true, no rules will be generated on templateID elements in the validator
     * @param ignoreCdaBasicRequirements   : if true, no rules will be generated on CDA Basic requirements in the validator
     * @param serviceName                  : service name that will be used in validation reports
     */
    public ValidatorConfiguration(BBRResource bbrResource, String templateVersionFilter, Boolean ignoreTemplateIdRequirements,
                                  Boolean ignoreCdaBasicRequirements, String serviceName) {
        this.bbrResource = bbrResource;
        setTemplateVersionFilter(templateVersionFilter);
        setIgnoreTemplateIdRequirements(ignoreTemplateIdRequirements);
        setIgnoreCdaBasicRequirements(ignoreCdaBasicRequirements);
        setServiceName(serviceName);
        projectNaming = new ProjectNaming(bbrResource);
    }

    public BBRResource getBbrResource() {
        return bbrResource;
    }

    /**
     * Getter for the bbrPath property.
     *
     * @return Value of the bbrPath property in {@link java.lang.String}
     */
    public String getBbrPath() {
        if (this.bbrPath != null) {
            return this.bbrPath;
        }
        return "";
    }

    /**
     * Setter for the bbrPath property directly from the input string.
     *
     * @param bbrPath value to set to the property.
     */
    public void setBbrPath(String bbrPath) {
        if (bbrPath != null) {
            this.bbrPath = bbrPath;
        } else {
            this.bbrPath = "";
        }
    }


    /**
     * Getter for the bbrPath property.
     *
     * @return Value of the bbrPath property in {@link java.lang.String}
     */
    public String getTemplateVersionFilter() {
        return this.templateVersionFilter;
    }

    /**
     * Setter for the templateVersionFilter property directly from the input string.
     *
     * @param templateVersionFilter value to set to the property.
     */
    public void setTemplateVersionFilter(String templateVersionFilter) {
        this.templateVersionFilter = templateVersionFilter;
    }

    /**
     * Getter for the ignoreTemplateIdRequirements property. Return false if null.
     *
     * @return Value of the ignoreTemplateIdRequirements property in {@link java.lang.Boolean}
     */
    public Boolean ignoreTemplateIdRequirements() {
        if (ignoreTemplateIdRequirements != null) {
            return ignoreTemplateIdRequirements;
        }
        return false;
    }

    /**
     * Setter for the ignoreTemplateIdRequirements property directly from the input string.
     *
     * @param ignoreTemplateIdRequirements value to set to the property.
     */
    public void setIgnoreTemplateIdRequirements(Boolean ignoreTemplateIdRequirements) {
        this.ignoreTemplateIdRequirements = ignoreTemplateIdRequirements;
    }

    /**
     * Getter for the ignoreCdaBasicRequirements property. Return false if null.
     *
     * @return Value of the ignoreTemplateIdRequirements property in {@link java.lang.Boolean}
     */
    public Boolean ignoreCdaBasicRequirements() {
        if (ignoreCdaBasicRequirements != null) {
            return ignoreCdaBasicRequirements;
        }
        return false;
    }

    /**
     * Setter for the ignoreTemplateIdRequirements property directly from the input string.
     *
     * @param ignoreCdaBasicRequirements value to set to the property.
     */
    public void setIgnoreCdaBasicRequirements(Boolean ignoreCdaBasicRequirements) {
        this.ignoreCdaBasicRequirements = ignoreCdaBasicRequirements;
    }


    /**
     * Getter for the bbrPath property.
     *
     * @return Value of the serviceName property in {@link java.lang.String}
     */
    public String getServiceName() {
        if (this.serviceName != null) {
            return serviceName;
        }
        return getProjectNaming().getProjectName();
    }

    /**
     * Setter for the serviceName property directly from the input string.
     *
     * @param serviceName service name
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Getter for the bbrPath property.
     *
     * @return Value of the ProjectNaming property in {@link ProjectNaming}
     */
    public ProjectNaming getProjectNaming() {
        return projectNaming;
    }

    /**
     * Setter for the projectNaming property directly from the input string.
     *
     * @param projectNaming project naming
     */
    public void setProjectNaming(ProjectNaming projectNaming) {
        this.projectNaming = projectNaming;
    }
}
