package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.WorkspaceCreationException;

public interface WorkspaceCreator {

    void resetWorkspace(Workspace workspace) throws WorkspaceCreationException;
}
