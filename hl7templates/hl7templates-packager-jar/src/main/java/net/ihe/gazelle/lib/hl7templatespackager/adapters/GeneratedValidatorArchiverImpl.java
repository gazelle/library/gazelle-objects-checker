package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import net.ihe.gazelle.lib.hl7templatespackager.application.ValidatorConfiguration;
import net.ihe.gazelle.lib.hl7templatespackager.application.GeneratedValidatorArchiver;
import net.ihe.gazelle.lib.hl7templatespackager.application.Workspace;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ValidatorArchiveException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * This class generates an archive from a generated validator.
 * @author wbars
 */
public class GeneratedValidatorArchiverImpl implements GeneratedValidatorArchiver {

    private String workspacesRoot;

    /**
     * Base constructor for the class
     */
    private GeneratedValidatorArchiverImpl(){
    }

    /**
     * Public constructor for the class
     * @param workspacesRoot workspace root
     */
    public GeneratedValidatorArchiverImpl(String workspacesRoot){
        setWorkspacesRoot(workspacesRoot);
    }

    /**
     * Getter for the workspacesRoot property.
     */
    private String getWorkspacesRoot() {
        if (this.workspacesRoot != null){
            return this.workspacesRoot;
        }
        return "";
    }

    /**
     * Setter for the workspacesRoot property
     * @param workspacesRoot : the path we want to use as root.
     */
    private void setWorkspacesRoot(String workspacesRoot) {
        if (workspacesRoot != null){
            this.workspacesRoot = workspacesRoot;
        } else {
            this.workspacesRoot = "";
        }
    }

    /**
     * Copy the generated validator and create a zip archive of it.
     * @param workspace : workspace where the generated validator can be found.
     * @param validatorConfiguration : holds the name of the project where the generated validator can be found.
     * @throws ValidatorArchiveException when the validator archive cannot be created. It can be thrown if the folder
     * cannot be copied to target destination or when the folder cannot be zipped.
     */
    public void generateArchiveFromGeneratedValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorArchiveException {
        try {
            String workspacePath = getWorkspacesRoot() + workspace.getName();
            String assemblerPath = workspacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + "-validator-jar/target/appassembler";
            FileUtils.copyDirectory(new File(assemblerPath), new File(workspacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + "-validator-app"));
            ZipUtility.zipDirectory(new File(workspacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + "-validator-app"),
                    new File(workspacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + "-validator-app.zip"));
        } catch (IOException e){
            throw new ValidatorArchiveException("Error creating an archive for the generated validator !", e);
        }
    }
}
