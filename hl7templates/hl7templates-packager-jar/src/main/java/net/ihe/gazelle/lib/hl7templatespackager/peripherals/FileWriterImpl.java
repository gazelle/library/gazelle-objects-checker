package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.FileWriter;

import java.io.*;
import java.net.URL;

public class FileWriterImpl implements FileWriter {

    /**
     * Write the content of a resource from an URL to a specific file.
     * @param resourceURL : URL of the resource to write in the file.
     * @param fileOutputPath : path of the file where the resource will be written.
     * @throws IOException throws IOException if writing failed
     */
    public void writeContentInFile(URL resourceURL, String fileOutputPath) throws IOException{
        try (InputStream resourceInputStream = resourceURL.openStream()) {
            if (resourceInputStream == null) {
                throw new IOException("Didn't found resource : " + resourceURL);
            }
            byte[] buffer = new byte[resourceInputStream.available()];
            File fileOutput = new File(fileOutputPath);
            try (OutputStream outStream = new FileOutputStream(fileOutput)){
                while (true) {
                    final int length = resourceInputStream.read(buffer);
                    if (length <= 0) {
                        break;
                    }
                    outStream.write(buffer, 0, length);
                }
            }
        }
    }
}
