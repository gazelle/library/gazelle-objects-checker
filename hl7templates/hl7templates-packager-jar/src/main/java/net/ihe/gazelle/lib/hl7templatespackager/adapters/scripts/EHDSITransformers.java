package net.ihe.gazelle.lib.hl7templatespackager.adapters.scripts;

import net.ihe.gazelle.tempgen.ehdsi.scripts.EHDSIFriendlyTransformer;
import net.ihe.gazelle.tempgen.ehdsi.scripts.EHDSIL1Transformer;
import net.ihe.gazelle.tempgen.ehdsi.scripts.EHDSIL3Transformer;

import javax.xml.bind.JAXBException;
import java.io.IOException;


public class EHDSITransformers {

    public static void main(String[] args) throws JAXBException, IOException {
        //Set required system properties for log4j to avoid errors (not used)
        System.setProperty("workspaceRoot", "/tmp");
        System.setProperty("generationDate", "tmp");
        EHDSITransformers e = new EHDSITransformers();
        e.transformBBRToFriendly();
    }

    public void transformBBRToFriendly() throws IOException, JAXBException {
        String bbrPath = System.getProperty("bbrPath");
        String bbrOutput = System.getProperty("bbrOutput");
        String validator = System.getProperty("validator");

        if (validator == null || "".equals(validator)) {
            throw new IllegalArgumentException("Validator name not set");
        }
        if (bbrPath == null || "".equals(bbrPath) || bbrOutput == null || "".equals(bbrOutput)) {
            throw new IOException("BBR File not set");
        }
        System.out.println("Using BBR: " + bbrPath);
        if (!bbrPath.endsWith(".xml")) {
            throw new IOException("Invalid XML File");
        }
        String[] args = new String[]{bbrPath, bbrOutput};
        switch (validator) {
            case "pivot":
                EHDSIL3Transformer.main(args);
                break;
            case "scanned":
                EHDSIL1Transformer.main(args);
                break;
            case "friendly":
                EHDSIL3Transformer.main(args);
                EHDSIFriendlyTransformer.main(args);
                break;
        }
    }
}
