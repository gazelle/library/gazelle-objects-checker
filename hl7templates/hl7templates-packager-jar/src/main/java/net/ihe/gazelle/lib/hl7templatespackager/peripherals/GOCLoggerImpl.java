package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.application.GOCLogger;
import net.ihe.gazelle.tempgen.action.FileReadWrite;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.validation.Notification;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used to log information on processes or to delimit logs with messages.
 * @author wbars
 */
//TODO CHANGE THE LOGGER TO ISSUE A REPORT [GOC-162]
public class GOCLoggerImpl implements GOCLogger {

    private org.slf4j.Logger log = LoggerFactory.getLogger(GOCLoggerImpl.class);

    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private String previousAction = "";

    private long startTime = 0;

    private long totalElapsedTime = 0;

    private String workspaceRoot;

    private String generationDate;

    private String GOCDetailedLogPath;

    private String GOCCmdLogPath;

    private PrintStream logStream;

    private enum Descriptor {
        STDOUT,
        STDERR
    }

    /**
     * Base constructor for the class
     */
    GOCLoggerImpl(){
        startLogging();
    }

    /**
     * Constructor with project workspace initialization
     * @param workspaceRoot the project root folder
     * @param generationDate formatted date of the generation
     */
    public GOCLoggerImpl(String workspaceRoot, String generationDate){
        this.workspaceRoot = workspaceRoot;
        this.generationDate = generationDate;
        this.GOCDetailedLogPath = workspaceRoot +"logs/log-"+generationDate+"/detailed_report.log" ;
        this.GOCCmdLogPath = workspaceRoot+"logs/log-"+generationDate+"/cmdOutput.log";

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(workspaceRoot +"logs/log-"+generationDate+"/generation_report.log",true);
            this.logStream = new PrintStream(fileOutputStream);
        } catch (FileNotFoundException e) {
            log.error("Failed to create a stream with synthetic log file, you can check all logs in: {}",GOCDetailedLogPath);
        }
        startLogging();
    }

    /**
     * Init log with current datetime
     */
    private void startLogging() {
        if(logStream != null){
            logStream.println("================ Generation Started at: [ "+formatter.format(new Date())+" ] ================\n\n");
            logStream.println("** This is a synthetic log file, for more detail please check " + GOCDetailedLogPath);
        }
    }


    /**
     * Log the output of the process
     * @param inputStream : the input stream to be logged
     * @param errorStream : the error stream to be logged
     */
    @Override
    public void logExecutionOutput(InputStream inputStream, InputStream errorStream, String command) {
        logInputStream(inputStream, command, Descriptor.STDOUT);
        logInputStream(errorStream, command,Descriptor.STDERR);
    }

    /**
     * Log a separator which indicated the end of the execution and validator generation, and the total elapsed time
     */
    @Override
    public void logEndOfExecution() {
        if(logStream != null){
            printClosingSeparator();
            long milliseconds = (this.totalElapsedTime/1000000)%1000;
            long seconds = (this.totalElapsedTime/(1000000 * 1000))%60;
            long minutes = (this.totalElapsedTime/(1000000L * 1000 *60));
            String stringToLog = "The generation took [ "+minutes+"m "+seconds+"s "+milliseconds+"ms ] to complete";
            logStream.println("\n"+stringToLog+"\n");
            log.info(stringToLog);
            printSeparatorHeader("End of validator generation");
        }
    }

    /**
     * Log the start and end of a task based on an action and message
     * @param action : message printed in the headers for closing and opening
     * @param message : description of the current action to display in the separation
     */
    @Override
    public void logSeparator(String action, String message) {
        if(logStream != null){
            //Close previous action log if exist
            if(!this.previousAction.equals("")){
                printClosingSeparator();
            }

            printSeparatorHeader(action+" has started");
            logStream.println("Description: "+message);
            logStream.println("\n** For more logs please check "+GOCDetailedLogPath+"\n");

            this.previousAction = action;
            this.startTime = System.nanoTime();
        }
    }


    private void printSeparatorHeader(String action){
        if(logStream != null){
            //Log to detailed log file
            log.info("///////////////////////////  "+action+"  ///////////////////////////\n\n");

            //Log to General log
            logStream.print("\n///////////////////////////  "+action+ "  ///////////////////////////\n\n");
        }

    }

    private void printClosingSeparator(){
        if(logStream != null){
            long elapsedTime = System.nanoTime() - startTime;
            long milliseconds = (elapsedTime/1000000)%1000;
            long seconds = (elapsedTime/(1000000*1000))%60;
            long minutes = elapsedTime/(1000000000L *60);
            String stringToLog = this.previousAction+" took [ '"+minutes+"m "+seconds+"s "+milliseconds+"ms' ] to complete";

            //log to detailed log file
            log.info(stringToLog+"\n");

            //log to generale log
            logStream.println("\n"+stringToLog);

            printSeparatorHeader(this.previousAction+ " has ended");
            this.totalElapsedTime += elapsedTime;
        }
    }


    private void logInputStream(InputStream inputStream, String command, Descriptor descriptor){
        String line;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            File file = new File(GOCCmdLogPath);
            if(file.createNewFile()){
                log.info(GOCCmdLogPath + " has been created");
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file,true));
            bufferedWriter.write("==========================\n");
            if(descriptor.equals(Descriptor.STDOUT)){
                bufferedWriter.write("STDOUT of executing: "+command+"\n\n");
            }
            else{
                bufferedWriter.write("STDERR of executing: "+command+"\n\n");
            }
            while ((line = in.readLine()) != null) {
                line += System.lineSeparator();
                bufferedWriter.write(line);
            }
            in.close();
            bufferedWriter.close();
        }
        catch (Exception e){
            log.error("Error in logging a command output",e);
        }
    }

    public void logProblems() {
        int i = 0;
        StringBuilder lo_errorReport = new StringBuilder(
                "Here are listed all inconsistency between the BBR and the CDA model used by the validator:\n\n-------------\n");
        for (Notification not : ProblemHandler.getDiagnostic()) {
            if (not instanceof net.ihe.gazelle.validation.Error) {

                lo_errorReport.append("Type        : ").append(not.getClass().getSimpleName()).append("\n");
                lo_errorReport.append("Process     : ").append(not.getTest()).append("\n");
                lo_errorReport.append("Location    : ").append(not.getLocation()).append("\n");
                lo_errorReport.append("Description : ").append(not.getDescription()).append("\n");
                lo_errorReport.append("-------------\n");
                i++;
            }
        }

        if(logStream != null){
            logStream.println("\n----------------- \n number of unconformity to CDA model : "+i+" \n-----------------\n");
            logStream.println("For more detailed log about the unconformities please check 'packager/bin/logs/cdaModelErrors.txt' file");
        }

        lo_errorReport.append("Number of inconsistency with CDA model :").append(i).append("\n");
        lo_errorReport.append("---------");
        createModelErrorLogs(lo_errorReport.toString());
    }

    private void createModelErrorLogs(String reportContent){

        String ls_reportPath = workspaceRoot + "logs/log-"+ generationDate + "/cdaModelErrors.log";

        try {
            FileReadWrite.printDoc(reportContent, ls_reportPath);
        } catch (IOException e){
            log.error("Could not create report on inconsistency with CDA model", e);
        }
    }

    //Not used in the actual cases
    private void createModelErrorLogs(String folderOutput, String reportContent){

        File file = new File(folderOutput);
        String ls_reportPath = folderOutput + File.separator + "cdaModelErrors.txt";

        String checkoutScriptPath = file.getAbsolutePath() + File.separator + "checkout.sh";
        try {
            FileReadWrite.printDoc(reportContent, ls_reportPath);
        } catch (IOException e){
            log.error("Could not create report on inconsistency with CDA model", e);
        }
    }

}
