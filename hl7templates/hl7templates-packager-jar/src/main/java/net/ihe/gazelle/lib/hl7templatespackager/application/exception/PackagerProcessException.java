package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class PackagerProcessException extends Exception{

    public PackagerProcessException(String message) {
        super(message);
    }

    public PackagerProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}
