package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class ValidatorArchiveException extends Exception{

    public ValidatorArchiveException(String message) {
        super(message);
    }

    public ValidatorArchiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
