package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import net.ihe.gazelle.lib.hl7templatespackager.application.*;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ModelGenerationException;
import net.ihe.gazelle.tempgen.action.GenerationProperties;
import net.ihe.gazelle.tempgen.action.HL7TemplatesConverter;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.PathBBRResource;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * This class allows to generate UML models from a given ValidatorConfiguration
 * @author wbars
 */
public class UMLModelGenerator implements ModelGenerator {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(UMLModelGenerator.class);
    private static final String VALIDATOR_JAR_SUFFIX = "-validator-jar";


    private String workspacesRoot;
    private GOCLogger gocLogger;

    /**
     * Base constructor for the class
     */
    private UMLModelGenerator(){
    }

    /**
     * Creates an instance of the class with a defined root path to workspaces.
     * @param workspacesRoot : root path where workspaces will be created.
     * @param gocLogger : The instance of the logger
     */
    public UMLModelGenerator(String workspacesRoot, GOCLogger gocLogger){
        setWorkspacesRoot(workspacesRoot);
        this.gocLogger = gocLogger;
    }


    /**
     * Getter for the workspaceRoot property.
     * @return the workspace root
     */
    public String getWorkspacesRoot() {
        if (this.workspacesRoot != null){
            return this.workspacesRoot;
        }
        return "";
    }

    /**
     * Setter for the workspaceRoot property
     * @param workspacesRoot : the path we want to use as root.
     */
    public void setWorkspacesRoot(String workspacesRoot) {
        if (workspacesRoot != null){
            this.workspacesRoot = workspacesRoot;
        } else {
            this.workspacesRoot = "";
        }
    }

    /**
     * Generates a new UML model from a given ValidatorConfiguration and save it as a file in the workspace
     * @param workspace : workspace where the file will be generated.
     * @param validatorConfiguration : ValidatorConfiguration used for generating the model. It holds all option used for generation.
     * @throws ModelGenerationException when the UML model cannot be generated. It can be caused by the ValidatorConfiguration conversion failing or because the method
     * fails to write the result in target file.
     */
    public void generateNewModel(Workspace workspace, ValidatorConfiguration validatorConfiguration)
            throws ModelGenerationException {
        HL7TemplatesConverter templatesConverter = new HL7TemplatesConverter();
        String workpacePath = getWorkspacesRoot() + workspace.getName();
        GenerationProperties.ignoreAssertionGeneration = false;
        File outUmlModelPath = new File( workpacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName()
                + VALIDATOR_JAR_SUFFIX + File.separator + "model" + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + ".uml");
        try {
            BBRResource bbrResource = validatorConfiguration.getBbrResource();
            Boolean ignoreTemplateIdRequirements = validatorConfiguration.ignoreTemplateIdRequirements();
            String tempalteVersionFilter = validatorConfiguration.getTemplateVersionFilter();
            String umlModelForBBR;
            umlModelForBBR = templatesConverter.convertArtDecorXMI(bbrResource, ignoreTemplateIdRequirements, tempalteVersionFilter);
            log.info("ValidatorConfiguration converted to UML.");
            FileUtils.writeStringToFile(outUmlModelPath, umlModelForBBR, "UTF-8");
            log.warn("Generated UML Model is saved at: "+outUmlModelPath);
            gocLogger.logProblems();
        } catch (JAXBException e){
            throw  new ModelGenerationException("Cannot convert ValidatorConfiguration to UML model !", e);
        } catch (IOException e){
            throw new ModelGenerationException("Cannot write model in target file ! (" + outUmlModelPath.getAbsolutePath() + ")", e);
        } catch (ClassCastException e){
            throw new ModelGenerationException("BBRResource not defined",e);
        }
    }
}
