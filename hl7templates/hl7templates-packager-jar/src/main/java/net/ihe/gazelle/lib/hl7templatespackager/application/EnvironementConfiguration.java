package net.ihe.gazelle.lib.hl7templatespackager.application;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.EnvironementException;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.HL7Templates2GOC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class EnvironementConfiguration {

    private static Map<String, String> systemVariablesMap = new THashMap<>();

    private static Logger log = LoggerFactory.getLogger(EnvironementConfiguration.class);


    private EnvironementConfiguration() throws EnvironementException {}

    private static void setSystemVariable(String key, String value) throws EnvironementException {
        if(key != null && value != null){
            System.setProperty(key,value);
            log.info("{} set to {}",key,System.getProperty(value));
        }
        else{
            throw new EnvironementException("null key or value are not allowed: ["+key+","+value+"]");
        }
    }

    public static void setSystemVariables(String ...args) throws EnvironementException {
        if(args.length == 0 || args.length%2 != 0){
            throw new EnvironementException("Problem in envorinement configuration: invalid number of paramaters");
        }
        for(int i=0;i<args.length-1;i++){
            systemVariablesMap.put(args[i],args[i+1]);
        }
    }

    public static boolean addSystemVariable(String key, String value){
        if(key != null && value != null){
            systemVariablesMap.put(key,value);
            return true;
        }
        return false;
    }

    public static void configure() throws EnvironementException {
        for(Map.Entry<String, String> variable:systemVariablesMap.entrySet()){
            setSystemVariable(variable.getKey(),variable.getValue());
        }
    }


}
