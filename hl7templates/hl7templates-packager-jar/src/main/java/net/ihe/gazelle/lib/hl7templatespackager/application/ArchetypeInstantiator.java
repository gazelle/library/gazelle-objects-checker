package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ArchetypeInstantiationException;

public interface ArchetypeInstantiator {
    void createValidatorArtifact(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ArchetypeInstantiationException;
}
