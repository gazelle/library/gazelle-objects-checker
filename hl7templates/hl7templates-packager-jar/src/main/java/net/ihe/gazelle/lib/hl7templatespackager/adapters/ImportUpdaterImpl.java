package net.ihe.gazelle.lib.hl7templatespackager.adapters;

//import net.ihe.gazelle.framework.hl7templatespackager.application.ProjectNaming;
import net.ihe.gazelle.lib.hl7templatespackager.application.ValidatorConfiguration;
import net.ihe.gazelle.lib.hl7templatespackager.application.ImportUpdater;
import net.ihe.gazelle.lib.hl7templatespackager.application.Workspace;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * This class updates the imports of generated classes to add imports of models classes.
 * @author wbars
 */
//TODO This class SHALL be deleted. This is not something we want to keep as it is just a workaround for something the plugin does not handle yet [GOC-160]
public class ImportUpdaterImpl implements ImportUpdater {

    private static Logger log = LoggerFactory.getLogger(ImportUpdaterImpl.class);
    private static final String PACKAGE_PATH = "-validator-jar/src/main/java/net/ihe/gazelle/";

    private String workspacesRoot;

    /**
     * Base constructor for the class
     */
    private ImportUpdaterImpl(){
    }

    public ImportUpdaterImpl(String workspacesRoot){
        setWorkspacesRoot(workspacesRoot);
    }

    /**
     * Getter for the workspacesRoot property.
     */
    private String getWorkspacesRoot() {
        if (this.workspacesRoot != null){
            return this.workspacesRoot;
        }
        return "";
    }

    /**
     * Setter for the workspacesRoot property
     * @param workspacesRoot : the path we want to use as root.
     */
    private void setWorkspacesRoot(String workspacesRoot) {
        if (workspacesRoot != null){
            this.workspacesRoot = workspacesRoot;
        } else {
            this.workspacesRoot = "";
        }
    }

    /**
     * Update imports in generated validator classes to import model classes based on which configuration is used.
     * @param workspace : workspace where the java code will be generated.
     * @param validatorConfiguration : holds the name of the project that will be used to generate the java code.
     * @throws IOException Throws IOException if updating failed
     */
    @Override
    public void updateImportsForValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws IOException {

        String workspacePath = getWorkspacesRoot() + workspace.getName();
        String propertiesFilePath = workspacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + "-validator-jar/src/main/resources/" +
                System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") + ".properties";
        File projectFile = new File(workspacePath + File.separator + validatorConfiguration.getProjectNaming().getProjectName() + PACKAGE_PATH +
                validatorConfiguration.getProjectNaming().getProjectName());
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesFilePath));
        String imports = properties.getProperty("imports");
        if (projectFile.exists()) {
            String[] listJavaImports = imports.split(",");
            StringBuilder repl = new StringBuilder();
            repl.append("import java.util.List;\n");
            for (String javaImport : listJavaImports) {
                repl.append("import ");
                repl.append(javaImport);
                repl.append(";\n");
            }
            replaceIntoFolder(projectFile.getAbsolutePath(), "import java.util.List;", repl.toString());
        }
    }

    /**
     * Replace a String in each file of a folder. Will also browse all children folder recursively.
     * @param toBeReplaced : string that must be replaced in files.
     * @param replacement : string to replace with.
     * @param projectPath : path to the project were strings should be replaced.
     * @throws IOException
     */
    private void replaceIntoFolder(String projectPath, String toBeReplaced, String replacement) throws IOException {
        File projectFile = new File(projectPath);
        log.info(projectFile.getAbsolutePath());
        File[] fileList = projectFile.listFiles();
        if (fileList != null){
            for (File file : fileList) {
                if (file.isDirectory()) {
                    replaceIntoFolder(file.getAbsolutePath(), toBeReplaced, replacement);
                } else {
                    replaceIntoFile(file, toBeReplaced, replacement);
                }
            }
        } else {
            log.warn("No file found in " + projectPath);
        }
    }

    /**
     * Replace all occurrences of a string pattern in the content a designated file.
     * @param file : file in which strings must be replaced.
     * @param toBeReplaced : string that must be replaced in files.
     * @param replacement : string to replace with.
     * @throws IOException
     */
    private void replaceIntoFile(File file, String toBeReplaced, String replacement) throws IOException {
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        content = content.replace(toBeReplaced, replacement);
        FileUtils.writeStringToFile(file, content, "UTF-8");
    }
}
