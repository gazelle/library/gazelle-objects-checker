package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.PathBBRResource;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class HL7Templates2GOC implements GOCEntry {


    static final String DOCUMENTATION_STRING = "\nHL7Templates2GOC -out outputFolder -bbr buildBlockRepositoryURL";

    @Option(name = "-out", metaVar = "outputFile", usage = "Output folder", required = true)
    private String outputFile;

    @Option(name = "-bbr", metaVar = "bbr", usage = "Output folder", required = true)
    private String bbr;

    @Option(name = "-serviceName", metaVar = "serviceName", usage = "Validation Service Name", required = false)
    private String serviceName;

    @Option(name = "-mvn", metaVar = "mvn", usage = "maven executable path", required = true)
    private String mvnExecPath = "mvn";

    @Option(name = "-ignoreTemplateIdRequirements", usage = "ignore TemplateId Requirements when generating the constraints (useful for C-CDA " +
            "validation tools)", required = false)
    private Boolean ignoreTemplateIdRequirements = false;

    @Option(name = "-ignoreCdaBasicRequirements", usage = "ignore CDA Basic Requirements when generating the constraints (useful for C-CDA " +
            "validation tools)", required = false)
    private Boolean ignoreCdaBasicRequirements = false;

    @Option(name = "-versionLabel", metaVar = "versionLabel", usage = "versionLabel that will be used during the generation", required = false)
    private String versionLabel;

    @Option(name = "-HL7TEMP_CDACONFFOLDERNAME", metaVar = "hl7temp_cdaconffoldername", usage = "hl7temp cda conf folder name", required = false)
    private String HL7TEMP_CDACONFFOLDERNAME = "cdabasic";

    @Deprecated
    @Option(name = "-rootClassName", metaVar = "rootClassName", usage = "Root Class Name", required = false)
    private String rootClassName;

    @Option(name = "-notoverride", usage = "Not override the modelDefinition.xml file", required = false)
    @Deprecated
    private Boolean notOverride = false;

    @Option(name = "-prop", metaVar = "properties", usage = "Path to customized java imports file", required = false)
    @Deprecated
    private String properties;

    @Deprecated
    @Option(name = "-in", metaVar = "inputFile", usage = "Input folder", required = false)
    private String inputFile;

    public String getVersionLabel() {
        return versionLabel;
    }

    public void setVersionLabel(String versionLabel) {
        this.versionLabel = versionLabel;
    }

    public Boolean getIgnoreTemplateIdRequirements() {
        return ignoreTemplateIdRequirements;
    }

    public void setIgnoreTemplateIdRequirements(Boolean ignoreTemplateIdRequirements) {
        this.ignoreTemplateIdRequirements = ignoreTemplateIdRequirements;
    }

    /**
     * Getter for the ignoreCdaBasicRequirements property.
     *
     * @return the value of the property.
     */
    public Boolean getIgnoreCdaBasicRequirements() {
        return ignoreCdaBasicRequirements;
    }

    /**
     * Setter for the ignoreCdaBasicRequirements property.
     *
     * @param ignoreCdaBasicRequirements value to set to the property.
     */
    public void setIgnoreCdaBasicRequirements(Boolean ignoreCdaBasicRequirements) {
        this.ignoreCdaBasicRequirements = ignoreCdaBasicRequirements;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMvnExecPath() {
        return mvnExecPath;
    }

    public void setMvnExecPath(String mvnExecPath) {
        this.mvnExecPath = mvnExecPath;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public String getBbr() {
        return bbr;
    }

    public void setBbr(String bbr) {
        this.bbr = bbr;
    }

    public String getHL7TEMP_CDACONFFOLDERNAME() {
        return HL7TEMP_CDACONFFOLDERNAME;
    }

    public void setHL7TEMP_CDACONFFOLDERNAME(String HL7TEMP_CDACONFFOLDERNAME) {
        this.HL7TEMP_CDACONFFOLDERNAME = HL7TEMP_CDACONFFOLDERNAME;
    }


    @Deprecated
    public String getInputFile() {
        if (inputFile == null) {
            inputFile = getOutputFile() + "/archetypes/";
        }
        return inputFile;
    }

    @Deprecated
    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    @Deprecated
    public String getProperties() {
        if (properties == null) {
            properties = getOutputFile() + "/archetypes/conf.properties";
        }
        return properties;
    }

    @Deprecated
    public void setProperties(String properties) {
        this.properties = properties;
    }

    @Deprecated
    public Boolean getNotOverride() {
        return notOverride;
    }

    @Deprecated
    public void setNotOverride(Boolean notOverride) {
        this.notOverride = notOverride;
    }

    /**
     * @return the rootClassName
     */
    @Deprecated
    public String getRootClassName() {
        return rootClassName;
    }

    /**
     * @param rootClassName the rootClassName to set
     */
    @Deprecated
    public void setRootClassName(String rootClassName) {
        this.rootClassName = rootClassName;
    }



    @Override
    public void runGOC(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);

            GOCExecutor gocExecutor = new GOCExecutor(outputFile);
            gocExecutor.execute(new PathBBRResource(bbr),mvnExecPath,versionLabel,
                    ignoreTemplateIdRequirements,ignoreCdaBasicRequirements, serviceName, HL7TEMP_CDACONFFOLDERNAME);

        } catch (CmdLineException e) {
            System.err.println("Exception in the execution of command line");
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {
        HL7Templates2GOC hl7Templates2GOC = new HL7Templates2GOC();
        hl7Templates2GOC.runGOC(args);
    }

}
