package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.ProcessExecutor;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.EnvironementException;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnvironementCheckerImpl implements EnvironementChecker {

    private static final String REG_JAVAVERSION = "([0-9]+)\\.?([0-9]+)";
    private static final Pattern PATT_JAVAVERSION = Pattern.compile(REG_JAVAVERSION);

    public void checkMVNPath(String mvn, ProcessExecutor processExecutor) throws EnvironementException {
        String command = mvn + " -v";
        try {
            processExecutor.executeProcess(command);
        } catch (IOException e) {
            throw new EnvironementException("Failed to find maven execution",e);
        }
    }

    @Override
    public void checkJavaVersion() throws EnvironementException {
        String javaVersion = System.getProperty("java.version");
        Matcher matcher = PATT_JAVAVERSION.matcher(javaVersion);
        if(matcher.find()){
            if(matcher.group(1) != null && matcher.group(2) != null){
                if(!matcher.group(1).equals("11")){
                    throw new EnvironementException("Bad Java version, Gazelle ObjectChecker requires Java 11 to run, your version: "+javaVersion);
                }
            }
            else{
                throw new EnvironementException("Failed to retrieve Java version: Format invalid");
            }
        }
        else {
            throw new EnvironementException("Failed to retrieve Java version: Match not found");
        }
    }


}
