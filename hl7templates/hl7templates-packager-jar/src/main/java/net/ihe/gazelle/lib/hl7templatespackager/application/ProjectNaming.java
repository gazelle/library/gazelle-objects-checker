package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.PathBBRResource;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.StreamBBRResource;
import net.ihe.gazelle.tempgen.flatten.action.ProjectFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

import java.net.URL;

/**
 * This class is responsible for the extraction of the name from the ValidatorConfiguration.
 * It will then be able to generate alternative names used for example in artifact generation from the archetype (upper case, capitalized, etc).
 * @author wbars
 */
public class ProjectNaming {

    private String projectName;

    /**
     * Base constructor for the class
     */
    ProjectNaming(){
    }

    /**
     * This constructor takes as input the path to the ValidatorConfiguration used for the validator generation.
     * It will extract a name from it, which will be used to generate the corresponding validator.
     * @param bbrPath : path to the ValidatorConfiguration used for the generation
     */
    ProjectNaming(BBRResource bbrResource)  {
        this.projectName = extractProjectName(bbrResource);
    }


    /**
     * Getter for the project name held by the instance.
     * @return Value of the projectName property in {@link java.lang.String}
     */
    public String getProjectName(){
        if(this.projectName != null){
            return this.projectName;
        }
        return "";
    }

    /**
     * Get the project name transformed in upper case.
     * @return Value of the transformed projectName property in {@link java.lang.String}
     */
    public String getProjectNameUpperCase(){
        return projectName.toUpperCase();
    }

    /**
     * Get the project name transformed with first letter capitalized.
     * @return Value of the transformed projectName property in {@link java.lang.String}
     */
    public String getProjectNameCapitalized(){
        return projectName.substring(0, 1).toUpperCase() + projectName.substring(1);
    }

    /**
     * Setter for the projectName property directly from the input string.
     * @param projectName
     */
    void setProjectName(String projectName){
        if (projectName != null){
            this.projectName = projectName;
        } else {
            this.projectName = "";
        }
    }

    /**
     * Setter for the projectName property. The value is extracted from the ValidatorConfiguration from which the path is given as parameter.
     * @param bbrPath path to the BRR from which the project name will be extracted
     */
    void setProjectNameFromBBR(String bbrPath){
        this.projectName = extractProjectName(bbrPath);
    }

    /**
     * Extract the name of the project from the ValidatorConfiguration found at path given in parameter.
     * @param bbrPath path to the BRR from which the project name will be extracted
     */
    private String extractProjectName(Decor decor) {
        String projectName = ProjectFlattenProc.flattenPrefix(decor.getProject().getPrefix());
        if (projectName.endsWith("-")) {
            projectName = projectName.substring(0, projectName.length()-1);
        }
        return projectName;
    }

    private String extractProjectName(String bbrPath){
        Decor decor = DecorMarshaller.loadDecor(bbrPath);
        return extractProjectName(decor);
    }

    private String extractProjectName(BBRResource bbrResource) {
        Decor decor = DecorMarshaller.loadDecor(bbrResource);
        return extractProjectName(decor);
    }


}
