package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ValidatorGenerationException;

public interface ValidatorGenerator {

    void generateProjectValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorGenerationException;
    void generateProjectCDATemplatesTree( Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorGenerationException;
    void extractDocumentation(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorGenerationException;
    void assembleProjectValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorGenerationException;
    void updateUtilityScripts(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorGenerationException;
}
