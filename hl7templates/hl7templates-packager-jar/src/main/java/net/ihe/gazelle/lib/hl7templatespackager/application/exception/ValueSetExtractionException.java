package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class ValueSetExtractionException  extends Exception{

    public ValueSetExtractionException(String message) {
        super(message);
    }

    public ValueSetExtractionException(String message, Throwable cause) {
        super(message, cause);
    }
}
