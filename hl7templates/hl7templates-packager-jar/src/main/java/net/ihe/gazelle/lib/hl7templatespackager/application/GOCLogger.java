package net.ihe.gazelle.lib.hl7templatespackager.application;

import java.io.InputStream;

public interface GOCLogger {

    void logExecutionOutput(InputStream inputStream, InputStream errorStream, String command);

    void logEndOfExecution();

    void logSeparator(String action, String message);

    void logProblems();
}
