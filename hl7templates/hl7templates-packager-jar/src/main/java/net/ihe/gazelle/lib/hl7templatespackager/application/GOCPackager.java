package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.*;

import java.io.IOException;

/**
 * This class will generate a validator given a workspace path and the ValidatorConfiguration file and options.
 * @author wbars
 */
public class GOCPackager {

	private GOCLogger gocLogger;
	private WorkspaceCreator workspaceCreator;
	private ArchetypeInstantiator archetypeInstantiator;
	private ModelGenerator modelGenerator;
	private ValidatorGenerator validatorGenerator;
	private GeneratedValidatorArchiver generatedValidatorArchiver;
	private ImportUpdater importUpdater;

	/**
	 * Base constructor for the class. Private as we always want to have a logger in our instance.
	 */
	private GOCPackager() {
	}

	/**
	 * Create an instance of GOCPackager with all neede dependency injected.
	 * @param gocLogger : logger that will be used during the packaging process.
	 * @param workspaceCreator : creates the workspace for the generation
	 * @param archetypeInstantiator : generate artifacts from model and validator archetypes
	 * @param modelGenerator : generates UML model from ValidatorConfiguration and option
	 * @param validatorGenerator : generates the validator from generated UML model
	 * @param generatedValidatorArchiver : packages generated validator in archive
	 * @param importUpdater : this shall be deleted
	 */
	public GOCPackager(GOCLogger gocLogger, WorkspaceCreator workspaceCreator, ArchetypeInstantiator archetypeInstantiator, ModelGenerator modelGenerator,
					   ValidatorGenerator validatorGenerator, GeneratedValidatorArchiver generatedValidatorArchiver, ImportUpdater importUpdater) {
		this.gocLogger = gocLogger;
		this.workspaceCreator = workspaceCreator;
		this.archetypeInstantiator = archetypeInstantiator;
		this.modelGenerator = modelGenerator;
		this.validatorGenerator = validatorGenerator;
		this.generatedValidatorArchiver = generatedValidatorArchiver;
		this.importUpdater = importUpdater;
	}

	/**
	 * Entry point of the whole packaging process to create a validator
	 * @param workspace : workspace for the generation.
	 * @param validatorConfiguration : ValidatorConfiguration used for the generation.
	 * @throws PackagerProcessException throws a PackagerProcessException if one of main operations failed
	 */
	public void packageBBRToGOCValidationTool(Workspace workspace, ValidatorConfiguration validatorConfiguration)
			throws PackagerProcessException {
		initWorkspace(workspace);
		createNeededArtifacts(workspace, validatorConfiguration);
		generateModelForValidator(workspace, validatorConfiguration);
		generateValidator(workspace, validatorConfiguration);
		packageGeneratedValidator(workspace, validatorConfiguration);
		gocLogger.logEndOfExecution();
	}

	private void initWorkspace(Workspace workspace) throws PackagerProcessException{
		gocLogger.logSeparator("Creating folder","Creates output folder with all resources /!\\ It will be deleted if it already exists");
		try {
			workspaceCreator.resetWorkspace(workspace);
		} catch (WorkspaceCreationException e){
			throw new PackagerProcessException("Cannot generate validator : Workspace cannot be created !", e);
		}
	}

	private void createNeededArtifacts(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws PackagerProcessException{
		try {
			gocLogger.logSeparator("Generating Artifact","Generate an artifact from the validator archetype");
			archetypeInstantiator.createValidatorArtifact(workspace, validatorConfiguration);
		} catch (ArchetypeInstantiationException e){
			throw new PackagerProcessException("Cannot generate validator : artifacts cannot be generated !", e);
		}
	}

	private void generateModelForValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration)
			throws PackagerProcessException{
		gocLogger.logSeparator("Generating UML models","Generates the UML model for the validatorConfiguration");
		try {
			modelGenerator.generateNewModel(workspace, validatorConfiguration);
		} catch (ModelGenerationException e) {
			throw new PackagerProcessException("Cannot generate validator : Model cannot be created from ValidatorConfiguration", e);
		}
	}

	private void generateValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws PackagerProcessException{
		try {
			gocLogger.logSeparator("update utility scripts","chmod +x on autoUpdateImports.sh & generateAndUpdate.sh");
			validatorGenerator.updateUtilityScripts(workspace,validatorConfiguration);
			gocLogger.logSeparator("Generating validator","Execute Maven plugin to generate the validator exec:exec@generateValidator");
			validatorGenerator.generateProjectValidator(workspace, validatorConfiguration);
			gocLogger.logSeparator("Generating CDA Template","Execute Maven plugin to generate the CDA Template mbgen:gencdatree");
			validatorGenerator.generateProjectCDATemplatesTree(workspace, validatorConfiguration);
		} catch (ValidatorGenerationException e){
			throw new PackagerProcessException("Cannot generate validator : Error generating the validator !", e);
		}
		try {
			gocLogger.logSeparator("Packaging","Maven package the jar");
			validatorGenerator.assembleProjectValidator (workspace, validatorConfiguration);
			gocLogger.logSeparator("Extracting Documentation","Execute Maven plugin to extract Documentation mbgen:genxmldoc");
			validatorGenerator.extractDocumentation(workspace, validatorConfiguration);
		} catch (ValidatorGenerationException e){
			throw new PackagerProcessException("Cannot generate validator : Error generating the validator !", e);
		}
	}

	private void packageGeneratedValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws PackagerProcessException{
		try {
			// Copy appassembler and zip it
			gocLogger.logSeparator("Compressing Assemble","Copy appassembler and zip it");
			generatedValidatorArchiver.generateArchiveFromGeneratedValidator(workspace, validatorConfiguration);
		} catch (ValidatorArchiveException e){
			throw new PackagerProcessException("Cannot archive the generated validator !", e);
		}
	}
}
