package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import java.io.IOException;

public interface FolderCreator {

    void resetFolder(String folderToResetPath) throws IOException;

    void createFolder(String folderToCreatePath) throws  IOException;
}
