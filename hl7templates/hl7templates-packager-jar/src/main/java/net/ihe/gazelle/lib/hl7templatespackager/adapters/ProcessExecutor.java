package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import java.io.IOException;
import java.util.List;

public interface ProcessExecutor {

    void executeProcess(String command) throws IOException;

    List<String> executeProcessAndReturn(String command) throws IOException;
}
