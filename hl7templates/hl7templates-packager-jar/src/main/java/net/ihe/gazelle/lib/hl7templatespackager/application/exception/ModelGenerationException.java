package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class ModelGenerationException  extends Exception{

    private ModelGenerationException(){
        super();
    }

    public ModelGenerationException(String message) {
        super(message);
    }

    public ModelGenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}
