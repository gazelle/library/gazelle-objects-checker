package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempgen.action.FileReadWrite;

/**
 * This class is responsible for updating workspaces in ModelsDefinition retrieved as resource.
 * It will replace the default workspace found in the document with the actual path of the resource in the workspace.
 * @author wbars
 */
class UpdateModelsDefinition {

	private static Logger log = LoggerFactory.getLogger(UpdateModelsDefinition.class);

	/**
	 * Base constructor for the class
	 */
	UpdateModelsDefinition() {
	}

	/**
	 * Update the path to the models defined in ModelsDefinition with the actual path in the workspace.
	 * The ModelsDefinition is retrieved from system properties and the workspace path to use as a replacement is in parameter.
	 * @param workspacePath : path to the workspace directory to use in ModelsDefinition.
	 * @throws IOException
	 */
	void updateModelDefinitions(String workspacePath) throws IOException{
		String pathResources = System.getProperty("MODELS_DESCRIBER",System.getProperty("HL7TEMP_RESOURCES_PATH", "../../hl7templates-resources") +
				File.separator +
				System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") +
				"/modelsDefinition.xml");
		try {
			String content = FileReadWrite.readDoc(pathResources);
			String folderOutputformatted = workspacePath.endsWith("/")?workspacePath.substring(0, workspacePath.length()-1):workspacePath;
			String replaceBy = updateWorkspacePath(content, folderOutputformatted);
			FileReadWrite.printDoc(replaceBy, pathResources);
		} catch (IOException e) {
			log.error("Problem with modelsDefinition.xml ! ", e);
			throw e;
		}
	}

	/**
	 * Replace the path to the models defined in ModelsDefinition with the actual path in the workspace in the text content of the definition.
	 * @param modelsDefinitionContent : text content of the ModelsDefinition.
	 * @param workspacePath : path to the workspace directory.
	 */
	String updateWorkspacePath(String modelsDefinitionContent, String workspacePath) throws IOException{
		if (modelsDefinitionContent != null) {
			String regex = "<path>(.*?)</path>";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(modelsDefinitionContent);
			String tobeReplaced = "";
			if(matcher.find()) {
				String stringUMLPath = matcher.group(1);
				tobeReplaced = extractWorkspace(stringUMLPath);
			}
			return modelsDefinitionContent.replace(tobeReplaced, workspacePath);
		}
		return null;
	}

	/**
	 * Extract workspace path to replace from models absolute path.
	 * @param modelPath : path to the model as defined in the ModelsDefinition.
	 */

	String extractWorkspace(String modelPath) throws IOException{
		String[] paths = modelPath.split("/");
		for (String path : paths) {
			if (path.contains("-model")) {
				return modelPath.substring(0, modelPath.indexOf(path)-1);
			}
		}
		throw new IOException("ModelDefinition does not define correct path to a model : " + modelPath);
	}

}
