package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import net.ihe.gazelle.goc.umlmodel.model.ModelUMLDesc;
import net.ihe.gazelle.goc.umlmodel.model.ModelsDefinition;
import net.ihe.gazelle.model.CdaModelRetriever;
import net.ihe.gazelle.model.CdaepsosModelRetriever;
import net.ihe.gazelle.model.CdaxehealthModelRetriever;
import net.ihe.gazelle.model.ModelRetriever;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

//TODO this will be replaced, packager will not be aware of resources. [GOC-161]
/**
 * This class is responsible for the retrieving of resources (models, etc) needed for the generation.
 * It will create files as needed during the generation process.
 * @author wbars
 */
class WorkspaceResourceCreator {

    private ModelRetriever modelRetriever;
    private String confName;
    private FolderCreator folderCreator;
    private FileWriter fileWriter;

    /**
     * Base constructor for the class
     */
    private WorkspaceResourceCreator(){
    }

    /**
     * Base constructor for the class.
     * @param folderCreator : folder creator used to create folders in the workspace.
     */
    WorkspaceResourceCreator(FolderCreator folderCreator, FileWriter fileWriter){
        //TODO change the way the conf is retrieved. [GOC-161]
        confName = System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic");
        //TODO inject proper implementation [GOC-161]
        Map<String, ModelRetriever> retrievers = new HashMap<>();
        retrievers.put("cdabasic", new CdaModelRetriever());
        retrievers.put("cdaepsos", new CdaepsosModelRetriever());
        retrievers.put("cdaxehealth", new CdaxehealthModelRetriever());

        ModelRetriever modelRetriever = retrievers.get(confName);
        if(modelRetriever != null){
            this.modelRetriever = modelRetriever;
        }
        else {
            //TODO [GOC-161]
            //throw new IOException("Requested conf " + confName + " does not exist !");
        }
        this.folderCreator = folderCreator;
        this.fileWriter = fileWriter;
    }

    /**
     * Retrieve all resources from the classpath and create the corresponding files in the workspace
     * @param workspacePath : path to the workspace where resources will be created.
     * @throws IOException
     */
    void retrieveNeededResources(String workspacePath) throws IOException {
        Map<String, URL> resources = modelRetriever.retrieveResources();
        Map<String, URL> models =  modelRetriever.retrieveModels();
        Map<String, URL> profiles = modelRetriever.retrieveUMLProfiles();

        ModelsDefinition modelsDefinition;
        try {
            modelsDefinition = getModelsDefinitionFromResources(resources);
        } catch (JAXBException e){
            throw new IOException(e);
        }
        createModelsFromModelsDefinition(workspacePath, modelsDefinition, models);
        createResourcesInFolder(new File(workspacePath).getParent() + "/hl7templates-resources/" + confName, resources);
        createResourcesInFolder(new File(workspacePath).getParent() + "/hl7templates-resources/uml/profiles", profiles);
        // Replace path in modelsDefinition.xml with output folder -> This is ok as hl7templates-resources is checked-out during packager generation
        UpdateModelsDefinition updateModelsDefinition = new UpdateModelsDefinition();
        updateModelsDefinition.updateModelDefinitions(workspacePath);

    }

    /**
     * Create files for resources in a map in a designated folder. The folder will be created if not existing.
     * @param folderOutput : path to the folder where resources will be created.
     * @param resources : map of resources that will be created in the folder.
     * @throws IOException
     */
    private void createResourcesInFolder(String folderOutput, Map<String, URL> resources) throws IOException{
        folderCreator.createFolder(folderOutput);
        for (String key : resources.keySet()) {
            fileWriter.writeContentInFile(resources.get(key), folderOutput + "/" + key);
        }
    }

    /**
     * Retrieve and unmarshall Models Definition from resources linked to use model.
     * @param resources : map of resources retrieved from used model.
     * @throws JAXBException
     */
    private ModelsDefinition getModelsDefinitionFromResources(Map<String, URL> resources) throws JAXBException {
        ModelsDefinition modelsDefinition;
        JAXBContext jxb = JAXBContext.newInstance(ModelsDefinition.class);
        Unmarshaller um = jxb.createUnmarshaller();
        modelsDefinition = (ModelsDefinition) um.unmarshal(resources.get("modelsDefinition.xml"));
        return modelsDefinition;
    }

    /**
     * Write the content of a resource from an URL to a specific file.
     * @param workspacePath : workspace path where to create resources.
     * @param modelsDefinition : models definition defining which models are needed for the generation.
     * @param models : map of all available model resources.
     * @throws IOException
     */
    private void createModelsFromModelsDefinition(String workspacePath, ModelsDefinition modelsDefinition, Map<String, URL> models) throws IOException{
        for (ModelUMLDesc modelDescription : modelsDefinition.getUMLModelsDescription().getModelUMLDesc()) {
            String modelPath = modelDescription.getRelativeXMIPath().replace("../", "");
            String subModelName = modelPath.substring(modelPath.lastIndexOf("/")+1);
            String folderName = modelPath.substring(0, modelPath.lastIndexOf("/"));
            folderCreator.createFolder(workspacePath + "/" + folderName);
            if (models.get(subModelName) == null) {
                throw new IOException("Didn't found model in resources : " + subModelName);
            }
            fileWriter.writeContentInFile(models.get(subModelName), workspacePath + "/" + modelPath);
        }
    }
}
