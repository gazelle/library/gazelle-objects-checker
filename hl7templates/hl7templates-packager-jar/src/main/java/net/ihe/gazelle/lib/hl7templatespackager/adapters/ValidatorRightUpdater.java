package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Update rights to be able to execute the validator from command lines.
 * @author wbars
 */
public class ValidatorRightUpdater {

    private static final String VALIDATE_SCRIPT_END_PATH = "/target/appassembler/bin/validator.sh";

    private static final String UPDATE_IMPORTS_SCRIPT = "/src/main/resources/autoUpdateImports.sh";

    private static final String GENERATE_AND_UPDATE_SCRIPT = "/src/main/resources/generateAndUpdate.sh";

    private Logger log = LoggerFactory.getLogger(ValidatorRightUpdater.class);

    private ProcessExecutor processExecutor;

    /**
     * Base constructor for the class
     */
    private ValidatorRightUpdater(){
    }

    /**
     * Public constructor for the class
     * @param processExecutor : process executor to execute the command to update rights on validator scripts.
     */
    public ValidatorRightUpdater(ProcessExecutor processExecutor){
        this.processExecutor = processExecutor;
    }


    /**
     * Update rights on validator script to be able to execute it to perform a validation.
     * @param projectPath : path of the project where the script can be found.
     * @throws IOException
     */
    void updateValidatorRights(String projectPath) throws IOException {
        String validateScriptPath = projectPath + VALIDATE_SCRIPT_END_PATH;
        String cmd = "chmod 777 \"" + validateScriptPath + "\"";
        log.warn(cmd);
        processExecutor.executeProcess(cmd);
    }

    public void updateScriptToBeExecutable(String absolutePath) throws IOException{
        String cmd = "chmod +x \"" + absolutePath + "\"";
        log.warn(cmd);
        processExecutor.executeProcess(cmd);
    }

    public void updateUtilityScripts(String projectPath) throws IOException{
        String updateImports = projectPath + UPDATE_IMPORTS_SCRIPT;
        String generateAndUpdate = projectPath + GENERATE_AND_UPDATE_SCRIPT;
        String cmd = "chmod +x \"" + updateImports + "\" \"" + generateAndUpdate + "\"";
        log.warn(cmd);
        processExecutor.executeProcess(cmd);
    }
}
