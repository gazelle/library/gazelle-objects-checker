package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

public interface GOCEntry {

    void runGOC(String args[]);
}
