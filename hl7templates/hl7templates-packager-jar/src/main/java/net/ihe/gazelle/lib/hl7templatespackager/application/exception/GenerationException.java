package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class GenerationException extends Exception{

    public GenerationException(String message) {
        super(message);
    }

    public GenerationException(String message, Throwable cause) {
        super(message, cause);
    }
}
