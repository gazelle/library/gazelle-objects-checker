package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.ArchetypeInstantiatorMaven;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.ProcessExecutor;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.ValueSetExtractorImpl;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.UMLModelGenerator;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.ValidatorRightUpdater;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.ValidatorGeneratorPlugin;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.GeneratedValidatorArchiverImpl;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.ImportUpdaterImpl;
import net.ihe.gazelle.lib.hl7templatespackager.adapters.WorkspaceCreatorFileSystem;
import net.ihe.gazelle.lib.hl7templatespackager.application.GOCLogger;
import net.ihe.gazelle.lib.hl7templatespackager.application.GOCPackager;
import net.ihe.gazelle.lib.hl7templatespackager.application.ValueSetExtractor;


/**
 * This class creates an instance of GOCPackager usable to generate a validator from a ValidatorConfiguration.
 * @author wbars
 */
public class GOCPackagerFactory {

    /**
     * Generate an instance of GOCPackager able to generate validators in a given workspace.
     * @param workspacesRoot : root path for the workspaces that will be used for generation
     * @param mvnExecutionPath : path to maven execution (eg: mvn)
     * @param generationDate : generation date, used for creating logs folder
     * @param gocLogger : The instance of the logger
     * @param processExecutor : the instance of the process executor
     * @return returns the packager
     */
    public GOCPackager createGOCPackager(String workspacesRoot, String mvnExecutionPath, String generationDate, GOCLogger gocLogger,
            ProcessExecutor processExecutor){

        ValueSetExtractor valueSetExtractor = new ValueSetExtractorImpl(workspacesRoot);
        return new GOCPackager(gocLogger,
                new WorkspaceCreatorFileSystem(workspacesRoot, new FolderCreatorImpl(), new FileWriterImpl()),
                new ArchetypeInstantiatorMaven(workspacesRoot, mvnExecutionPath, gocLogger, processExecutor, valueSetExtractor),
                new UMLModelGenerator(workspacesRoot, gocLogger),
                new ValidatorGeneratorPlugin(workspacesRoot, mvnExecutionPath, gocLogger, processExecutor,
                        new ValidatorRightUpdater(processExecutor)),
                new GeneratedValidatorArchiverImpl(workspacesRoot),
                new ImportUpdaterImpl(workspacesRoot));
    }
}
