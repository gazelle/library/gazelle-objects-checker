package net.ihe.gazelle.lib.hl7templatespackager.peripherals;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.ProcessExecutor;
import net.ihe.gazelle.lib.hl7templatespackager.application.*;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.EnvironementException;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.PackagerProcessException;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GOCExecutor {

    private static Logger log = null;

    private String generationDate;

    private String outputFile;

    private String hl7Resources;


    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");

    public GOCExecutor(String outputFile){
        this.outputFile = outputFile;
    }

    public ValidatorConfiguration execute(BBRResource bbrResource, String mvnExecPath){
        return execute(bbrResource,mvnExecPath,null,true,false,null,null);
    }

    public ValidatorConfiguration execute(BBRResource bbrResource,String mvnExecPath,
                                          String versionLabel, Boolean ignoreTemplateIdRequirements,
                                          Boolean ignoreCdaBasicRequirements, String serviceName, String hl7Conf) {

        try{
            addSubDirectoryToOutput("/validator");

            // initialize log variable
            initLog();
            GOCLogger gocLogger = new GOCLoggerImpl(getWorkspaceRoot(), generationDate);

            ProcessExecutor processExecutor = new ProcessExecutorBash(gocLogger);

            //check environement configuration
            EnvironementChecker environementChecker = new EnvironementCheckerImpl();
            environementChecker.checkMVNPath(mvnExecPath,processExecutor);
            environementChecker.checkJavaVersion();


            this.hl7Resources = getWorkspaceRoot()+"/hl7templates-resources";

            String HL7ConfFolder = (hl7Conf == null) ? "cdabasic":hl7Conf;

            EnvironementConfiguration.setSystemVariables("HL7TEMP_CDACONFFOLDERNAME",HL7ConfFolder,
                    "HL7TEMP_RESOURCES_PATH",this.hl7Resources,"JAVA_EXEC",System.getProperty("java.home")+"/bin/java");

            EnvironementConfiguration.configure();

            log.info("Generation started at : {}",this.generationDate);
            log.info("Workspace root is : {}",getWorkspaceRoot());
            log.info("Workspace name is : {}",getWorkspaceName());

            GOCPackager packager = new GOCPackagerFactory().createGOCPackager(getWorkspaceRoot(), mvnExecPath, this.generationDate,
                    gocLogger, processExecutor);

            ValidatorConfiguration validatorConfiguration = new ValidatorConfiguration(bbrResource, versionLabel,
                    ignoreTemplateIdRequirements, ignoreCdaBasicRequirements,
                    serviceName);
            packager.packageBBRToGOCValidationTool(new Workspace(getWorkspaceName()),validatorConfiguration);

            return validatorConfiguration;

        }
        catch (EnvironementException e){
            log.error("Problem in environement configuration",e);
        }
        catch (PackagerProcessException e){
            log.error("Problem during packaging validator",e);
        }

        return null;
    }

    /**
     * initialize log4j used variable before start logging<br>
     * And set the generationDate field<br>
     * <b>Variable name:</b> <i>${generationDate}</i><br>
     * <b>Usage:</b> as a system property
     */
    private void initLog(){
        this.generationDate = this.formatter.format(new Date());
        System.setProperty("generationDate",this.generationDate);
        System.setProperty("workspaceRoot",getWorkspaceRoot());
        log = LoggerFactory.getLogger(HL7Templates2GOC.class);
    }

    /**
     * Method to add sub-directories to the provided output path
     * To encapsulate validator and logs in one directory
     * Provide empty string to use the old behavior
     * @param subDirectory sub-directorie(s), started with '/'
     */
    private void addSubDirectoryToOutput(String subDirectory){
        if(subDirectory == null){
            subDirectory = "";
        }
        this.outputFile  = this.outputFile + subDirectory;
    }

    private String getWorkspaceName() {
        return this.outputFile.substring(this.outputFile.lastIndexOf("/") + 1);
    }

    private String getWorkspaceRoot() {
        return this.outputFile.substring(0, this.outputFile.lastIndexOf("/") + 1);
    }

    public String getGenerationDate() {
        return generationDate;
    }
}
