package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ValidatorArchiveException;

public interface GeneratedValidatorArchiver {

    void generateArchiveFromGeneratedValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws ValidatorArchiveException;
}
