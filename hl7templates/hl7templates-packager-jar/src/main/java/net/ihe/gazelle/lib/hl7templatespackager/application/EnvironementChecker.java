package net.ihe.gazelle.lib.hl7templatespackager.application;

import net.ihe.gazelle.lib.hl7templatespackager.adapters.ProcessExecutor;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.EnvironementException;

public interface EnvironementChecker {

    void checkMVNPath(String command, ProcessExecutor processExecutor) throws EnvironementException;

    void checkJavaVersion() throws EnvironementException;

}
