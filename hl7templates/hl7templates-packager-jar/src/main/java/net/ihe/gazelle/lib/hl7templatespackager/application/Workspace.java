package net.ihe.gazelle.lib.hl7templatespackager.application;

public class Workspace {

    private String name;

    public Workspace(){
    }

    public Workspace (String name){
        setName(name);
    }


    public String getName() {
        if (this.name != null){
            return name;
        }
        return "";
    }

    public void setName(String name) {
        if (name != null){
            this.name = name;
        } else {
            this.name = "";
        }
    }
}
