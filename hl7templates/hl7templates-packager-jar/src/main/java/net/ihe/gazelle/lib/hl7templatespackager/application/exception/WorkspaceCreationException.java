package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class WorkspaceCreationException extends Exception{

    private WorkspaceCreationException(){
        super();
    }

    public WorkspaceCreationException(String message) {
        super(message);
    }

    public WorkspaceCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}
