package net.ihe.gazelle.lib.hl7templatespackager.application.exception;

public class ArchetypeInstantiationException extends Exception{

    private ArchetypeInstantiationException(){
        super();
    }

    public ArchetypeInstantiationException(String message) {
        super(message);
    }

    public ArchetypeInstantiationException(String message, Throwable cause) {
        super(message, cause);
    }
}
