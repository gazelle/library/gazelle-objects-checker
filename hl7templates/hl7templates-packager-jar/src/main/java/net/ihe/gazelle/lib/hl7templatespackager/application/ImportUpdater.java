package net.ihe.gazelle.lib.hl7templatespackager.application;

import java.io.IOException;

public interface ImportUpdater {
    void updateImportsForValidator(Workspace workspace, ValidatorConfiguration validatorConfiguration) throws IOException;
}
