package net.ihe.gazelle.lib.hl7templatespackager.adapters;

import net.ihe.gazelle.goc.ValidatorInstantiator;
import net.ihe.gazelle.lib.hl7templatespackager.application.*;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ArchetypeInstantiationException;
import net.ihe.gazelle.lib.hl7templatespackager.application.exception.ValueSetExtractionException;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.FolderCreatorImpl;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * This class allows to generate artifact from different archetypes used for validator generation.
 * @author wbars
 */
public class ArchetypeInstantiatorMaven implements ArchetypeInstantiator {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(ArchetypeInstantiatorMaven.class);
    //this is supposed to be the version of the packager artifact. It is for now used as artifact version. See what we will use
    private static String gocVersion = null;
    private static final String CD_COMMAND = "cd \"";
    private static final String REPOSITORY = "https://gazelle.ihe.net/nexus/content/groups/public/";

    private String workspacesRoot;
    private String mvnExecutionPath;
    private GOCLogger gocLogger;
    private ProcessExecutor processExecutor;
    private ValueSetExtractor valueSetExtractor;

    /**
     * Base constructor for the class
     */
    private ArchetypeInstantiatorMaven(){
    }

    /**
     * Creates an instance of the class with a defined root path to create archetypes instances.
     * @param workspacesRoot : root path where workspaces will be created.
     * @param mvnExecutionPath : binary path for maven
     * @param gocLogger : The instance of the logger
     * @param processExecutor : The instance of the process Executor
     * @param valueSetExtractor : The instance of the valueSet Extractor
     */
    public ArchetypeInstantiatorMaven(String workspacesRoot, String mvnExecutionPath, GOCLogger gocLogger, ProcessExecutor processExecutor,
                                      ValueSetExtractor valueSetExtractor){
        setWorkspacesRoot(workspacesRoot);
        setMvnExecutionPath(mvnExecutionPath);
        this.gocLogger = gocLogger;
        this.processExecutor = processExecutor;
        this.valueSetExtractor = valueSetExtractor;
        if(gocVersion == null){
            gocVersion = getGOCVersion();
        }
    }

    /**
     * Protected constructor for tests.
     * @param mvnExecutionPath path of used binary maven.
     * @param processExecutor Instance of processExecutor
     */
    protected ArchetypeInstantiatorMaven(String mvnExecutionPath, ProcessExecutor processExecutor){
        setMvnExecutionPath(mvnExecutionPath);
        this.processExecutor = processExecutor;
    }

    /**
     * Getter for the workspaceRoot property.
     */
    private String getWorkspacesRoot() {
        if (this.workspacesRoot != null){
            return this.workspacesRoot;
        }
        return "";
    }

    /**
     * Setter for the workspaceRoot property
     * @param workspacesRoot : the path we want to use as root.
     */
    private void setWorkspacesRoot(String workspacesRoot) {
        if (workspacesRoot != null){
            this.workspacesRoot = workspacesRoot;
        } else {
            this.workspacesRoot = "";
        }
    }

    /**
     * Getter for the workspaceRoot property.
     */
    private String getMvnExecutionPath() {
        if (this.mvnExecutionPath != null){
            return this.mvnExecutionPath;
        }
        return "mvn";
    }

    /**
     * Setter for the workspaceRoot property
     * @param mvnExecutionPath : the path we want to use as root.
     */
    private void setMvnExecutionPath(String mvnExecutionPath) {
        if (mvnExecutionPath != null){
            this.mvnExecutionPath = mvnExecutionPath;
        } else {
            this.mvnExecutionPath = "mvn";
        }
    }

    /**
     * Retrieve all resources from the classpath and create the corresponding files in the workspace
     * @param workspace : workspace where the artifact will be created.
     * @param validatorConfiguration : ValidatorConfiguration used for the generation. Contains information on the validator to generate .
     * @throws ArchetypeInstantiationException when the validator artifact is not correctly generated.
     */
    public void createValidatorArtifact(Workspace workspace, ValidatorConfiguration validatorConfiguration)
            throws ArchetypeInstantiationException {

        String workspacePath = getWorkspacesRoot() + workspace.getName();
        String configuration = System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic");
        String createArchetypeCommand = ValidatorInstantiator.generateArchetype(workspacePath, getMvnExecutionPath(), gocVersion,
                validatorConfiguration.getProjectNaming().getProjectName(), validatorConfiguration.getProjectNaming().getProjectNameUpperCase(), configuration,
                validatorConfiguration.getProjectNaming().getProjectNameCapitalized(), validatorConfiguration.getServiceName(), workspacePath,
                validatorConfiguration.ignoreCdaBasicRequirements().toString(),System.getProperty("JAVA_EXEC"));
        log.warn("Command executed to generate model artifact : \n" + createArchetypeCommand);

        log.info("Trying to look for archetype in local repository...");
        if(!generateArchetypeFromRepository(createArchetypeCommand,null)){
            log.warn("Archetype not found locally, retrieving from remote repository {} ",REPOSITORY);
            if(!generateArchetypeFromRepository(createArchetypeCommand,REPOSITORY)){
                log.error("Could not find archetype in remote repository: {}", REPOSITORY);
                throw new ArchetypeInstantiationException("Cannot generate validator artifact !");
            }
        }

        gocLogger.logSeparator("Extracting Value-sets","Extract Value-sets from the ValidatorConfiguration and put them in project resources");
        try {
            valueSetExtractor.extractValueSets(workspace, validatorConfiguration);
        } catch (ValueSetExtractionException e){
            throw new ArchetypeInstantiationException("Error extracting value-sets from the ValidatorConfiguration !", e);
        }
    }

    public String getMavenRepo(){
        String javaHome = System.getProperty("JAVA_EXEC");
        String javaEnv = "";
        if(javaHome != null && !javaHome.isEmpty()){
            javaEnv = "env $JAVA_HOME="+javaHome;
        }
        String command = javaEnv + " " + getMvnExecutionPath() + " help:evaluate -Dexpression=settings.localRepository -q -DforceStdout";
        List<String> output = null;
        try {
            output = processExecutor.executeProcessAndReturn(command);
        } catch (IOException e) {
            log.error("Couldn't get maven local repository {}", e);
            return null;
        }
        if(output != null && output.size()>0){
            return output.get(0);
        }
        return null;
    }

    protected boolean checkIfArchetypeInstalled(){
        String mavenRepo = getMavenRepo();
        String groupId = "net.ihe.gazelle.goc";
        String artifactId = "generated-validator";
        String version = ValidatorInstantiator.getVersion();
        String dir = mavenRepo + "/" + getPathFromGroupId(groupId) + artifactId + "/"+ version;
        Path path = Paths.get(dir);
        return Files.exists(path);
    }

    protected String getPathFromGroupId(String groupId){
        if(groupId == null || groupId.isEmpty()){
            return null;
        }
        return (groupId.replace(".","/") + "/");

    }


    private boolean generateArchetypeFromRepository(String createArtifactCommand, String repository){
        try {
            if(repository != null && !repository.isEmpty()){
                createArtifactCommand += " -DarchetypeRepository=\""+repository+"\"";
            }
            processExecutor.executeProcess(createArtifactCommand);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private String getGOCVersion(){
        String version = ArchetypeInstantiatorMaven.class.getPackage().getImplementationVersion();
        if(version == null){
            version = System.getProperty("GOC_VERSION");
            if(version == null){
                log.error("Could not find GOC version, trying to get it from latest release in nexus");
                version = getLatestGOCVersion();
            }
        }
        log.info("GOC version is {}",version);
        return version;
    }

    //Get latest GOC Version from nexus
    private String getLatestGOCVersion(){
        String command = "curl \"https://gazelle.ihe.net/nexus/service/local/repositories/releases/index_content/?groupIdHint=net.ihe.gazelle&artifactIdHint=gazelle-objects-checker\" 2>&1 " +
                " | grep  \"version\" | grep -o -e \"[0-9.]*\" | head -n 1";
        List<String> output = null;
        try {
            output = processExecutor.executeProcessAndReturn(command);
            if(output != null && output.size()>0){
                return output.get(0);
            }
        } catch (IOException e) {
            log.error("Couldn't get latest GOC version {}", e);
            return null;
        }
        return null;
    }


}
