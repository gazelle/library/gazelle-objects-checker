package net.ihe.gazelle.goc.common.utils;

public class XPATHOCLRule {

    private String content;
    private OperationKind operation;

    public XPATHOCLRule(OperationKind operation, String content) {
        this.content = content;
        this.operation = operation;
    }

    public XPATHOCLRule(OperationKind operation) {
        this.operation = operation;
    }

    public XPATHOCLRule(){}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OperationKind getOperation() {
        return operation;
    }

    public void setOperation(OperationKind operation) {
        this.operation = operation;
    }

}
