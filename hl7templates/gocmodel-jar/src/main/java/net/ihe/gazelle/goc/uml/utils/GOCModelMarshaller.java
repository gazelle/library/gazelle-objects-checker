package net.ihe.gazelle.goc.uml.utils;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * <p>GOCModelMarshaller class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class GOCModelMarshaller {
	
	private GOCModelMarshaller() {}
	
	/**
	 * <p>readGOCObject.</p>
	 *
	 * @param is a {@link java.io.InputStream} object.
	 * @return a {@link java.lang.Object} object.
	 * @throws javax.xml.bind.JAXBException if any.
	 */
	public static Object readGOCObject(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.goc.xmi:net.ihe.gazelle.goc.uml:net.ihe.gazelle.goc.ecore:net.ihe.gazelle.goc.xmm");
		Unmarshaller um = jc.createUnmarshaller();
		return um.unmarshal(is);
	}
	
	/**
	 * <p>printGOCObject.</p>
	 *
	 * @param goc a {@link java.lang.Object} object.
	 * @param os a {@link java.io.OutputStream} object.
	 * @throws javax.xml.bind.JAXBException if any.
	 */
	public static void printGOCObject(Object goc, OutputStream os) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.goc.xmi:net.ihe.gazelle.goc.uml:net.ihe.gazelle.goc.ecore:net.ihe.gazelle.goc.xmm");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(goc, os);
	}
	

}
