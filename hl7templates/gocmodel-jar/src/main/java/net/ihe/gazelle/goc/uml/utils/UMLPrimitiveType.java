package net.ihe.gazelle.goc.uml.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>UMLPrimitiveType class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public enum UMLPrimitiveType {

	BOOLEAN("Boolean"), STRING("String"), INTEGER("Integer"), DOUBLE("Double");

	private String value;

	private UMLPrimitiveType(String value) {
		this.value = value;
	}

	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <p>listUMLPrimitives.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public static List<String> listUMLPrimitives() {
		List<String> res = new ArrayList<>();
		for (UMLPrimitiveType uu : UMLPrimitiveType.values()) {
			res.add(uu.value);
		}
		return res;
	}

}
