package net.ihe.gazelle.goc.uml.utils;

import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.xmm.ElementImport;

/**
 * <p>ElementImportUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class ElementImportUtil {
	
	private ElementImportUtil() {}
	
	/**
	 * <p>findElementImport.</p>
	 *
	 * @param model a {@link net.ihe.gazelle.goc.uml.Model} object.
	 * @param name a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.goc.xmm.ElementImport} object.
	 */
	public static ElementImport findElementImport(Model model, String name) {
		if (model != null) {
			for (ElementImport ei : model.getElementImport()) {
				String id = null;
				if (ei.getImportedElement() != null && ei.getImportedElement().getHref() != null && 
						ei.getImportedElement().getHref().contains("#")) {
					id = ei.getImportedElement().getHref().split("#")[1];
				}
				if (id != null) {
					String nameElement = CDAClassesMap.findElementNameById(id);
					if (nameElement!= null && nameElement.equals(name)) {
						return ei;
					}
				}
			}
		}
		return null;
	}

}
