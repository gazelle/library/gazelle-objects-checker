package net.ihe.gazelle.goc.umlmodel.util;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.umlmodel.model.ModelsDefinition;

/**
 * <p>ModelsDefinitionUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class ModelsDefinitionUtil {
	
	private static Logger log = LoggerFactory.getLogger(ModelsDefinitionUtil.class);
	
	private ModelsDefinitionUtil() {}
	
	/**
	 * <p>unmarshallModelsDefinition.</p>
	 *
	 * @param path a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.goc.umlmodel.model.ModelsDefinition} object.
	 */
	public static ModelsDefinition unmarshallModelsDefinition(String path) {
		try {
			JAXBContext jxb = JAXBContext.newInstance(ModelsDefinition.class);
			Unmarshaller um = jxb.createUnmarshaller();
			return (ModelsDefinition) um.unmarshal(new File(path));
		}
		catch(Exception e) {
			log.error("not able to unmarshall ModelsDefinition", e);
			return null;
		}
	}
	
}
