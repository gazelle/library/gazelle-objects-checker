package net.ihe.gazelle.goc.common.utils;

public enum OperationKind {
    AND("and"),
    OR("or"),
    EMPTY("");

    private String value;

    private OperationKind(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static OperationKind getOperationKind(String type){
        switch (type.toLowerCase()){
            case "and": return AND;
            case "or": return OR;
            case "": return EMPTY;
            default: return null;
        }
    }
}
