//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2014.08.29 at 01:16:10 PM CEST
//


package net.ihe.gazelle.goc.uml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.goc.xmm.ElementImport;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.goc.xmm.ProfileApplication;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice maxOccurs="unbounded"&gt;
 *           &lt;element name="packagedElement" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *           &lt;element name="elementImport" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="packageImport" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *         &lt;element name="profileApplication" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}NCName" /&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "packagedElement",
        "elementImport",
        "packageImport",
        "profileApplication"
})
@XmlRootElement(name = "Model")
public class Model {

    @XmlElement(name = "packagedElement", namespace = "")
    private List<PackagedElement> packagedElement;

    @XmlElement(name = "elementImport", namespace = "")
    private List<ElementImport> elementImport;


    private Object packageImport;
    @XmlElement(required = true)
    private List<ProfileApplication> profileApplication;
    @XmlAttribute(name = "name", required = true)
    private String name;
    @XmlAttribute(name = "id", namespace = "http://schema.omg.org/spec/XMI/2.1")
    private String id;

    /**
     * <p>Getter for the field <code>profileApplication</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ProfileApplication> getProfileApplication() {
        if (profileApplication == null) {
            profileApplication = new ArrayList<>();
        }
        return profileApplication;
    }

    /**
     * <p>Setter for the field <code>profileApplication</code>.</p>
     *
     * @param profileApplication a {@link java.util.List} object.
     */
    public void setProfileApplication(List<ProfileApplication> profileApplication) {
        this.profileApplication = profileApplication;
    }

    /**
     * <p>Getter for the field <code>packagedElement</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PackagedElement> getPackagedElement() {
        if (packagedElement == null) {
            packagedElement = new ArrayList<>();
        }
        return packagedElement;
    }

    /**
     * <p>Setter for the field <code>packagedElement</code>.</p>
     *
     * @param packagedElement a {@link java.util.List} object.
     */
    public void setPackagedElement(List<PackagedElement> packagedElement) {
        this.packagedElement = packagedElement;
    }

    /**
     * <p>Getter for the field <code>elementImport</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ElementImport> getElementImport() {
        if (elementImport == null) {
            elementImport = new ArrayList<>();
        }
        return elementImport;
    }

    /**
     * <p>Setter for the field <code>elementImport</code>.</p>
     *
     * @param elementImport a {@link java.util.List} object.
     */
    public void setElementImport(List<ElementImport> elementImport) {
        this.elementImport = elementImport;
    }

    /**
     * Gets the value of the packageImport property.
     *
     * @return a {@link java.lang.Object} object.
     */
    public Object getPackageImport() {
        return packageImport;
    }

    /**
     * Sets the value of the packageImport property.
     *
     * @param value allowed object is
     *              {@link java.lang.Object}
     */
    public void setPackageImport(Object value) {
        this.packageImport = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link java.lang.String}
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is
     *              {@link java.lang.String}
     */
    public void setId(String value) {
        this.id = value;
    }

}
