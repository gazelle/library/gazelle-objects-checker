package net.ihe.gazelle.goc.scripts;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


// /!\ ALERT: Don't touch this class! you may damage your system

public class DeleteResourcesForTest {
    public static void main(String[] args) {

        try {
            Properties properties = new Properties();
            InputStream inputStream = DeleteResourcesForTest.class.getResourceAsStream("/env.properties");
            properties.load(inputStream);
            String workspace = properties.getProperty("TEMP_TESTS_WORKSPACE");
            FileUtils.deleteDirectory(new File(workspace));
            System.out.println("Resources deleted successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
