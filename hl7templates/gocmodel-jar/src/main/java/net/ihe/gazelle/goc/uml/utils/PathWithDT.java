package net.ihe.gazelle.goc.uml.utils;

/**
 * <p>PathWithDT class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public class PathWithDT {
	
	private String elementName;
	
	private String elementDT;
	
	/**
	 * <p>Constructor for PathWithDT.</p>
	 */
	public PathWithDT() {
		// basic constructor
	}
	
	/**
	 * <p>Constructor for PathWithDT.</p>
	 *
	 * @param elementName a {@link java.lang.String} object.
	 */
	public PathWithDT(String elementName) {
		super();
		this.elementName = elementName;
	}

	/**
	 * <p>Constructor for PathWithDT.</p>
	 *
	 * @param elementName a {@link java.lang.String} object.
	 * @param elementDT a {@link java.lang.String} object.
	 */
	public PathWithDT(String elementName, String elementDT) {
		super();
		this.elementName = elementName;
		this.elementDT = elementDT;
	}

	/**
	 * <p>Getter for the field <code>elementName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getElementName() {
		return elementName;
	}

	/**
	 * <p>Setter for the field <code>elementName</code>.</p>
	 *
	 * @param elementName a {@link java.lang.String} object.
	 */
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	/**
	 * <p>Getter for the field <code>elementDT</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getElementDT() {
		return elementDT;
	}

	/**
	 * <p>Setter for the field <code>elementDT</code>.</p>
	 *
	 * @param elementDT a {@link java.lang.String} object.
	 */
	public void setElementDT(String elementDT) {
		this.elementDT = elementDT;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((elementDT == null) ? 0 : elementDT.hashCode());
		result = prime * result
				+ ((elementName == null) ? 0 : elementName.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PathWithDT other = (PathWithDT) obj;
		if (elementDT == null) {
			if (other.elementDT != null) {
				return false;
			}
		} else if (!elementDT.equals(other.elementDT)) {
			return false;
		}
		if (elementName == null) {
			if (other.elementName != null) {
				return false;
			}
		} else if (!elementName.equals(other.elementName)) {
			return false;
		}
		return true;
	}
	

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "PDT[" + elementName + ", " + elementDT + "]";
	}
	
}
