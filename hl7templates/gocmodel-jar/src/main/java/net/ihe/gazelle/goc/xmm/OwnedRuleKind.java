package net.ihe.gazelle.goc.xmm;

/**
 * <p>OwnedRuleType class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public enum OwnedRuleKind {
	
	CONTEXT("Context"), LENGTH ("Length"), DATATYPE ("Datatype"), VOCABULARY ("Vocabulary"), 
	FIXEDVAL("Fixed value"), CARDINALITY ("Cardinality"), CLOSED ("Closed"), MANDATORY("Mandatory"),
	CHOICE("Choice");
	
	private String value;
	
	private OwnedRuleKind(String value) {
		this.value = value;
	}
	
	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue() {
		return value;
	}
	
}
