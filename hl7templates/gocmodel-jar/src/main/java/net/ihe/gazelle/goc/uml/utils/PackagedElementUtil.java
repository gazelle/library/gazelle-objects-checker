package net.ihe.gazelle.goc.uml.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.goc.xmm.General;
import net.ihe.gazelle.goc.xmm.Generalization;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <p>PackagedElementUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class PackagedElementUtil {
	
	private PackagedElementUtil() {}
	
	/**
	 * <p>initPackagedElement.</p>
	 *
	 * @return a {@link net.ihe.gazelle.goc.xmm.PackagedElement} object.
	 */
	public static PackagedElement initPackagedElement() {
		PackagedElement pe = new PackagedElement();
		pe.setType("uml:Class");
		pe.setId(EcoreUtil.generateUUID());
		return pe;
	}
	
	/**
	 * <p>addGeneralizationToPackagedElement.</p>
	 *
	 * @param pe a {@link net.ihe.gazelle.goc.xmm.PackagedElement} object.
	 * @param tdName a {@link java.lang.String} object.
	 */
	public static void addGeneralizationToPackagedElement(PackagedElement pe, String tdName) {
		pe.setGeneralization(new Generalization());
		pe.getGeneralization().setGeneral(new General());
		pe.getGeneralization().getGeneral().setType("uml:Class");
		
		pe.getGeneralization().getGeneral().setHref(CDAClassesMap.getRelativeXMILink(tdName));
	}
	
	/**
	 * <p>findOwnedRulesByContent.</p>
	 *
	 * @param pe a {@link net.ihe.gazelle.goc.xmm.PackagedElement} object.
	 * @param content a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<OwnedRule> findOwnedRulesByContent(PackagedElement pe, String content) {
		if (pe != null && content != null && pe.getOwnedRule() != null) {
			List<OwnedRule> res = new ArrayList<>();
			for (OwnedRule ownedRule : pe.getOwnedRule()) {
				if (ownedRule.getOwnedComment().getBody().contains(content)) {
					res.add(ownedRule);
				}
			}
			return res;
		}
		return new ArrayList<>();
	}

}
