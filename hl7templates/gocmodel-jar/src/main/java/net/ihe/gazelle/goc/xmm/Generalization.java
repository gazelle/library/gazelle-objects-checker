//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.29 at 12:55:12 PM CEST 
//


package net.ihe.gazelle.goc.xmm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{xmm}general"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute ref="{xmm}id"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "general"
})
@XmlRootElement(name = "generalization")
public class Generalization {

    @XmlElement(required = true)
    private General general;
    @XmlAttribute(name = "id", namespace = "http://schema.omg.org/spec/XMI/2.1")
    private String id;

    /**
     * Gets the value of the general property.
     *
     * @return a {@link net.ihe.gazelle.goc.xmm.General} object.
     */
    public General getGeneral() {
        return general;
    }

    /**
     * Sets the value of the general property.
     *
     * @param value
     *     allowed object is
     *     {@link net.ihe.gazelle.goc.xmm.General}
     */
    public void setGeneral(General value) {
        this.general = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return a {@link java.lang.String} object.
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link java.lang.String}
     */
    public void setId(String value) {
        this.id = value;
    }

}
