//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.29 at 01:16:10 PM CEST 
//
/**
 * JAXB model for UML namespace
 * 
 * @author <a href="abderrazek.boufahja@ihe-europe.net">Abderrazek Boufahja</a>
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.eclipse.org/uml2/3.0.0/UML", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package net.ihe.gazelle.goc.uml;
