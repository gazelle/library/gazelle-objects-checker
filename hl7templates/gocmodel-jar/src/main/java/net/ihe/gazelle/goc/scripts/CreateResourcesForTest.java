package net.ihe.gazelle.goc.scripts;

import net.ihe.gazelle.model.utils.ModelUMLDesc;
import net.ihe.gazelle.model.utils.ModelsDefinition;
import net.ihe.gazelle.model.CdaModelRetriever;
import org.apache.commons.io.FileUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateResourcesForTest {


    public static void main(String[] args) throws IOException{

        Properties properties = new Properties();
        InputStream inputStream = CreateResourcesForTest.class.getResourceAsStream("/env.properties");
        properties.load(inputStream);
        String workspacePath = properties.getProperty("TEMP_TESTS_WORKSPACE");

        System.out.println("Creating resources for tests...");

        CdaModelRetriever modelRetriever = new CdaModelRetriever();

        Map<String, URL> resources = modelRetriever.retrieveResources();
        Map<String, URL> models =  modelRetriever.retrieveModels();
        Map<String, URL> profiles = modelRetriever.retrieveUMLProfiles();

        ModelsDefinition modelsDefinition;
        try {
            modelsDefinition = getModelsDefinitionFromResources(resources);
        } catch (JAXBException e){
            throw new IOException(e);
        }
        for (ModelUMLDesc modelDescription : modelsDefinition.getUMLModelsDescription().getModelUMLDesc()) {
            String modelPath = modelDescription.getRelativeXMIPath().replace("../", "");
            String subModelName = modelPath.substring(modelPath.lastIndexOf("/")+1);
            String folderName = modelPath.substring(0, modelPath.lastIndexOf("/"));
            createFolder(workspacePath + "/" + folderName);
            if (models.get(subModelName) == null) {
                throw new IOException("Didn't found model in resources : " + subModelName);
            }
            writeContentInFile(models.get(subModelName), workspacePath + "/" + modelPath);
        }

        createFolder(new File(workspacePath) + "/hl7templates-resources/cdabasic");
        for (String key : resources.keySet()) {
            writeContentInFile(resources.get(key), new File(workspacePath) + "/hl7templates-resources/cdabasic" + "/" + key);
        }


        createFolder(new File(workspacePath) + "/hl7templates-resources/uml/profiles");
        for (String key : profiles.keySet()) {
            writeContentInFile(profiles.get(key), new File(workspacePath) + "/hl7templates-resources/uml/profiles" + "/" + key);
        }

        String pathResources = workspacePath + "/hl7templates-resources" + File.separator + "cdabasic/modelsDefinition.xml";
        try {
            String content = readDoc(pathResources);
            String folderOutputformatted = workspacePath.endsWith("/")?workspacePath.substring(0, workspacePath.length()-1):workspacePath;
            String replaceBy = updateWorkspacePath(content, folderOutputformatted);
            printDoc(replaceBy, pathResources);
        } catch (IOException e) {
            throw e;
        }

        String source =  workspacePath + "/hl7templates-resources";
        File srcDir = new File(source);

        String destination =  workspacePath + "hl7templates/hl7templates-resources";
        File destDir = new File(destination);

        try {
            FileUtils.copyDirectory(srcDir, destDir);
            System.out.println("Resources created successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ModelsDefinition getModelsDefinitionFromResources(Map<String, URL> resources) throws JAXBException {
        ModelsDefinition modelsDefinition;
        JAXBContext jxb = JAXBContext.newInstance(ModelsDefinition.class);
        Unmarshaller um = jxb.createUnmarshaller();
        modelsDefinition = (ModelsDefinition) um.unmarshal(resources.get("modelsDefinition.xml"));
        return modelsDefinition;
    }

    private static void createFolder(String folderToCreatePath) throws IOException {
        File folderToCreate = new File(folderToCreatePath);
        if (folderToCreate.exists()){
            System.out.println("Folder " + folderToCreate.getAbsolutePath() + " already exists.");
        } else if (!folderToCreate.mkdirs()){
            throw new IOException("Cannot create folder " + folderToCreate.getAbsolutePath());
        }
    }

    private static void writeContentInFile(URL resourceURL, String fileOutputPath) throws IOException{
        try (InputStream resourceInputStream = resourceURL.openStream()) {
            if (resourceInputStream == null) {
                throw new IOException("Didn't found resource : " + resourceURL);
            }
            byte[] buffer = new byte[resourceInputStream.available()];
            File fileOutput = new File(fileOutputPath);
            try (OutputStream outStream = new FileOutputStream(fileOutput)){
                while (true) {
                    final int length = resourceInputStream.read(buffer);
                    if (length <= 0) {
                        break;
                    }
                    outStream.write(buffer, 0, length);
                }
            }
        }
    }

    public static String readDoc(String name) throws IOException {
        BufferedReader scanner = new BufferedReader(new FileReader(name));
        StringBuilder res = new StringBuilder();
        try {
            String line = scanner.readLine();
            while (line != null) {
                res.append(line + "\n");
                line = scanner.readLine();
            }
        } finally {
            scanner.close();
        }
        return res.toString();
    }

    public static void printDoc(String doc, String name) throws IOException {
        try (FileWriter fw = new FileWriter(new File(name))){
            fw.append(doc);
            fw.close();
        }
    }

    private static String updateWorkspacePath(String modelsDefinitionContent, String workspacePath) throws IOException{
        if (modelsDefinitionContent != null) {
            String regex = "<path>(.*?)</path>";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(modelsDefinitionContent);
            String tobeReplaced = "";
            if(matcher.find()) {
                String stringUMLPath = matcher.group(1);
                tobeReplaced = extractWorkspace(stringUMLPath);
            }
            return modelsDefinitionContent.replace(tobeReplaced, workspacePath);
        }
        return null;
    }

    private static String extractWorkspace(String modelPath) throws IOException{
        String[] paths = modelPath.split("/");
        for (String path : paths) {
            if (path.contains("-model")) {
                return modelPath.substring(0, modelPath.indexOf(path)-1);
            }
        }
        throw new IOException("ModelDefinition does not define correct path to a model : " + modelPath);
    }
}
