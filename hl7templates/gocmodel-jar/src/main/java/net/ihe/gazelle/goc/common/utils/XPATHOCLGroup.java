package net.ihe.gazelle.goc.common.utils;

import java.util.ArrayList;
import java.util.List;

public class XPATHOCLGroup
{
    private List<XPATHOCLRule> rules;

    OperationKind operation;

    public XPATHOCLGroup(OperationKind operation) {
        this.operation = operation;
    }

    public OperationKind getOperation() {
        return operation;
    }

    public void setOperation(OperationKind operation) {
        this.operation = operation;
    }

    public XPATHOCLGroup() {
        this.rules = new ArrayList<>();
    }

    public List<XPATHOCLRule> getRules() {
        return rules;
    }

    public void setRules(List<XPATHOCLRule> rules) {
        this.rules = rules;
    }
}