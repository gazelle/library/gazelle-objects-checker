package net.ihe.gazelle.goc.common.utils;

public class XSITypeOCL extends XPATHOCLRule {

    private String type;
    private Boolean negativeOperation;

    public XSITypeOCL(OperationKind operationKind , Boolean negativeOperation, String type) {
        super(operationKind);
        this.type = type;
        this.negativeOperation = negativeOperation;
    }

    public XSITypeOCL(OperationKind operation, Boolean negativeOperation, String type, String content) {
        super(operation, content);
        this.type = type;
        this.negativeOperation = negativeOperation;
    }

    public XSITypeOCL() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean isNegativeOperation() {
        return negativeOperation;
    }

    public void setNegativeOperation(Boolean negativeOperation) {
        this.negativeOperation = negativeOperation;
    }
}
