@javax.xml.bind.annotation.XmlSchema(xmlns = {   
        @XmlNs(namespaceURI = "http://schema.omg.org/spec/XMI/2.1", prefix = "xmi"),  
        @XmlNs(namespaceURI = "http://www.eclipse.org/uml2/3.0.0/UML", prefix = "uml"),
        @XmlNs(namespaceURI = "http://www.eclipse.org/emf/2002/Ecore", prefix = "ecore"),
        @XmlNs(namespaceURI = "http://www.eclipse.org/uml2/schemas/Ecore/5", prefix = "Ecore"),
        @XmlNs(namespaceURI = "http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43", prefix = "TemplateDefiner")
    },
    namespace = "http://www.eclipse.org/uml2/schemas/Ecore/5", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package net.ihe.gazelle.goc.ecore;
import javax.xml.bind.annotation.XmlNs;

