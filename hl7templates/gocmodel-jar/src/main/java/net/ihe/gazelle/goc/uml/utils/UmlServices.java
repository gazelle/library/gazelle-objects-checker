package net.ihe.gazelle.goc.uml.utils;

import java.util.Iterator;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
final class UmlServices {

  private UmlServices() {
  }

  /**
   * Return the stereotype if is applied to element.
   * 
   * @param elt
   *          Element used.
   * @param stereotype
   *          Stereotype to return.
   * 
   * @return the stereotype.
   * 
   * @throws ENodeCastException
   *           the e node cast exception
   */
  private static Stereotype getStereotype(Element elt, String stereotype) {
    Iterator<Stereotype> iter = elt.getAppliedStereotypes().iterator();
    while (iter.hasNext()) {
      Stereotype element = iter.next();
      if (element.getName().equals(stereotype)) {
        return element;
      }
    }
    return null;
  }

  /**
   * Return the stereotype value if is applied to element.
   * 
   * @param elt
   *          Element used.
   * @param stereotype
   *          Stereotype to return.
   * @param propertyName
   *          the property name
   * 
   * @return Object contain the value.
   * 
   * @throws ENodeCastException
   *           the e node cast exception
   */
  static Object getStereotypeValue(Element elt, String stereotype, String propertyName) {
    // search with real stereotype
    Stereotype stereotypeFound = getStereotype(elt, stereotype);
    if (stereotypeFound == null) {
      return null;
    }
    return elt.getValue(stereotypeFound, propertyName);
  }

  /**
   * Verify if an Element have a stereotype. Use keyword and profile to find stereotype. Multiple
   * stereotype are allow.
   * 
   * @param elt
   *          Element used.
   * @param stereotype
   *          Stereotype to search.
   * 
   * @return true if found. false else.
   * 
   * @throws ENodeCastException
   *           the e node cast exception
   */
  static boolean hasStereotype(Element elt, String stereotype) {
    return getStereotype(elt, stereotype) != null;
  }

}
