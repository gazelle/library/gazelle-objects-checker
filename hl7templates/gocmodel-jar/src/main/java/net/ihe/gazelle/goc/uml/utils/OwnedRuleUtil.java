package net.ihe.gazelle.goc.uml.utils;

import java.util.List;

import net.ihe.gazelle.goc.xmm.OwnedComment;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.Specification;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <p>OwnedRuleUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public class OwnedRuleUtil {
	
	private static String reqIdentifier = "req";
	
	private static int number = 1;
	
	/**
	 * <p>Setter for the field <code>reqIdentifier</code>.</p>
	 *
	 * @param identifier a {@link java.lang.String} object.
	 */
	public static void setReqIdentifier(String identifier){
		OwnedRuleUtil.reqIdentifier = identifier;
	}
	
	/**
	 * <p>initOwnedRule.</p>
	 *
	 * @return a {@link net.ihe.gazelle.goc.xmm.OwnedRule} object.
	 */
	public static OwnedRule initOwnedRule(){
		OwnedRule ownedRule = new OwnedRule();
		ownedRule.setId(EcoreUtil.generateUUID());
		ownedRule.setName(createConstraintName());
		OwnedComment owc = new OwnedComment();
		ownedRule.setOwnedComment(owc);
		owc.setAnnotatedElement(ownedRule.getId());
		ownedRule.setSpecification(new Specification());
		ownedRule.getSpecification().setType("uml:OpaqueExpression");
		ownedRule.getSpecification().setId(EcoreUtil.generateUUID());
		ownedRule.getSpecification().setName(ownedRule.getName()+ "_body");
		ownedRule.getSpecification().setLanguage("OCL");
		return ownedRule;
	}

	/**
	 * <p>createConstraintName.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	protected static String createConstraintName() {
		String res = reqIdentifier;
		int numb = OwnedRuleUtil.number++;
		if (numb<10) {
			res = res + "00";
		}
		else if(numb<100) {
			res = res + "0";
		}
		res = res + numb;
		return res;
	}
	
	/**
	 * <p>containStringDescription.</p>
	 *
	 * @param lor a {@link java.util.List} object.
	 * @param str a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean containStringDescription(List<OwnedRule> lor, String str) {
		if (lor != null && str != null) {
			for (OwnedRule ownedRule : lor) {
				if (ownedRule != null && ownedRule.getOwnedComment() != null && ownedRule.getOwnedComment().getBody() != null && 
						ownedRule.getOwnedComment().getBody().contains(str)) {
					return true;
				}
			}
		}
		return false;
	}

}
