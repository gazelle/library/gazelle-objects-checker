package net.ihe.gazelle.goc.uml.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.PackageableElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;

/**
 * <p>CDAClassesMap class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class CDAClassesMap {


    private static Logger log = LoggerFactory.getLogger(CDAClassesMap.class);

    private static List<ModelDescriber> listModelDescribers = ModelDescriberInitializer.initializeListModelDescriber();

    private CDAClassesMap() {
    }

    /**
     * <p>findElementNameById.</p>
     *
     * @param id a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String findElementNameById(String id) {
        if (id != null) {
            for (ModelDescriber modelDescriber : listModelDescribers) {
                for (Entry<String, String> nameAndId : modelDescriber.getMapListUMLElementsToUMLId().entrySet()) {
                    if (nameAndId.getValue().equals(id)) {
                        return nameAndId.getKey();
                    }
                }
            }
        }
        return null;
    }

    /**
     * <p>getIIXMIMappingForElementImport.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public static List<String> getIIXMIMappingForElementImport() {
        List<String> res = new ArrayList<>();
        for (ModelDescriber modelDescriber : listModelDescribers) {
            if (modelDescriber.getListTemplateIdentifierUMLId() != null && !modelDescriber.getListTemplateIdentifierUMLId().isEmpty()) {
                res.add(modelDescriber.getRelativeXMIPath() + "#" + modelDescriber.getListTemplateIdentifierUMLId().get(0));
            }
        }
        return res;
    }

    static Map<String, String> initializeMapFromEListPackageableElements(
            Map<String, String> mapListVOCToUMLId2,
            EList<PackageableElement> elpack) {
        if (elpack == null) {
            log.info("CDAClassesMap::initializeMapForModel() the model identifier is not recognized");
            return null;
        }
        for (PackageableElement packageableElement : elpack) {
            if (packageableElement instanceof Enumeration || packageableElement instanceof org.eclipse.uml2.uml.Class) {
                mapListVOCToUMLId2.put(packageableElement.getName(), EcoreUtil.getURI(packageableElement).fragment());
            } else if (packageableElement instanceof org.eclipse.uml2.uml.Package) {
                org.eclipse.uml2.uml.Package pack = (org.eclipse.uml2.uml.Package) packageableElement;
                initializeMapFromEListPackageableElements(mapListVOCToUMLId2, pack.getPackagedElements());
                mapListVOCToUMLId2.put(packageableElement.getName(), EcoreUtil.getURI(packageableElement).fragment());
            }
        }
        return mapListVOCToUMLId2;
    }

    /**
     * <p>getRelativeXMILink.</p>
     *
     * @param element a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getRelativeXMILink(String element) {
        for (ModelDescriber modelDescriber : listModelDescribers) {
            if (modelDescriber.getMapListUMLElementsToUMLId().get(element) != null) {
                return modelDescriber.getRelativeXMIPath() + "#" + modelDescriber.getMapListUMLElementsToUMLId().get(element);
            }
        }
        return null;
    }

    /**
     * <p>getXMIType.</p>
     *
     * @param element a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getXMIType(String element) {
        if (element != null) {
            if (UMLLoader.isEnumeration(element)) {
                return "uml:Enumeration";
            } else if (UMLLoader.isUMLClass(element)) {
                return "uml:Class";
            }
        }
        return null;
    }

    /**
     * <p>getPackagedElements.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link org.eclipse.emf.common.util.EList} object.
     */
    public static EList<PackageableElement> getPackagedElements(String identifier) {
        for (ModelDescriber modelDescriber : listModelDescribers) {
            if (modelDescriber.getIdentifier().equals(identifier)) {
                return modelDescriber.getModelUML().getPackagedElements();
            }
        }
        return null;
    }

    /**
     * <p>lookForPackageableElementFromName.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link org.eclipse.uml2.uml.PackageableElement} object.
     */
    public static PackageableElement lookForPackageableElementFromName(String name) {
        if (name == null) {
            return null;
        }
        for (ModelDescriber modelDescriber : listModelDescribers) {
            Model modelDesc = modelDescriber.getModelUML();
            PackageableElement pe = findPackageableElement(modelDesc, name);
            if (pe != null) {
                return pe;
            }
        }
        return null;
    }

    /**
     * <p>lookForPackageableElementFromName.</p>
     *
     * @param name        a {@link java.lang.String} object.
     * @param packageName string
     * @return a {@link org.eclipse.uml2.uml.PackageableElement} object.
     */
    public static PackageableElement lookForPackageableElementFromName(String name, String packageName) {
        if (name == null || packageName == null) {
            return null;
        }
        for (ModelDescriber modelDescriber : listModelDescribers) {
            Model modelDesc = modelDescriber.getModelUML();
            PackageableElement pe = findPackageableElement(modelDesc, name);
            if (pe != null && pe.getModel().getName().equals(packageName)) {
                return pe;
            }
        }
        return null;
    }

    private static PackageableElement findPackageableElement(org.eclipse.uml2.uml.Package modelDesc, String name) {
        PackageableElement pe = modelDesc.getPackagedElement(name);
        if (pe != null) {
            return pe;
        }
        for (PackageableElement pel : modelDesc.getPackagedElements()) {
            if (pel instanceof org.eclipse.uml2.uml.Package) {
                org.eclipse.uml2.uml.Package pack = (org.eclipse.uml2.uml.Package) pel;
                pe = findPackageableElement(pack, name);
                if (pe != null) {
                    return pe;
                }
            }
        }
        return null;
    }

}
