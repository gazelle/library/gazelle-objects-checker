package net.ihe.gazelle.goc.uml.utils;

import net.ihe.gazelle.goc.template.definer.model.Taml;

/**
 * <p>TAMLUtil class.</p>
 *
 * @author Abderrazek Boufahja
 * @version $Id: $Id
 */
public final class TAMLUtil {
	
	private TAMLUtil() {}
	
	/**
	 * <p>createTAML.</p>
	 *
	 * @param baseConstraint a {@link java.lang.String} object.
	 * @param labelsComplete a {@link java.lang.String} object.
	 * @param targetIDScheme a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.goc.template.definer.model.Taml} object.
	 */
	public static Taml createTAML(String baseConstraint, String labelsComplete, String targetIDScheme) {
		Taml taml = new Taml();
		taml.setBaseConstraint(baseConstraint);
		taml.setIDs(labelsComplete);
		taml.setTargetIDScheme(targetIDScheme);
		return taml;
	}

}
