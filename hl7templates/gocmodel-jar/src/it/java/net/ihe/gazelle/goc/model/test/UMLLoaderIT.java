package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import java.util.Set;

import org.eclipse.uml2.uml.PackageableElement;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class UMLLoaderIT {

	@Test
	public void testGetPropertyTypeNameString1() {
		String path = "POCDMT000040Section.templateId";
		String prop = UMLLoader.getPropertyTypeName(path);
		assertTrue(prop.equals("II"));
		path = "POCDMT000040Section";
		prop = UMLLoader.getPropertyTypeName(path);
		System.out.println("prop = " + prop);
		assertTrue(prop.equals("POCDMT000040Section"));
		path = "POCDMT000040Observation";
		prop = UMLLoader.getPropertyTypeName(path);
		System.out.println("prop = " + prop);
		assertTrue(prop.equals("POCDMT000040Observation"));
	}
	@Test
	public void testGetPropertyTypeNameString2() {
		String path = "POCDMT000040Section.templateId.extension";
		String prop = UMLLoader.getPropertyTypeName(path);
		assertTrue(prop.equals("String"));
		path = "POCDMT000040Section.templateId.root";
		prop = UMLLoader.getPropertyTypeName(path);
		assertTrue(prop.equals("String"));
		path = "II.displayable";
		prop = UMLLoader.getPropertyTypeName(path);
		assertTrue(prop.equals("Boolean"));
		path = "BXITCD.qty";
		prop = UMLLoader.getPropertyTypeName(path);
		assertTrue(prop.equals("Integer"));
		path = "PQ.value";
		prop = UMLLoader.getPropertyTypeName(path);
		assertTrue(prop.equals("Double"));
		System.out.println("prop = "+ prop);
	}

	@Test
	public void testGetPropertyTypeNameClassString() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetMaxAttributeStringString() {
		int upper = UMLLoader.getMaxAttribute("POCDMT000040RecordTarget", "patientRole");
		assertTrue(upper == 1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040PatientRole", "id");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040PatientRole", "addr");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040RecordTarget.patientRole", "id");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040RecordTarget", "typeCode");
		assertTrue(upper == 1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040PatientRole", "classCode");
		assertTrue(upper == 1);
		upper = UMLLoader.getMaxAttribute("EN", "use");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute(null, "use");
		assertTrue(upper == 0);
		upper = UMLLoader.getMaxAttribute("AA", "use");
		assertTrue(upper == 0);
	}

	@Test
	public void testGetMinAttributeStringString() {
		int lower = UMLLoader.getMinAttribute("POCDMT000040RecordTarget", "patientRole");
		assertTrue(lower == 1);
		lower = UMLLoader.getMinAttribute("POCDMT000040PatientRole", "id");
		assertTrue(lower == 1);
		lower = UMLLoader.getMinAttribute("POCDMT000040PatientRole", "addr");
		assertTrue(lower == 0);
		lower = UMLLoader.getMinAttribute("POCDMT000040RecordTarget.patientRole", "id");
		assertTrue(lower == 1);
		lower = UMLLoader.getMinAttribute("POCDMT000040PatientRole", "classCode");
		assertTrue(lower == 0);
		lower = UMLLoader.getMinAttribute(null, "classCode");
		assertTrue(lower == 0);
		lower = UMLLoader.getMinAttribute("AA", "classCode");
		assertTrue(lower == 0);
	}

	@Test
	public void testGetMinAttributeClassString() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetMaxAttributeClassString() {
//		fail("Not yet implemented");
	}
	
	@Test
	public void testCheckIfEnumerationContainsValue() throws Exception {
		boolean check = UMLLoader.checkIfEnumerationContainsValue("XActClassDocumentEntryAct", "SPLY");
		assertFalse(check);
		check = UMLLoader.checkIfEnumerationContainsValue("XActClassDocumentEntryAct", "ACT");
		assertTrue(check);
		check = UMLLoader.checkIfEnumerationContainsValue("ActMood", "EVN.CRT");
		assertTrue(check);
		check = UMLLoader.checkIfEnumerationContainsValue("ActMood", "EVNCRT");
		assertFalse(check);
	}
	
	@Test
	public void testGetUMLNameForLitteral() throws Exception {
		String check = UMLLoader.getUMLNameForLitteral("XActClassDocumentEntryAct", "SPLY");
		assertEquals(check, null);
		check = UMLLoader.getUMLNameForLitteral("XActClassDocumentEntryAct", "ACT");
		assertEquals(check, "ACT");
		check = UMLLoader.getUMLNameForLitteral("ActMood", "EVN.CRT");
		assertEquals(check, "EVNCRT");
		check = UMLLoader.getUMLNameForLitteral("ActMood", "EVNCRT");
		assertNull(check);
	}
	
	@Test
	public void testGetMaxAttributeString() throws Exception {
		int upper = UMLLoader.getMaxAttribute("POCDMT000040RecordTarget.patientRole");
		assertTrue(upper == 1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040PatientRole.id");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040PatientRole.addr");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040RecordTarget.patientRole.id");
		assertTrue(upper == -1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040RecordTarget.typeCode");
		assertTrue(upper == 1);
		upper = UMLLoader.getMaxAttribute("POCDMT000040PatientRole.classCode");
		assertTrue(upper == 1);
		upper = UMLLoader.getMaxAttribute("EN", "use");
		assertTrue(upper == -1);
	}
	
	@Test
	public void testVerifyTheParentOfType1IsType2() throws Exception {
		assertTrue(UMLLoader.verifyTheParentOfType1IsType2("AD", "ANY"));
		assertTrue(UMLLoader.verifyTheParentOfType1IsType2("TEL", "URL"));
		assertTrue(UMLLoader.verifyTheParentOfType1IsType2("ST", "ED"));
		assertTrue(UMLLoader.verifyTheParentOfType1IsType2("IVLTS", "ANY"));
		assertFalse(UMLLoader.verifyTheParentOfType1IsType2("IVLTS", "ANYsss"));
		assertFalse(UMLLoader.verifyTheParentOfType1IsType2("aaaa", "ANY"));
		assertFalse(UMLLoader.verifyTheParentOfType1IsType2(null, null));
		assertFalse(UMLLoader.verifyTheParentOfType1IsType2("AD", null));
		assertFalse(UMLLoader.verifyTheParentOfType1IsType2("NullFlavor", "AD"));
	}
	
	@Test
	public void testGetListPropertiesNames() throws Exception {
		Set<String> ls = UMLLoader.getListPropertiesNames("AD");
		assertTrue(ls.contains("buildingNumberSuffix"));
		assertTrue(!ls.contains("_parent"));
		ls = UMLLoader.getListPropertiesNames("POCDMT000040Act");
		assertTrue(ls.contains("classCode"));
		assertTrue(ls.contains("statusCode"));
	}
	
	@Test
	public void testGetListPropertiesNamesWithoutEAttributes() throws Exception {
		Set<String> ls = UMLLoader.getListPropertiesNamesWithoutEAttributes("POCDMT000040Act");
		assertTrue(ls.contains("id"));
		assertFalse(ls.contains("classCode"));
	}
	
	@Test
	public void testLookForPackageableElementFromName() throws Exception {
		PackageableElement aa = UMLLoader.lookForPackageableElementFromName("AD");
		assertTrue(aa != null);
		aa = UMLLoader.lookForPackageableElementFromName("AAAAAAAAA");
		assertTrue(aa == null);
	}
	
	@Test
	public void testLookForPackageableElementFromName2() throws Exception {
		PackageableElement aa = UMLLoader.lookForPackageableElementFromName("AD", "datatypes");
		assertTrue(aa != null);
	}
	
	@Test
	public void testIsEnumeration() throws Exception {
		assertTrue(UMLLoader.isEnumeration("POCDMT000040ClinicalDocument.classCode"));
		assertTrue(UMLLoader.isEnumeration("SetOperator"));
		assertTrue(UMLLoader.isEnumeration("SXCMTS.operator"));
		assertFalse(UMLLoader.isEnumeration("IVLTS"));
		assertFalse(UMLLoader.isEnumeration("IVLTS.high"));
		assertTrue(UMLLoader.isEnumeration("BinaryDataEncoding"));
	}
	
	@Test
	public void testGetDefaultValue() throws Exception {
		assertTrue(UMLLoader.getDefaultValue("POCDMT000040EntryRelationship", "contextConductionInd").equals("true"));
		assertTrue(UMLLoader.getDefaultValue(null, null) == null);
		assertTrue(UMLLoader.getDefaultValue("NullFlavor", "value") == null);
	}
	
	@Test
	public void testIsUMLClass() throws Exception {
		assertFalse(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument.classCode"));
		assertTrue(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument.id"));
		assertTrue(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument"));
		assertTrue(UMLLoader.isUMLClass("AD"));
		assertFalse(UMLLoader.isUMLClass("AAAAAAAAAAAA"));
	}
	
	@Test
	public void testIsAnAttribute() throws Exception {
		assertTrue(UMLLoader.isAnAttribute("POCDMT000040ClinicalDocument.classCode"));
		assertTrue(UMLLoader.isAnAttribute("POCDMT000040ClinicalDocument.id.root"));
		assertTrue(UMLLoader.isAnAttribute("POCDMT000040ClinicalDocument.code.code"));
		assertTrue(UMLLoader.isAnAttribute("CD.code"));
		assertFalse(UMLLoader.isAnAttribute("POCDMT000040ClinicalDocument.id"));
		assertFalse(UMLLoader.isAnAttribute("POCDMT000040ClinicalDocument"));
		assertFalse(UMLLoader.isAnAttribute("AD"));
		assertFalse(UMLLoader.isAnAttribute("AAAAAAAAAAAA"));
		assertFalse(UMLLoader.isAnAttribute(null));
		assertFalse(UMLLoader.isAnAttribute(""));
	}
	
	@Test
	public void testDatatypeHasMixedElements() throws Exception {
		assertTrue(UMLLoader.datatypeHasMixedElements("BIN"));
		assertTrue(UMLLoader.datatypeHasMixedElements("EN"));
		assertTrue(UMLLoader.datatypeHasMixedElements("ST"));
		assertTrue(UMLLoader.datatypeHasMixedElements("AD"));
		assertTrue(UMLLoader.datatypeHasMixedElements("ED"));
		assertFalse(UMLLoader.datatypeHasMixedElements("II"));
		assertFalse(UMLLoader.datatypeHasMixedElements("ANY"));
		assertFalse(UMLLoader.datatypeHasMixedElements("TEL"));
		assertFalse(UMLLoader.datatypeHasMixedElements(null));
		assertFalse(UMLLoader.datatypeHasMixedElements("AAAAAA"));
		assertFalse(UMLLoader.datatypeHasMixedElements(""));
	}
	
	@Test
	public void testIsUMLClass2() throws Exception {
		assertFalse(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument.id.root"));
		assertFalse(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument.id.rrrrrrr"));
		assertTrue(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument.component.structuredBody.component.section.id"));
		assertFalse(UMLLoader.isUMLClass("POCDMT000040ClinicalDocument.component.structuredBody.component.section.id.root"));
	}
	
	@Test
	public void testIsEnumeration2() throws Exception {
		assertFalse(UMLLoader.isEnumeration("POCDMT000040ClinicalDocument.id.root"));
		assertTrue(UMLLoader.isEnumeration("POCDMT000040ClinicalDocument.component.structuredBody.component.section.classCode"));
	}

}
