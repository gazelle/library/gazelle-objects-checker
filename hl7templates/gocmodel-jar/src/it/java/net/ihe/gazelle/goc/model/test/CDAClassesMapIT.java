package net.ihe.gazelle.goc.model.test;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.PackageableElement;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.CDAClassesMap;

import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class CDAClassesMapIT {

	@Test
	public void testFindElementNameById() {
		String name = CDAClassesMap.findElementNameById("_DFpnZq70EeGxJei_o6JmIA");
		assertTrue(name.equals("ActClassCondition"));
		name = CDAClassesMap.findElementNameById("_DFpncK70EeGxJei_o6JmIA");
		assertTrue(name.equals("ActClassContract"));
		name = CDAClassesMap.findElementNameById("_2lqjIK-1EeGe86G5-WRfGA");
		assertTrue(name.equals("CommonOperations"));
		name = CDAClassesMap.findElementNameById("_-hdEwB_sEeWrn8bQrW4qmA");
		assertTrue(name.equals("DatatypesUtil"));
		name = CDAClassesMap.findElementNameById("_DALSyq70EeGxJei_o6JmIA");
		assertTrue(name.equals("POCDMT000040Component5"));
		name = CDAClassesMap.findElementNameById("_CyjsXq70EeGxJei_o6JmIA");
		assertTrue(name.equals("StrucDocBr"));
	}
	
//	@Test
//	public void testInitializeMapFromModel0() throws Exception {
//		Map<String, String> mapListdtToUMLId2 = new THashMap<String, String>();
//		CDAClassesMap.initializeMapFromModel(mapListdtToUMLId2, "datatypes");
//		String name = CDAClassesMap.findElementNameById("_CuDm1q70EeGxJei_o6JmIA");
//		assertTrue(name.equals("ANY"));
//	}
	
//	@Test
//	public void testInitializeMapFromModel1() throws Exception {
//		Map<String, String> mapListVOCToUMLId = new THashMap<String, String>();
//		mapListVOCToUMLId .put("ActClass", "_DFpnFa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassComposition", "_DFpnW670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassCondition", "_DFpnZq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassContract", "_DFpncK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassControlAct", "_DFpneq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassDocument", "_DFpnha70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassEntry", "_DFpni670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassExtract", "_DFpnla70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassFinancialContract", "_DFpnnq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassObservation", "_DFpnqa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassObservationSeries", "_DFpnuq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassOrganizer", "_DFpnw670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassPublicHealthCase", "_DFpnz670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassROI", "_DFpn2K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassRoot", "_DFpn4a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClassSupply", "_DFpoI670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActClinicalDocument", "_DFpoKK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActContainer", "_DFpoLa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActMood", "_DFpoQ670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActMoodCompletionTrack", "_DFpoUq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActMoodIntent", "_DFpoYq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActMoodPredicate", "_DFpocq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipAccounting", "_DFpofq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipConditional", "_DFpoia70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipCostTracking", "_DFpola70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipExcerpt", "_DFponq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipFulfills", "_DFpop670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipHasComponent", "_DFporq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipObjective", "_DFpota70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipOutcome", "_DFpovq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipPertains", "_DFpoyq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipPosting", "_DFpo5670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipReason", "_DFpo8K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipReplacement", "_DFpo-a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipSequel", "_DFppAq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ActRelationshipType", "_DFppHa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("AdditionalLocator", "_DFqOKK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("AddressPartType", "_DFqOMq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("AddressUse", "_DFqOUK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ApplicationMediaType", "_DFqOXq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("AskedButUnknown", "_DFqOaK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("AudioMediaType", "_DFqOca70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("BuildingNumber", "_DFqOe670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("CalendarCycle", "_DFqOha70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("CalendarCycleOneLetter", "_DFqOn670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("CalendarCycleTwoLetter", "_DFqOrq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("CommunicationFunctionType", "_DFqOxq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("CompressionAlgorithm", "_DFqO0K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ContextControl", "_DFqO1670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ContextControlAdditive", "_DFqO3K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ContextControlNonPropagating", "_DFqO5a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ContextControlOverriding", "_DFqO8K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ContextControlPropagating", "_DFqO-a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("Currency", "_DFqPAq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("DeliveryAddressLine", "_DFqPJK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClass", "_DFqPMa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassContainer", "_DFqPTa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassDevice", "_DFqPVq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassLivingSubject", "_DFqPXK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassManufacturedMaterial", "_DFqPaa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassMaterial", "_DFqPcq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassNonPersonLivingSubject", "_DFqPgq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassOrganization", "_DFqPj670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassPlace", "_DFqPlq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityClassRoot", "_DFqPnq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityDeterminer", "_DFqPvK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityDeterminerDetermined", "_DFqPwK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityNamePartQualifier", "_DFqPx670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityNamePartType", "_DFqP1a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityNameSearchUse", "_DFqP3a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("EntityNameUse", "_DFqP5670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("HasSupport", "_DFqP_a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("HomeAddressUse", "_DFqQBq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ImageMediaType", "_DFqQEK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("IntegrityCheckAlgorithm", "_DFqQHK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("LicensedEntityRole", "_DFqQIa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("MediaType", "_DFqQK670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ModelMediaType", "_DFqQR670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("MultipartMediaType", "_DFqQT670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("NamePseudonymUse", "_DFqQV670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("NameRepresentationUse", "_DFqQYK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("NoInformation", "_DFqQaq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("OrganizationNamePartQualifier", "_DFqQfq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("OrganizationNameUse", "_DFqQhq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("Other", "_DFqQla70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationAncillary", "_DFqQn670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationIndirectTarget", "_DFq1N670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationInformationGenerator", "_DFq1RK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationInformationRecipient", "_DFq1T670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationPhysicalPerformer", "_DFq1XK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationTargetDevice", "_DFq1Y670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationTargetDirect", "_DFq1ba70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationTargetLocation", "_DFq1fq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationTargetSubject", "_DFq1h670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationType", "_DFq1jK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ParticipationVerifier", "_DFq1v670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("PersonNamePartAffixTypes", "_DFq1ya70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("PersonNamePartChangeQualifier", "_DFq11K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("PersonNamePartMiscQualifier", "_DFq13q70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("PersonNamePartQualifier", "_DFq15q70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("PersonNameUse", "_DFq19670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("PostalAddressUse", "_DFq2Dq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("ProbabilityDistributionType", "_DFq2Hq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RelatedLinkType", "_DFq2Lq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClass", "_DFq2O670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassAgent", "_DFq2ha70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassAssignedEntity", "_DFq2lK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassAssociative", "_DFq2na70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassContact", "_DFq20a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassDistributedMaterial", "_DFq22670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassEmployee", "_DFq25K70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassInactiveIngredient", "_DFq27a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassIngredientEntity", "_DFq2-a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassInvestigationSubject", "_DFq3Cq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassIsSpeciesEntity", "_DFq3FK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassLocatedEntity", "_DFq3Ha70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassManufacturedProduct", "_DFq3Jq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassMutualRelationship", "_DFq3K670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassOntological", "_DFq3T670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassPartitive", "_DFq3W670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassPassive", "_DFq3dK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassRelationshipFormal", "_DFq3ja70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassRoot", "_DFq3sa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassServiceDeliveryLocation", "_DFrcWq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleClassSpecimen", "_DFrcYK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("RoleLinkType", "_DFrcZq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("SetOperator", "_DFrcc670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("State", "_DFrce670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("StreetAddressLine", "_DFrchK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("StreetName", "_DFrck670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("TelecommunicationAddressUse", "_DFrcna70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("TemporallyPertains", "_DFrcrK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("TextMediaType", "_DFrctK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("TimingEvent", "_DFrcwa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("Unknown", "_DFrc0a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("URLScheme", "_DFrc3a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("VideoMediaType", "_DFrc7q70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("WorkPlaceAddressUse", "_DFrc9670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActClassDocumentEntryAct", "_DFrdAa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActClassDocumentEntryOrganizer", "_DFrdDa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodDefEvn", "_DFrdEq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodDefEvnRqoPrmsPrp", "_DFrdG670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodDocumentObservation", "_DFrdJ670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodEvnOrdPrmsPrp", "_DFrdMa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodIntentEvent", "_DFrdPK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodOrdPrms", "_DFrdS670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodOrdPrmsEvn", "_DFrdUq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActMoodRqoPrpAptArq", "_DFrdXq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActRelationshipDocument", "_DFrdaa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActRelationshipEntry", "_DFrdb670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActRelationshipEntryRelationship", "_DFrddK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActRelationshipExternalReference", "_DFrdga70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActRelationshipPatientTransport", "_DFrdiq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XActRelationshipPertinentInfo", "_DFrdk670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDeterminerInstanceKind", "_DFrdn670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDocumentActMood", "_DFrdqK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDocumentEncounterMood", "_DFrds670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDocumentEntrySubject", "_DFrdva70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDocumentProcedureMood", "_DFrdx670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDocumentSubject", "_DFrd0q70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XDocumentSubstanceMood", "_DFrd1670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XEncounterParticipant", "_DFrd3670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XEncounterPerformerParticipation", "_DFrd5670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XEntityClassDocumentReceiving", "_DFrd8a70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XEntityClassPersonOrOrgReceiving", "_DFrd_q70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XInformationRecipient", "_DFreCq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XInformationRecipientRole", "_DFreD670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XOrganizationNamePartType", "_DFreFK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XParticipationAuthorPerformer", "_DFreHq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XParticipationEntVrf", "_DFreJ670EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XParticipationPrfEntVrf", "_DFreMK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XParticipationVrfRespSprfWit", "_DFreOq70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XPersonNamePartType", "_DFreRa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XRoleClassAccommodationRequestor", "_DFreUa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XRoleClassCoverage", "_DFreXK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XRoleClassCoverageInvoice", "_DFreZa70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XRoleClassCredentialedEntity", "_DFrecK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XRoleClassPayeePolicyRelationship", "_DFrefK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("XServiceEventPerformer", "_DFreiK70EeGxJei_o6JmIA");
//		mapListVOCToUMLId.put("NullFlavor", "_DFrejq70EeGxJei_o6JmIA");
//		Map<String, String> mapListVOCToUMLId2 = new THashMap<String, String>();
//		CDAClassesMap.initializeMapFromModel(mapListVOCToUMLId2, "voc");
//		assertTrue(mapListVOCToUMLId2.size() == 181);
//		boolean containAll = true;
//		for (String str : mapListVOCToUMLId.keySet()) {
//			if (!mapListVOCToUMLId2.containsKey(str)) {
//				containAll = false;
//			}
//		}
//		assertTrue(containAll);
//	}
	
//	@Test
//	public void testInitializeMapFromModel2() throws Exception {
//		Map<String, String> mapListNBlockToUMLId = new THashMap<String, String>();
//		mapListNBlockToUMLId .put("StrucDocBr", "_CyjsXq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocCaption", "_CyjsX670EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocLinkHtml", "_Cyjse670EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocFootnote", "_CyjsmK70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocContent", "_CyjswK70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocSub", "_CykTGa70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocSup", "_CykTH670EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocFootnoteRef", "_CykTKa70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocRenderMultiMedia", "_CykTM670EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocParagraph", "_CykTTa70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocList", "_CykTiK70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocItem", "_CykTl670EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTable", "_CykT3a70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocCol", "_CykUAq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocColgroup", "_CykUHK70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocThead", "_CykUPq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTr", "_CykUUq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTh", "_CykUaq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTd", "_CykUuq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTfoot", "_CykVIq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTbody", "_Cyk6EK70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocText", "_Cyk6Q670EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTitle", "_Cyk6ia70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTitleContent", "_Cyk6qq70EeGxJei_o6JmIA");
//		mapListNBlockToUMLId.put("StrucDocTitleFootnote", "_Cyk60a70EeGxJei_o6JmIA");
//		Map<String, String> mapListVOCToUMLId2 = new THashMap<String, String>();
//		CDAClassesMap.initializeMapFromModel(mapListVOCToUMLId2, "nblock");
//		boolean containAll = true;
//		for (String str : mapListNBlockToUMLId.keySet()) {
//			if (!mapListVOCToUMLId2.containsKey(str)) {
//				containAll = false;
//			}
//		}
//		assertTrue(containAll);
//	}
	
//	@Test
//	public void testInitializeMapFromModel3() throws Exception {
//		Map<String, String> mapListdtToUMLId2 = new THashMap<String, String>();
//		CDAClassesMap.initializeMapFromModel(mapListdtToUMLId2, "cda");
//		String name = CDAClassesMap.findElementNameById("_DAHnUq70EeGxJei_o6JmIA");
//		assertTrue(name.equals("POCDMT000040PatientRole"));
//		System.out.println(mapListdtToUMLId2);
//	}
	
	@Test
	public void testGetIIXMIMappingForElementImport() throws Exception {
		List<String> iiref = CDAClassesMap.getIIXMIMappingForElementImport();
		assertTrue(iiref.contains("../../datatypes-model/models/datatypes.uml#_CuEO9q70EeGxJei_o6JmIA"));
	}
	
	@Test
	public void testLookForPackageableElementFromName() throws Exception {
		PackageableElement aa = CDAClassesMap.lookForPackageableElementFromName("AD");
		assertTrue(aa != null);
		aa = CDAClassesMap.lookForPackageableElementFromName("AAAAAAAAA");
		assertTrue(aa == null);
		aa = CDAClassesMap.lookForPackageableElementFromName(null);
		assertTrue(aa == null);
	}
	
	@Test
	public void testLookForPackageableElementFromName2() throws Exception {
		PackageableElement aa = CDAClassesMap.lookForPackageableElementFromName("CommonOperationsStatic");
		assertTrue(aa != null);
	}
	
	@Test
	public void testGetXMIType() throws Exception {
		assertTrue(CDAClassesMap.getXMIType("ActClass").equals("uml:Enumeration"));
		assertTrue(CDAClassesMap.getXMIType("II").equals("uml:Class"));
		assertTrue(CDAClassesMap.getXMIType("aa") == null);
		assertTrue(CDAClassesMap.getXMIType(null) == null);
	}
	
	@Test
	public void testGetPackagedElements1() throws Exception {
		EList<PackageableElement> aa = CDAClassesMap.getPackagedElements("datatypes");
		boolean contrainsII = false;
		for (PackageableElement packageableElement : aa) {
			if (packageableElement.getName() != null && packageableElement.getName().equals("II")) {
				contrainsII = true;
			}
		}
		assertTrue(contrainsII);
	}
	
	@Test
	public void testGetPackagedElements2() throws Exception {
		EList<PackageableElement> aa = CDAClassesMap.getPackagedElements("aa");
		assertTrue(aa == null);
	}
	
	@Test
	public void testGetRelativeXMILink() throws Exception {
		assertNull(CDAClassesMap.getRelativeXMILink(null));
		String aa = CDAClassesMap.getRelativeXMILink("II");
		assertTrue(aa.equals("../../datatypes-model/models/datatypes.uml#_CuEO9q70EeGxJei_o6JmIA"));
		aa = CDAClassesMap.getRelativeXMILink("NullFlavor");
		assertTrue(aa.equals("../../voc-model/models/voc.uml#_DFrejq70EeGxJei_o6JmIA"));
	}

}
