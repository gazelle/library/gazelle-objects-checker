package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;

public class UmlServicesIT {

  @Test
  public void testGetStereotypeValue() {
    assertTrue(UMLLoader.datatypeHasMixedElements("BIN"));
  }

}
