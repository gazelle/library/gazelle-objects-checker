package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import java.util.List;

import net.ihe.gazelle.goc.uml.utils.ModelDescriberInitializer;
import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;
import net.ihe.gazelle.goc.umlmodel.util.ModelDescriberUtil;

import org.junit.Test;

public class ModelDescriberUtilIT {

	@Test
	public void testGetModelDescriberByIdentifier1() {
		String pathResources = "src/test/resources/testModels1.xml";
		List<ModelDescriber> aa = ModelDescriberInitializer.initializeListModelDescriber(pathResources);
		assertTrue(ModelDescriberUtil.getModelDescriberByIdentifier("cda", aa) != null);
		
	}

}
