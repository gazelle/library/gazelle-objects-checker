package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedComment;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;

import org.junit.Test;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PackagedElementUtilIT {

	@Test
	public void testInitPackagedElement() {
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		assertTrue(pe != null);
		assertTrue(pe.getOwnedRule() != null);
		assertTrue(pe.getId() != null);
	}

	@Test
	public void testAddGeneralizationToPackagedElement() {
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		PackagedElementUtil.addGeneralizationToPackagedElement(pe, "POCDMT000040Act");
		assertTrue(pe.getGeneralization() != null);
		assertTrue(pe.getGeneralization().getGeneral() != null);
		assertTrue(pe.getGeneralization().getGeneral().getHref().equals(
				"../../cda-model/models/cda.uml#_DAJdBa70EeGxJei_o6JmIA"));
	}
	
	@Test
	public void testAddGeneralizationToPackagedElement2() {
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		PackagedElementUtil.addGeneralizationToPackagedElement(pe, "AD");
		assertTrue(pe.getGeneralization() != null);
		assertTrue(pe.getGeneralization().getGeneral() != null);
		assertTrue(pe.getGeneralization().getGeneral().getHref().equals(
				"../../datatypes-model/models/datatypes.uml#_CuDmcq70EeGxJei_o6JmIA"));
	}
	
	@Test
	public void testFindOwnedRulesByContent() throws Exception {
		PackagedElement currentPackagedElement = PackagedElementUtil.initPackagedElement();
		currentPackagedElement.getOwnedRule().add(new OwnedRule());
		currentPackagedElement.getOwnedRule().get(0).setOwnedComment(new OwnedComment());
		currentPackagedElement.getOwnedRule().get(0).getOwnedComment().setBody("test new");
		currentPackagedElement.getOwnedRule().add(new OwnedRule());
		currentPackagedElement.getOwnedRule().get(1).setOwnedComment(new OwnedComment());
		currentPackagedElement.getOwnedRule().get(1).getOwnedComment().setBody("test 2");
		assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "new").size()==1);
		assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "test").size()==2);
	}

}
