package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.ProfileApplication;

import org.junit.Test;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class XMIUtilIT extends XMIUtil {
	
	@Test
	public void testCreateXMI() {
		XMI xmi = createXMI("epsos");
		assertTrue(xmi.getEpackage().getPackageName().equals("net.ihe.gazelle.epsos"));
		assertTrue(xmi.getModel().getProfileApplication().size()==2);
		assertTrue(xmi.getModel().getElementImport().size()>0);
	}

	@Test
	public void testCreateEcoreProfileApplication() {
		ProfileApplication pa = createEcoreProfileApplication();
		assertTrue(pa != null);
	}

	@Test
	public void testCreateGOCProfileApplication() {
		ProfileApplication pg = createGOCProfileApplication();
		assertTrue(pg != null);
	}

}
