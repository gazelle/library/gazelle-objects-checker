package net.ihe.gazelle.goc.model.test;

import net.ihe.gazelle.goc.uml.utils.ModelDescriberInitializer;
import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;
import net.ihe.gazelle.goc.umlmodel.util.ModelDescriberUtil;

import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ModelDescriberInitializerIT {

	@Test
	public void testInitializeListModelDescriberString1() {
		String pathResources = "src/test/resources/testModels1.xml";
		List<ModelDescriber> aa = ModelDescriberInitializer.initializeListModelDescriber(pathResources);
		assertEquals(aa.size(), 5);
		
		ModelDescriber md = ModelDescriberUtil.getModelDescriberByIdentifier("common", aa);
		assertEquals("common", md.getIdentifier());
		assertTrue(md.getPath().contains("/common-models/models/common.uml"));
		assertEquals("../../common-models/models/common.uml", md.getRelativeXMIPath());
		assertEquals("CommonClasses", md.getModelUML().getName());
		assertEquals(0, md.getListTemplateIdentifierUMLId().size());
		assertNotNull(md.getMapListUMLElementsToUMLId().get("CommonOperationsStatic"));
		
		md = ModelDescriberUtil.getModelDescriberByIdentifier("datatypes", aa);
		assertEquals("datatypes", md.getIdentifier());
		assertTrue(md.getPath().contains("/datatypes-model/models/datatypes.uml"));
		assertEquals("../../datatypes-model/models/datatypes.uml", md.getRelativeXMIPath());
		assertEquals("datatypes", md.getModelUML().getName());
		assertEquals(1, md.getListTemplateIdentifierUMLId().size());
		assertNotNull(md.getMapListUMLElementsToUMLId().get("AD"));
	}


	//This test ignore because it's too specefic to cdaepsos
	@Ignore
	@Test
	public void testInitializeListModelDescriberString2() {
		String pathResources = "src/test/resources/testModels2.xml";
		List<ModelDescriber> aa = ModelDescriberInitializer.initializeListModelDescriber(pathResources);
		assertEquals(1, aa.size());
		assertEquals("medication", aa.get(0).getIdentifier());
		assertEquals("_VzV5mM86EeGVX9h3ivlQvw", aa.get(0).getMapListUMLElementsToUMLId().get("EntityClassMaterial2"));
	}

}
