package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;

public class UMLPrimitiveTypeTest {

	@Test
	public void testGetValue() {
		UMLPrimitiveType prim = UMLPrimitiveType.BOOLEAN;
		assertTrue(prim.getValue().equals("Boolean"));
	}

	@Test
	public void testListUMLPrimitives() {
		List<String> aa = UMLPrimitiveType.listUMLPrimitives();
		assertTrue(aa.contains("Boolean"));
	}

}
