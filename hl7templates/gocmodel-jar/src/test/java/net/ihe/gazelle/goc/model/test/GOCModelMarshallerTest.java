package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.goc.uml.utils.GOCModelMarshaller;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.PackagedElement;

import org.junit.Test;

public class GOCModelMarshallerTest {

	@Test
	public void testPrintGOCObject1() throws JAXBException {
		XMI xmi = new XMI();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GOCModelMarshaller.printGOCObject(xmi, baos);
		String[] listNamespace = baos.toString().split("\\s+");
		assertTrue(Arrays.asList(listNamespace).size() == 11);
		assertTrue(Arrays.asList(listNamespace).containsAll(Arrays.asList(new String[] {"<?xml", "version=\"1.0\"", "encoding=\"UTF-8\"", 
				"standalone=\"yes\"?>", "<xmi:XMI", "xmlns:uml=\"http://www.eclipse.org/uml2/3.0.0/UML\"", "xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\"", 
				"xmlns:TemplateDefiner=\"http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43\"", "xmlns:xmi=\"http://schema.omg.org/spec/XMI/2.1\"", 
				"xmlns:Ecore=\"http://www.eclipse.org/uml2/schemas/Ecore/5\"", "xmlns:ns7=\"xmm\"/>"})));
		
	}
	
	@Test
	public void testPrintGOCObject2() throws JAXBException {
		PackagedElement pe = new PackagedElement();
		pe.setName("test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GOCModelMarshaller.printGOCObject(pe, baos);
		String[] listNamespace = baos.toString().split("\\s+");
		assertTrue(Arrays.asList(listNamespace).size() == 12);
		System.out.println(baos.toString());
		assertTrue(Arrays.asList(listNamespace).containsAll(Arrays.asList(new String[] {"<?xml", "version=\"1.0\"", "encoding=\"UTF-8\"", "standalone=\"yes\"?>", 
				"<packagedElement", "xmlns:uml=\"http://www.eclipse.org/uml2/3.0.0/UML\"", "xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\"", 
				"xmlns:TemplateDefiner=\"http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43\"", "xmlns:xmi=\"http://schema.omg.org/spec/XMI/2.1\"", 
				"xmlns:Ecore=\"http://www.eclipse.org/uml2/schemas/Ecore/5\"", "xmlns:ns7=\"xmm\"", "name=\"test\"/>"})));
	}
	
	@Test
	public void testReadGOCObject() throws Exception {
		String xmi = "<xmi:XMI xmi:version=\"2.1\" "
				+ "xmlns:xmi=\"http://schema.omg.org/spec/XMI/2.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
				+ "xmlns:CDAProfiles=\"http:///schemas/CDAProfiles/_UilrcDPwEeKkMqTY7dIUfA/9\" "
				+ "xmlns:Ecore=\"http://www.eclipse.org/uml2/schemas/Ecore/5\" "
				+ "xmlns:TemplateDefiner=\"http:///schemas/TemplateDefiner/_GOoNYHwMEeKEDd8BBul2Tg/40\" "
				+ "xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\" xmlns:uml=\"http://www.eclipse.org/uml2/3.0.0/UML\" "
				+ "xsi:schemaLocation=\"http:///schemas/CDAProfiles/_UilrcDPwEeKkMqTY7dIUfA/9 "
				+ "../../common-models/models/cda-profiles.uml#_UingoDPwEeKkMqTY7dIUfA "
				+ "http://www.eclipse.org/uml2/schemas/Ecore/5 pathmap://UML_PROFILES/Ecore.profile.uml#_z1OFcHjqEdy8S4Cr8Rc_NA "
				+ "http:///schemas/TemplateDefiner/_GOoNYHwMEeKEDd8BBul2Tg/40 ../../common-models/models/common-profile.uml#_L1HqYLk9EeadT_oVto_uPg\"/>";
		XMI xmiobj = (XMI) GOCModelMarshaller.readGOCObject(new ByteArrayInputStream(xmi.getBytes()));
		assertTrue(xmiobj != null);
	}

}
