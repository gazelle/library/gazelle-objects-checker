package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import java.util.Arrays;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedComment;
import net.ihe.gazelle.goc.xmm.OwnedRule;

import org.junit.Test;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class OwnedRuleUtilTest extends OwnedRuleUtil {

	@Test
	public void testInitOwnedRule() {
		OwnedRuleUtil.setReqIdentifier("req");
		OwnedRule or = OwnedRuleUtil.initOwnedRule();
		assertTrue(or != null);
		assertTrue(or.getId() != null);
		assertTrue(or.getName().contains("req"));
	}
	
	@Test
	public void testContainStringDescription() throws Exception {
		OwnedRule or = OwnedRuleUtil.initOwnedRule();
		or.setOwnedComment(new OwnedComment());
		or.getOwnedComment().setBody("test 1");
		OwnedRule or2 = OwnedRuleUtil.initOwnedRule();
		or2.setOwnedComment(new OwnedComment());
		or2.getOwnedComment().setBody("test 2");
		assertTrue(OwnedRuleUtil.containStringDescription(Arrays.asList(new OwnedRule[] {or, or2}), "test"));
	}
	
	@Test
	public void testSetReqIdentifier() throws Exception {
		OwnedRuleUtil.setReqIdentifier("aaaa");
		OwnedRule or = OwnedRuleUtil.initOwnedRule();
		assertTrue(or.getName().indexOf("aaa") == 0);
	}
	
	@Test
	public void testCreateConstraintName() throws Exception {
		OwnedRuleUtil.setReqIdentifier("aaaa");
		String aa = createConstraintName();
		String bb = createConstraintName();
		int vala = Integer.parseInt(aa.substring(4));
		int valb = Integer.parseInt(bb.substring(4));
		assertTrue(vala-valb == (-1));
	}
	
	@Test
	public void testCreateConstraintName2() throws Exception {
		OwnedRuleUtil.setReqIdentifier("aaaa");
		String aa = "";
		for (int i=0;i<20;i++) {
			aa = createConstraintName();
		}
		System.out.println(aa);
		
		assertTrue(Integer.valueOf(aa.substring(4))<100);
		assertTrue(aa.length()==7);
	}

}
