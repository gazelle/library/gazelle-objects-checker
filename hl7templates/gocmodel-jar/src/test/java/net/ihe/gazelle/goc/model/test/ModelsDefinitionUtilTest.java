package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import net.ihe.gazelle.goc.umlmodel.model.ModelsDefinition;
import net.ihe.gazelle.goc.umlmodel.util.ModelsDefinitionUtil;

import org.junit.Test;

public class ModelsDefinitionUtilTest {

	@Test
	public void testUnmarshallModelsDefinition() {
		ModelsDefinition aa = ModelsDefinitionUtil.unmarshallModelsDefinition("src/test/resources/testModels1.xml");
		assertTrue(aa != null);
		assertTrue(aa.getUMLModelsDescription().getModelUMLDesc().size()>0);
	}

}
