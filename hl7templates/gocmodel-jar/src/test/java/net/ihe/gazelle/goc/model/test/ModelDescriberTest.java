package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import gnu.trove.map.hash.THashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.ParameterableElement;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StringExpression;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.junit.Test;

import net.ihe.gazelle.goc.umlmodel.model.ModelDescriber;

public class ModelDescriberTest {

	@Test
	public void testHashCode() {
		ModelDescriber md = new ModelDescriber();
		md.setIdentifier("datatypes");
		ModelDescriber md2 = new ModelDescriber();
		md2.setIdentifier("datatypes");
		assertTrue(md.hashCode() == md2.hashCode());
	}

	@Test
	public void testGetListTemplateIdentifierUMLId() {
		ModelDescriber md = new ModelDescriber();
		List<String> ll = new ArrayList<String>();
		md.setListTemplateIdentifierUMLId(ll);
		assertTrue(md.getListTemplateIdentifierUMLId() == ll);
	}

	@Test
	public void testGetModelUML() {
		ModelDescriber md = new ModelDescriber();
		assertTrue(md.getModelUML() == null);
	}

	@Test
	public void testGetPath() {
		ModelDescriber md = new ModelDescriber();
		md.setPath("/path");
		assertTrue(md.getPath().equals("/path"));
	}

	@Test
	public void testGetRelativeXMIPath() {
		ModelDescriber md = new ModelDescriber();
		md.setRelativeXMIPath("/path");
		assertTrue(md.getRelativeXMIPath().equals("/path"));
	}

	@Test
	public void testGetIdentifier() {
		ModelDescriber md = new ModelDescriber();
		md.setIdentifier("aa");
		assertTrue(md.getIdentifier().equals("aa"));
	}

	@Test
	public void testGetMapListUMLElementsToUMLId() {
		ModelDescriber md = new ModelDescriber();
		md.setMapListUMLElementsToUMLId(new THashMap<String, String>());
		md.getMapListUMLElementsToUMLId().put("aa", "bb");
		assertTrue(md.getMapListUMLElementsToUMLId().size()==1);
	}


	@Test
	public void testEqualsObject() {
		ModelDescriber md = new ModelDescriber();
		md.setRelativeXMIPath("/path");
		ModelDescriber md2 = new ModelDescriber();
		md2.setRelativeXMIPath("/path");
		assertTrue(md.equals(md2));
	}

}
