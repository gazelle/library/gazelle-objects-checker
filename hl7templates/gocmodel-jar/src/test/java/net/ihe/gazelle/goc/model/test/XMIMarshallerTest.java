package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.goc.template.definer.model.TemplateSpec;
import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goc.xmm.PackagedElement;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Test;

public class XMIMarshallerTest {

	@Test
	public void testLoadXMI() throws FileNotFoundException, JAXBException {
		XMI xmi = XMIMarshaller.loadXMI(new FileInputStream("src/test/resources/test.xmi"));
		assertTrue(xmi.getModel() != null);
		assertTrue(xmi.getModel().getPackagedElement().size()==1);
		assertTrue(xmi.getModel().getPackagedElement().get(0).getId().equals("test"));
	}

	@Test
	public void testPrintXMI() throws JAXBException {
		XMI xmi = new XMI();
		TemplateSpec ts = new TemplateSpec();
		ts.setId(EcoreUtil.generateUUID());
		xmi.getTemplateSpec().add(ts);
		Model model = new Model();
		xmi.setModel(model);
		PackagedElement pe = new PackagedElement();
		model.getPackagedElement().add(pe);
		pe.setId("test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		System.out.println(baos);
		assertTrue(baos.toString().contains("<packagedElement xmi:id=\"test\"/>"));
		assertTrue(baos.toString().contains("<TemplateDefiner:TemplateSpec id="));
	}
	
	@Test
	public void testPrintAndLoad() throws Exception {
		XMI xmi = new XMI();
		TemplateSpec ts = new TemplateSpec();
		ts.setId(EcoreUtil.generateUUID());
		xmi.getTemplateSpec().add(ts);
		Model model = new Model();
		xmi.setModel(model);
		PackagedElement pe = new PackagedElement();
		model.getPackagedElement().add(pe);
		pe.setId("test");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		XMI xmi2 = XMIMarshaller.loadXMI(new ByteArrayInputStream(baos.toByteArray()));
		assertTrue(xmi2.getModel().getPackagedElement().get(0).getId().equals("test"));
	}
	
	@Test
	public void testAddPossibleRuleType() throws Exception {
		XMI xmi = new XMI();
		Model model = new Model();
		xmi.setModel(model);
		PackagedElement pe = new PackagedElement();
		model.getPackagedElement().add(pe);
		pe.setId("test");
		OwnedRule orul = OwnedRuleUtil.initOwnedRule();
		orul.setName("toto");
		orul.setOwnedRuleType(OwnedRuleType.INFO);
		pe.getOwnedRule().add(orul);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		assertTrue(baos.toString().contains("<TemplateDefiner:ConstraintType"));
		assertTrue(baos.toString().contains("type=\"INFO\""));
	}
	
	@Test
	public void testAddPossibleRuleKind() throws Exception {
		XMI xmi = new XMI();
		Model model = new Model();
		xmi.setModel(model);
		PackagedElement pe = new PackagedElement();
		model.getPackagedElement().add(pe);
		pe.setId("test");
		OwnedRule orul = OwnedRuleUtil.initOwnedRule();
		orul.setName("toto");
		orul.setOwnedRuleKind(OwnedRuleKind.CARDINALITY);
		pe.getOwnedRule().add(orul);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		assertTrue(baos.toString().contains("<TemplateDefiner:ConstraintKind"));
		assertTrue(baos.toString().contains("kind=\"Cardinality\""));
	}

}
