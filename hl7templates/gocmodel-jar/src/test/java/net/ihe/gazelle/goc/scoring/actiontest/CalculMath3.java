package net.ihe.gazelle.goc.scoring.actiontest;

import java.util.Arrays;

import Jama.Matrix;



public class CalculMath3 {
	
	public static void main(String[] args) {
		double[][] containment = {
				{0, 1, 0, 0, 0, 0, 0}, 
				{0, 0, 1, 0, 0, 0, 0}, 
				{0, 0, 0, 0, 1, 0, 0}, 
				{0, 0, 0, 0, 0, 0, 1}, 
				{0, 0, 0, 0, 0, 1, 0}, 
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0}};
		
		double[][] parent = {
				{0, 0, 1, 0, 0, 0, 0}, 
				{0, 0, 0, 0, 1, 0, 0}, 
				{0, 0, 0, 0, 0, 0, 0}, 
				{0, 0, 0, 0, 0, 0, 0}, 
				{0, 0, 0, 0, 0, 0, 0}, 
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0}};
		
		double[][] un = {
				{1, 1, 1, 1, 1, 1, 1}, 
				{1, 1, 1, 1, 1, 1, 1}, 
				{1, 1, 1, 1, 1, 1, 1}, 
				{1, 1, 1, 1, 1, 1, 1}, 
				{1, 1, 1, 1, 1, 1, 1}, 
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1}};
		
		Matrix alpha = new Matrix(containment);
		Matrix beta = new Matrix(parent);
		Matrix unn = new Matrix(un);
		
		Matrix beta_fois_alpha = beta.times(alpha); // Beta x Alpha
		Matrix kaka = beta_fois_alpha.plus(alpha); // Beta x Alpha + Alpha
		Matrix essay = alpha.times(beta_fois_alpha);
		//Matrix test = kaka.times(beta); // (Beta x Alpha + Alpha) x Beta
		//Matrix finalMat = kaka.minus(test); // (Beta x Alpha + Alpha) - ( (Beta x Alpha + Alpha) x Beta)
		System.out.println("-tests--");
		System.out.println(toString(beta_fois_alpha.getArray()));
		System.out.println("---------------");
		System.out.println(toString(alpha.times(beta).getArray()));
		
	}
	
	public static String toString(double[][] M) {
	    String separator = ", ";
	    StringBuffer result = new StringBuffer();

	    // iterate over the first dimension
	    for (int i = 0; i < M.length; i++) {
	        // iterate over the second dimension
	        for(int j = 0; j < M[i].length; j++){
	            result.append(M[i][j]);
	            result.append(separator);
	        }
	        // remove the last separator
	        result.setLength(result.length() - separator.length());
	        // add a line break.
	        result.append("\n");
	    }
	    return result.toString();
	}

}
