package net.ihe.gazelle.goc.model.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.PathWithDT;
import net.ihe.gazelle.goc.uml.utils.PathWithDTUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PathWithDTUtilTest {

	@Test
	public void testExtractPathFromListPathWithDT() {
		List<PathWithDT> lpwdt = new ArrayList<PathWithDT>();
		lpwdt.add(new PathWithDT("act", null));
		lpwdt.add(new PathWithDT("id", null));
		System.out.println(PathWithDTUtil.extractPathFromListPathWithDT(lpwdt));
		assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(lpwdt).equals("act.id"));
	}

	@Test
	public void testExtractTypePathFromListPathWithDT() {
		List<PathWithDT> lpwdt = new ArrayList<PathWithDT>();
		lpwdt.add(new PathWithDT("observation", null));
		lpwdt.add(new PathWithDT("value", "CD"));
		assertTrue(PathWithDTUtil.extractTypedPathFromListPathWithDT(lpwdt).equals("CD"));
	}
	
	@Test
	public void testExtractTypePathFromListPathWithDT2() throws Exception {
		List<PathWithDT> lpwdt = new ArrayList<PathWithDT>();
		lpwdt.add(new PathWithDT("act", null));
		lpwdt.add(new PathWithDT("id", null));
		assertTrue(PathWithDTUtil.extractTypedPathFromListPathWithDT(lpwdt).equals("act.id"));
	}
	
	@Test
	public void testExtractTypedPathFromListPathWithDTWithoutLastDTChecking1() throws Exception {
		List<PathWithDT> lpwdt = new ArrayList<PathWithDT>();
		lpwdt.add(new PathWithDT("act", null));
		lpwdt.add(new PathWithDT("id", null));
		assertTrue(PathWithDTUtil.extractTypedPathFromListPathWithDTWithoutLastDTChecking(lpwdt).equals("act.id"));
	}
	
	@Test
	public void testExtractTypedPathFromListPathWithDTWithoutLastDTChecking2() throws Exception {
		List<PathWithDT> lpwdt = new ArrayList<PathWithDT>();
		lpwdt.add(new PathWithDT("act", null));
		lpwdt.add(new PathWithDT("id", "CD"));
		assertTrue(PathWithDTUtil.extractTypedPathFromListPathWithDTWithoutLastDTChecking(lpwdt).equals("act.id"));
	}
	
	@Test
	public void testExtractTypedPathFromListPathWithDTWithoutLastDTChecking3() throws Exception {
		List<PathWithDT> lpwdt = new ArrayList<PathWithDT>();
		lpwdt.add(new PathWithDT("act", "AAA"));
		lpwdt.add(new PathWithDT("id", "CD"));
		assertTrue(PathWithDTUtil.extractTypedPathFromListPathWithDTWithoutLastDTChecking(lpwdt).equals("AAA.id"));
	}

}
