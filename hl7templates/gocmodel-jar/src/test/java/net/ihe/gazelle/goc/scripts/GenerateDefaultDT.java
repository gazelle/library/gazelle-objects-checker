package net.ihe.gazelle.goc.scripts;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;

import net.ihe.gazelle.goc.uml.utils.CDAClassesMap;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class GenerateDefaultDT {
	
	
	public static void main(String[] args) {
		extractDTfromCDA();
	}
	
	private static void extractDTfromCDA() {
		EList<PackageableElement> pe = CDAClassesMap.getPackagedElements("cda");
		extractDTs(pe, false);
	}

	private static void extractDTfromDT() {
		EList<PackageableElement> pe = CDAClassesMap.getPackagedElements("datatypes");
		extractDTs(pe, true);
	}

	private static void extractDTs(EList<PackageableElement> pe, boolean addEmpty) {
		int i = 0;
		for (PackageableElement packageableElement : pe) {
			if (packageableElement instanceof org.eclipse.uml2.uml.Class){
				org.eclipse.uml2.uml.Class clas = (org.eclipse.uml2.uml.Class)packageableElement;
				EList<Property> attr = clas.getAttributes();
				if (realAttrSize(attr)==0) continue;
				System.out.print("<DT name='" + clas.getName() + "'");
				boolean ended = false;
				for (Generalization gen: clas.getGeneralizations()) {
					if (gen.getGeneral() instanceof Class && !ended){
						System.out.println(" ref='" + ((Class)gen.getGeneral()).getName() + (realAttrSize(attr)==0?"'/>":"'>"));
						ended = true;
					}
				}
				for (Classifier gen: clas.getGenerals()) {
					if (gen instanceof Class && !ended){
						System.out.println(" ref='" + ((Class)gen).getName() + (realAttrSize(attr)==0?"'/>":"'>"));
						ended = true;
					}
				}
				if (!ended && realAttrSize(attr)>0){
					System.out.println(">");
				}
				
				for (Property property : attr) {
					String propName = property.getName(); 
					if (propName == null || propName.equals("__parent") || propName.equals("mixed") || propName.equals("group")){
						continue;
					}
					String propTypeName = property.getType().getName();
					if (propTypeName != null){
						continue;
					}
					System.out.println("\t<attrDT attrName='" + property.getName() + 
							"' defaultDT='" + property.getType().getName() + "' />");
					i++;
				}
				if (realAttrSize(attr)>0){
					System.out.println("</DT>");
				}
			}
		}
		System.out.println("i = " + i);
	}
	
	private static int realAttrSize(EList<Property> attr){
		int val = 0;
		for (Property property : attr) {
			String propName = property.getName(); 
			if (propName == null || propName.equals("__parent") || propName.equals("mixed") || propName.equals("group")){
				continue;
			}
			String propTypeName = property.getType().getName();
			if (propTypeName != null){
				continue;
			}
			val++;
		}
		return val;
	}

}
