package net.ihe.gazelle.goctests.interlay.utils;

import java.util.Random;

final public class StringUtils {

    private StringUtils(){}

    private static final Random random = new Random();

    public static String getRandomString(int length){

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }
}
