package net.ihe.gazelle.goctests.application.models;

import net.ihe.gazelle.validation.DetailedResult;

public class ValidationResult {

    private DetailedResult detailedResult;

    private String usedCDA;


    public ValidationResult(DetailedResult detailedResult, String usedCDA) {
        this.detailedResult = detailedResult;
        this.usedCDA = usedCDA;
    }

    public DetailedResult getDetailedResult() {
        return detailedResult;
    }

    public String getUsedCDA() {
        return usedCDA;
    }

}
