package net.ihe.gazelle.goctests.application.models;

import net.ihe.gazelle.goctests.application.specifications.SpecificationType;

import java.text.SimpleDateFormat;
import java.util.Date;


public class TestResult {

    private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private GeneratedValidator usedValidator;

    private Date validationDate;

    private String formattedValidationDate;

    private String usedCDA;

    private SpecificationType specificationType;

    private OracleResult oracleResult;


    public TestResult() {
    }

    public TestResult(GeneratedValidator usedValidator, String usedCDA,
                      SpecificationType specificationType, OracleResult oracleResult) {
        this.usedValidator = usedValidator;
        this.usedCDA = usedCDA;
        this.specificationType = specificationType;
        this.validationDate = new Date();
        this.formattedValidationDate = this.formatter.format(validationDate);
        this.oracleResult = oracleResult;
    }

    public GeneratedValidator getUsedValidator() {
        return usedValidator;
    }

    public Date getValidationDate() {
        return validationDate;
    }

    public String getFormattedValidationDate(){
        return formattedValidationDate;
    }


    public String getUsedCDA() {
        return usedCDA;
    }

    public SpecificationType getSpecificationType() {
        return specificationType;
    }

    public OracleResult getOracleResult() {
        return oracleResult;
    }

    @Override
    public String toString() {
        return "TestResult{" +
                " usedValidator=" + usedValidator +
                ", validationDate=" + validationDate +
                ", formattedValidationDate='" + formattedValidationDate + '\'' +
                ", usedCDA='" + usedCDA + '\'' +
                ", specificationType=" + specificationType +
                ", oracleResult=" + oracleResult +
                '}';
    }
}
