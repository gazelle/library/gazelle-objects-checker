package net.ihe.gazelle.goctests.interlay.transformers;

public class TestResultTransformerFactory {

    private TestResultTransformerFactory(){}

    public static TestResultTransformer createTestResultTransformer(TransformationType transformationType){
        switch (transformationType){
            case STANDARD:
                return new StandardTestResultTransformer();
            case XML:
                return new XMLTestResultTransformer();

            default:
                return null;
        }
    }
}
