package net.ihe.gazelle.goctests.application;

import net.ihe.gazelle.goctests.application.oracles.exceptions.OracleException;
import net.ihe.gazelle.validation.DetailedResult;

import java.net.URL;

/**
 * This Adapter interface provide a single API to get DetailedResult
 */
public interface DetailedResultAdapter {

    DetailedResult getDetailedResult(URL url) throws OracleException;
}
