package net.ihe.gazelle.goctests.application.specifications;

import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.exceptions.ReportGenerationException;
import net.ihe.gazelle.goctests.application.exceptions.TestRunnerException;

public interface SpecificationTestRunner {

    /**
     * Running a Specification Test aims to:
     * <ul>
     *     <li>1- Generate a validator</li>
     *     <li>2- Validate CDA document</li>
     *     <li>3- Run Oracle Test</li>
     * </ul>
     * To generate a report describing the state of theses Tests (PASSED | FAILED)
     * @throws TestRunnerException
     */
    void runTest() throws TestRunnerException;

    /**
     * Generated report based on the related test
     * This Test could be transformed based on local Transformation APIs
     * @return Report as TestResult object
     * @throws ReportGenerationException if generating result failed
     */
    TestResult generateResult() throws ReportGenerationException;
}
