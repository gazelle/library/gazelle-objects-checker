package net.ihe.gazelle.goctests.application.specifications;

import net.ihe.gazelle.goctests.application.CDAValidator;
import net.ihe.gazelle.goctests.application.ResourceRetreiver;
import net.ihe.gazelle.goctests.application.ValidatorGenerator;
import net.ihe.gazelle.goctests.application.exceptions.ReportGenerationException;
import net.ihe.gazelle.goctests.application.exceptions.TestRunnerException;
import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;

public class FixedvalTestRunner extends SpecificationTestRunnerImpl{

    public FixedvalTestRunner(ResourceRetreiver resourceRetreiver, CDAValidator cdaValidator, ValidatorGenerator validatorGenerator, TestOracle testOracle) {
        super(SpecificationType.FIXEDVAL, resourceRetreiver, cdaValidator, validatorGenerator, testOracle);
    }

    @Override
    public void runTest() throws TestRunnerException {
        //Additional processing if needed

        super.runTest();
    }

    @Override
    public TestResult generateResult() throws ReportGenerationException {
        //Additional processing if needed

        return super.generateResult();
    }
}
