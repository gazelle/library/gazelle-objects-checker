package net.ihe.gazelle.goctests.application.exceptions;

public class ValidatorGenerationException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Validator Generation process failed: ";

    public ValidatorGenerationException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public ValidatorGenerationException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
