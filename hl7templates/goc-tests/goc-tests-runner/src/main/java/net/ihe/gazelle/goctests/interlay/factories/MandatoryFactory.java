package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.MandatoryTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.oracles.MandatoryTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.interlay.*;

public class MandatoryFactory implements SpecificationFactory{

    @Override
    public SpecificationTestRunner createTestRunner() {
        return new MandatoryTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new MandatoryTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
