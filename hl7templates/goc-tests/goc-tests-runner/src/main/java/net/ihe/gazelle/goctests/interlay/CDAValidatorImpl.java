package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.interlay.exceptions.ProcessExecutionException;
import net.ihe.gazelle.goctests.application.exceptions.ValidationException;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;
import net.ihe.gazelle.goctests.application.models.ValidationResult;
import net.ihe.gazelle.gen.common.DetailedResultMarshaller;
import net.ihe.gazelle.goctests.application.CDAValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.goctests.interlay.utils.StringUtils;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CDAValidatorImpl implements CDAValidator {

    private static final int RESULT_FILENAME_LENGTH = 20;

    private static final String OUTPUT_EXTENSION = ".xml";

    private ProcessExecutor processExecutor;

    public CDAValidatorImpl(ProcessExecutor processExecutor) {
        this.processExecutor = processExecutor;
    }

    @Override
    public ValidationResult validateDocument(String documentPath, GeneratedValidator validator) throws ValidationException {
        if(validator == null){
            throw new ValidationException("Validator is not defined: NULL");
        }
        Path validationResultFolder = EnvironementConfiguration.getInstance().getResultFolder();
        if(validationResultFolder == null){
            throw new ValidationException("Validation Result folder output not set in: "+EnvironementConfiguration.APPLICATION_PROPERTIES_FILE);
        }

        // Prepare validation command
        String resultFileName = StringUtils.getRandomString(RESULT_FILENAME_LENGTH)+OUTPUT_EXTENSION;
        String resultFilePath =  validationResultFolder.toAbsolutePath().toString()+"/"+resultFileName;

        StringBuilder command = new StringBuilder(validator.getValidatorBinaryPath())
                .append(" -path ").append(documentPath)
                .append(" -out ").append(resultFilePath)
                .append(" -resources ").append(validator.getValidatorResources());

        //Validate
        try {
            processExecutor.executeProcess(command.toString());
            DetailedResult detailedResult = DetailedResultMarshaller.load(new FileInputStream(resultFilePath));

            return new ValidationResult(detailedResult,documentPath);

        } catch (ProcessExecutionException e) {
            throw new ValidationException("Cannot execute CDA validation command",e);
        } catch (FileNotFoundException e){
            throw new ValidationException("CDA Valdiation Result not found at: "+resultFilePath,e);
        }catch (JAXBException e){
            throw new ValidationException("Failed to parse DetailedResult from "+resultFileName,e);
        }

    }

    @Override
    public ValidationResult validateDocument(URL documentURL, GeneratedValidator validator) throws ValidationException {
        try {
            String cdaTemporaryCopy = EnvironementConfiguration.getInstance()
                    .getResultFolder().toAbsolutePath().toString() + "/" + StringUtils.getRandomString(15);
            Files.copy(documentURL.openStream(), Paths.get(cdaTemporaryCopy));
            String documentPath = new File(cdaTemporaryCopy).getAbsolutePath();
            return validateDocument(documentPath,validator);
        } catch (IOException e) {
            throw new ValidationException("Failed to copy CDA for validation");
        }
    }
}

