package net.ihe.gazelle.goctests.application.oracles;


import net.ihe.gazelle.goctests.application.DetailedResultAdapter;
import net.ihe.gazelle.goctests.application.ResourceRetreiver;
import net.ihe.gazelle.goctests.application.models.OracleResult;
import net.ihe.gazelle.goctests.application.exceptions.ConfigurationException;
import net.ihe.gazelle.goctests.application.oracles.exceptions.OracleException;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;


import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
abstract public class TestOracleImpl implements TestOracle {

    private SpecificationType specificationType;

    private ResourceRetreiver resourceRetreiver;

    private DetailedResultAdapter detailedResultAdapter;

    protected TestOracleImpl(SpecificationType specificationType, ResourceRetreiver resourceRetreiver, DetailedResultAdapter detailedResultAdapter) {
        this.specificationType = specificationType;
        this.resourceRetreiver = resourceRetreiver;
        this.detailedResultAdapter = detailedResultAdapter;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public OracleResult compareWithExpectedResult(DetailedResult actualResult) throws OracleException {

        if (specificationType == null){
            throw new OracleException("FATAL: Specification type not specified");
        }

        if(actualResult == null || actualResult.getMDAValidation() == null){
            throw new OracleException("Validation result not correctly set");
        }

        try {
            URL expectedResultURL = resourceRetreiver.retreiveExpectedResultResource(specificationType);
            DetailedResult expectedResult = detailedResultAdapter.getDetailedResult(expectedResultURL);
            if(expectedResult == null || expectedResult.getMDAValidation() == null){
                throw new OracleException("Problem in local resources");
            }

            // Filter on specification and cast to Notification
            List<Notification> filteredExcpected = filterDetailResultBySpec(expectedResult);
            List<Notification> filteredActual = filterDetailResultBySpec(actualResult);

            // Look for missing elements
            List<Notification> missedElements = matchElements1inElements2(filteredExcpected, filteredActual);

            // Look for unexpected (additional) elements
            List<Notification> unexpectedElements = matchElements1inElements2(filteredActual, filteredExcpected);

            OracleResult oracleResult =
                    new OracleResult(filteredActual,filteredExcpected,missedElements,unexpectedElements);

            return oracleResult;

        } catch (ConfigurationException e) {
            throw new OracleException("Could not retreive Expected Result resources path",e);
        }
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public boolean matchingCriteria(Notification target, List<Notification> elements) {
       for(Notification element : elements){
           if(target.getLocation().equals(element.getLocation())
                   && target.getDescription().equals(element.getDescription())){
               return true;
           }
       }
       return false;
    }

    public SpecificationType getSpecificationType() {
        return specificationType;
    }

    private List<Notification> filterDetailResultBySpec(DetailedResult detailedResult){
        return detailedResult
                .getMDAValidation().getWarningOrErrorOrNote()
                .stream().map(e->(Notification)e)
                .filter(this::filteringCriteria)
                .collect(Collectors.toList());
    }

    private boolean filteringCriteria(Notification notification){
        return (specificationType.equals(SpecificationType.GLOBAL) ||
                (notification.getType() !=null && notification.getType()
                        .equalsIgnoreCase(specificationType.getValue())));
    }

    private List<Notification> matchElements1inElements2(List<Notification> elements1, List<Notification> elements2){
        return elements1
                .stream().filter(e->!matchingCriteria(e,elements2))
                .collect(Collectors.toList());

    }
}
