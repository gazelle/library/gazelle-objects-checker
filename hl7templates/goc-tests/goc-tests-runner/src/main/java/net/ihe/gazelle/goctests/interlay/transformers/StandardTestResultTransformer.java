package net.ihe.gazelle.goctests.interlay.transformers;

import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.goctests.interlay.utils.PrintersUtils;
import net.ihe.gazelle.goctests.interlay.exceptions.ResultTransformationException;

public class StandardTestResultTransformer implements TestResultTransformer {

    private static final String HEADER = "============ START TEST RESULT ============";

    private static final String FOOTER = "============ END TEST RESULT ============";

    @Override
    public void transformResult(TestResult testResult) throws ResultTransformationException {
        System.out.println(testResult);
        System.out.println(HEADER);
        System.out.println("Test performed at: "+testResult.getFormattedValidationDate());
        System.out.println("Test Result: "+getFormattedTestResult(testResult.getOracleResult().isValid()));
        System.out.println("Test Type: "+ PrintersUtils.info(testResult.getSpecificationType().getValue()));
        System.out.println("Processed Rules: "+testResult.getOracleResult().getProcessed().size());
        System.out.println("Excpected Rules: "+testResult.getOracleResult().getExpectedElements().size());
        System.out.println("Missing Rules: "+getFormattedMissingRules(testResult.getOracleResult().getMissedElements().size()));
        System.out.println("Test Global: "+getFormattedIsGlobal(testResult.getSpecificationType()));
        System.out.println("Used CDA: "+testResult.getUsedCDA());
        System.out.println("Used BBR: "+testResult.getUsedValidator().getUsedBBR());
        System.out.println(FOOTER);
    }

    private static String getFormattedTestResult(boolean passed){
        return passed? PrintersUtils.success("PASSED"): PrintersUtils.failed("FAILED");
    }

    private static String getFormattedIsGlobal(SpecificationType specificationType){
        return specificationType.equals(SpecificationType.GLOBAL)?
                PrintersUtils.success("True"): PrintersUtils.failed("False");
    }

    private static String getFormattedMissingRules(int missingRules){
        return missingRules==0? PrintersUtils.success(Integer.toString(missingRules)): PrintersUtils.failed(Integer.toString(missingRules));
    }
}
