package net.ihe.gazelle.goctests.application;

import net.ihe.gazelle.goctests.application.exceptions.ConfigurationException;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;

import java.net.URL;

/**
 * This Interface provide APIs to retreive needed resources
 * Concrete Implementations could be found in the <b>Interlay</b> layer
 */

public interface ResourceRetreiver {

    /**
     * Retreive BBR Resources based on specification
     * @param specificationType specification
     * @return URL from resource
     * @throws ConfigurationException if cannot retreive resource
     */
    URL retreiveBBRResource(SpecificationType specificationType) throws ConfigurationException;

    /**
     * Retreive CDA Resources based on specification
     * @param specificationType specification
     * @return URL from resource
     * @throws ConfigurationException if cannot retreive resource
     */

    URL retreiveCDAResource(SpecificationType specificationType) throws ConfigurationException;

    /**
     * Retreive ExpectedResult Resources based on specification
     * @param specificationType specification
     * @return URL from resource
     * @throws ConfigurationException if cannot retreive resource
     */

    URL retreiveExpectedResultResource(SpecificationType specificationType) throws ConfigurationException;

    /**
     * Implement this method in a matter to match value of
     * Specification Enumartion, with keys in property files
     * default behavior is to replace " " with ""
     * @param specificationType specification
     * @return URL from resource
     */
    String adaptSpecification(SpecificationType specificationType);

    /**
     * Retreive HL7 Config Resources based on specification
     * @param specificationType specification
     * @return URL from resource
     * @throws ConfigurationException if cannot retreive resource
     */

    String retreiveHL7Config() throws ConfigurationException;

}
