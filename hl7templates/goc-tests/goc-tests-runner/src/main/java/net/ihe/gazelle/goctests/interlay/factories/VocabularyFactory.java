package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.specifications.VocabularyTestRunner;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.application.oracles.VocabularyTestOracle;
import net.ihe.gazelle.goctests.interlay.*;

public class VocabularyFactory implements SpecificationFactory{

    @Override
    public SpecificationTestRunner createTestRunner() {
        return new VocabularyTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash())
                ,new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new VocabularyTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
