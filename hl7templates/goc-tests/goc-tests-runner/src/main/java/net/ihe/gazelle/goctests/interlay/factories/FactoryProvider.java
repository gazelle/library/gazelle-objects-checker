package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.SpecificationType;

final public class FactoryProvider {

    private FactoryProvider(){}

    public static SpecificationFactory create(SpecificationType specificationType){
        switch (specificationType){
            case CARDINALITY:
                return new CardinalityFactory();
            case MANDATORY:
                return new MandatoryFactory();
            case CONTEXT:
                return new ContextFactory();
            case VOCABULARY:
                return new VocabularyFactory();
            case DATATYPE:
                return new DatatypeFactory();
            case FIXEDVAL:
                return new FixedvalFactory();
            case CLOSED:
                return new ClosedFactory();
            case CHOICE:
                return new ChoiceFactory();
            case GLOBAL:
                return new GlobalFactory();
            default:
                return null;
        }
    }
}
