package net.ihe.gazelle.goctests.application.oracles;

import net.ihe.gazelle.goctests.application.models.OracleResult;
import net.ihe.gazelle.goctests.application.oracles.exceptions.OracleException;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

/**
 * This interface provides APIs to compare <b>actual</b> TestResult with the <b>expected</b> one
 */
public interface TestOracle {

    /**
     * comparing actual with expected is done with comparing:
     * <ul>
     *     <li>Erros</li>
     *     <li>Warnings</li>
     *     <li>Infos</li>
     *     <li>Notes</li>
     * </ul>
     * @param actualResult the actual result to compare with predefined result
     * @return the oracle result
     * @throws OracleException if cannot compare results
     */
    OracleResult compareWithExpectedResult(DetailedResult actualResult) throws OracleException;


    /**
     * The default pre-defined criteria is to search for match based on
     * the <b>Location</b> and <b>Description</b> of Notification object
     * in a list of Notifications objects.
     * You can override this behavior in subclasses
     * @param target target to filter with
     * @param elements elements to filter
     * @return True if a math found, false otherwise
     */
    boolean matchingCriteria(Notification target, List<Notification> elements);


    SpecificationType getSpecificationType();
}
