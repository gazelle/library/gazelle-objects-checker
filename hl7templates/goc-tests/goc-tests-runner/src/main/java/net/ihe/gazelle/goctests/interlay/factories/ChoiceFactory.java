package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.oracles.ChoiceTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.application.specifications.ChoiceTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.interlay.*;

public class ChoiceFactory implements SpecificationFactory{

    @Override
    public SpecificationTestRunner createTestRunner() {
        return new ChoiceTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new ChoiceTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
