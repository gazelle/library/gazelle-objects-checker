package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.interlay.exceptions.EnvironementException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

final public class EnvironementConfiguration {


    public final static String[] PROPERTIES_FILES = {"application.properties","specifications.properties"};

    public final static String APPLICATION_PROPERTIES_FILE = PROPERTIES_FILES[0];

    private final Properties properties = new Properties();

    private final static EnvironementConfiguration INSTANCE = new EnvironementConfiguration();

    private final static String TEMPORARY_FOLDER = "/tmp/";

    private final static String GENERATION_PREFIX = "GOCGeneration";

    private Path gocOutputFolder;

    private Path resultFolder;

    private boolean persistValidator;

    private boolean persistResult;

    /**
     * Private constructor for singleton design pattern
     */
    private EnvironementConfiguration(){
        try{
            loadProperties();
        }
        catch (IOException e){
            System.err.println("WARN: couldn't load application.properties file: "+e.getMessage());
        }
    }

    private void loadProperties() throws IOException {
        for(String file:PROPERTIES_FILES){
            InputStream inputStream = EnvironementConfiguration.class.getClassLoader().getResourceAsStream(file);
            properties.load(inputStream);
        }
    }

    public static EnvironementConfiguration getInstance(){
        return INSTANCE;
    }

    public String getProperty(String propertyName){
        return properties.getProperty(propertyName);
    }

    public Path getGocOutputFolder(){
        return gocOutputFolder;
    }

    public Path getResultFolder() {
        return resultFolder;
    }

    public void initEnvironement() throws EnvironementException {
        String outputFolderPrefix = getProperty("goc.outputFolderPrefix");
        String resultFolderPrefix = getProperty("goc.resultFolderPrefix");

        this.persistValidator = "true".equals(getProperty("goc.persistValidator"));
        this.persistResult = "true".equals(getProperty("goc.persistResult"));

        this.gocOutputFolder = initWorkspace(outputFolderPrefix,"Output Folder");
        this.resultFolder = initWorkspace(resultFolderPrefix, "Result Folder");
    }

    public void cleanEnvironement() {
        System.out.println("Start cleaning...");
        if(this.gocOutputFolder == null || this.resultFolder == null){
            System.out.println("outputFolder and/or resultFolder not set, probably initialisation failed");
            return;
        }
        try {
            if(!this.persistValidator && Files.exists(this.gocOutputFolder)){
                FileUtils.deleteDirectory(this.gocOutputFolder.toFile());
            }
            else{
                System.out.println("Generated validator persisted at: "+this.gocOutputFolder.toAbsolutePath().toString());
            }
            if(!this.persistResult && Files.exists(this.resultFolder)){
                FileUtils.deleteDirectory(this.resultFolder.toFile());
            }
            else {
                System.out.println("Validation result is persisted at: "+this.resultFolder.toAbsolutePath().toString());
            }
            System.out.println("Workspace cleaned successfully");
        } catch (IOException e) {
            System.out.println("Cleaning failed, please check the stacktrace");
            e.printStackTrace();
        }
    }


    private Path initWorkspace(String prefix, String type) throws EnvironementException {
        if(prefix == null){
            throw new EnvironementException("GOC "+type+" prefix path is not set in "+EnvironementConfiguration.APPLICATION_PROPERTIES_FILE);
        }
        File outputFolderDir = new File(TEMPORARY_FOLDER+prefix);
        if(!outputFolderDir.exists()){
            if(outputFolderDir.mkdirs()){
                System.out.println("INFO: "+type+" Temporary directory created at: "+TEMPORARY_FOLDER+prefix);
            }
            else {
                throw new EnvironementException("Could not create GOC "+type);
            }
        }
        else {
            System.out.println("INFO: "+type+" Temporary directory exist already, if there was previous generation it will be overwritten");
        }
        Path dir = outputFolderDir.toPath();
        try {
            Path out = Files.createTempDirectory(dir,GENERATION_PREFIX);
            return out;
        } catch (IOException e) {
            throw new EnvironementException(e.getMessage());
        }
    }
}

