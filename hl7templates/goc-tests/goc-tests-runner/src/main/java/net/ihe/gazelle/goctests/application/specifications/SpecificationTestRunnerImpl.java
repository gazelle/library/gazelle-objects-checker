package net.ihe.gazelle.goctests.application.specifications;


import net.ihe.gazelle.goctests.application.CDAValidator;
import net.ihe.gazelle.goctests.application.ResourceRetreiver;
import net.ihe.gazelle.goctests.application.ValidatorGenerator;
import net.ihe.gazelle.goctests.application.models.OracleResult;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;
import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.models.ValidationResult;

import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.application.oracles.exceptions.OracleException;

import net.ihe.gazelle.goctests.application.exceptions.ValidatorGenerationException;
import net.ihe.gazelle.goctests.application.exceptions.ValidationException;
import net.ihe.gazelle.goctests.application.exceptions.ReportGenerationException;
import net.ihe.gazelle.goctests.application.exceptions.TestRunnerException;
import net.ihe.gazelle.goctests.application.exceptions.ConfigurationException;


import java.net.URL;

abstract public class SpecificationTestRunnerImpl implements SpecificationTestRunner{

    private URL BBR;

    private URL CDA;

    private ValidationResult validationResult;

    private GeneratedValidator generatedValidator;

    private OracleResult oracleResult;

    private SpecificationType specificationType;

    private ResourceRetreiver resourceRetreiver;

    private CDAValidator cdaValidator;

    private ValidatorGenerator validatorGenerator;

    private TestOracle testOracle;

    private String hl7Config;

    protected SpecificationTestRunnerImpl(SpecificationType specificationType, ResourceRetreiver resourceRetreiver,
                                       CDAValidator cdaValidator, ValidatorGenerator validatorGenerator, TestOracle testOracle) {
        this.specificationType = specificationType;
        this.resourceRetreiver = resourceRetreiver;
        this.cdaValidator = cdaValidator;
        this.validatorGenerator = validatorGenerator;
        this.testOracle = testOracle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runTest() throws TestRunnerException {

        //Check if specification specialisation is instantiated
        if(this.specificationType == null){
            throw new TestRunnerException("CDA is not set, Test Failed");
        }
        try {
            this.BBR = resourceRetreiver.retreiveBBRResource(this.specificationType);
            this.CDA = resourceRetreiver.retreiveCDAResource(this.specificationType);
            this.hl7Config = resourceRetreiver.retreiveHL7Config();
        } catch (ConfigurationException e) {
            throw new TestRunnerException("Problem in BBR and/or CDA",e);
        }

        // Check if we could load resources correctly
        if(this.BBR == null){
            throw new TestRunnerException("BBR is not set, Test Failed");
        }
        if(this.CDA == null){
            throw new TestRunnerException("CDA is not set, Test Failed");
        }
        if(generatedValidator != null || validationResult != null){
            throw new TestRunnerException("Something wrong happened please re-run the application: previous generationg already in the queue");
        }

        //Generate Validator
        try {
            this.generatedValidator = this.validatorGenerator.generateValidator(BBR, hl7Config);
        } catch (ValidatorGenerationException e) {
            throw new TestRunnerException("Exception durin generating validator",e);
        }

        //Validator CDA & set detailedResult
        try {
            this.validationResult = this.cdaValidator.validateDocument(CDA,generatedValidator);

            //Comparing with expected "Test Oracle"
            if(testOracle == null){
                throw new TestRunnerException("Authentication not implemented for this specification type, Test failed.");
            }

            //Guarentee the match between Test Oracle and Specification Runner
            if(!this.testOracle.getSpecificationType().equals(this.specificationType)){
                throw new TestRunnerException("Fatal Error: Test Oracle and Spec Runner doesn't have same specification type");
            }

            this.oracleResult = this.testOracle.compareWithExpectedResult(validationResult.getDetailedResult());
            if(this.oracleResult == null){
                throw new TestRunnerException("Authentication not performed correctly");
            }

        } catch (ValidationException e) {
            throw new TestRunnerException("Exception during validating CDA document",e);
        } catch (OracleException e) {
            throw new TestRunnerException("Exception during Authentication CDA Document",e);
        }

        //Destroy validation resources
        this.CDA = null;
        this.BBR = null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TestResult generateResult() throws ReportGenerationException {

        //Check if detailedResult and validator are set
        if(this.validationResult == null){
            throw new ReportGenerationException("Validation resutl not found, probably validation not performed");
        }
        if(this.generatedValidator == null){
            throw new ReportGenerationException("Validator not found, probably validation not performed");
        }
        if(this.oracleResult == null){
            throw new ReportGenerationException("Authentication result not found!");
        }

        //Generate Report
        TestResult testResult = new TestResult(this.generatedValidator,
                this.validationResult.getUsedCDA(),this.specificationType, this.oracleResult);

        //Destroy current validation
        this.generatedValidator = null;
        this.validationResult = null;

        //Return testResult
        return testResult;
    }
}
