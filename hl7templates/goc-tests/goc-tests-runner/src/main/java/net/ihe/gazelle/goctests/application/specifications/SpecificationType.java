package net.ihe.gazelle.goctests.application.specifications;


public enum SpecificationType {


    GLOBAL("global"),
    CARDINALITY("cardinality"),
    VOCABULARY("vocabulary"),
    MANDATORY("mandatory"),
    CONTEXT("context"),
    DATATYPE("datatype"),
    FIXEDVAL("fixed value"),
    CLOSED("closed"),
    CHOICE("choice");


    private String value;

    private SpecificationType(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static final String possibleValues = generatePossibleValues() ;

    public static SpecificationType of(String value){
        for(SpecificationType specificationType : SpecificationType.values()){
            if(specificationType.getValue().equals(value)){
                return specificationType;
            }
        }
        return null;
    }

    private static String generatePossibleValues(){
       StringBuilder result = new StringBuilder("[");
       for(SpecificationType s : SpecificationType.values()){
           result.append(s.value).append(",").append("\n");
       }
       result.append("]");
       return result.toString();
    }

    public static String getPossibleValues(){
        return possibleValues;
    }
}
