package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.ContextTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.oracles.ContextTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.interlay.*;

public class ContextFactory implements SpecificationFactory{
    @Override
    public SpecificationTestRunner createTestRunner() {
        return new ContextTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new ContextTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
