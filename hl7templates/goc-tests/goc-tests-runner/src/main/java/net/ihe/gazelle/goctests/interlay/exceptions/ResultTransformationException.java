package net.ihe.gazelle.goctests.interlay.exceptions;

public class ResultTransformationException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Result Transformation failed: ";

    public ResultTransformationException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public ResultTransformationException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
