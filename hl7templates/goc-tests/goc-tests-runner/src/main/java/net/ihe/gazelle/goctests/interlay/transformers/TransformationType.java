package net.ihe.gazelle.goctests.interlay.transformers;

public enum TransformationType {

    STANDARD("standard"),
    XML("xml");

    private String value;

    private TransformationType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TransformationType of(String value){
        for(TransformationType transformationType : TransformationType.values()){
            if(transformationType.getValue().equals(value)){
                return transformationType;
            }
        }
        return null;
    }
}
