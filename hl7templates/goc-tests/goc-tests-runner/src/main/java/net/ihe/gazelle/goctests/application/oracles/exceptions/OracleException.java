package net.ihe.gazelle.goctests.application.oracles.exceptions;

public class OracleException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Test Oracle failed: ";

    public OracleException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public OracleException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
