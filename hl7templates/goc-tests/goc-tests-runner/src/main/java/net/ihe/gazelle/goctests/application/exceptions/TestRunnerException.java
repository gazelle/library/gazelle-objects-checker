package net.ihe.gazelle.goctests.application.exceptions;

public class TestRunnerException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Test Running failed: ";

    public TestRunnerException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public TestRunnerException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
