package net.ihe.gazelle.goctests.application.models;


import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement
@XmlType(propOrder = {"validationDate","testResult","specification","usedCDA","processedElements"
        ,"expectedElements","missedElements","unexpectedElements","usedValidator"})
@XmlAccessorType(XmlAccessType.FIELD)
public class TestResultReport implements Serializable {

    @XmlElement(name = "TestResult")
    private String testResult;

    @XmlElement(name = "Validator")
    private GeneratedValidator usedValidator;

    @XmlElement(name = "ValidationDate")
    private String validationDate;

    @XmlElement(name = "ValidateDocument")
    private String usedCDA;

    @XmlElement(name = "Specification")
    private String specification;

    @XmlElement(name = "NumberProcessedRules")
    private int processedElements;

    @XmlElement(name = "NumberExpectedRules")
    private int expectedElements;

    @XmlElement(name = "MissedRules")
    private NotificationElements missedElements;

    @XmlElement(name = "UnexpectedRules")
    private NotificationElements unexpectedElements;




    public TestResultReport() {
    }

    public TestResultReport(String testResult, GeneratedValidator usedValidator, String generationDate, String usedCDA, String specification,
                            int processedElements, int expectedElements, NotificationElements missedElements, NotificationElements unexpectedElements) {
        this.testResult = testResult;
        this.usedValidator = usedValidator;
        this.validationDate = generationDate;
        this.usedCDA = usedCDA;
        this.specification = specification;
        this.processedElements = processedElements;
        this.expectedElements = expectedElements;
        this.missedElements = missedElements;
        this.unexpectedElements = unexpectedElements;

    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public GeneratedValidator getUsedValidator() {
        return usedValidator;
    }

    public void setUsedValidator(GeneratedValidator usedValidator) {
        this.usedValidator = usedValidator;
    }

    public String getUsedCDA() {
        return usedCDA;
    }

    public void setUsedCDA(String usedCDA) {
        this.usedCDA = usedCDA;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public int getProcessedElements() {
        return processedElements;
    }

    public void setProcessedElements(int processedElements) {
        this.processedElements = processedElements;
    }

    public int getExpectedElements() {
        return expectedElements;
    }

    public void setExpectedElements(int expectedElements) {
        this.expectedElements = expectedElements;
    }

    public NotificationElements getMissedElements() {
        return missedElements;
    }

    public void setMissedElements(NotificationElements missedElements) {
        this.missedElements = missedElements;
    }

    public NotificationElements getUnexpectedElements() {
        return unexpectedElements;
    }

    public void setUnexpectedElements(NotificationElements unexpectedElements) {
        this.unexpectedElements = unexpectedElements;
    }

    @Override
    public String toString() {
        return "TestResultReport{" +
                "testResult='" + testResult + '\'' +
                ", usedValidator=" + usedValidator +
                ", validationDate='" + validationDate + '\'' +
                ", usedCDA='" + usedCDA + '\'' +
                ", specification='" + specification + '\'' +
                ", processedElements=" + processedElements +
                ", expectedElements=" + expectedElements +
                ", missedElements=" + missedElements +
                ", unexpectedElements=" + unexpectedElements +
                '}';
    }

}
