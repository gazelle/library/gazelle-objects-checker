package net.ihe.gazelle.goctests.interlay.utils;

import net.ihe.gazelle.goctests.application.models.TestResultReport;
import net.ihe.gazelle.goctests.interlay.EnvironementConfiguration;
import net.ihe.gazelle.goctests.interlay.exceptions.TestResultMarhsallException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

final public class TestResultMarshaller {

    private TestResultMarshaller(){}

    public static String save(TestResultReport testResultReport, String fileName, String directory) throws TestResultMarhsallException {


        if (!Files.isDirectory(Path.of(directory))){
            throw new TestResultMarhsallException("The path "+directory+" doesn't exist or not a directory");
        }

        String reportPath = directory+"/"+fileName;
        File file = new File(reportPath);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(TestResultReport.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(testResultReport,file);
        } catch (JAXBException e) {
            throw new TestResultMarhsallException("Marshalling failed",e);
        }
        return reportPath;
    }


    public static String save(TestResultReport testResultReport, String fileName) throws TestResultMarhsallException {
        String xmlReportPath = EnvironementConfiguration.getInstance().getProperty("result.type.xmlpath");
        if(xmlReportPath == null){
            throw new TestResultMarhsallException("The XML report path is not set in application.properties file, could not marshall result");
        }

        return save(testResultReport,fileName,xmlReportPath);

    }


}
