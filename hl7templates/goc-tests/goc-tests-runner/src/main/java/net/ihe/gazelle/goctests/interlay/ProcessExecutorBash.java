package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.interlay.exceptions.ProcessExecutionException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ProcessExecutorBash implements ProcessExecutor{

    @Override
    public List<String> executeProcessAndReturn(String command) throws ProcessExecutionException {
        ArrayList<String> output;
        try {
            Process pr = Runtime.getRuntime().exec(command);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            int result = pr.waitFor();
            if (result != 0){
                throw new ProcessExecutionException("Cannot execute process with code :" + result);
            }
            output = new ArrayList<>();
            String line = null ;
            while((line = bufferedReader.readLine()) != null){
                output.add(line);
            }
        }catch (IOException e){
            throw new ProcessExecutionException("Failed to execute command",e);
        }
        catch (InterruptedException e){
            throw new ProcessExecutionException("Execution interrupted",e);
        }
        return output;
    }

    @Override
    public void executeProcess(String command) throws ProcessExecutionException {

        System.out.println("Executin command: "+command);

        try{
            Process pr = Runtime.getRuntime().exec(command);
            int result = pr.waitFor();
            if (result != 0){
                throw new ProcessExecutionException("Cannot execute process with code :" + result);
            }
            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line;
            System.out.println("Standard output of executing: "+command);
            while((line = input.readLine()) != null){
                System.out.println(line);
            }
            BufferedReader errors = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
            System.out.println("Standard error of executing: "+command);
            while((line = errors.readLine()) != null){
                System.out.println(line);
            }
        }catch (IOException e){
            throw new ProcessExecutionException("Failed to execute command: "+command,e);
        }
        catch (InterruptedException e){
            throw new ProcessExecutionException("Execution interrupted",e);
        }

    }
}
