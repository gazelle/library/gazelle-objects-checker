package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.oracles.DatatypeTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.application.specifications.DatatypeTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.interlay.*;

public class DatatypeFactory implements SpecificationFactory{

    @Override
    public SpecificationTestRunner createTestRunner() {
        return new DatatypeTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new DatatypeTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
