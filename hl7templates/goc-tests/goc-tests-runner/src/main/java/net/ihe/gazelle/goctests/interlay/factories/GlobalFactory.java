package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.GlobalTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.oracles.GlobalTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.interlay.*;

public class GlobalFactory implements SpecificationFactory{
    @Override
    public SpecificationTestRunner createTestRunner() {
        return new GlobalTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new GlobalTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
