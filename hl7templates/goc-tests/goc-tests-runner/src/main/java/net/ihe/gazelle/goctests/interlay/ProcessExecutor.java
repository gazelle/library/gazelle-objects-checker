package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.interlay.exceptions.ProcessExecutionException;

import java.util.List;

public interface ProcessExecutor {

    void executeProcess(String command) throws ProcessExecutionException;

    List<String> executeProcessAndReturn(String command) throws ProcessExecutionException;

}
