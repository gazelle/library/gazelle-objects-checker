package net.ihe.gazelle.goctests.application.exceptions;

public class ValidationException extends Exception{

    private static final String EXCEPTION_MESSAGE = "CDA Document Validation failed: ";

    public ValidationException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public ValidationException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
