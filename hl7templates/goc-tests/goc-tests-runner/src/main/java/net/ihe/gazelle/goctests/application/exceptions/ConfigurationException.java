package net.ihe.gazelle.goctests.application.exceptions;

public class ConfigurationException extends Exception {

    private static final String EXCEPTION_MESSAGE = "Configuration problem: ";

    public ConfigurationException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
