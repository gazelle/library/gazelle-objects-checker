package net.ihe.gazelle.goctests.application;

import net.ihe.gazelle.goctests.application.exceptions.ValidatorGenerationException;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;
import net.ihe.gazelle.goctests.interlay.GenerationConfiguration;

import java.net.URL;

public interface ValidatorGenerator {


    /**
     * Generate Model Based Validator with {@link GenerationConfiguration} parameter
     * @param generationConfiguration object holding all generation configuration
     * @return GeneratedValidator, the model definition of the validator
     * @throws ValidatorGenerationException if generation failed
     */
    GeneratedValidator generateValidator(GenerationConfiguration generationConfiguration) throws ValidatorGenerationException;

    /**
     * Generate Model Based Validator with String parameters
     * @param bbrURL The path to the BBR file (XML)
     * @param outputFolder Main output directory that will contains /Validator and /logs
     * @param mvn The path to the Maven <b>Binary</b>
     * @return GeneratedValidator, the model definition of the validator
     * @throws ValidatorGenerationException if generation failed
     */
    GeneratedValidator generateValidator(URL bbrURL, String outputFolder, String mvn, String hl7Config) throws ValidatorGenerationException;


    /**
     * <b>Generation EntryPoint</b><br>
     * Generate Model Based Validator with URL with:
     * <ul>
     *     <li><b>OutputFolder:</b> Temporary folder for Testing</li>
     *     <li><b>MavenBinary:</b> From application.properties File</li>
     * </ul>
     * @param bbrURL URL of BBR Resource
     * @param hl7Config HL7 Configuration
     * @return GeneratedValidator, the model definition of the validator
     * @throws ValidatorGenerationException if generation failed
     */
    GeneratedValidator generateValidator(URL bbrURL, String hl7Config) throws ValidatorGenerationException;



}
