package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;

public interface SpecificationFactory {

    SpecificationTestRunner createTestRunner();

    TestOracle createTestOracle();

}
