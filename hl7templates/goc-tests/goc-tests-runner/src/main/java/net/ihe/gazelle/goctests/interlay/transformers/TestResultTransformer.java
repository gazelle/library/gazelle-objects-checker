package net.ihe.gazelle.goctests.interlay.transformers;

import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.interlay.exceptions.ResultTransformationException;

public interface TestResultTransformer {


    void transformResult(TestResult testResult) throws ResultTransformationException;
}
