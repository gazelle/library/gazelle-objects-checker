package net.ihe.gazelle.goctests.application;

import net.ihe.gazelle.goctests.application.exceptions.ValidationException;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;
import net.ihe.gazelle.goctests.application.models.ValidationResult;

import java.net.URL;

public interface CDAValidator {

    /**
     * Validate CDA Document based on path and a validator
     * throught the MBVal CLI (validator.sh)
     * @param documentPath String path
     * @param validator the generated validator object
     * @return The validation result object
     * @throws ValidationException if valdiation failed
     */
    ValidationResult validateDocument(String documentPath, GeneratedValidator validator) throws ValidationException;

    /**
     * Validate CDA Document based on URL and a validator
     * throught the MBVal CLI (validator.sh)
     * @param documentURL URL Path (from resources)
     * @param validator the generated validator object
     * @return The validation result object
     * @throws ValidationException if validation failed
     */
    ValidationResult validateDocument(URL documentURL, GeneratedValidator validator) throws ValidationException;
}
