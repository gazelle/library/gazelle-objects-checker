package net.ihe.gazelle.goctests.application.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GeneratedValidator implements Serializable {

    public static final String ROOT_LOG_DIRECTORY = "logs";

    public static final String ROOT_LOG_PREFIX = "log-";

    public static final String HL7TEMPLATE_RESOURCES_PATH = "hl7templates-resources";

    public static final String VALIDATOR_APP_SUFFIX = "-validator-app";

    public static final String ROOT_VALIDATOR_DIRECTORY = "validator";

    public static final String VALIDATOR_BINARY_SHELL = "/bin/validator.sh";

    public static final String VALIDATOR_RESOURCES_DIRECTORY = "/bin/resources";

    @XmlElement(name = "OutputFolder")
    private String outputFolder;

    @XmlElement(name = "Logs")
    private String logFolder;

    @XmlElement(name = "GenerationDate")
    private String generationDate;

    @XmlElement(name = "BBR")
    private String usedBBR;

    @XmlElement(name = "HL7Resources")
    private String hl7TemplateResources;

    @XmlElement(name = "ValidatorBinary")
    private String validatorBinaryPath;

    @XmlElement(name = "ProjectName")
    private String projectName;

    @XmlElement(name = "ValidatorResources")
    private String validatorResources;


    /**
     * Private base constructor for only XML Marshalling use by JAXB
     */
    private GeneratedValidator() {
    }

    public GeneratedValidator(String outputFolder, String generationDate, String projectName, String usedBBR) {
        this.outputFolder = outputFolder;
        this.generationDate = generationDate;
        this.projectName = projectName;
        this.usedBBR = usedBBR;
        this.logFolder = outputFolder+"/"+ROOT_LOG_DIRECTORY+"/"+ROOT_LOG_PREFIX+generationDate+"/";
        this.validatorBinaryPath = outputFolder+"/"+ROOT_VALIDATOR_DIRECTORY+"/"+projectName+VALIDATOR_APP_SUFFIX+VALIDATOR_BINARY_SHELL;
        this.hl7TemplateResources = outputFolder+"/"+HL7TEMPLATE_RESOURCES_PATH;
        this.validatorResources = outputFolder+"/"+ROOT_VALIDATOR_DIRECTORY+"/"+projectName+VALIDATOR_APP_SUFFIX+VALIDATOR_RESOURCES_DIRECTORY;
    }


    public String getUsedBBR() {
        return usedBBR;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public String getLogFolder() {
        return logFolder;
    }

    public String getGenerationDate() {
        return generationDate;
    }

    public String getHl7TemplateResources() {
        return hl7TemplateResources;
    }

    public String getValidatorBinaryPath() {
        return validatorBinaryPath;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getValidatorResources() {
        return validatorResources;
    }


    public void setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public void setLogFolder(String logFolder) {
        this.logFolder = logFolder;
    }

    public void setGenerationDate(String generationDate) {
        this.generationDate = generationDate;
    }

    public void setUsedBBR(String usedBBR) {
        this.usedBBR = usedBBR;
    }

    public void setHl7TemplateResources(String hl7TemplateResources) {
        this.hl7TemplateResources = hl7TemplateResources;
    }

    public void setValidatorBinaryPath(String validatorBinaryPath) {
        this.validatorBinaryPath = validatorBinaryPath;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setValidatorResources(String validatorResources) {
        this.validatorResources = validatorResources;
    }

    @Override
    public String toString() {
        return "GeneratedValidator{" +
                "outputFolder='" + outputFolder + '\'' +
                ", logFolder='" + logFolder + '\'' +
                ", generationDate='" + generationDate + '\'' +
                ", usedBBR='" + usedBBR + '\'' +
                ", hl7TemplateResources='" + hl7TemplateResources + '\'' +
                ", validatorBinaryPath='" + validatorBinaryPath + '\'' +
                ", projectName='" + projectName + '\'' +
                ", validatorResources='" + validatorResources + '\'' +
                '}';
    }


}
