package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.oracles.ClosedTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.application.specifications.ClosedSpecificationRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.interlay.*;

public class ClosedFactory implements SpecificationFactory{

    @Override
    public SpecificationTestRunner createTestRunner() {
        return new ClosedSpecificationRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new ClosedTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
