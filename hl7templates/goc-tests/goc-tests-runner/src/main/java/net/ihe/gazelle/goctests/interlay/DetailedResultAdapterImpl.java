package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.gen.common.DetailedResultMarshaller;
import net.ihe.gazelle.goctests.application.DetailedResultAdapter;
import net.ihe.gazelle.goctests.application.oracles.exceptions.OracleException;
import net.ihe.gazelle.validation.DetailedResult;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.URL;

public class DetailedResultAdapterImpl implements DetailedResultAdapter {

    @Override
    public DetailedResult getDetailedResult(URL url) throws OracleException {
        try {
            DetailedResult expectedResult = DetailedResultMarshaller.load(url.openStream());
            return expectedResult;
        } catch (JAXBException e) {
            throw new OracleException("Failed parsing Expected Result");
        } catch (IOException e) {
            throw new OracleException("Could not load Expected Result resources");
        }
    }
}
