package net.ihe.gazelle.goctests.interlay;


import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.goctests.application.exceptions.ReportGenerationException;
import net.ihe.gazelle.goctests.application.exceptions.TestRunnerException;

import net.ihe.gazelle.goctests.interlay.exceptions.EnvironementException;
import net.ihe.gazelle.goctests.interlay.exceptions.ResultTransformationException;
import net.ihe.gazelle.goctests.interlay.exceptions.SpecificationException;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

/**
 * GOC System Tests Command Line Interface
 */

public class GOCTestEntryCLI implements GOCTestEntry {

    @Option(name = "--spec-type",aliases = {"-S"}, metaVar = "Specification Test Type",
            usage = "Specification test from list (see Readme.md)", required = false)
    private String specificationTypeString;

    public void runSystemTest(String[] args){
        CmdLineParser parser = new CmdLineParser(this);

        SpecificationType specificationType;
        try {
            parser.parseArgument(args);
            if(specificationTypeString == null){
                specificationType = SpecificationType.GLOBAL;
            }
            else{
                specificationType = SpecificationType.of(specificationTypeString);
            }
            if(specificationType == null){
                throw new SpecificationException("Provided specification not recognized, " +
                        "please choose from the following list: "+SpecificationType.getPossibleValues());
            }

            SystemTestExecutor executor = new SystemTestExecutor(specificationType);
            executor.execute();

        } catch (CmdLineException | SpecificationException | TestRunnerException
                | ReportGenerationException | ResultTransformationException | EnvironementException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        GOCTestEntryCLI gocTests = new GOCTestEntryCLI();
        gocTests.runSystemTest(args);
        
    }


}
