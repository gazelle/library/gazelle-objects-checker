package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.interlay.exceptions.ProcessExecutionException;
import net.ihe.gazelle.goctests.application.exceptions.ValidatorGenerationException;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;
import net.ihe.gazelle.goctests.application.ValidatorGenerator;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.StreamBBRResource;
import net.ihe.gazelle.lib.hl7templatespackager.application.ProjectNaming;
import net.ihe.gazelle.lib.hl7templatespackager.application.ValidatorConfiguration;
import net.ihe.gazelle.lib.hl7templatespackager.peripherals.GOCExecutor;
import net.ihe.gazelle.goctests.interlay.utils.ValidatorGeneratorUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;


public class ValidatorGeneratorImpl implements ValidatorGenerator {


    @Override
    public GeneratedValidator generateValidator(GenerationConfiguration generationConfiguration) throws ValidatorGenerationException {

        System.out.println("Start generation for test");

        String outputFolder = generationConfiguration.getOutputFolder();
        URL bbrUrl = generationConfiguration.getBbrUrl();
        String mvn = generationConfiguration.getMvn();

        Boolean ignoreTemplateIdRequirements = generationConfiguration.isIgnoreTemplateIdRequirements();
        Boolean ignoreCdaBasicRequirements = generationConfiguration.isIgnoreCdaBasicRequirements();
        String serviceName = generationConfiguration.getServiceName();
        String versionLabel = generationConfiguration.getVersionLabel();
        String hl7Conf = generationConfiguration.getHl7Conf();

        //Run GOC
        GOCExecutor gocExecutor = new GOCExecutor(outputFolder);
        ValidatorConfiguration validatorConfiguration = gocExecutor.execute(new StreamBBRResource(bbrUrl),mvn,
                versionLabel, ignoreTemplateIdRequirements, ignoreCdaBasicRequirements, serviceName, hl7Conf);

        StringBuilder logPath = new StringBuilder(outputFolder)
                .append("/").append(GeneratedValidator.ROOT_LOG_DIRECTORY)
                .append("/").append(GeneratedValidator.ROOT_LOG_PREFIX)
                .append(gocExecutor.getGenerationDate());

        if(validatorConfiguration == null){
            throw new ValidatorGenerationException("Please refer to the logs at: "+ logPath.toString());
        }

        //Buil Generated Validator Object
        ProjectNaming projectNaming = validatorConfiguration.getProjectNaming();
        String projectName = projectNaming.getProjectName();

        GeneratedValidator validator = new GeneratedValidator(outputFolder, gocExecutor.getGenerationDate(),projectName,bbrUrl.getPath());

        // Setup Rights
        try {
            ValidatorGeneratorUtils.makeGeneratedValidatorExecutable(validator);
            System.out.println("Validator generated successfully");
        } catch (ProcessExecutionException | IOException e) {
            throw new ValidatorGenerationException("Failed to make generated validator executable",e);
        }

        return validator;
    }

    @Override
    public GeneratedValidator generateValidator(URL bbrUrl, String outputFolder, String mvn, String hl7Config) throws ValidatorGenerationException {
        GenerationConfiguration generationConfiguration = new GenerationConfiguration(bbrUrl, outputFolder, mvn);
        generationConfiguration.setHl7Conf(hl7Config);
        return this.generateValidator(generationConfiguration);

    }

    @Override
    public GeneratedValidator generateValidator(URL bbrURL, String hl7Config) throws ValidatorGenerationException {

        // Get maven path
        String mvn = EnvironementConfiguration.getInstance().getProperty("maven.path");
        if(mvn == null){
            throw new ValidatorGenerationException("Maven path is not set in "+EnvironementConfiguration.APPLICATION_PROPERTIES_FILE);
        }

        // Check if GOC environment is setup
        Path path = EnvironementConfiguration.getInstance().getGocOutputFolder();
        if(path == null){
            throw new ValidatorGenerationException("GOC Output folder is not set");
        }

        return generateValidator(bbrURL,path.toAbsolutePath().toString(),mvn, hl7Config);

    }
}
