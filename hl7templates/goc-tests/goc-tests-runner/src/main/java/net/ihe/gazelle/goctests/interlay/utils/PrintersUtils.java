package net.ihe.gazelle.goctests.interlay.utils;



public class PrintersUtils {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private PrintersUtils(){}

    public static String success(String string){
        return ANSI_RESET+ANSI_GREEN+string+ANSI_RESET;
    }

    public static String failed(String string){
        return ANSI_RESET+ANSI_RED+string+ANSI_RESET;
    }

    public static String warning(String string){
        return ANSI_RESET+ANSI_YELLOW+string+ANSI_RESET;
    }

    public static String info(String string){
        return ANSI_RESET+ANSI_BLUE+string+ANSI_RESET;
    }

}
