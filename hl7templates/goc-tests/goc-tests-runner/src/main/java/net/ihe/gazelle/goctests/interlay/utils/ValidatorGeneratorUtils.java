package net.ihe.gazelle.goctests.interlay.utils;

import net.ihe.gazelle.goctests.interlay.exceptions.ProcessExecutionException;
import net.ihe.gazelle.goctests.application.exceptions.ValidatorGenerationException;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.EnumSet;

final public class ValidatorGeneratorUtils {

    private ValidatorGeneratorUtils(){};


    /**
     * This utility method makes the validator executable with POSIX Permessions for 'OWNER'.
     * The method could be used to check if the generation process succeeded
     * @param validator generated validator reference
     * @throws ProcessExecutionException if exeuction process failed
     * @throws ValidatorGenerationException if generation failed
     * @throws IOException if system internal error occured
     */

    public static void makeGeneratedValidatorExecutable(GeneratedValidator validator) throws ProcessExecutionException, ValidatorGenerationException, IOException {
        if(validator == null){
            throw new ValidatorGenerationException("Validator not defined: NULL");
        }
        String validatorBinary = validator.getValidatorBinaryPath();
        Path validatorBinaryPath = Path.of(validatorBinary);
        if(!Files.exists(validatorBinaryPath)){
            throw new ValidatorGenerationException("Validator binary not found at: "+validatorBinary);
        }
        if(Files.isExecutable(validatorBinaryPath)){
            System.out.println("INFO: Validator Binary is already executable");
        }
        else{
            Files.setPosixFilePermissions(validatorBinaryPath,EnumSet.of(PosixFilePermission.OWNER_EXECUTE, PosixFilePermission.OWNER_READ));
            System.out.println("Rights updated for validator");
        }
    }

}
