package net.ihe.gazelle.goctests.interlay;


import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.exceptions.TestRunnerException;
import net.ihe.gazelle.goctests.application.exceptions.ReportGenerationException;

import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.goctests.interlay.exceptions.EnvironementException;
import net.ihe.gazelle.goctests.interlay.exceptions.SpecificationException;
import net.ihe.gazelle.goctests.interlay.exceptions.ResultTransformationException;
import net.ihe.gazelle.goctests.interlay.factories.FactoryProvider;
import net.ihe.gazelle.goctests.interlay.factories.SpecificationFactory;
import net.ihe.gazelle.goctests.interlay.transformers.TestResultTransformer;
import net.ihe.gazelle.goctests.interlay.transformers.TestResultTransformerFactory;
import net.ihe.gazelle.goctests.interlay.transformers.TransformationType;


final public class SystemTestExecutor {

    private SpecificationType specificationType;

    public SystemTestExecutor(SpecificationType specificationType) {
        this.specificationType = specificationType;
    }

    /**
     * Main entry point to execute the GOC System Testing, and return testResult,
     * for better integration with other modules
     * @return The raw TestResult Object
     * @throws SpecificationException if cannot parse specification
     * @throws TestRunnerException if Test run failed
     * @throws ReportGenerationException if Report generation failed
     * @throws ResultTransformationException if Report transformation failed
     */
    public TestResult execute() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {

        TestResult testResult = new TestResult();
        try{
            if(specificationType == null){
                throw new SpecificationException("Specification is null");
            }

            EnvironementConfiguration.getInstance().initEnvironement();

            SpecificationFactory factory = FactoryProvider.create(specificationType);
            SpecificationTestRunner runner = factory.createTestRunner();
            if(runner == null){
                throw new SpecificationException("Specification not implemented");
            }

            runner.runTest();
            testResult = runner.generateResult();

            String transformationString = EnvironementConfiguration.getInstance().getProperty("result.type");
            TransformationType transformationType;
            if(transformationString == null){
                transformationType = TransformationType.STANDARD;
            }
            else{
                transformationType = TransformationType.of(transformationString);
            }
            if(transformationType == null){
                throw new ResultTransformationException("Invalid Transformation Tyoe at: result.type");
            }

            TestResultTransformer transformer = TestResultTransformerFactory.createTestResultTransformer(transformationType);
            if(transformer == null){
                throw new ResultTransformationException("An Error has occured during transformation, probably type not implemented yet");
            }

            transformer.transformResult(testResult);

            return testResult;

        } catch (ReportGenerationException e) {
            throw new ReportGenerationException("Problem during report generation",e);
        } catch (TestRunnerException e) {
            throw new TestRunnerException("Problem during running test",e);
        } catch (SpecificationException e) {
            throw new SpecificationException("Probelm with specification",e);
        } catch (EnvironementException e) {
            throw new EnvironementException("Something wrong with the environement configuration",e);
        } catch (ResultTransformationException e) {
            System.err.println("ERROR: Transformation Failed, report not generated, but Test performed");
            return testResult;
        } finally {
            EnvironementConfiguration.getInstance().cleanEnvironement();
        }

    }


}
