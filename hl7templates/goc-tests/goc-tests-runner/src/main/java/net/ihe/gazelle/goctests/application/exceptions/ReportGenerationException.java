package net.ihe.gazelle.goctests.application.exceptions;

public class ReportGenerationException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Report Generation Failed: ";

    public ReportGenerationException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public ReportGenerationException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
