package net.ihe.gazelle.goctests.application.models;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement
@XmlType(propOrder = {"count","warningOrErrorOrNote"})
@XmlAccessorType(XmlAccessType.FIELD)
public class NotificationElements {

    @XmlElement(name = "Count")
    private int count;

    @XmlElementWrapper(name = "Rules")
    @XmlElements({@XmlElement(
            name = "Warning",
            type = Warning.class
    ), @XmlElement(
            name = "Note",
            type = Note.class
    ), @XmlElement(
            name = "Info",
            type = Info.class
    ), @XmlElement(
            name = "Error",
            type = Error.class
    )})
    private List<Notification> warningOrErrorOrNote;

    public NotificationElements() {
    }

    public NotificationElements(List<Notification> elements) {
        this.warningOrErrorOrNote = elements;
        this.count = elements.size();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public void setWarningOrErrorOrNote2(List<Notification> warningOrErrorOrNote) {
        this.warningOrErrorOrNote = warningOrErrorOrNote;
    }
}