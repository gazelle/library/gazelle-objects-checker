package net.ihe.gazelle.goctests.interlay.transformers;

import net.ihe.gazelle.goctests.application.models.NotificationElements;
import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.models.TestResultReport;
import net.ihe.gazelle.goctests.interlay.exceptions.ResultTransformationException;
import net.ihe.gazelle.goctests.interlay.exceptions.TestResultMarhsallException;
import net.ihe.gazelle.goctests.interlay.utils.TestResultMarshaller;

import java.text.SimpleDateFormat;

public class XMLTestResultTransformer implements TestResultTransformer{

    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");

    private static final String XML_EXTENSION = ".xml";

    private static final String DASH = "-";

    @Override
    public void transformResult(TestResult testResult) throws ResultTransformationException {

        try {
            String result = testResult.getOracleResult().isValid()?"PASSED":"FAILED";

            TestResultReport report = new TestResultReport();
            report.setTestResult(result);
            report.setUsedValidator(testResult.getUsedValidator());
            report.setValidationDate(testResult.getFormattedValidationDate());
            report.setSpecification(testResult.getSpecificationType().getValue());
            report.setExpectedElements(testResult.getOracleResult().getExpectedElements().size());
            report.setProcessedElements(testResult.getOracleResult().getProcessed().size());
            report.setMissedElements(new NotificationElements(testResult.getOracleResult().getMissedElements()));
            report.setUnexpectedElements(new NotificationElements(testResult.getOracleResult().getUnexpectedElements()));

            StringBuilder reportFileName = new StringBuilder(testResult.getSpecificationType().getValue())
                    .append(DASH)
                    .append(formatter.format(testResult.getValidationDate()))
                    .append(XML_EXTENSION);
            
            String savedAt = TestResultMarshaller.save(report, reportFileName.toString());
            System.out.println("Report Transformed to XML at: "+savedAt);

        } catch (TestResultMarhsallException e) {
            throw new ResultTransformationException("could not save report as XML",e);
        }

    }
}
