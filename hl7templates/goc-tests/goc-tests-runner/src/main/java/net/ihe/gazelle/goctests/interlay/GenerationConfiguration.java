package net.ihe.gazelle.goctests.interlay;

import java.net.URL;

public class GenerationConfiguration {

    private URL bbrUrl;

    private String outputFolder;

    private String mvn;

    private Boolean ignoreTemplateIdRequirements = Boolean.FALSE;

    private Boolean ignoreCdaBasicRequirements = Boolean.FALSE;

    private String serviceName;

    private String versionLabel;

    private String hl7Conf;


    public GenerationConfiguration(URL bbrUrl, String outputFolder, String mvn) {
        this.bbrUrl = bbrUrl;
        this.outputFolder = outputFolder;
        this.mvn = mvn;
    }

    public GenerationConfiguration(URL bbrUrl, String outputFolder, String mvn, Boolean ignoreTemplateIdRequirements,
                                   Boolean ignoreCdaBasicRequirements, String serviceName, String hl7Conf) {
        this.bbrUrl = bbrUrl;
        this.outputFolder = outputFolder;
        this.mvn = mvn;
        this.ignoreTemplateIdRequirements = ignoreTemplateIdRequirements;
        this.ignoreCdaBasicRequirements = ignoreCdaBasicRequirements;
        this.serviceName = serviceName;
        this.hl7Conf = hl7Conf;
    }

    public URL getBbrUrl() {
        return bbrUrl;
    }

    public void setBbrUrl(URL bbrUrl) {
        this.bbrUrl = bbrUrl;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public void setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public String getMvn() {
        return mvn;
    }

    public void setMvn(String mvn) {
        this.mvn = mvn;
    }

    public Boolean isIgnoreTemplateIdRequirements() {
        return ignoreTemplateIdRequirements;
    }

    public void setIgnoreTemplateIdRequirements(Boolean ignoreTemplateIdRequirements) {
        this.ignoreTemplateIdRequirements = ignoreTemplateIdRequirements;
    }

    public Boolean isIgnoreCdaBasicRequirements() {
        return ignoreCdaBasicRequirements;
    }

    public void setIgnoreCdaBasicRequirements(Boolean ignoreCdaBasicRequirements) {
        this.ignoreCdaBasicRequirements = ignoreCdaBasicRequirements;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVersionLabel() {
        return versionLabel;
    }

    public void setVersionLabel(String versionLabel) {
        this.versionLabel = versionLabel;
    }

    public String getHl7Conf() {
        return hl7Conf;
    }

    public void setHl7Conf(String hl7Conf) {
        this.hl7Conf = hl7Conf;
    }
}
