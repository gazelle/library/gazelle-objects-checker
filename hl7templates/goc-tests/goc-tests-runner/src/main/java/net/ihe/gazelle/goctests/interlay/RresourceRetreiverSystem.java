package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.application.ResourceRetreiver;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.goctests.application.exceptions.ConfigurationException;
import org.eclipse.emf.ecore.resource.impl.PlatformResourceURIHandlerImpl;

import java.net.URL;
import java.util.regex.Pattern;

/**
 * Implementation of ResourceRetreiver API
 * Loading resources from FileSystem (i.e <b>.properties</b> files)
 */
public class RresourceRetreiverSystem implements ResourceRetreiver {

    private final static String BBR_SUFFIX = ".bbr";

    private final static String CDA_SUFFIX = ".cda";

    private final static String EXPECTED_SUFFIX = ".expected";

    private final static String EMPTY = "";

    private final static String SPACE = " ";

    private static final String ADAPTATION_PATTERN_STRING = SPACE;

    private static final Pattern ADAPTATION_PATTERN = Pattern.compile(ADAPTATION_PATTERN_STRING);

    private static final String NULL_STRING = "Specification Type is null";

    /**
     * Retreive BBR resource from .properties specifications files with .bbr suffix
     * @param specificationType specification
     * @return URL of Resource
     * @throws ConfigurationException if resource retreive failed
     */
    @Override
    public URL retreiveBBRResource(SpecificationType specificationType) throws ConfigurationException {

        if(specificationType  == null){
            throw new ConfigurationException(NULL_STRING);
        }
        String specificationString = adaptSpecification(specificationType);

        String bbrPath = EnvironementConfiguration.getInstance().getProperty(specificationString+BBR_SUFFIX);
        if(bbrPath == null){
            throw new ConfigurationException("BBR property not set correctly at: "+specificationString+BBR_SUFFIX);
        }
        URL bbrURL = RresourceRetreiverSystem.class.getClassLoader().getResource(bbrPath);
        if(bbrURL == null){
            throw new ConfigurationException("Could not retreive BBR Resource at: "+bbrPath);
        }
        return bbrURL;
    }

    /**
     * Retreive CDA resource from .properties specifications files with .cda suffix
     * @param specificationType specification
     * @return URL resource of CDA
     * @throws ConfigurationException if resource retreive failed
     */
    @Override
    public URL retreiveCDAResource(SpecificationType specificationType) throws ConfigurationException {

        if(specificationType  == null){
            throw new ConfigurationException(NULL_STRING);
        }
        String specificationString = adaptSpecification(specificationType);

        String cdaPath = EnvironementConfiguration.getInstance().getProperty(specificationString+CDA_SUFFIX);
        if(cdaPath == null){
            throw new ConfigurationException("CDA property not set correctly at: "+specificationString+CDA_SUFFIX);
        }
        URL cdaURL = RresourceRetreiverSystem.class.getClassLoader().getResource(cdaPath);
        if(cdaURL == null){
            throw new ConfigurationException("Could not retreive CDA Resource at: "+cdaPath);
        }
        return cdaURL;
    }

    /**
     * Retreive ExpectedResult resource from .properties specifications files with .expected suffix
     * @param specificationType specification
     * @return URL resource of CDA
     * @throws ConfigurationException if resource retreive failed
     */
    @Override
    public URL retreiveExpectedResultResource(SpecificationType specificationType) throws ConfigurationException {

        if(specificationType  == null){
            throw new ConfigurationException(NULL_STRING);
        }
        String specificationString = adaptSpecification(specificationType);

        String specificationPath = EnvironementConfiguration.getInstance().getProperty(specificationString+EXPECTED_SUFFIX);
        if(specificationPath == null){
            throw new ConfigurationException("Expected result configuration not found");
        }
        URL url = RresourceRetreiverSystem.class.getClassLoader().getResource(specificationPath);
        if(url == null){
            throw new ConfigurationException("Could not load Expected Result resources at: "+specificationString+EXPECTED_SUFFIX);
        }
        return url;
    }


    /**
     * {@inheritDoc}
     */
    public String adaptSpecification(SpecificationType specificationType){
        return ADAPTATION_PATTERN.matcher(specificationType.getValue()).replaceAll(EMPTY);
    }

    @Override
    public String retreiveHL7Config() throws ConfigurationException {
        return System.getProperty("hl7Conf","cdabasic");
    }
}
