package net.ihe.gazelle.goctests.interlay;

public interface GOCTestEntry {

    void runSystemTest(String[] args);

}
