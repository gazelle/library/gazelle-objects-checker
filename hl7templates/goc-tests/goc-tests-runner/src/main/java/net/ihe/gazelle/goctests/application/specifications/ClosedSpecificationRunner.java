package net.ihe.gazelle.goctests.application.specifications;

import net.ihe.gazelle.goctests.application.CDAValidator;
import net.ihe.gazelle.goctests.application.ResourceRetreiver;
import net.ihe.gazelle.goctests.application.ValidatorGenerator;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;

public class ClosedSpecificationRunner extends SpecificationTestRunnerImpl{

    public ClosedSpecificationRunner(ResourceRetreiver resourceRetreiver, CDAValidator cdaValidator, ValidatorGenerator validatorGenerator, TestOracle testOracle) {
        super(SpecificationType.CLOSED, resourceRetreiver, cdaValidator, validatorGenerator, testOracle);
    }
}
