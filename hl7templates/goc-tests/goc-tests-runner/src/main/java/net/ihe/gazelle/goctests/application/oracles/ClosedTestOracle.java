package net.ihe.gazelle.goctests.application.oracles;

import net.ihe.gazelle.goctests.application.DetailedResultAdapter;
import net.ihe.gazelle.goctests.application.ResourceRetreiver;
import net.ihe.gazelle.goctests.application.models.OracleResult;
import net.ihe.gazelle.goctests.application.oracles.exceptions.OracleException;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

public class ClosedTestOracle extends TestOracleImpl{

    public ClosedTestOracle(ResourceRetreiver resourceRetreiver, DetailedResultAdapter detailedResultAdapter) {
        super(SpecificationType.CLOSED, resourceRetreiver, detailedResultAdapter);
    }

    @Override
    public OracleResult compareWithExpectedResult(DetailedResult actualResult) throws OracleException {
        // Add more processing if needed

        return super.compareWithExpectedResult(actualResult);
    }

    @Override
    public boolean matchingCriteria(Notification target, List<Notification> elements) {
        // Add more processing if needed

        return super.matchingCriteria(target, elements);
    }
}
