package net.ihe.gazelle.goctests.interlay.exceptions;

public class TestResultMarhsallException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Failed to marshall Test Result to XML: ";

    public TestResultMarhsallException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public TestResultMarhsallException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
