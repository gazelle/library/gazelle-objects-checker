package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.oracles.FixedvalTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.application.specifications.FixedvalTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.interlay.*;

public class FixedvalFactory implements SpecificationFactory{

    @Override
    public SpecificationTestRunner createTestRunner() {
        return new FixedvalTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new FixedvalTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
