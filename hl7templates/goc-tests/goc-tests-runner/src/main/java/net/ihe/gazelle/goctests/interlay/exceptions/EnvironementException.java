package net.ihe.gazelle.goctests.interlay.exceptions;

public class EnvironementException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Environement Exception: ";

    public EnvironementException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public EnvironementException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
