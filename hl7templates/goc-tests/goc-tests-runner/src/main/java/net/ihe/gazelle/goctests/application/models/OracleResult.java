package net.ihe.gazelle.goctests.application.models;

;
import net.ihe.gazelle.validation.Notification;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class OracleResult {

    private boolean valid;

    private List<Notification> processed;

    private List<Notification> expectedElements;

    private List<Notification> missedElements;

    private List<Notification> unexpectedElements;

    /**
     * Private base constructor for only XML Marshalling use by JAXB
     */
    private OracleResult(){}

    public OracleResult(List<Notification> processed, List<Notification> expectedElements, List<Notification> missedElements, List<Notification> unexpectedElements) {
        this.processed = processed;
        this.expectedElements = expectedElements;
        this.missedElements = missedElements;
        this.unexpectedElements = unexpectedElements;
        this.valid = ((missedElements.size() == 0) && (unexpectedElements.size() == 0));
    }

    public boolean isValid() {
        return valid;
    }

    public List<Notification> getProcessed() {
        return processed;
    }

    public List<Notification> getExpectedElements() {
        return expectedElements;
    }

    public List<Notification> getMissedElements() {
        return missedElements;
    }

    public List<Notification> getUnexpectedElements() {
        return unexpectedElements;
    }

    // TODO: 04/01/2022 Check if accessors need to be private

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setProcessed(List<Notification> processed) {
        this.processed = processed;
    }

    public void setExpectedElements(List<Notification> expectedElements) {
        this.expectedElements = expectedElements;
    }

    public void setMissedElements(List<Notification> missedElements) {
        this.missedElements = missedElements;
    }

    public void setUnexpectedElements(List<Notification> unexpectedElements) {
        this.unexpectedElements = unexpectedElements;
    }

    @Override
    public String toString() {
        return "OracleResult{" +
                "valid=" + valid +
                ", processed=" + processed +
                ", expected=" + expectedElements +
                ", missedElements=" + missedElements +
                ", unexpectedElements=" + unexpectedElements +
                '}';
    }
}
