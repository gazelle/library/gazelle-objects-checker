package net.ihe.gazelle.goctests.interlay.exceptions;

public class SpecificationException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Problem with provided specification: ";

    public SpecificationException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public SpecificationException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
