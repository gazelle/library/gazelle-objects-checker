package net.ihe.gazelle.goctests.interlay.exceptions;

public class ProcessExecutionException extends Exception{

    private static final String EXCEPTION_MESSAGE = "Process Execution failed: ";

    public ProcessExecutionException(String message) {
        super(EXCEPTION_MESSAGE+message);
    }

    public ProcessExecutionException(String message, Throwable cause) {
        super(EXCEPTION_MESSAGE+message, cause);
    }
}
