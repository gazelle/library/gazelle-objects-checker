package net.ihe.gazelle.goctests.interlay.factories;

import net.ihe.gazelle.goctests.application.specifications.CardinalityTestRunner;
import net.ihe.gazelle.goctests.application.specifications.SpecificationTestRunner;
import net.ihe.gazelle.goctests.application.oracles.CardinalityTestOracle;
import net.ihe.gazelle.goctests.application.oracles.TestOracle;
import net.ihe.gazelle.goctests.interlay.*;

public class CardinalityFactory implements SpecificationFactory{



    @Override
    public SpecificationTestRunner createTestRunner() {
        return new CardinalityTestRunner(new RresourceRetreiverSystem(),new CDAValidatorImpl(new ProcessExecutorBash()),
                new ValidatorGeneratorImpl(), createTestOracle());
    }

    @Override
    public TestOracle createTestOracle() {
        return new CardinalityTestOracle(new RresourceRetreiverSystem(),new DetailedResultAdapterImpl());
    }
}
