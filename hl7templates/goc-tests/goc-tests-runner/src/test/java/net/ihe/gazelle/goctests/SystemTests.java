package net.ihe.gazelle.goctests;

import net.ihe.gazelle.goctests.application.exceptions.ReportGenerationException;
import net.ihe.gazelle.goctests.application.exceptions.TestRunnerException;
import net.ihe.gazelle.goctests.application.models.TestResult;
import net.ihe.gazelle.goctests.application.specifications.SpecificationType;
import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.goctests.interlay.SystemTestExecutor;
import net.ihe.gazelle.goctests.interlay.exceptions.EnvironementException;
import net.ihe.gazelle.goctests.interlay.exceptions.ResultTransformationException;
import net.ihe.gazelle.goctests.interlay.exceptions.SpecificationException;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class SystemTests {

    SpecificationType specificationType;


    @Ignore
    @Test
    public void globalTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {

        this.specificationType = SpecificationType.GLOBAL;
        runTest();

    }


    @Ignore
    @Covers(requirements = {"GOC-001","GOC-002","GOC-003","GOC-004","GOC-025","GOC-026"}, testType = TestType.SYSTEM_TEST)
    @Test
    public void cardinalityTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {

        this.specificationType = SpecificationType.CARDINALITY;
        runTest();

    }

    @Ignore
    @Covers(requirements = {"GOC-006","GOC-027"}, testType = TestType.SYSTEM_TEST)
    @Test
    public void mandatoryTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {
        this.specificationType = SpecificationType.MANDATORY;
        runTest();

    }

    @Ignore
    @Covers(requirements = {"GOC-013","GOC-017","GOC-018","GOC-036","GOC-038","GOC-039","GOC-040","GOC-041",
                            "GOC-121","GOC-122","GOC-123","GOC-124","GOC-145"})
    @Test
    public void vocabularyTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {
        this.specificationType = SpecificationType.VOCABULARY;
        runTest();

    }

    @Ignore
    @Covers(requirements = {"GOC-012"}, testType = TestType.SYSTEM_TEST)
    @Test
    public void fixedValueTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {
        this.specificationType = SpecificationType.FIXEDVAL;
        runTest();

    }

    @Ignore
    @Covers(requirements = {"GOC-029","GOC-062","GOC-63"}, testType = TestType.SYSTEM_TEST)
    @Test
    public void datatypeTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {
        this.specificationType = SpecificationType.DATATYPE;
        runTest();

    }

    @Ignore
    @Covers(requirements = {"GOC-079","GOC-082","GOC-083","GOC-084","GOC-085","GOC-086"}, testType = TestType.SYSTEM_TEST)
    @Test
    public void contextTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {
        this.specificationType = SpecificationType.CONTEXT;
        runTest();

    }
    @Ignore
    @Test
    public void choiceTest() throws ReportGenerationException, TestRunnerException, SpecificationException,
            EnvironementException, ResultTransformationException {
        this.specificationType = SpecificationType.CHOICE;
        runTest();

    }


    private void runTest() throws ReportGenerationException, TestRunnerException,
            SpecificationException, EnvironementException, ResultTransformationException {

        SystemTestExecutor executor = new SystemTestExecutor(this.specificationType);
        TestResult testResult = executor.execute();

        customAssert(testResult);
    }


    private void customAssert(TestResult testResult){
        assertEquals(this.specificationType,testResult.getSpecificationType());
        assertEquals(0,testResult.getOracleResult().getMissedElements().size());
        assertEquals(0,testResult.getOracleResult().getUnexpectedElements().size());
        assertTrue(testResult.getOracleResult().isValid());
    }
}
