package net.ihe.gazelle.goctests.interlay;

import net.ihe.gazelle.goctests.application.exceptions.ValidatorGenerationException;
import net.ihe.gazelle.goctests.application.models.GeneratedValidator;
import net.ihe.gazelle.goctests.application.ValidatorGenerator;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class ValidatorGeneratorTest {

    /**
     * This is made as utility for local tests, to be ignore at package time
     * due to performance issues
     */
    @Ignore
    @Test
    public void generateValidatorTest() throws MalformedURLException {

        ValidatorGenerator validatorGenerator = new ValidatorGeneratorImpl();
        String bbr = "[YOUR BBR PATH]";
        String output = "[YOUR CDA PATH]";
        String mvn = "[YOUR MVN PATH]";

        GenerationConfiguration generationConfiguration = new GenerationConfiguration(new File(bbr).toURI().toURL(),output,mvn);
        //generationConfiguration.setHl7Conf("cdaxehealth");
        try {
            GeneratedValidator validator = validatorGenerator.generateValidator(generationConfiguration);
            org.junit.Assert.assertTrue(validator.getValidatorBinaryPath() != null);
            System.out.println(validator);
        } catch (ValidatorGenerationException e) {
            e.printStackTrace();
        }
    }
}