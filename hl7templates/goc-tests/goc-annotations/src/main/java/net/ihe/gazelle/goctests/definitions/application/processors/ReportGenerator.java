package net.ihe.gazelle.goctests.definitions.application.processors;

import net.ihe.gazelle.goctests.definitions.application.CSVParser;
import net.ihe.gazelle.goctests.definitions.application.CSVWriterException;
import net.ihe.gazelle.goctests.definitions.application.models.AnnotatedTestMethod;
import net.ihe.gazelle.goctests.definitions.application.CSVWriter;

import javax.lang.model.element.Element;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReportGenerator {

    private final List<AnnotatedTestMethod> validMethods;
    private final List<Element> invalidMethods;

    public ReportGenerator(List<AnnotatedTestMethod> validMethods, List<Element> invalidMethods) {
        this.validMethods = validMethods;
        this.invalidMethods = invalidMethods;
    }

    public void generateReport() throws CSVWriterException, IOException {
        String filePath = System.getProperty("csv");

        if(filePath == null || filePath.isEmpty()){
            throw new CSVWriterException("CSV File path not provided, please set it as system property");
        }

        List<String[]> document = new ArrayList<>();
        for(AnnotatedTestMethod method : validMethods){
            document.add(new String[]{method.getMethodName(),
                    String.join(CSVParser.REQUIREMENTS_DELIMITER,method.getRequirements()),
                    method.getTestType().getValue(),
                    method.getPackageName(),
                    method.getClassName(),
                    method.getComment()});
        }
        CSVWriter.writeDataToCSV(document,filePath);
        System.out.println("Report created successfully at: "+filePath);

        // TODO: 25/01/2022 Add invalid methods to report


    }


}
