package net.ihe.gazelle.goctests.definitions.application;

public class CSVWriterException extends Exception{

    public CSVWriterException(String message) {
        super(message);
    }

    public CSVWriterException(String message, Throwable cause) {
        super(message, cause);
    }
}
