package net.ihe.gazelle.goctests.definitions.application;

import net.ihe.gazelle.goctests.definitions.application.models.CSVColumn;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CSVWriter {

    private final static String CSV_HEADER = CSVColumn.CSV_HEADER;

    private static final Pattern ESCAPED_DATA_PATT = Pattern.compile("\\R");

    private CSVWriter(){}

    public static String convertLineToCSV(String[] line){
        return Stream.of(line)
                .map(CSVWriter::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public static void writeDataToCSV(List<String[]> data, String filePath) throws IOException {
        boolean filExist = Files.exists(Path.of(filePath));
        FileWriter fileWriter = new FileWriter(filePath,true);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        if(!filExist){
            printWriter.println(CSV_HEADER);
        }
        data.stream()
                .map(CSVWriter::convertLineToCSV)
                .forEach(printWriter::println);
        printWriter.close();
    }


    public static String escapeSpecialCharacters(String data) {
        String escapedData = ESCAPED_DATA_PATT.matcher(data).replaceAll(" ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }


}
