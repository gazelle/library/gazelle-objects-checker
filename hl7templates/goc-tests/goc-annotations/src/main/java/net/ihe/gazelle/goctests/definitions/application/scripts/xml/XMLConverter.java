package net.ihe.gazelle.goctests.definitions.application.scripts.xml;

import net.ihe.gazelle.goctests.definitions.application.CSVParser;
import net.ihe.gazelle.goctests.definitions.application.CSVParsingException;
import net.ihe.gazelle.goctests.definitions.application.models.CoverageReport;
import net.ihe.gazelle.goctests.definitions.application.scripts.Converter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class XMLConverter implements Converter {


    public void generate(String csvPath, String desPath) throws CSVParsingException {
        CoverageReport report = CSVParser.parseCoverageReport(csvPath);
        try {
            coverageReportMarshaller(report,desPath);
        } catch (JAXBException e) {
            throw new CSVParsingException("Report marshalling failed",e);
        }
        System.out.println("XML Generated at: "+desPath);
    }

    private static void coverageReportMarshaller(CoverageReport report, String path) throws JAXBException {
            JAXBContext jaxbContext = JAXBContext.newInstance(CoverageReport.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(report,new File(path));

    }
}
