package net.ihe.gazelle.goctests.definitions.application.scripts;

import net.ihe.gazelle.goctests.definitions.application.CSVParsingException;

import java.util.Arrays;

public class ConvertCSV {



    public static void main(String[] args) {

        if(args.length<3){
            System.err.println("Invalid number of parameters, CSV Conversion couldn't be done");
        }
        else {
            String csvPath = args[0];
            String destPath = args[1];
            if(csvPath == null || destPath == null){
                System.err.println("Invalid CSV/Report path, report coverage not converted");
            }
            else {
                ConverterType type = args[2] == null ? ConverterType.XML : ConverterType.of(args[2]);
                if(type == null){
                    System.err.println("Invalid converter type");
                }
                else{
                    Converter converter = ConverterFactory.getConverter(type);
                    try {
                        converter.generate(csvPath,destPath);
                    } catch (CSVParsingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
