package net.ihe.gazelle.goctests.definitions.application.models;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "Coverage")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoverageReport {

    @XmlElementWrapper(name = "Requirements")
    @XmlElement(name = "Requirement")
    private List<Requirement> requirements;

    private List<AnnotatedTestMethod> invalidMethods;

    public CoverageReport() {
        requirements = new ArrayList<>();
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public List<AnnotatedTestMethod> getInvalidMethods() {
        return invalidMethods;
    }

    public void setInvalidMethods(List<AnnotatedTestMethod> invalidMethods) {
        this.invalidMethods = invalidMethods;
    }
}
