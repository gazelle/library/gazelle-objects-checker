package net.ihe.gazelle.goctests.definitions.application;

public class CSVParsingException extends Exception{

    public CSVParsingException(String message) {
        super(message);
    }

    public CSVParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
