package net.ihe.gazelle.goctests.definitions.annotations;

public enum TestType {

    SYSTEM_TEST("system-test"),
    UNIT_TEST("unit-test"),
    INTEGRATION_TEST("integration-test");

    private String value;

    private TestType(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }

    public static TestType of(String value){
        for(TestType testType : TestType.values()){
            if(testType.value.equals(value)){
                return testType;
            }
        }
        return null;
    }

}
