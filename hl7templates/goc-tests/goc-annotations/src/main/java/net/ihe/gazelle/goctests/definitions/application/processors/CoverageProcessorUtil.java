package net.ihe.gazelle.goctests.definitions.application.processors;

import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.goctests.definitions.application.models.AnnotatedTestMethod;

import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import java.util.*;
import java.util.stream.Collectors;

final public class CoverageProcessorUtil {

    private CoverageProcessorUtil(){}


    public static boolean isElementIdentifierValide(Element element){
        if(element == null || element.getSimpleName() == null){
            return false;
        }
        return isElementIdentifierValide(element.getSimpleName().toString());
    }


    public static boolean isElementIdentifierValide(String stringIdentifier){
        if(stringIdentifier == null){
            return  false;
        }
        for(MethodTestIdentifier methodTestIdentifier:MethodTestIdentifier.values()){
            if(stringIdentifier.startsWith(methodTestIdentifier.getValue())
                    ||stringIdentifier.endsWith(methodTestIdentifier.getValue())){
                return  true;
            }
        }
        return false;
    }

    public static List<AnnotatedTestMethod> getAnnotatedTestMethods(List<Element> methods){
          return  methods.stream()
                        .map(CoverageProcessorUtil::constructAnnotatedTestMethod)
                        .collect(Collectors.toList());
    }





    private static AnnotatedTestMethod constructAnnotatedTestMethod(Element method){
        String methodName = method.getSimpleName().toString();
        String[] requirements = method.getAnnotation(Covers.class).requirements();
        TestType testType = method.getAnnotation(Covers.class).testType();
        String comment = method.getAnnotation(Covers.class).comment();
        String packageName = ((PackageElement) (method.getEnclosingElement().getEnclosingElement())).toString();
        String className = method.getEnclosingElement().getSimpleName().toString();
        return  new AnnotatedTestMethod(methodName, requirements, testType, packageName, className, comment);
    }

}
