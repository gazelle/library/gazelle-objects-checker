package net.ihe.gazelle.goctests.definitions.application.scripts;

import net.ihe.gazelle.goctests.definitions.application.scripts.excel.ExcelConverter;
import net.ihe.gazelle.goctests.definitions.application.scripts.xml.XMLConverter;

public class ConverterFactory {

    private ConverterFactory() {
    }

    public static Converter getConverter(ConverterType converterType){
        switch (converterType){
            case XML:
                return new XMLConverter();
            case EXCEL:
                return new ExcelConverter();
            default:
                return null;
        }
    }
}
