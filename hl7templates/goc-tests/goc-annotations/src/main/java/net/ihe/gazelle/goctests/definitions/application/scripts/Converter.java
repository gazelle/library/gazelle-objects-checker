package net.ihe.gazelle.goctests.definitions.application.scripts;

import net.ihe.gazelle.goctests.definitions.application.CSVParsingException;

public interface Converter {

    void generate(String csvPath, String destPath) throws CSVParsingException;
}
