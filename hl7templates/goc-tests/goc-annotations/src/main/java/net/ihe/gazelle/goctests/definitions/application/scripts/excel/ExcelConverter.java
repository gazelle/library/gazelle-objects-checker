package net.ihe.gazelle.goctests.definitions.application.scripts.excel;

import net.ihe.gazelle.goctests.definitions.application.CSVParsingException;
import net.ihe.gazelle.goctests.definitions.application.scripts.Converter;

public class ExcelConverter implements Converter {

    @Override
    public void generate(String csvPath, String destPath) throws CSVParsingException {
        // nothing so far
    }
}
