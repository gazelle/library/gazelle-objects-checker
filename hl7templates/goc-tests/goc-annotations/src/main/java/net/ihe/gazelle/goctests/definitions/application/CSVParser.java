package net.ihe.gazelle.goctests.definitions.application;

import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.goctests.definitions.application.models.AnnotatedTestMethod;
import net.ihe.gazelle.goctests.definitions.application.models.CSVColumn;
import net.ihe.gazelle.goctests.definitions.application.models.CoverageReport;
import net.ihe.gazelle.goctests.definitions.application.models.Requirement;

import java.io.*;
import java.util.*;

public class CSVParser {

    public static final String REQUIREMENTS_DELIMITER = ";";

    private CSVParser(){}

    public static CoverageReport parseCoverageReport(String csvPath) throws CSVParsingException {
        File file = new File(csvPath);
        if(!file.exists()){
            throw new CSVParsingException("CSV File does not exist");
        }
        try(BufferedReader reader = new BufferedReader(new FileReader(csvPath));) {
            String line;
            int lineCount = 0;
            List<AnnotatedTestMethod> methods = new ArrayList<>();
            while((line = reader.readLine()) != null){
                if(lineCount == 0){
                    if(!checkCSVColumnsConformance(line)){
                        throw new CSVParsingException("CSV File doesn't match expected model, verification was by columns");
                    }
                }
                else{
                    String[] values = line.split(",");
                    if(values.length < 5){
                        System.err.println("Invalide line format: missing colomns in "+line);
                        System.err.println("Skipping entry");
                    }
                    else{
                        // TODO: 13/01/2022 This could be improved to be dynamic with reflection
                        TestType testType = TestType.of(values[2]);
                        if(testType == null){
                            System.err.println("Invalid testype found: "+values[2]+" at line: "+lineCount);
                            System.err.println("Skipping entry");
                            continue;
                        }
                        String methodName = values[0];
                        String[] requirements = values[1].split(REQUIREMENTS_DELIMITER);
                        String packageName = values[3];
                        String className = values[4];
                        String comment = values.length > 5 ? values[5]:"";
                        methods.add(new AnnotatedTestMethod(methodName, requirements, testType, packageName, className, comment));
                    }
                }
                lineCount++;
            }

            Map<String, List<AnnotatedTestMethod>> map = getRequirementsMap(methods);
            return convertMapToCoverageReport(map);

        } catch (FileNotFoundException e) {
            throw new CSVParsingException("CSV File not found or is a directory",e);
        } catch (IOException e) {
            throw new CSVParsingException("Could not read from file",e);
        }


    }

    private static boolean checkCSVColumnsConformance(String columns){
        return CSVColumn.CSV_HEADER.equals(columns);
    }

    private static Map<String,List<AnnotatedTestMethod>> getRequirementsMap(List<AnnotatedTestMethod> methods){
        Map<String,List<AnnotatedTestMethod>> map = new TreeMap<>();
        for(AnnotatedTestMethod method : methods){
            String[] requirementsString = method.getRequirements();
            for(String reqString : requirementsString){
                if(map.containsKey(reqString)){
                    map.get(reqString).add(method);
                }
                else{
                    List<AnnotatedTestMethod> temp = new ArrayList<>();
                    temp.add(method);
                    map.put(reqString, temp);
                }
            }
        }
        return map;
    }

    private static CoverageReport convertMapToCoverageReport(Map<String,List<AnnotatedTestMethod>> map){
        CoverageReport report = new CoverageReport();
        for(Map.Entry<String, List<AnnotatedTestMethod>> entry : map.entrySet()){
            if(entry.getKey() != null){
                Requirement requirement = new Requirement(entry.getKey());
                requirement.setMethods(entry.getValue());
                report.getRequirements().add(requirement);
            }
        }
        return report;
    }
}
