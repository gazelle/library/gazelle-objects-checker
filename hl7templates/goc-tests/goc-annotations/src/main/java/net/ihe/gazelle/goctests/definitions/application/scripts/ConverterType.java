package net.ihe.gazelle.goctests.definitions.application.scripts;

public enum ConverterType {

    XML("xml"),
    EXCEL("excel");

    private String value;

    ConverterType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ConverterType of(String type){
        for(ConverterType t : ConverterType.values()){
            if(t.value.equals(type)){
                return t;
            }
        }
        return null;
    }
}
