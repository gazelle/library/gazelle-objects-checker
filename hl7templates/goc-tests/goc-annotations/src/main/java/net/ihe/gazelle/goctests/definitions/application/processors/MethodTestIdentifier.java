package net.ihe.gazelle.goctests.definitions.application.processors;

/**
 * Allowed prefixes and suffixes for test methods, to be recognized by the test-coverage processing
 */

public enum MethodTestIdentifier {

    test("test"),
    Test("Test"),
    it("it"),
    It("It"),
    IT("IT");

    private String value;

    private MethodTestIdentifier(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
