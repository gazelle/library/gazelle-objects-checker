package net.ihe.gazelle.goctests.definitions.application.models;

import net.ihe.gazelle.goctests.definitions.annotations.TestType;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AnnotatedTestMethod {

    @XmlElement(name = "MethodName")
    private String methodName;

    @XmlTransient
    private String[] requirements;

    @XmlElement(name = "Type")
    private TestType testType;

    @XmlElement(name = "Package")
    private String packageName;

    @XmlElement(name = "ClassName")
    private String className;

    @XmlElement(name = "Comment")
    private String comment;

    public AnnotatedTestMethod() {
    }

    public AnnotatedTestMethod(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }

    public AnnotatedTestMethod(String methodName, String[] requirements, TestType testType, String packageName, String className) {
        this(methodName, requirements, testType, packageName, className, "");

    }

    public AnnotatedTestMethod(String methodName, String[] requirements, TestType testType, String packageName, String className, String comment) {
        this.methodName = methodName;
        this.requirements = requirements;
        this.testType = testType;
        this.packageName = packageName;
        this.className = className;
        this.comment = comment;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String[] getRequirements() {
        return requirements;
    }

    public void setRequirements(String[] requirements) {
        this.requirements = requirements;
    }

    public TestType getTestType() {
        return testType;
    }

    public void setTestType(TestType testType) {
        this.testType = testType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "AnnotatedTestMethod{" +
                "methodName='" + methodName + '\'' +
                ", requirements=" + Arrays.toString(requirements) +
                ", testType=" + testType +
                ", packageName='" + packageName + '\'' +
                ", className='" + className + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
