package net.ihe.gazelle.goctests.definitions.application.processors;

import com.google.auto.service.AutoService;
import net.ihe.gazelle.goctests.definitions.application.CSVWriterException;
import net.ihe.gazelle.goctests.definitions.application.models.AnnotatedTestMethod;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SupportedAnnotationTypes("net.ihe.gazelle.goctests.*")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
public class CoverageProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        System.out.println("Start coverage processing");

        for(TypeElement annotation : annotations){

            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            Map<Boolean, List<Element>> annotatedMethods = annotatedElements.stream()
                    .collect(Collectors.partitioningBy(element ->
                            (CoverageProcessorUtil.isElementIdentifierValide(element))));

            //Generate the report
            List<AnnotatedTestMethod> validMethods = CoverageProcessorUtil.getAnnotatedTestMethods(annotatedMethods.get(true));
            ReportGenerator reportGenerator = new ReportGenerator(validMethods, annotatedMethods.get(false));
            try {
                reportGenerator.generateReport();
            } catch (IOException | CSVWriterException e) {
                System.err.println("The following error has occured: "+e.getMessage());
                System.err.println("Coverage report not generated");
            }

        }

        return false;
    }


}
