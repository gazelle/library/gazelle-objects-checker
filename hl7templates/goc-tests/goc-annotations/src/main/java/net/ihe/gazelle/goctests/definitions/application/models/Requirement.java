package net.ihe.gazelle.goctests.definitions.application.models;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Requirement {

    private String id;

    @XmlElementWrapper(name = "Methods")
    @XmlElement(name = "Method")
    private List<AnnotatedTestMethod> methods;

    public Requirement() {
    }

    public Requirement(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public List<AnnotatedTestMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<AnnotatedTestMethod> methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        return "Requirement{" +
                "id='" + id + '\'' +
                ", methods=" + methods +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Requirement)) return false;
        Requirement that = (Requirement) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean equalsById(String id){
        return this.id.equals(id);
    }
}
