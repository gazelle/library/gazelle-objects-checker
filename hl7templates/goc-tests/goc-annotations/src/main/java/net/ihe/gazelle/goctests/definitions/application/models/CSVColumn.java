package net.ihe.gazelle.goctests.definitions.application.models;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum CSVColumn {

    METHOD_NAME("Method Name"),
    REQUIREMENTS("Requirements"),
    TEST_TYPE("Test Type"),
    PACKAGE_NAME("Package Name"),
    CLASS_NAME("Class Name"),
    COMMENT("Comment");

    public static final String CSV_HEADER = getHeaderForCSV();

    CSVColumn(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }

    private static String getHeaderForCSV(){
        return Arrays.stream(CSVColumn.values())
                .map(CSVColumn::getValue)
                .collect(Collectors.joining(","));
    }

    public static CSVColumn of(String value){
        for(CSVColumn col : CSVColumn.values()){
            if(col.value.equals(value)){
                return col;
            }
        }
        return null;
    }
}
