package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.flatten.action.AssertFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Context;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AssertFlattenProcIT {

	@Test
	public void testProcess() {
		Assert asser = new Assert();
		asser.setTest("haha");
		TemplateDefinition td = new TemplateDefinition();
		td.setContext(new Context());
		td.setId("1.2.3");
		RuleDefinition rd = new RuleDefinition();
		rd.setName("hl7:observation");
		RuleDefinition templateId = new RuleDefinition();
		rd.getLetOrAssertOrReport().add(templateId);
		templateId.setName("hl7:templateId");
		templateId.getAttribute().add(new Attribute());
		templateId.getAttribute().get(0).setRoot("1.2.3");
		td.getAttributeOrChoiceOrElement().add(rd);
		td.getAttributeOrChoiceOrElement().add(asser);
		rd.setParentObject(td);
		asser.setParentObject(td);
		(new AssertFlattenProc()).process(asser);
		assertTrue(asser.getParentObject()==rd);
	}

}
