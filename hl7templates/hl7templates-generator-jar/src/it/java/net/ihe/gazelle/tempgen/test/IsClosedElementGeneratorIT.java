package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.rules.analyzer.IsClosedElementGenerator;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IsClosedElementGeneratorIT extends IsClosedElementGenerator {
	
	Decor decIsClosed = null;
	
	RuleDefinition firstRDIsClosed = null;
	
	@Before
	public void setUp(){
		decIsClosed = DecorMarshaller.loadDecor("src/test/resources/rd/decor_closed.xml");
		firstRDIsClosed = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decIsClosed.getRules()).get(0));
	}

	@Test
	public void testGetSubElement() {
		IsClosedElementGenerator gen = new IsClosedElementGenerator();
		gen.setSubElement("test");
		assertTrue(gen.getSubElement().equals("test"));
	}

	@Test
	public void testGenerateOCLConstraint() {
		IsClosedElementGenerator gen = new IsClosedElementGenerator();
		gen.setSubElement("templateId");
		String ocl = gen.generateOCLConstraint(firstRDIsClosed);
		assertTrue(ocl.equals("self.templateId->reject(not nullFlavor.oclIsUndefined())->size()=0"));
	}
	
	@Test
	public void testGenerateCommentConstraint1() {
		IsClosedElementGenerator gen = new IsClosedElementGenerator();
		gen.setSubElement("templateId");
		String comment = gen.generateCommentConstraint(firstRDIsClosed);
		assertTrue(comment.contains("the element is closed"));
	}

	@Test
	public void testGenerateCommentConstraint2() {
		IsClosedElementGenerator gen = new IsClosedElementGenerator();
		gen.setSubElement("templateId");
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decIsClosed.getRules()).get(1));
		String comment = gen.generateCommentConstraint(rd);
		assertTrue(comment.contains("(the template is closed)"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue((new IsClosedElementGenerator()).getProcessIdentifier().equals(AnalyzerEnum.RD_ISCLOSED_PROCESS.getValue()));
	}
	
}
