package net.ihe.gazelle.tempgen.choice.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceAnalyzerEnum;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMaximumMultiplicityANalyzer;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceMaximumMultiplicityANalyzerIT extends ChoiceInitTest {

	@Test
	public void testGenerateOCLConstraint() {
		String ocl = (new ChoiceMaximumMultiplicityANalyzer()).generateOCLConstraint(currentChoiceDefinition);
		assertTrue(ocl.equals("self.participantRole->forAll( (playingDevice->size()+playingEntity->size())<=2)"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		String comment = (new ChoiceMaximumMultiplicityANalyzer()).generateCommentConstraint(currentChoiceDefinition);
		assertTrue(comment.equals("In CDA Participant (Body), in /hl7:participant, "
				+ "the number of elements of type 'playingDevice', 'playingEntity' SHALL be lower or equal to 2"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue((new ChoiceMaximumMultiplicityANalyzer().getProcessIdentifier().equals(ChoiceAnalyzerEnum.CH_MAX_PROCESS.getValue())));
	}

}
