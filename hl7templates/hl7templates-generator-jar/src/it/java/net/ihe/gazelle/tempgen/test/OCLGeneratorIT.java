package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.Pair;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class OCLGeneratorIT extends OCLGenerator {
	
Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
	}

	@Test
	public void testGenerateRuleFromPath() {
		String path = 
				"//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		String context = this.generateRuleFromPath(dparent, null);
		assertTrue(context.equals("self.assignedAuthor.assignedPerson.name.family"));
		path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor";
		dparent = DPathExtractor.extractDElementFromDPath(path);
		context = this.generateRuleFromPath(dparent, null);
		assertTrue(context.equals("self.assignedAuthor"));
		path = "//hl7:author/hl7:assignedAuthor";
		dparent = DPathExtractor.extractDElementFromDPath(path);
		context = this.generateRuleFromPath(dparent, null);
		assertTrue(context.equals("self.assignedAuthor"));
	}
	
	@Test
	public void testGenerateContextRuleStringInteger(){
		String path = 
				"//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		String context = this.generateContextRule(dparent, null, null);
		String resp = "self.assignedAuthor.assignedPerson.name.family";
		dparent = DPathExtractor.extractDElementFromDPath(path);
		assertTrue(context.equals(resp));
		context = this.generateContextRule(dparent, 4, null);
		assertTrue(context.equals(resp));
		context = this.generateContextRule(dparent, 3, null);
		assertTrue(context.equals("self.assignedAuthor.assignedPerson.name"));
		dparent = DPathExtractor.extractDElementFromDPath(path);
		context = this.generateContextRule(dparent, 1, null);
		assertTrue(context.equals("self.assignedAuthor"));
		context = this.generateContextRule(dparent, 0, null);
		assertTrue(context.equals("self"));
	}
	
	@Test
	public void testGenerateContextRuleStringIntegerInteger(){
		String path = 
				"//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		String context = this.generateContextRule(dparent, null, null);
		String resp = "self.assignedAuthor.assignedPerson.name.family";
		assertTrue(context.equals(resp));
		context = this.generateContextRule(dparent, 4, null, null);
		assertTrue(context.equals(resp));
		context = this.generateContextRule(dparent, 4, 1, null);
		assertTrue(context.equals("assignedPerson.name.family"));
		context = this.generateContextRule(dparent, 4, 0, null);
		assertTrue(context.equals("assignedAuthor.assignedPerson.name.family"));
		context = this.generateContextRule(dparent, 4, 2, null);
		assertTrue(context.equals("name.family"));
		context = this.generateContextRule(dparent, 4, 3, null);
		assertTrue(context.equals("family"));
		context = this.generateContextRule(dparent, 4, 4, null);
		assertTrue(context.equals(""));
	}
	
	@Test
	public void testGenerateContextRuleDParentIntegerIntegerObject1() throws Exception {
		String path = "/hl7:section/hl7:id";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		String ocl = this.generateContextRule(dparent, 0, 0, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 1, 1, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 0, 1, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 0, -1, null);
		assertTrue(ocl.equals("self"));
		ocl = this.generateContextRule(dparent, 0, null, null);
		assertTrue(ocl.equals("self"));
		ocl = this.generateContextRule(dparent, 1, -1, null);
		assertTrue(ocl.equals("self.id"));
		ocl = this.generateContextRule(dparent, 1, 2, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 1, 10, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 2, 10, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 2, 1, null);
		assertTrue(ocl.equals(""));
		assertTrue(ProblemHandler.findNotification("OCLGenerator::generateContextRule is handling a non sense value") != null);
	}
	
	@Test
	public void testGenerateContextRuleDParentIntegerIntegerObject2() throws Exception {
		String path = "/hl7:section/hl7:entry/hl7:observation[hl7:id/@root='1.2.3']/hl7:code/@code";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		String ocl = this.generateContextRule(dparent, 0, 0, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 1, 1, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 0, 1, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 0, -1, null);
		assertTrue(ocl.equals("self"));
		ocl = this.generateContextRule(dparent, 0, null, null);
		assertTrue(ocl.equals("self"));
		ocl = this.generateContextRule(dparent, 1, 2, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 1, -1, null);
		assertTrue(ocl.equals("self.entry"));
		ocl = this.generateContextRule(dparent, 2, 100, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 2, 3, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 2, 2, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 2, 1, null);
		assertTrue(ocl.equals("observation"));
		ocl = this.generateContextRule(dparent, 2, 0, null);
		assertTrue(ocl.equals("entry.observation"));
		ocl = this.generateContextRule(dparent, 2, -1, null);
		assertTrue(ocl.equals("self.entry.observation"));
		ocl = this.generateContextRule(dparent, 3, 100, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 3, 3, null);
		assertTrue(ocl.equals(""));
		ocl = this.generateContextRule(dparent, 3, 2, null);
		assertTrue(ocl.equals("code"));
		ocl = this.generateContextRule(dparent, 3, 1, null);
		assertTrue(ocl.equals("observation.code"));
		ocl = this.generateContextRule(dparent, 3, 0, null);
		assertTrue(ocl.equals("entry.observation.code"));
		ocl = this.generateContextRule(dparent, 3, -1, null);
		assertTrue(ocl.equals("self.entry.observation.code"));
		ocl = this.generateContextRule(dparent, 4, -1, null);
		assertTrue(ocl.equals("self.entry.observation.code.code"));
		ocl = this.generateContextRule(dparent, 4, 3, null);
		assertTrue(ocl.equals("code"));
		ocl = this.generateContextRule(dparent, 4, 2, null);
		assertTrue(ocl.equals("code.code"));
		ocl = this.generateContextRule(dparent, 4, 4, null);
		assertTrue(ocl.equals(""));
	}
	
	@Test
	public void testGenerateContextRuleDParentIntegerIntegerObject3() throws Exception {
		String path = "/hl7:aa/hl7:bb/zz:cc";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		String ocl = this.generateContextRule(dparent, 1, 0, dparent);
		assertTrue(ocl.equals("bb"));
		ocl = this.generateContextRule(dparent, 1, -1, dparent);
		assertTrue(ocl.equals("self.bb"));
		assertTrue(ProblemHandler.findNotification("The kind of parent is unknown") != null);
	}
	
	@Test
	public void testGenerateContextRuleDParentIntegerIntegerObject4() throws Exception {
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_obs_extra.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(obs);
		
		assertTrue(DPathExtractor.createPathFromDParent(dp).equals(
				"/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/"
				+ "hl7:entryRelationship[hl7:observation/hl7:templateId/@root='1.2.3']/"
				+ "hl7:observation"));
		String ocl = this.generateContextRule(dp, 1, -1, obs);
		assertTrue(ocl.equals("self.entryRelationship"));
		ocl = this.generateContextRule(dp, 2, -1, obs);
		assertTrue(ocl.equals("self.entryRelationship->select(observation.templateId.root->exists(aa : String| aa = '1.2.3')).observation"));
		ocl = this.generateContextRule(dp, 2, 0, obs);
		assertTrue(ocl.equals("entryRelationship.observation"));
	}
	
	@Test
	public void testGetExtraSelectionByDistinguisherForRuleDefinition() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		Pair<String, String> dis = new Pair<String, String>();
		dis.setObject1("/hl7:code/@code");
		dis.setObject2("48766-0");
		String res = OCLGenerator.getExtraSelectionByDistinguisherForRuleDefinition(obs, dis);
		assertTrue(res.equals("(not (code.oclIsUndefined() or code.code.oclIsUndefined())) and code.code='48766-0'"));
	}
	
	@Test
	public void testGetExtraSelectionIfNeededRuleDefinition1(){
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		String ss = OCLGenerator.getExtraSelectionIfNeeded(obs);
		assertTrue(ss.equals(""));
	}
	
	@Test
	public void testGetExtraSelectionIfNeededRuleDefinition2() throws FileNotFoundException, JAXBException{
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		String ss = OCLGenerator.getExtraSelectionIfNeeded(entr);
		assertTrue(ss.equals("->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')"));
	}
	
	@Test
	public void testGetExtraSelectionIfNeededRuleDefinition3() throws FileNotFoundException, JAXBException{
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist2.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		ChoiceDefinition cd = RuleDefinitionUtil.getChoices(entr).get(0);
		RuleDefinition obs = ChoiceDefinitionUtil.getElements(cd).get(1);
		String ss = OCLGenerator.getExtraSelectionIfNeeded(obs);
		assertTrue(ss.equals("->select((not (code.oclIsUndefined() or code.code.oclIsUndefined())) and code.code='48766-0')"));
	}
	
	/**
	 * jira GOC-46
	 * @throws FileNotFoundException
	 * @throws JAXBException
	 */
	@Test
	public void testGetExtraSelectionIfNeededRuleDefinition4() throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_operator.xml");
		RuleDefinition substanceAdministration = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition effectiveTime = RuleDefinitionUtil.getElementByName(substanceAdministration, "cda:effectiveTime");
		String ss = OCLGenerator.getExtraSelectionIfNeeded(effectiveTime);
		assertTrue(ss.equals("->select((not ((not (operator.oclIsUndefined())) and operator=SetOperator::A)))"));
	}
	
	@Test
	public void testGetExtraDistinguisherIfNeeded1(){
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		String ss = OCLGenerator.getExtraDistinguisherIfNeeded(obs);
		assertTrue(ss.equals("(not (code.oclIsUndefined() or code.code.oclIsUndefined())) and code.code='48766-0'"));
	}
	
	@Test
	public void testGenerateRuleToParentFromPath1(){
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition targ = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String ss = (new OCLGenerator()).generateRuleToParentFromPath(targ);
		assertTrue(ss.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0').observation"));
	}
	
	@Test
	public void testGenerateRuleToParentFromPath2() throws FileNotFoundException, JAXBException{
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition targ = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String ss = (new OCLGenerator()).generateRuleToParentFromPath(targ);
		assertTrue(ss.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and "
				+ "observation.code.code='48766-0').observation"));
	}
	
	@Test
	public void testGenerateRuleToParentFromPath3(){
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_templ.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(1);
		String ss = (new OCLGenerator()).generateRuleToParentFromPath(templateId);
		assertTrue(ss.equals("self"));
	}
	
	@Test
	public void testGenerateMaxCardinalityOfPath() throws Exception {
		String path = 
				"//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		DParent dparent = DPathExtractor.extractDElementFromDPath(path);
		int max = this.generateMaxCardinalityOfPath(dparent, null, null);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 4, null);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 4, 1);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 4, 2);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 3, 2);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 2, 1);
		assertTrue(max == 1);
		path = "//hl7:section[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:id/@root";
		dparent = DPathExtractor.extractDElementFromDPath(path);
		max = this.generateMaxCardinalityOfPath(dparent, null, null);
		assertTrue(max == 1);
		max = this.generateMaxCardinalityOfPath(dparent, 2, null);
		assertTrue(max == 1);
		max = this.generateMaxCardinalityOfPath(dparent, 2, 1);
		assertTrue(max == 1);
		path = "//hl7:patient/hl7:guardian/hl7:templateId/@root";
		dparent = DPathExtractor.extractDElementFromDPath(path);
		max = this.generateMaxCardinalityOfPath(dparent, null, null);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 3, 2);
		assertTrue(max == 1);
		max = this.generateMaxCardinalityOfPath(dparent, 3, 1);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 2, 1);
		assertTrue(max == -1);
		max = this.generateMaxCardinalityOfPath(dparent, 1, 0);
		assertTrue(max == -1);
	}
	
	@Test
	public void testGenerateRuleContextToRuleDefinition1() throws Exception {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition targ = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String ss = (new OCLGenerator()).generateRuleContextToRuleDefinition(targ);
		assertTrue(ss.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0').observation.targetSiteCode"));
	}
	
	@Test
	public void testGenerateRuleContextToRuleDefinition2() throws Exception {
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition targ = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String ss = (new OCLGenerator()).generateRuleContextToRuleDefinition(targ);
		assertTrue(ss.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and "
				+ "observation.code.code='48766-0').observation.targetSiteCode"));
	}
	
	@Test
	public void testAddTypeConstrainedIfNeeded() throws Exception {
		DElement del = new DElement();
		assertTrue(OCLGenerator.addTypeConstrainedIfNeeded(del).equals(""));
		del.setExtendedType("AD");
		assertTrue(OCLGenerator.addTypeConstrainedIfNeeded(del).equals("->select(oclIsKindOf(AD)).oclAsType(AD)"));
		DAttribute dat = new DAttribute();
		assertTrue(OCLGenerator.addTypeConstrainedIfNeeded(dat).equals(""));
	}
	
	/**
	 * test for the negative distinguisher
	 * @throws Exception
	 */
	@Test
	public void testGetExtraDistinguisherIfNeeded2() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting7.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(2);
		String ss = OCLGenerator.getExtraDistinguisherIfNeeded(templateId);
		assertTrue(ss.equals("(not ((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1')) and "
				+ "(not ((not (root.oclIsUndefined())) and root='1.2.3'))"));
	}
	
	/**
	 * test for the negative distinguisher
	 * @throws Exception
	 */
	@Test
	public void testGetExtraDistinguisherIfNeeded3() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(1);
		String ss = OCLGenerator.getExtraDistinguisherIfNeeded(templateId);
		assertTrue(ss.equals("(not ((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'))"));
	}
	
	/**
	 * test for the negative distinguisher
	 * @throws Exception
	 */
	@Test
	public void testGetExtraDistinguisherIfNeeded4() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entry = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:entry").get(0);
		String ss = OCLGenerator.getExtraDistinguisherIfNeeded(entry);
		assertTrue(ss.equals("(not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntry::COMP"));
		entry = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:entry").get(1);
		ss = OCLGenerator.getExtraDistinguisherIfNeeded(entry);
		assertTrue(ss.equals("(not ((not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntry::COMP)) and "
				+ "(not (observation.templateId.root->exists(aa : String| aa = '5.5.5')))"));
		entry = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:entry").get(2);
		ss = OCLGenerator.getExtraDistinguisherIfNeeded(entry);
		assertTrue(ss.equals("observation.templateId.root->exists(aa : String| aa = '5.5.5')"));
	}
	
	/**
	 * test for the negative distinguisher
	 * @throws Exception
	 */
	@Test
	public void testGetExtraDistinguisherIfNeeded5() throws Exception {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting8.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(0);
		String ss = OCLGenerator.getExtraDistinguisherIfNeeded(templateId);
		assertTrue(ss.equals("(not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'"));
		templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(1);
		ss = OCLGenerator.getExtraDistinguisherIfNeeded(templateId);
		assertTrue(ss.equals(""));
		templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(2);
		ss = OCLGenerator.getExtraDistinguisherIfNeeded(templateId);
		assertTrue(ss.equals(""));
	}
	
}
