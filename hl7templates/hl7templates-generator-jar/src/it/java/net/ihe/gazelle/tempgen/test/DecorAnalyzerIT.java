package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.tempgen.action.DecorAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

public class DecorAnalyzerIT extends DecorAnalyzer {
	
	@Before
	public void before() throws FileNotFoundException, JAXBException {
		this.currentDecor = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		this.xmi = XMIUtil.createXMI("test");
	}
	
	@Test
	public void testProcess() throws JAXBException {
		(new DecorAnalyzer()).process(currentDecor, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertTrue(xmiStr.contains("<uml:Model name=\"test\" xmi:id="));
		assertTrue(xmiStr.contains("<packagedElement name=\"EntryProblemConcern\""));
		assertTrue(xmiStr.contains("<body>In Problem Concern, in /hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2'], "
				+ "the attribute classCode SHALL have the value 'ACT' if present</body>"));
		assertTrue(xmiStr.contains("<body>self.moodCode.oclIsUndefined() or self.moodCode=XDocumentActMood::EVN</body>"));
		assertTrue(xmiStr.contains("<language>OCL</language>"));
		assertTrue(xmiStr.contains("<elementImport"));
		assertTrue(xmiStr.contains("<importedElement href=\"../../datatypes-model/models/datatypes.uml#_CuEO9q70EeGxJei_o6JmIA\" xmi:type=\"uml:Class\"/>"));
		assertTrue(xmiStr.contains("<TemplateDefiner:taml IDs=\"CONF-524;CONF-525;conf-16563\" base_Constraint="));
		assertTrue(xmiStr.contains(" <TemplateDefiner:TemplateSpec base_Class="));
	}

	@Test
	public void testProcessRules() throws JAXBException {
		this.processRules(this.currentDecor.getRules());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertTrue(xmiStr.contains("<body>self.moodCode.oclIsUndefined() or self.moodCode=XDocumentActMood::EVN</body>"));
	}

}
