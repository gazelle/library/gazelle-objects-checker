package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrContextConductionIndAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrContextConductionIndAnalyzerIT {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entry");
		Attribute attr = entr.getAttribute().get(0);
		String ocl = (new AttrContextConductionIndAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.entry->forAll(contextConductionInd.oclIsUndefined() or contextConductionInd=true)"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entry");
		Attribute attr = entr.getAttribute().get(0);
		String comment = (new AttrContextConductionIndAnalyzer()).generateCommentConstraint(attr);
		assertTrue(comment.equals("In Hospital Discharge Diagnosis Section, in "
				+ "/cda:section[cda:templateId/@root='2.16.840.1.113883.10.20.22.2.24']/cda:entry, the attribute contextConductionInd SHALL "
				+ "have the value 'true' if present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrContextConductionIndAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_CONTEXTCONDUCTIONIND_PROCESS.getValue()));
	}

}
