package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.tempgen.action.RulesAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class RulesAnalyzerIT extends RulesAnalyzer {
	
	@Before
	public void before() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		this.currentRules = dec.getRules();
		this.xmi = XMIUtil.createXMI("aaa");
	}

	@Test
	public void testProcess() throws JAXBException {
		(new RulesAnalyzer()).process(currentRules, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertTrue(xmiStr.contains(" <body>self.classCode.oclIsUndefined() or self.classCode=XActClassDocumentEntryAct::ACT</body>"));
	}

	@Test
	public void testProcessTemplates1() throws JAXBException {
		this.processTemplates(RulesUtil.getTemplates(this.currentRules));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertFalse(xmiStr.contains(" <body>self.classCode.oclIsUndefined() or self.classCode=XActClassDocumentEntryAct::ACT</body>"));
	}
	
	@Test
	public void testProcessTemplates2() throws JAXBException {
		this.listCDATemplates = RulesUtil.getTemplates(this.currentRules);
		this.processTemplates(RulesUtil.getTemplates(this.currentRules));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertTrue(xmiStr.contains(" <body>self.classCode.oclIsUndefined() or self.classCode=XActClassDocumentEntryAct::ACT</body>"));
	}

}
