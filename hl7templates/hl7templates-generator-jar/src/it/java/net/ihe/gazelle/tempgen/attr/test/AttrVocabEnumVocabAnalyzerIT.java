package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrVocabEnumVocabAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrVocabEnumVocabAnalyzerIT extends AttrVocabEnumVocabAnalyzer {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_vocab_attr.xml");
	}

	@Test
	public void testExtractConstraintForVocabulary1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Attribute attr = firstRD.getAttribute().get(1);
		Vocabulary vocabulary = new Vocabulary();
		vocabulary.setCode("test");
		String ocl = this.extractConstraintForVocabulary(vocabulary, attr);
		assertTrue(ocl.equals("CommonOperationsStatic::stringValueOf(aa)='test'"));
	}
	
	@Test
	public void testExtractConstraintForVocabulary2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Attribute attr = firstRD.getAttribute().get(1);
		Vocabulary vocabulary = new Vocabulary();
		vocabulary.setValueSet("1.2.3");
		String ocl = this.extractConstraintForVocabulary(vocabulary, attr);
		assertTrue(ocl.equals("CommonOperationsStatic::matchesCodeToValueSet('1.2.3', CommonOperationsStatic::stringValueOf(aa))"));
	}

}
