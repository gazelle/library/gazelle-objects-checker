package net.ihe.gazelle.tempgen.assertions.test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.action.AssertAnalyzer;
import net.ihe.gazelle.tempgen.action.HL7TemplatesConverter;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.util.List;
import static org.junit.Assert.*;



public class XPATHValidationIT extends AssertAnalyzer {

    // Bad cases

    private Decor decorTemplate = null;
//    private List<TemplateDefinition> templateDefinitions = null;
    private TemplateDefinition currentTemplate = null;
    private RuleDefinition currentRuleD = null;
    private List<Assert> assertList = null;

    @Before
    public void setUp(){
        decorTemplate = DecorMarshaller.loadDecor("src/test/resources/assertions/decor_assertions.xml");
        TemplateDefinition templateDefinition =  RulesUtil.getTemplates(decorTemplate.getRules()).get(0);
        currentRuleD = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(templateDefinition),"cda:value");
        this.assertList = RuleDefinitionUtil.getAsserts(currentRuleD);
    }

    @Test
    public void testReadAssertion(){
        assertEquals(1, assertList.size());
        String expected = "not(@xsi:type='CD') or @codeSystem='2.16.840.1.113883.6.96'";
        assertEquals(expected,assertList.get(0).getTest());
    }

    @Test
    public void testGenerateOCL(){
        assertEquals(1, assertList.size());
        this.currentAssert = this.assertList.get(0);
        assertNotNull(this.currentAssert);
        this.flattenAssert(currentAssert,0);
        assertNotNull(this.currentAssert.getTestFlatten());
        String ocl = this.generateOCLConstraint();
        assertFalse(ocl.contains("@xsi:type"));
        assertTrue(ocl.contains("oclIsKindOf"));
    }

    // TODO: 18/10/2021 This test pass correctly locally but not in pipeline, to be reviewed
    @Ignore
    @Test
    public void testProcessAssert(){
        this.currentAssert = this.assertList.get(0);
        PackagedElement packagedElement = PackagedElementUtil.initPackagedElement();
        this.process(currentAssert,packagedElement);
        assertEquals(1,packagedElement.getOwnedRule().size());
        String expected = "((self.value->forAll(not(oclIsKindOf(CD)))) or (self.value->forAll(aa | CommonOperationsStatic::validateByXPATHV2(aa, '@codeSystem=\\u00272.16.840.1.113883.6.96\\u0027') )))";
        assertEquals(expected,packagedElement.getOwnedRule().get(0).getSpecification().getBody());
    }

    @Test
    public void testGenerateOCLWithLargeCase(){
        String test = "@xsi:type='ED' and (@codeSystem='1.2.3.4.5' or not(@xsi:type='CD')) or (@code='AB' and @xsi:type='CD')";
        Assert assertion = new Assert();
        assertion.setParentObject(currentRuleD);
        assertion.setTest(test);
        this.flattenAssert(assertion,0);
        this.currentAssert = assertion;
        String ocl = this.generateOCLConstraint();
        assertEquals(3,StringUtils.countMatches(ocl,"oclIsKindOf"));
        assertEquals(2,StringUtils.countMatches(ocl,"CommonOperationsStatic::validateByXPATHV2"));

    }





}
