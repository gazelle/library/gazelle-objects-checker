package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrRepresentationAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class AttrRepresentationAnalyzerIT {

	@Test
	public void testGenerateOCLConstraint() throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/attr/decor_attr_representation.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
		Attribute attr = RuleDefinitionUtil.getAttributeByName(auth, "representation");
		String ocl = (new AttrRepresentationAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.value->forAll(representation.oclIsUndefined() or representation=BinaryDataEncoding::B64)"));
	}

	@Test
	public void testGenerateCommentConstraint() throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/attr/decor_attr_representation.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
		Attribute attr = RuleDefinitionUtil.getAttributeByName(auth, "representation");
		String ocl = (new AttrRepresentationAnalyzer()).generateCommentConstraint(attr);
		assertTrue(ocl.equals("In Logo Entry, in /hl7:observationMedia[hl7:templateId/@root='1.2.40.0.34.11.1.3.2']/hl7:value, "
				+ "the attribute representation SHALL have the value 'B64' if present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue((new AttrRepresentationAnalyzer()).getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_REPRESENTATION_PROCESS.getValue()));
	}

}
