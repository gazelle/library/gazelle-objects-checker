package net.ihe.gazelle.tempgen.RDprocessors.test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.tempgen.action.RuleDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.validation.Notification;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class VocabularyProcessorsIT extends RuleDefinitionAnalyzer {

    @Ignore
    @Test
    public void print(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_vocab_VS_dynamic.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        RuleDefinition observation = RuleDefinitionUtil.getElementByName(code,"hl7:observation");
        this.selectedRuleDefinition = code;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(code.getVocabulary());


        for(OwnedRule ownedRule : this.currentPackagedElement.getOwnedRule()){
            System.out.println(ownedRule.getSpecification().getBody());
            System.out.println(ownedRule.getOwnedRuleKind().getValue());
            System.out.println(ownedRule.getOwnedRuleType().getValue());
        }
        for(Notification notification : ProblemHandler.getDiagnostic()){
            System.out.println(notification.getDescription());
        }
    }


    @Covers(requirements = {"GOC-012"},testType = TestType.INTEGRATION_TEST, comment = "RuleDefinitions Coverage")
    @Test
    public void testProcessVocabularys_FixedVal(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_vocab_fixdedVal.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        this.selectedRuleDefinition = value;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(value.getVocabulary());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("self.value->forAll( (not nullFlavor.oclIsUndefined()) " +
                "or (((oclAsType(CS).codeSystem.oclIsUndefined()) or oclAsType(CS).codeSystem='1.2.3')))",
                ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.VOCABULARY, ownedRule.getOwnedRuleKind());
        assertEquals(OwnedRuleType.ERROR, ownedRule.getOwnedRuleType());
    }

    @Covers(requirements = {"GOC-013","GOC-017"},testType = TestType.INTEGRATION_TEST, comment = "RuleDefinitions Coverage")
    @Test
    public void testProcessVocabularys_VS(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_vocab_VS.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        this.selectedRuleDefinition = value;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(value.getVocabulary());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("self.value->forAll( (not nullFlavor.oclIsUndefined()) or ((not oclAsType(CS).code.oclIsUndefined()) and " +
                        "CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.10&version=2015-01-01T01:02:02', oclAsType(CS).code)))",
                        ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.VOCABULARY, ownedRule.getOwnedRuleKind());
        assertEquals(OwnedRuleType.ERROR, ownedRule.getOwnedRuleType());
    }

    @Covers(requirements = {"GOC-018"},testType = TestType.INTEGRATION_TEST, comment = "RuleDefinitions Coverage")
    @Test
    public void testProcessVocabularys_VS_dynamic(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_vocab_VS_dynamic.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        this.selectedRuleDefinition = value;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(value.getVocabulary());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("self.value->forAll( (not nullFlavor.oclIsUndefined()) or ((not oclAsType(CS).code.oclIsUndefined()) " +
                        "and CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.10', oclAsType(CS).code)))",
                ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.VOCABULARY, ownedRule.getOwnedRuleKind());
        assertEquals(OwnedRuleType.ERROR, ownedRule.getOwnedRuleType());
    }


}
