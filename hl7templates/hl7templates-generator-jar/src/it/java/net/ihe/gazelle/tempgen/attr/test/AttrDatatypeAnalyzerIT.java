package net.ihe.gazelle.tempgen.attr.test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrDatatypeAnalyzer;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class AttrDatatypeAnalyzerIT {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
//        System.setProperty("HL7TEMP_RESOURCES_PATH", "../hl7templates-resources");
    }

    @Test
    public void testGetDatatype() {
        AttrDatatypeAnalyzer adt = new AttrDatatypeAnalyzer();
        adt.setDatatype(AttDatatype.BIN);
        assertTrue(adt.getDatatype().equals(AttDatatype.BIN));
    }

    @Test
    public void testGenerateOCLConstraint1() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
        Attribute attr = code.getAttribute().get(0);
        AttrDatatypeAnalyzer aa = new AttrDatatypeAnalyzer();
        aa.setDatatype(AttDatatype.getAttDatatypeByName(attr.getDatatype().getLocalPart()));
        String ocl = (new AttrDatatypeAnalyzer()).generateOCLConstraint(attr);
        assertTrue(ocl.equals("self.code->forAll(code.oclIsUndefined() or CommonOperationsStatic::isTSType(code))"));
    }

    @Test
    public void testGenerateOCLConstraint2() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
        Attribute attr = id.getAttribute().get(0);
        AttrDatatypeAnalyzer aa = new AttrDatatypeAnalyzer();
        aa.setDatatype(AttDatatype.getAttDatatypeByName(attr.getDatatype().getLocalPart()));
        String ocl = (new AttrDatatypeAnalyzer()).generateOCLConstraint(attr);
        assertTrue(ocl.equals("self.id->forAll(displayable.oclIsUndefined() or CommonOperationsStatic::matches(displayable.toString(), '" +
				"(true|false)'))"));
    }

    @Test
    public void testGenerateOCLConstraint3() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(1));
        RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        Attribute attr = id.getAttribute().get(1);
        AttrDatatypeAnalyzer aa = new AttrDatatypeAnalyzer();
        aa.setDatatype(AttDatatype.getAttDatatypeByName(attr.getDatatype().getLocalPart()));
        String ocl = (new AttrDatatypeAnalyzer()).generateOCLConstraint(attr);
        assertTrue(ocl.equals("self.value->select(oclIsKindOf(PQ)).oclAsType(PQ)->forAll(value.oclIsUndefined() or CommonOperationsStatic::matches" +
                "(value, '\\\\d+(\\\\.0)?'))"));
    }

    @Test
    public void testGenerateCommentConstraint() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
        Attribute attr = code.getAttribute().get(0);
        AttrDatatypeAnalyzer aa = new AttrDatatypeAnalyzer();
        aa.setDatatype(AttDatatype.getAttDatatypeByName(attr.getDatatype().getLocalPart()));
        String comment = (new AttrDatatypeAnalyzer()).generateCommentConstraint(attr);
        assertTrue(comment.equals("In Section Coded Social History, " +
                "in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:code, " +
                "the attribute code SHALL have the datatype 'ts' if present"));
    }

    @Test
    public void testGetProcessIdentifier() {
        assertTrue((new AttrDatatypeAnalyzer()).getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_DT_PROCESS.getValue()));
    }

}
