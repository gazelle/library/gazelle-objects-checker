package net.ihe.gazelle.tempgen.choice.test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.action.ChoiceDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.action.RuleDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenDecor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AssertionChoicesIT extends ChoiceDefinitionAnalyzer {

    Decor decor = null;
    TemplateDefinition td = null;
    RuleDefinition observation;
    ChoiceDefinition currentChoice;
    List<RuleDefinition> elements;
    PackagedElement pe;

    @Before
    public void setUp(){
        decor = DecorMarshaller.loadDecor("src/test/resources/choices/decor_assertionChoices3.xml");
//        decor = DecorMarshaller.loadDecor("src/test/resources/choices/decor_helper.xml");
        decor = DecorUtil.cleanupForEffectiveDate(decor);
        decor = GeneralFlattenDecor.generalFlattenDecor(decor);
        td = RulesUtil.getTemplates(decor.getRules()).get(0);
        observation = TemplateDefinitionUtil.getFirstElement(td);
        currentChoice = RuleDefinitionUtil.getChoices(observation).get(0);
        elements = ChoiceDefinitionUtil.getElements(currentChoice);
        pe = new PackagedElement();
    }

    /**
     * This functionality is deprecated and the processAndReturn is not used anymore.
     * Cause: Drop the processPredicate feature and delegate it to flattenning
     */
    @Deprecated
    @Ignore
    @Test
    public void TestPrintChoices(){
        List<RuleDefinition> ruleDefinitions = ChoiceDefinitionUtil.getElements(currentChoice);
        for(RuleDefinition element:ruleDefinitions){
            System.out.println(element.getName());
        }
        assertEquals(ruleDefinitions.size(),3);
    }

    /**
     * This functionality is deprecated and the processAndReturn is not used anymore.
     * Cause: Drop the processPredicate feature and delegate it to flattenning
     */
    @Deprecated
    @Ignore
    @Test
    public void testProcessAndReturnRDTest(){
        List<OwnedRule> rules = (new RuleDefinitionAnalyzer()).processAndReturn(elements.get(1), pe);
        assertEquals(rules.size(),4);
        assertEquals(rules.get(0).getOwnedRuleKind(), OwnedRuleKind.FIXEDVAL);
        assertEquals(rules.get(0).getSpecification().getBody(),"self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::SPRT)->forAll(typeCode.oclIsUndefined() or typeCode=XActRelationshipEntryRelationship::SPRT)");
    }

    /**
     * This functionality is deprecated and the processAndReturn is not used anymore.
     * Cause: Drop the processPredicate feature and delegate it to flattenning
     */
    @Deprecated
    @Ignore
    @Test
    public void testProcessPredicatesTest(){
        this.currentPackagedElement = pe;
        this.currentChoiceDefinition = currentChoice;
        processPredicates(elements);
        assertEquals(this.currentPackagedElement.getOwnedRule().size(),1);
        assertEquals(this.currentPackagedElement.getOwnedRule().get(0).getSpecification().getBody(),
                "((((self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::GEVL)->forAll(typeCode.oclIsUndefined() or typeCode=XActRelationshipEntryRelationship::GEVL)) and (self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::GEVL)->reject(not nullFlavor.oclIsUndefined())->forAll(aa | CommonOperationsStatic::validateByXPATHV2(aa, '@typeCode=\\u0027GEVL\\u0027') )))) or (((self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::SPRT)->forAll(typeCode.oclIsUndefined() or typeCode=XActRelationshipEntryRelationship::SPRT)) and (self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::SPRT)->forAll((not nullFlavor.oclIsUndefined()) or (not inversionInd.oclIsUndefined()))) and (self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::SPRT)->forAll(inversionInd.oclIsUndefined() or inversionInd=true)) and (self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::SPRT)->size()<2))) or (((self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::RSON)->forAll(typeCode.oclIsUndefined() or typeCode=XActRelationshipEntryRelationship::RSON)) and (self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::RSON)->reject(not nullFlavor.oclIsUndefined())->forAll(aa | CommonOperationsStatic::validateByXPATHV2(aa, '@typeCode=\\u0027RSON\\u0027') )))))");
    }

    /**
     * This functionality is deprecated and the processAndReturn is not used anymore.
     * Cause: Drop the processPredicate feature and delegate it to flattenning
     */
    @Deprecated
    @Ignore
    @Test
    public void testProcessTest(){
        (new ChoiceDefinitionAnalyzer()).process(currentChoice,pe);
        assertEquals(pe.getOwnedRule().size(),2);

    }

    /**
     * This functionality is deprecated and the processAndReturn is not used anymore.
     * Cause: Drop the processPredicate feature and delegate it to flattenning
     */
    @Deprecated
    @Ignore
    @Test
    public void testGenerateSubRuleOfAssertionElementsTest(){
        assertTrue(choiceHasPredicates(elements) && !choiceHasNumberedPredicates(elements));
        List<List<OwnedRule>> rules = (new ChoiceDefinitionAnalyzer()).generateSubRuleOfAssertionElements(elements);
        assertEquals(rules.size(),3);
        assertEquals(rules.get(0).size(),2);
        assertEquals(rules.get(1).size(),4);
        assertEquals(rules.get(2).size(),2);
        assertEquals(rules.get(0).get(1).getOwnedRuleKind(),OwnedRuleKind.CONTEXT);
    }

    /**
     * This functionality is deprecated and the processAndReturn is not used anymore.
     * Cause: Drop the processPredicate feature and delegate it to flattenning
     */
    @Deprecated
    @Ignore
    @Test
    public void testGenerateGeneraleRuleForAssertionChoiceTest(){
        assertTrue(choiceHasPredicates(elements) && !choiceHasNumberedPredicates(elements));
        this.currentChoiceDefinition = currentChoice;
        List<List<OwnedRule>> rules = generateSubRuleOfAssertionElements(elements);
        OwnedRule generaleRule = generateGeneraleRuleForChoice(rules);
        assertEquals(OwnedRuleKind.CHOICE, generaleRule.getOwnedRuleKind());

    }
}
