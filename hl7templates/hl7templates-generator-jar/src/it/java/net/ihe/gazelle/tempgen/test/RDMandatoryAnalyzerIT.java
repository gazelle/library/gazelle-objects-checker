package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.RDMandatoryAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMandatoryAnalyzerIT {
	
	Decor decorTemplatesMandatory = null;
	Decor decorTemplatesNotMandatory = null;
	
	@Before
	public void setUp(){
		decorTemplatesMandatory = DecorMarshaller.loadDecor("src/test/resources/decor_custodian.xml");
		decorTemplatesNotMandatory = DecorMarshaller.loadDecor("src/test/resources/decor_custodian_optional.xml");
	}

	@Covers(requirements = {"GOC-006","GOC-027"}, testType = TestType.INTEGRATION_TEST)
	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplatesMandatory.getRules()).get(0));
		RuleDefinition custodian = RuleDefinitionUtil.getElementByName(firstRD, "cda:custodian");
		assertTrue((new RDMandatoryAnalyzer()).generateOCLConstraint(custodian).equals(
				"self.custodian->forAll(nullFlavor.oclIsUndefined())"));
		RuleDefinition assignedCustodian = RuleDefinitionUtil.getElementByName(custodian, "cda:assignedCustodian");
		assertTrue((new RDMandatoryAnalyzer()).generateOCLConstraint(assignedCustodian).equals(
				"self.custodian.assignedCustodian->forAll(nullFlavor.oclIsUndefined())"));
	}


	/**
	 * /!\ Should Covers GOC-005 but gives wrong output in the low layer,
	 * Behaviour correctly provided at processing time, needed "processIsMandatoryTest" to cover
	 */
	@Ignore
	@Test
	public void testGenerateOCLConstraintNotMandatory(){
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplatesNotMandatory.getRules()).get(0));
		RuleDefinition custodian = RuleDefinitionUtil.getElementByName(firstRD, "cda:custodian");
		RuleDefinition assignedCustodian = RuleDefinitionUtil.getElementByName(custodian, "cda:assignedCustodian");
		String gen = (new RDMandatoryAnalyzer()).generateOCLConstraint(assignedCustodian);
		System.out.println(gen);
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplatesMandatory.getRules()).get(0));
		RuleDefinition custodian = RuleDefinitionUtil.getElementByName(firstRD, "cda:custodian");
		assertTrue((new RDMandatoryAnalyzer()).generateCommentConstraint(custodian).equals(
				"In US Realm Header, in /cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.1'], "
				+ "the element(s) cda:custodian SHALL not have nullFlavor (mandatory)"));
		RuleDefinition assignedCustodian = RuleDefinitionUtil.getElementByName(custodian, "cda:assignedCustodian");
		assertTrue((new RDMandatoryAnalyzer()).generateCommentConstraint(assignedCustodian).equals(
				"In US Realm Header, in /cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/cda:custodian, "
				+ "the element(s) cda:assignedCustodian SHALL not have nullFlavor (mandatory)"));
	}

}
