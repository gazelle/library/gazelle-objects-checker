package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.CDAPathGenerator;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class CDAPathGeneratorIT {
	
	private Decor currentDecor = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");

	@Test
	public void testExtractCDAPath1() throws FileNotFoundException, JAXBException {
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		assertTrue(CDAPathGenerator.extractCDAPath(td).equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
	}
	
	@Test
	public void testExtractCDAPath2() throws FileNotFoundException, JAXBException {
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(td);
		assertTrue(CDAPathGenerator.extractCDAPath(rd).equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
		rd = RuleDefinitionUtil.getElements(rd).get(0);
		assertTrue(CDAPathGenerator.extractCDAPath(rd).equals(
				"/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
	}
	
	@Test
	public void testExtractCDAPath3() throws FileNotFoundException, JAXBException {
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(td);
		assertTrue(CDAPathGenerator.extractCDAPath(rd).equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
		assertTrue(CDAPathGenerator.extractCDAPath(rd.getAttribute().get(0)).equals(
				"/hl7:act[@classCode='ACT']/@classCode"));
	}
	
	@Test
	public void testExtractCDAPath4() throws FileNotFoundException, JAXBException {
		Decor currentDecor = DecorMarshaller.loadDecor("src/test/resources/decor_testpath.xml");
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		assertTrue(CDAPathGenerator.extractCDAPath(td).equals("/hl7:templateId"));
	}
	
	@Test
	public void testExtractCDAPathChoiceDefinition() throws FileNotFoundException, JAXBException {
		Decor currentDecor = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(td), "hl7:participantRole");
		ChoiceDefinition currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0);
		assertTrue(CDAPathGenerator.extractCDAPath(currentChoiceDefinition).equals("/hl7:participant/hl7:participantRole"));
	}
	
	@Test
	public void testExtractCDAPathIncludeDefinition() throws FileNotFoundException, JAXBException {
		Decor currentDecor = DecorMarshaller.loadDecor("src/test/resources/decor_flat_let.xml");
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		RuleDefinition section = TemplateDefinitionUtil.getElementsByName(td, "hl7:section").get(0);
		RuleDefinition entry = RuleDefinitionUtil.getElementByName(section, "hl7:entry");
		IncludeDefinition id = RuleDefinitionUtil.getIncludes(entry).get(0);
		assertTrue(CDAPathGenerator.extractCDAPath(id).equals(
				"/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry"));
	}
	
	@Test
	public void testExtractCDAPathAssert() throws FileNotFoundException, JAXBException {
		Decor currentDecor = DecorMarshaller.loadDecor(new FileInputStream("src/test/resources/decor_flat_let.xml"));
		TemplateDefinition td = RulesUtil.getTemplates(currentDecor.getRules()).get(0);
		Assert ass = TemplateDefinitionUtil.getAsserts(td).get(0);
		System.out.println(CDAPathGenerator.extractCDAPath(ass));
		assertTrue(CDAPathGenerator.extractCDAPath(ass).equals(
				"/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']"));
	}

}
