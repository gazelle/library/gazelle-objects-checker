package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.RDMaximumMultiplicityAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMaximumMultiplicityAnalyzerIT {

	static final int MAX_INTEGER = 1000000000;


	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
	}

	@Covers(requirements = {"GOC-003","GOC-026"}, testType = TestType.INTEGRATION_TEST)
	@Test
	public void testGenerateMaximumMultiplicityBodyConstraint1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String gen = (new RDMaximumMultiplicityAnalyzer()).generateOCLConstraint(status);
		assertTrue(gen.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0').observation->forAll(targetSiteCode->size()<2)"));
	}
	
	@Test
	public void testGenerateMaximumMultiplicityBodyConstraint2() {
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String gen = (new RDMaximumMultiplicityAnalyzer()).generateOCLConstraint(status);
		assertTrue(gen.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and "
				+ "observation.code.code='48766-0').observation->forAll(targetSiteCode->size()<2)"));
	}


	//This case figures in the oclGeneration level (low) but ignored in the processing level (high)
	@Covers(requirements = {"GOC-004"}, testType = TestType.INTEGRATION_TEST)
	@Test
	public void testGenerateMaximumMultiplicityBodyConstraintNMulplicity(){
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		String gen = (new RDMaximumMultiplicityAnalyzer()).generateOCLConstraint(entry);
		assertTrue(gen.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')->size()<"+Integer.valueOf(MAX_INTEGER+1)));
	}

	@Test
	public void testGenerateMaximumMultiplicityDescriptionRule() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		String gen = (new RDMaximumMultiplicityAnalyzer()).generateCommentConstraint(status);
		assertTrue(gen.equals("In Problem Concern, "
				+ "/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:entryRelationship[hl7:observation/hl7:code/@code='48766-0']/hl7:observation SHALL "
				+ "contain at most ONE hl7:targetSiteCode"));
	}

	@Test
	public void testGenerateMaximumMultiplicityDescriptionRuleNamescape() {
		Decor decorNamespace = DecorMarshaller.loadDecor("src/test/resources/decor_obs_namespace.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorNamespace.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "cda:observation");
		RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "cda:targetSiteCode");
		String gen = (new RDMaximumMultiplicityAnalyzer()).generateCommentConstraint(status);
		assertEquals("In Problem Concern, "
				+ "/cda:act[cda:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/cda:entryRelationship[cda:observation/cda:code/@code='48766-0']/cda:observation SHALL "
				+ "contain at most ONE cda:targetSiteCode", gen);
	}

}
