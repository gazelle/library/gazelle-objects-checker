package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainMinimumMultiplicityAnalyzerIT {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter.xml");
    }

    @Covers(requirements = {"GOC-002"}, testType = TestType.INTEGRATION_TEST)
    @Test
    public void testGenerateOCLConstraint1() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");

        String gen = (new ContainMinimumMultiplicityAnalyzer()).generateOCLConstraint(entr);
        String expectedConstraint = "(not self.nullFlavor.oclIsUndefined()) or " +
                "self.entry->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.entry->reject(not nullFlavor.oclIsUndefined()).observation->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.entry->reject(not nullFlavor.oclIsUndefined()).observation->reject(not nullFlavor.oclIsUndefined())" +
                ".templateId->select((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4')->size()>0";
        assertEquals(expectedConstraint, gen);
    }

    @Test
    public void testGenerateCommentConstraint() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        String gen = (new ContainMinimumMultiplicityAnalyzer()).generateCommentConstraint(entr);
        assertEquals("In Section Coded Social History, /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16" +
                ".1']/hl7:entry/hl7:observation/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] SHALL contain at least ONE " +
                "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']", gen);
    }

}