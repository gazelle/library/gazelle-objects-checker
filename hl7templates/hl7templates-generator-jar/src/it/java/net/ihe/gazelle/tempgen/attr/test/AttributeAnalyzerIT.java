package net.ihe.gazelle.tempgen.attr.test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.GOCModelMarshaller;
import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.tempgen.action.AttributeAnalyzer;
import net.ihe.gazelle.tempgen.action.DistinguisherFollower;
import net.ihe.gazelle.tempgen.action.TamlHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

import static org.junit.Assert.*;

/**
 * @author Abderrazek Boufahja
 */
public class AttributeAnalyzerIT extends AttributeAnalyzer {

    Decor decorTemplates = null;
    Decor contTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_multi.xml");
        contTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
//        System.setProperty("HL7TEMP_RESOURCES_PATH", "../hl7templates-resources");
    }

    @Test
    public void testProcess() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.process(selectedAttribute, this.currentPackagedElement);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessClassCode() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.processClassCode(this.selectedAttribute.getValue());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedComment().getBody().contains("classCode"));
    }

    @Test
    public void testProcessContextConductionInd() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(contTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entry");
        this.selectedAttribute = entr.getAttribute().get(0);
        this.processContextConductionInd(this.selectedAttribute.isContextConductionInd());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedComment().getBody().contains("ontextConductionInd"));
    }

    @Test
    public void testProcessContextControlCode() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(contTemplates.getRules()).get(0));
        RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
        this.selectedAttribute = subj.getAttribute().get(0);
        this.processContextControlCode(this.selectedAttribute.getContextControlCode());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedComment().getBody().contains("ontextControlCode"));
    }

    @Test
    public void testProcessDatatype1() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(contTemplates.getRules()).get(0));
        RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
        RuleDefinition subjrel = RuleDefinitionUtil.getElementByName(subj, "cda:relatedSubject");
        RuleDefinition add = RuleDefinitionUtil.getElementByName(subjrel, "cda:addr");
        this.selectedAttribute = add.getAttribute().get(0);
        this.processDatatype(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processDatatype(new QName("st"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processDatatype(new QName("uid"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessDatatype2() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(contTemplates.getRules()).get(0));
        RuleDefinition temp = RuleDefinitionUtil.getElementByName(firstRD, "cda:templateId");
        this.selectedAttribute = temp.getAttribute().get(0);
        this.processDatatype(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processDatatype(new QName("st"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processDatatype(new QName("int"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessDeterminerCode() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition author = RuleDefinitionUtil.getElementByName(firstRD, "hl7:author");
        RuleDefinition assignedAuthor = RuleDefinitionUtil.getElementByName(author, "hl7:assignedAuthor");
        this.selectedAttribute = assignedAuthor.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processDeterminerCode(this.selectedAttribute.getDeterminerCode());
        // the determinerCode in the wrong place
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        RuleDefinition person = RuleDefinitionUtil.getElementByName(assignedAuthor, "hl7:assignedPerson");
        this.selectedAttribute = person.getAttribute().get(0);
        this.processDeterminerCode(this.selectedAttribute.getDeterminerCode());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessExtension() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.processExtension("aaaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedAttribute = id.getAttribute().get(0);
        this.processExtension("aaaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessIndependentInd1() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processIndependentInd(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessIndependentInd2() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
        this.selectedAttribute = id.getAttribute().get(0);
        this.processIndependentInd(true);
        // there is no independentInd in id element
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessIndependentInd3() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.processIndependentInd(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessInstitutionSpecified1() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processInstitutionSpecified(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessInstitutionSpecified2() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
        this.selectedAttribute = id.getAttribute().get(0);
        this.processInstitutionSpecified(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessInstitutionSpecified3() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "cda:author");
        RuleDefinition time = RuleDefinitionUtil.getElementByName(auth, "cda:time");
        this.selectedAttribute = time.getAttribute().get(0);
        this.processInstitutionSpecified(Boolean.TRUE);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessInversionInd() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(0);
        this.processInversionInd(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processInversionInd(false);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        this.processInversionInd(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 2);
    }

    @Test
    public void testProcessIsOptional() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(0);
        this.processIsOptional(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processIsOptional(false);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 2);
        this.processIsOptional(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 4);
    }

    @Test
    public void testProcessItem1() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(0);
        XMI xmi = XMIUtil.createXMI("aa");
        TamlHandler.addTamlToXMI(xmi);
        int number = xmi.getTaml().size();
        this.processItem(null);
        TamlHandler.addTamlToXMI(xmi);
        assertTrue(xmi.getTaml().size() == 2 * number);
    }

    @Test
    public void testProcessItem2() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(0);
        this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
        XMI xmi = XMIUtil.createXMI("aa");
        TamlHandler.addTamlToXMI(xmi);
        int number = xmi.getTaml().size();
        this.processItem(null);
        TamlHandler.addTamlToXMI(xmi);
        assertTrue(xmi.getTaml().size() == 2 * number);
    }

    @Test
    public void testProcessItem3() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(0);
        this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
        XMI xmi = XMIUtil.createXMI("aa");
        TamlHandler.addTamlToXMI(xmi);
        int number = xmi.getTaml().size();
        this.processIsOptional(false);
        this.processItem(null);
        TamlHandler.addTamlToXMI(xmi);
        assertTrue(xmi.getTaml().size() == 2 * number + 2);
    }

    @Test
    public void testProcessMediaType() {
//		fail("Not yet implemented");
    }

    @Test
    public void testProcessMoodCode1() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMoodCode("EVN");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessMoodCode2() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMoodCode("AAAAA");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessMoodCode3() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMoodCode(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessNegationInd1() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processNegationInd(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessNegationInd2() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processNegationInd("aaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessNegationInd3() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processNegationInd("true");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessNegationInd4() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processNegationInd("false");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessNullFlavor() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(0);
        this.processNullFlavor(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processNullFlavor("AAAAAaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processNullFlavor(this.selectedAttribute.getNullFlavor());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessOperator() {
//		fail("Not yet implemented");
    }

    @Test
    public void testProcessProhibited() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedAttribute = entr.getAttribute().get(1);
        this.processProhibited(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processProhibited(false);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processProhibited(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessQualifier() {
//		fail("Not yet implemented");
    }

    @Test
    public void testProcessRepresentation() {
//		fail("Not yet implemented");
    }

    @Test
    public void testProcessRoot() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
        this.selectedAttribute = id.getAttribute().get(0);
        this.processRoot(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processRoot("aaaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "aaaa").isEmpty());
    }

    @Test
    public void testProcessTypeCode() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "cda:author");
        this.selectedAttribute = auth.getAttribute().get(0);
        this.processTypeCode(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processTypeCode("AAAAAAAA");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processTypeCode("AUT");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessUnit() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs4.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "cda:author");
        this.selectedAttribute = auth.getAttribute().get(0);
        this.processUnit(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processUnit("AAAAAAAA");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        RuleDefinition effec = RuleDefinitionUtil.getElementByName(firstRD, "cda:effectiveTime");
        this.selectedAttribute = effec.getAttribute().get(0);
        this.processUnit("kg");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "cda:value");
        this.selectedAttribute = value.getAttribute().get(0);
        this.processUnit("kg");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessUse() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "cda:author");
        RuleDefinition asa = RuleDefinitionUtil.getElementByName(auth, "cda:assignedAuthor");
        RuleDefinition addr = RuleDefinitionUtil.getElementByName(asa, "cda:addr");
        this.selectedAttribute = addr.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processUse(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processUse("AAAAA");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
        int numb = this.currentPackagedElement.getOwnedRule().size();
        this.processUse("H");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > numb);
    }

    @Test
    public void testProcessValue1() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition tdi = RuleDefinitionUtil.getElementsByName(firstRD, "cda:templateId").get(1);
        this.selectedAttribute = tdi.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processValue(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processValue("aaaaaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessValue2() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition tdi = RuleDefinitionUtil.getElementsByName(firstRD, "cda:templateId").get(1);
        this.selectedAttribute = tdi.getAttribute().get(1);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processValue("aaaaaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessValue3() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition tdi = RuleDefinitionUtil.getElementsByName(firstRD, "cda:templateId").get(1);
        this.selectedAttribute = tdi.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        DistinguisherFollower.followDistinguisher("/cda:observation[cda:templateId/@root='2.16.840.1.113883.10.20.6.2.12']/"
                + "cda:templateId[@root='4.3.2']/@root", "4.3.2");
        this.processValue("4.3.2");
        assertEquals( 0, this.currentPackagedElement.getOwnedRule().size());
    }

    @Test
    public void testProcessValue4() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition tdi = RuleDefinitionUtil.getElementsByName(firstRD, "cda:templateId").get(1);
        this.selectedAttribute = tdi.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        DistinguisherFollower.unFollowDistinguisher("/cda:observation[cda:templateId/@root='2.16.840.1.113883.10.20.6.2.12']/"
                + "cda:templateId[@root='4.3.2']/@root");
        this.processValue("4.3.2");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessVocabularys1() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_attr_vocab1.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition ass = RuleDefinitionUtil.getElementByName(firstRD, "hl7:assignedAuthor");
        RuleDefinition tel = RuleDefinitionUtil.getElementByName(ass, "hl7:telecom");
        this.selectedAttribute = tel.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(selectedAttribute.getVocabulary());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GOCModelMarshaller.printGOCObject(this.currentPackagedElement, baos);
        String res = baos.toString();
        assertTrue(res.contains("self.assignedAuthor.telecom.use-&gt;forAll( aa | aa.oclIsUndefined() or "
                + "(CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.40&amp;version=2013-06-03T00:00:00', aa)))"));
    }

    @Test
    public void testProcessVocabularys2() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_attr_vocab1.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(selectedAttribute.getVocabulary());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GOCModelMarshaller.printGOCObject(this.currentPackagedElement, baos);
        String res = baos.toString();
        assertTrue(res.contains("<body>self.typeCode-&gt;forAll( aa | aa.oclIsUndefined() or "
                + "(CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.41&amp;version=2013-06-03T00:00:01', CommonOperationsStatic::stringValueOf(aa))))</body>"));
    }

    @Test
    public void testProcessVocabularys3() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_attr_vocab2.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedAttribute = firstRD.getAttribute().get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(selectedAttribute.getVocabulary());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GOCModelMarshaller.printGOCObject(this.currentPackagedElement, baos);
        String res = baos.toString();
        assertTrue(res.contains("self.typeCode-&gt;forAll( aa | aa.oclIsUndefined() or (CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.41', CommonOperationsStatic::stringValueOf(aa))) or (CommonOperationsStatic::stringValueOf(aa)='PRS'))"));
    }

    @Test
    public void testAttributeIsCDAAttribute() {
        RuleDefinition ruleDefinition = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        Attribute attr = new Attribute();
        attr.setParentObject(ruleDefinition);
        assertTrue(this.attributeIsCDAAttribute(attr, "classCode"));
        assertFalse(this.attributeIsCDAAttribute(attr, "typeCode"));
        assertTrue(this.attributeIsCDAAttribute(attr, "moodCode"));
    }

    @Test
    public void testAttributeIsCDAAttribute2() throws Exception {
        RuleDefinition ruleDefinition = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        Attribute attr = new Attribute();
        attr.setParentObject(ruleDefinition);
        attr.setName("classCode");
        assertTrue(this.attributeIsCDAAttribute(attr));
        attr.setName("typeCode");
        assertFalse(this.attributeIsCDAAttribute(attr));
    }

    @Test
    public void testAttributeIsCDAAttributeAndValueExists() {
        RuleDefinition ruleDefinition = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        Attribute attr = new Attribute();
        attr.setParentObject(ruleDefinition);
        assertTrue(this.attributeIsCDAAttributeAndValueExists(attr, "classCode", "SPLY"));
        assertFalse(this.attributeIsCDAAttributeAndValueExists(attr, "classCode", "AAAA"));
    }

}
