package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrVocabAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrVocabAnalyzerIT extends AttrVocabAnalyzer {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_vocab_attr.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition dq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:doseQuantity");
		Attribute attr = dq.getAttribute().get(0);
		String ocl = (new AttrVocabAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.doseQuantity.unit->" +
				"forAll( aa | aa.oclIsUndefined() or (CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.16&version=2016-10-10T00:00:00', aa)))"));
	}
	
	@Test
	public void testGenerateOCLConstraint2() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_vocab_attr_sequoia.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
		RuleDefinition recordTarget = RuleDefinitionUtil.getElementByName(firstRD, "cda:recordTarget");
		RuleDefinition patientRole = RuleDefinitionUtil.getElementByName(recordTarget, "cda:patientRole");
		RuleDefinition patientRoleAddr = RuleDefinitionUtil.getElementByName(patientRole, "cda:addr");
		Attribute attr = patientRoleAddr.getAttribute().get(1);
		String ocl = (new AttrVocabAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.recordTarget.patientRole.addr.use->forAll( aa | aa.oclIsUndefined() or "
				+ "(CommonOperationsStatic::matchesCodeToValueSet('2.16.840.1.113883.1.11.10637&version=2005-05-01T00:00:00', aa)))"));
	}

	@Test
	public void testExtractConstraintForVocabulary() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition dq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:doseQuantity");
		Attribute attr = dq.getAttribute().get(0);
		Vocabulary vocabulary = new Vocabulary();
		vocabulary.setCode("test");
		String ocl = this.extractConstraintForVocabulary(vocabulary, attr);
		assertTrue(ocl.equals("aa='test'"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition dq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:doseQuantity");
		Attribute attr = dq.getAttribute().get(0);
		String comment = this.generateCommentConstraint(attr);
		assertTrue(comment.equals("In Entry Medication Summary, the code of " +
				"/hl7:substanceAdministration[hl7:templateId/@root='1.3.6.1.4.1.12559.11.10.1.3.1.3.4']/hl7:doseQuantity/@unit SHALL be " +
				"from the valueSet 1.3.6.1.4.1.12559.11.10.1.3.1.42.16 (flexibility : 2016-10-10T00:00:00)"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(this.getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue()));
	}

}
