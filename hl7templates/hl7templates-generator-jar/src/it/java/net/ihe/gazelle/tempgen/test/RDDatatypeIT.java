package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.RDDatatypeAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDDatatypeIT {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition value = RuleDefinitionUtil.getElementByName(obs, "hl7:value");
		String gen = (new RDDatatypeAnalyzer()).generateOCLConstraint(value);
		assertTrue(gen.equals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0').observation.value->forAll(oclIsKindOf(CD))"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition value = RuleDefinitionUtil.getElementByName(obs, "hl7:value");
		String gen = (new RDDatatypeAnalyzer()).generateCommentConstraint(value);
		assertTrue(gen.equals(
				"In Problem Concern, "
				+ "in /hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:entryRelationship[hl7:observation/hl7:code/@code='48766-0']/hl7:observation, "
				+ "the element(s) hl7:value SHALL be from the datatype CD"));
	}

}
