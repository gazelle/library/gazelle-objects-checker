package net.ihe.gazelle.tempgen.choice.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceAnalyzerEnum;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMinimumMultiplicityANalyzer;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceMinimumMultiplicityANalyzerIT extends ChoiceInitTest {

	@Test
	public void testGenerateOCLConstraint() {
		String ocl = (new ChoiceMinimumMultiplicityANalyzer()).generateOCLConstraint(currentChoiceDefinition);
		assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.participantRole->reject(not nullFlavor.oclIsUndefined())" +
				"->forAll( (playingDevice->size()+playingEntity->size())>=1)", ocl);
	}

	@Test
	public void testGenerateCommentConstraint() {
		String comment = (new ChoiceMinimumMultiplicityANalyzer()).generateCommentConstraint(currentChoiceDefinition);
		assertEquals("In CDA Participant (Body), in /hl7:participant, "
				+ "the number of elements of type 'playingDevice', 'playingEntity' SHALL be bigger or equal to 1", comment);
	}

	@Test
	public void testGetProcessIdentifier() {
		assertEquals(ChoiceAnalyzerEnum.CH_MIN_PROCESS.getValue(),new ChoiceMinimumMultiplicityANalyzer().getProcessIdentifier());
	}

}
