package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.tempgen.action.DecorAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

public class ValueSetFlexibility2IT extends DecorAnalyzer {
	
	@Before
	public void before() {
		this.currentDecor = DecorMarshaller.loadDecor("src/test/resources/valuesetflex/decor_vs_dynamic2.xml");
		this.xmi = XMIUtil.createXMI("test");
	}
	
	@Test
	public void testProcess() throws JAXBException {
		(new DecorAnalyzer()).process(currentDecor, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertEquals(2, xmi.getModel().getPackagedElement().size());
		assertEquals(9, xmi.getModel().getPackagedElement().get(0).getOwnedRule().size());
		assertTrue(xmiStr.contains("In US Realm Header, the code of "
				+ "/cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/cda:confidentialityCode SHALL be from"
				+ " the valueSet 2.16.840.1.113883.1.11.16926"));
		assertTrue(xmiStr.contains("In US Realm Header, the code of "
				+ "/cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/cda:languageCode SHALL be from"
				+ " the valueSet 2.16.840.1.113883.1.11.11526"));
	}

}
