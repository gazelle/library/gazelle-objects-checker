package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.flatten.action.LetFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class LetFlattenProcIT {
	
	Decor decorTemplates = null;
	
	Decor decFlat = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_1.xml");
		decFlat = DecorMarshaller.loadDecor("src/test/resources/decor_flat_let.xml");
	}

	@Test
	public void testProcess1() {
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
		Let let = TemplateDefinitionUtil.getLets(td).get(0);
		(new LetFlattenProc()).process(let);
		assertTrue(let.getParentObject()==td);
		assertTrue(TemplateDefinitionUtil.getLets(td).size()==1);
		assertTrue(RuleDefinitionUtil.getLets(sec).size()==0);
		
	}
	
	@Test
	public void testProcess2() {
		TemplateDefinition td = RulesUtil.getTemplates(decFlat.getRules()).get(0);
		RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
		Let let = TemplateDefinitionUtil.getLets(td).get(0);
		assertTrue(TemplateDefinitionUtil.getLets(td).size()==1);
		assertTrue(RuleDefinitionUtil.getLets(sec).size()==0);
		(new LetFlattenProc()).process(let);
		assertTrue(let.getParentObject()==sec);
		assertTrue(TemplateDefinitionUtil.getLets(td).size()==0);
		assertTrue(RuleDefinitionUtil.getLets(sec).size()==1);
		
	}

}
