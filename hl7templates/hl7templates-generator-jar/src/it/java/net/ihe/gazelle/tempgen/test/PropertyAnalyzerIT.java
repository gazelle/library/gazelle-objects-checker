package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.action.PropertyAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PropertyAnalyzerIT extends PropertyAnalyzer {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_prop.xml");
	}

	@Test
	public void testProcessProperty() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition qq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
		PackagedElement pe = new PackagedElement(); 
		List<OwnedRule> listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		(new PropertyAnalyzer()).process(qq.getProperty(), pe, listGeneratedOwnedRule);
		assertTrue(pe.getOwnedRule().size()==1);
		assertTrue(pe.getOwnedRule().get(0).getSpecification().getBody() != null);
	}

	@Test
	public void testGenerateOCLConstraint1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition qq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
		this.setListSelectedProperty(qq.getProperty());
		String ocl = this.generateOCLConstraint();
		assertTrue(ocl.equals("self.quantity->reject(not nullFlavor.oclIsUndefined())->" +
				"forAll(((not unit.oclIsUndefined()) and unit='cm' and (not value.oclIsUndefined()) and " +
				"CommonOperationsStatic::isGreaterOrEqual(value, '22') and (not value.oclIsUndefined()) and " +
				"CommonOperationsStatic::extractFractionDigits(value)='0'))"));
	}
	@Test
	public void testGenerateOCLConstraint2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition qq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
		this.setListSelectedProperty(qq.getProperty());
		Property prop = new Property();
		prop.setValue("toto");
		prop.setParentObject(qq);
		this.getListSelectedProperty().add(prop);
		String ocl = this.generateOCLConstraint();
		assertTrue(ocl.contains("((not unit.oclIsUndefined()) and unit='cm' and (not value.oclIsUndefined()) " +
				"and CommonOperationsStatic::isGreaterOrEqual(value, '22') and (not value.oclIsUndefined()) " +
				"and CommonOperationsStatic::extractFractionDigits(value)='0') or ((not value.oclIsUndefined()) and value='toto'"));
	}

	@Test
	public void testExtractOCLFromProp() {
		Property prop = new Property();
		prop.setValue("toto");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not value.oclIsUndefined()) and value='toto'"));
	}

	@Test
	public void testProcessFractionDigits1() {
		Property prop = new Property();
		prop.setFractionDigits("2!");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not value.oclIsUndefined()) and CommonOperationsStatic::extractNumberDigits(value)=2"));
	}
	
	@Test
	public void testProcessFractionDigits2() {
		Property prop = new Property();
		prop.setFractionDigits("22");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not value.oclIsUndefined()) and CommonOperationsStatic::extractNumberDigits(value)>=22"));
	}

	@Test
	public void testProcessValue() {
		Property prop = new Property();
		prop.setValue("toto");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not value.oclIsUndefined()) and value='toto'"));
	}
	
	@Test
	public void testProcessValue2() {
		Property prop = new Property();
		prop.setValue("toto");
		prop.setUnit("kg");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not unit.oclIsUndefined()) and unit='kg' and (not value.oclIsUndefined()) and value='toto'"));
	}
	
	@Test
	public void testProcessValue3() {
		Property prop = new Property();
		prop.setValue("toto");
		prop.setUnit("kg");
		String ocl = this.processValue(prop, "haha");
		assertTrue(ocl.equals("haha and (not value.oclIsUndefined()) and value='toto'"));
	}

	@Test
	public void testProcessMaxLength() {
		Property prop = new Property();
		prop.setMaxLength(3);
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("getListStringValues()->forAll(st : String | st.size()<=3)"));
	}
	
	@Test
	public void testProcessMaxLength2() {
		Property prop = new Property();
		prop.setMaxLength(3);
		String ocl = this.processMaxLength(prop, "haha");
		assertTrue(ocl.equals("haha and getListStringValues()->forAll(st : String | st.size()<=3)"));
	}

	@Test
	public void testProcessMinLength() {
		Property prop = new Property();
		prop.setMinLength(3);
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("getListStringValues()->forAll(st : String | st.size()>=3)"));
	}
	
	@Test
	public void testProcessMinLength2() {
		Property prop = new Property();
		prop.setMinLength(3);
		String ocl = this.processMinLength(prop, "haha");
		assertTrue(ocl.equals("haha and getListStringValues()->forAll(st : String | st.size()>=3)"));
	}

	@Test
	public void testProcessMaxInclude() {
		Property prop = new Property();
		prop.setMaxInclude("3.33");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not value.oclIsUndefined()) and CommonOperationsStatic::isLowerOrEqual(value, '3.33')"));
	}
	
	@Test
	public void testProcessMaxInclude2() {
		Property prop = new Property();
		prop.setMaxInclude("3.33");
		String ocl = this.processMaxInclude(prop, "haha");
		assertTrue(ocl.equals("haha and (not value.oclIsUndefined()) and CommonOperationsStatic::isLowerOrEqual(value, '3.33')"));
	}

	@Test
	public void testProcessMinInclude() {
		Property prop = new Property();
		prop.setMinInclude("3.33");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not value.oclIsUndefined()) and CommonOperationsStatic::isGreaterOrEqual(value, '3.33')"));
	}
	
	@Test
	public void testProcessMinInclude2() {
		Property prop = new Property();
		prop.setMinInclude("3.33");
		String ocl = this.processMinInclude(prop, "haha");
		assertTrue(ocl.equals("haha and (not value.oclIsUndefined()) and CommonOperationsStatic::isGreaterOrEqual(value, '3.33')"));
	}

	@Test
	public void testProcessCurrency() {
		Property prop = new Property();
		prop.setCurrency("dolar");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not currency.oclIsUndefined()) and currency='dolar'"));
	}
	
	@Test
	public void testProcessCurrency2() {
		Property prop = new Property();
		prop.setCurrency("dolar");
		String ocl = this.processCurrency(prop, "haha");
		assertTrue(ocl.equals("haha and (not currency.oclIsUndefined()) and currency='dolar'"));
	}

	@Test
	public void testProcessUnit() {
		Property prop = new Property();
		prop.setUnit("cm");
		String ocl = this.extractOCLFromProp(prop);
		assertTrue(ocl.equals("(not unit.oclIsUndefined()) and unit='cm'"));
	}
	
	@Test
	public void testProcessUnit2() {
		Property prop = new Property();
		prop.setUnit("cm");
		String ocl = this.processUnit(prop, "haha");
		assertTrue(ocl.equals("haha and (not unit.oclIsUndefined()) and unit='cm'"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition qq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
		this.setListSelectedProperty(qq.getProperty());
		String comment = this.generateCommentConstraint();
		assertTrue(comment.equals("In epSOS CDA Supply, in /hl7:supply[hl7:templateId/@root='1.3.6.1.4.1.12559.11.10.1.3.1.3.3']/hl7:quantity, "
				+ "the unit attribute shall have the value cm and the value attribute shall be bigger than 22 and "
				+ "the value attribute shall have exactly 0 digits"));
	}
	
	@Test
	public void testGenerateCommentConstraint2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition qq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
		this.setListSelectedProperty(qq.getProperty());
		Property prop = new Property();
		prop.setUnit("kg");
		this.getListSelectedProperty().add(prop);
		String comment = this.generateCommentConstraint();
		assertTrue(comment.equals("In epSOS CDA Supply, in /hl7:supply[hl7:templateId/@root='1.3.6.1.4.1.12559.11.10.1.3.1.3.3']/hl7:quantity, "
				+ "(the unit attribute shall have the value cm and the value attribute shall be bigger than 22 and "
				+ "the value attribute shall have exactly 0 digits) OR (the unit attribute shall have the value kg)"));
	}
	
	@Test
	public void testGenerateDescrForProp() {
		Property prop = new Property();
		prop.setUnit("cm");
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the unit attribute shall have the value cm"));
	}
	
	@Test
	public void testGenerateDescrForProp2() {
		Property prop = new Property();
		prop.setCurrency("euro");
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the currency attribute shall have the value euro"));
	}
	
	@Test
	public void testGenerateDescrForProp3() {
		Property prop = new Property();
		prop.setMaxInclude("aa");
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the value attribute shall be lower than aa"));
	}
	
	@Test
	public void testGenerateDescrForProp4() {
		Property prop = new Property();
		prop.setMinLength(3);
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the string value shall a length lower than 3"));
	}
	
	@Test
	public void testGenerateDescrForProp5() {
		Property prop = new Property();
		prop.setMaxLength(3);
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the string value shall a length bigger than 3"));
	}
	
	@Test
	public void testGenerateDescrForProp6() {
		Property prop = new Property();
		prop.setValue("aa");
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the value attribute shall have the value aa"));
	}
	
	@Test
	public void testGenerateDescrForProp7() {
		Property prop = new Property();
		prop.setFractionDigits("5!");
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the value attribute shall have exactly 5 digits"));
	}
	
	@Test
	public void testGenerateDescrForProp8() {
		Property prop = new Property();
		prop.setFractionDigits("5");
		String comment = this.generateDescrForProp(prop);
		assertTrue(comment.equals("the value attribute shall have at least 5 digits"));
	}

	@Test
	public void testExtractValidProperties() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition qq = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
		Property p1 = new Property();
		p1.setUnit("cm");
		p1.setParentObject(qq);
		qq.getProperty().add(p1);
		Property p2 = new Property();
		p2.setUnit("cm");
		p2.setCurrency("dolar");
		p2.setParentObject(qq);
		qq.getProperty().add(p2);
		List<Property> aa = this.extractValidProperties(qq.getProperty());
		assertTrue(aa.size()==qq.getProperty().size()-1);
		assertTrue(this.extractValidProperties(null).isEmpty());
	}
	
}
