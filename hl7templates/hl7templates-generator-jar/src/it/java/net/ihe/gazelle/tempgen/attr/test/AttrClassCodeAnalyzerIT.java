package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrClassCodeAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrClassCodeAnalyzerIT {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs2.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Attribute attr = firstRD.getAttribute().get(0);
		String ocl = (new AttrClassCodeAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.classCode.oclIsUndefined() or self.classCode=ActClassObservation::OBS"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Attribute attr = firstRD.getAttribute().get(0);
		String comment = (new AttrClassCodeAnalyzer()).generateCommentConstraint(attr);
		assertEquals("In Text Observation, in /cda:observation[cda:templateId/@root='2.16.840.1.113883.10.20.6.2.12'], "
				+ "the attribute classCode SHALL have the value 'OBS' if present",comment);
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrClassCodeAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_CLASSCODE_PROCESS.getValue()));
	}

}
