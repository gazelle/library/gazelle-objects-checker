package net.ihe.gazelle.tempgen.test;

import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.rules.analyzer.RDMinimumMultiplicityAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import net.ihe.gazelle.goctests.definitions.annotations.Covers;

import static org.junit.Assert.assertEquals;

public class RDMinimumMultiplicityAnalyzerIT {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
    }

    @Covers(requirements = {"GOC-002","GOC-025"}, testType = TestType.INTEGRATION_TEST)
    @Test
    public void testGenerateOCLConstraint1Test() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
        RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(status);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or " +
                "self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')" +
                "->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')" +
                "->reject(not nullFlavor.oclIsUndefined()).observation->forAll((not nullFlavor.oclIsUndefined()) or targetSiteCode->size()>0)", gen);
    }

    @Covers(requirements = {"GOC-001"}, testType = TestType.INTEGRATION_TEST)
    @Test
    public void testGenerateOCLConstraint0Minimum(){
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs_conf_O.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(entry);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.entryRelationship" +
                "->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')->size()>0", gen);
    }

    @Test
    public void testGenerateOCLConfmR(){
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs_conf_R.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
//        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
//        RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(entr);
        System.out.println(gen);
    }

    @Test
    public void testGenerateOCLConstraint2() throws FileNotFoundException, JAXBException {
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist1.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
        RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(status);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or " +
                "self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')" +
                "->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) and observation.code.code='48766-0')" +
                "->reject(not nullFlavor.oclIsUndefined()).observation->forAll((not nullFlavor.oclIsUndefined()) or targetSiteCode->size()>0)", gen);
    }

    @Test
    public void testGenerateOCLConstraint3() throws FileNotFoundException, JAXBException {
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist2.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        ChoiceDefinition cd = RuleDefinitionUtil.getChoices(entr).get(0);
        RuleDefinition obs = ChoiceDefinitionUtil.getElements(cd).get(0);
        RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(status);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or " +
                "self.entryRelationship->select((not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntryRelationship::REFR)" +
                "->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.entryRelationship->select((not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntryRelationship::REFR)" +
                "->reject(not nullFlavor.oclIsUndefined()).observation->select((not ((not (code.oclIsUndefined() or code.code.oclIsUndefined())) and code.code='48766-0')))" +
                "->forAll((not nullFlavor.oclIsUndefined()) or targetSiteCode->size()>0)", gen);
    }

    @Test
    public void testGenerateOCLConstraint4() throws FileNotFoundException, JAXBException {
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist2.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        ChoiceDefinition cd = RuleDefinitionUtil.getChoices(entr).get(0);
        RuleDefinition obs = ChoiceDefinitionUtil.getElements(cd).get(1);
        RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(status);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or " +
                "self.entryRelationship->select((not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntryRelationship::REFR)" +
                "->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.entryRelationship->select((not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntryRelationship::REFR)" +
                "->reject(not nullFlavor.oclIsUndefined()).observation->select((not (code.oclIsUndefined() or code.code.oclIsUndefined())) and code.code='48766-0')" +
                "->forAll((not nullFlavor.oclIsUndefined()) or targetSiteCode->size()>0)", gen);
    }

    @Test
    public void testGenerateOCLConstraint5() throws FileNotFoundException, JAXBException {
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_templ.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition templateId = RuleDefinitionUtil.getElementsByName(firstRD, "hl7:templateId").get(1);
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(templateId);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.templateId->"
                + "select((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.1.3')->size()>0", gen);
    }

    @Test
    public void testGenerateOCLConstraint6() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_abrumet_rs.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "hl7:component");
        RuleDefinition structuredBody = RuleDefinitionUtil.getElementByName(comp, "hl7:structuredBody");
        RuleDefinition component2 = RuleDefinitionUtil.getElementByName(structuredBody, "hl7:component");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateOCLConstraint(component2);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or " +
                "self.component->reject(not nullFlavor.oclIsUndefined())->size()=0 or " +
                "self.component->reject(not nullFlavor.oclIsUndefined()).structuredBody->forAll((not nullFlavor.oclIsUndefined()) or " +
                "component->select(section.templateId.root->exists(aa : String| aa = '1.3.6.1.4.1.19376.1.5.3.1.3.1'))->size()>0)", gen);
    }

    @Test
    public void testGenerateCommentConstraint() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
        RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateCommentConstraint(status);
        assertEquals("In Problem Concern, "
                + "/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5" +
				".2']/hl7:entryRelationship[hl7:observation/hl7:code/@code='48766-0']/hl7:observation "
                + "SHALL contain at least ONE hl7:targetSiteCode", gen);
    }

    @Test
    public void testGenerateCommentConstraint2() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_path_desc.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition documentationOf = RuleDefinitionUtil.getElementByName(firstRD, "cda:documentationOf");
        RuleDefinition serviceEvent = RuleDefinitionUtil.getElementByName(documentationOf, "cda:serviceEvent");
        RuleDefinition temp = RuleDefinitionUtil.getElementByName(serviceEvent, "cda:templateId");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateCommentConstraint(temp);
        assertEquals("In GEE Progress Note, "
                + "/cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.9']/cda:documentationOf/cda:serviceEvent "
                + "SHALL contain at least ONE cda:templateId[@root='2.16.840.1.113883.10.20.21.3.1']", gen);
    }

    @Test
    public void testGenerateCommentConstraint3() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_abrumet_rs.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "hl7:component");
        RuleDefinition structuredBody = RuleDefinitionUtil.getElementByName(comp, "hl7:structuredBody");
        RuleDefinition component2 = RuleDefinitionUtil.getElementByName(structuredBody, "hl7:component");
        String gen = (new RDMinimumMultiplicityAnalyzer()).generateCommentConstraint(component2);
        assertEquals("In Abrumet Referral Summary Document, "
                + "/hl7:ClinicalDocument[hl7:templateId/@root='1.3.6.1.4.1.48336.11.1.10.5']/hl7:component/hl7:structuredBody SHALL "
                + "contain at least ONE hl7:component[hl7:section/hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.1']", gen);
    }

    @Test
    public void testGetProcessIdentifier() {
        assertEquals(AnalyzerEnum.RD_MIN_MULTIPLICITY_PROCESS.getValue(), (new RDMinimumMultiplicityAnalyzer()).getProcessIdentifier());
    }

}
