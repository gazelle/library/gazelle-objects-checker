package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.tempgen.action.DecorAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

public class ValueSetFlexibility3IT extends DecorAnalyzer {
	
	@Before
	public void before() {
		this.currentDecor = DecorMarshaller.loadDecor("src/test/resources/valuesetflex/decor_vs_dynamic3.xml");
		this.xmi = XMIUtil.createXMI("test");
	}
	
	@Test
	public void testProcess() throws JAXBException {
		(new DecorAnalyzer()).process(currentDecor, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		String xmiStr = baos.toString();
		assertTrue(xmi.getModel().getPackagedElement().size() == 2);
		assertTrue(xmi.getModel().getPackagedElement().get(0).getOwnedRule().size() == 9);
		assertTrue(xmiStr.contains("In US Realm Header, the code of "
				+ "/cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/cda:confidentialityCode SHALL be from"
				+ " the valueSet 2.16.840.1.113883.1.11.16926"));
		assertTrue(xmiStr.contains("In US Realm Header, the code of "
				+ "/cda:ClinicalDocument[cda:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/cda:languageCode SHALL be from"
				+ " the valueSet 2.16.840.1.113883.1.11.11526"));
	}

}
