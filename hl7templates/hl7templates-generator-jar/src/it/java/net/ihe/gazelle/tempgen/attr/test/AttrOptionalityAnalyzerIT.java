package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrOptionalityAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrOptionalityAnalyzerIT {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
	}

	@Test
	public void testGetAttrName() {
		AttrOptionalityAnalyzer aoa = new AttrOptionalityAnalyzer();
		aoa.setAttrName("test");
		assertTrue(aoa.getAttrName().equals("test"));
	}

	@Test
	public void testGenerateOCLConstraint1() throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		AttrOptionalityAnalyzer aoa = new AttrOptionalityAnalyzer();
		aoa.setAttrName("code");
		String ocl = aoa.generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.code->forAll((not nullFlavor.oclIsUndefined()) or (not code.oclIsUndefined()))"));
	}
	
	@Test
	public void testGenerateOCLConstraint2() throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = new Attribute();
		attr.setName("nullFlavor");
		attr.setParentObject(code);
		code.getAttribute().add(attr);
		AttrOptionalityAnalyzer aoa = new AttrOptionalityAnalyzer();
		aoa.setAttrName("nullFlavor");
		String ocl = aoa.generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.code->forAll(not nullFlavor.oclIsUndefined())"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		AttrOptionalityAnalyzer aoa = new AttrOptionalityAnalyzer();
		aoa.setAttrName("code");
		String comment = aoa.generateCommentConstraint(attr);
		assertTrue(comment.equals("In Section Coded Social History, "
				+ "in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:code, the attribute code SHALL be present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrOptionalityAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_OPT_PROCESS.getValue()));
	}

}
