package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.tempgen.action.RootElementsAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class RootElementsAnalyzerIT extends RootElementsAnalyzer {
	
	static String OCL = "self.templateId->exists(ii : II | (not ii.root.oclIsUndefined()) and ((ii.root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2')))";
	
	@Before
	public void before() {
		this.xmi = XMIUtil.createXMI("aaa");
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_clin.xml");
		this.currentRules = dec.getRules();
		
	}

	@Test
	public void testProcess() throws JAXBException {
		(new RootElementsAnalyzer()).process(currentRules, xmi, RulesUtil.getTemplates(this.currentRules));
		assertTrue(this.xmi.getConstraintsSpec().size()==1);
		assertTrue(this.xmi.getModel().getPackagedElement().size()==1);
	}

	@Test
	public void testGenerateOwnedRuleForRoot() {
		OwnedRule aa = this.generateOwnedRuleForRoot(RulesUtil.getTemplates(this.currentRules), "aaaaa");
		assertTrue(aa.getConstrainedElement().equals("aaaaa"));
		assertTrue(aa.getSpecification().getBody().equals(OCL));
	}
	
	@Test
	public void testGenerateOCLConstraint() throws Exception {
		String aa = this.generateOCLConstraint(RulesUtil.getTemplates(this.currentRules));
		assertTrue(aa.equals(OCL));
		aa = this.generateOCLConstraint(null);
		assertTrue(aa == null || aa.equals(""));
	}
	
	@Test
	public void testGenerateCommentConstraint1() throws Exception {
		String aa = this.generateCommentConstraint(RulesUtil.getTemplates(this.currentRules));
		assertTrue(aa.equals("The CDA ClinicalDocument SHALL have a templateId equals to '1.3.6.1.4.1.19376.1.5.3.1.4.5.2'"));
		aa = this.generateCommentConstraint(null);
		assertTrue(aa == null || aa.equals(""));
	}
	
	@Test
	public void testGenerateCommentConstraint2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_clin2.xml");
		String aa = this.generateCommentConstraint(RulesUtil.getTemplates(dec.getRules()));
		assertTrue(aa.equals("The CDA ClinicalDocument SHALL have a templateId from this list of ID : "
				+ "{ 1.3.6.1.4.1.19376.1.5.3.1.4.5.2, 1.3.6.1.4.1.19376.1.5.3.1.4.5.3 }"));
	}
	
	@Test
	public void testExtractListRootTemplateDefinitions() throws Exception {
		List<TemplateDefinition> aa = extractListRootTemplateDefinitions(RulesUtil.getTemplates(this.currentRules));
		assertTrue(aa.size() == 1);
		aa = extractListRootTemplateDefinitions(null);
		assertTrue(aa == null || aa.size() == 0);
	}

}
