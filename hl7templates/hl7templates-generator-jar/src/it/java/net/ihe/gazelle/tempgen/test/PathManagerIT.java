package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.PathManager;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PathManagerIT extends PathManager {

	@Test
	public void testFindPathTypeNameDParent1() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path)).equals("EnFamily"));
	}
	
	@Test
	public void testFindPathTypeNameDParentInt1() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 0).equals("POCDMT000040Author"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 1).equals("POCDMT000040AssignedAuthor"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 2).equals("POCDMT000040Person"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 3).equals("PN"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 4).equals("EnFamily"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 5).equals("EnFamily"));
	}
	
	@Test
	public void testFindPathTypeNameDParentInt2() {
		String path = "//hl7:observation/hl7:effectiveTime/hl7:low";
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 0).equals("POCDMT000040Observation"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 1).equals("IVLTS"));
		assertTrue((new PathManager()).findPathTypeName(DPathExtractor.extractDElementFromDPath(path), 2).equals("IVXBTS"));
	}
	
	@Test
	public void testExtractNameFromPointer1() throws Exception {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		DParent par = DPathExtractor.extractDElementFromDPath(path);
		String name = this.extractNameFromPointer(0, par);
		assertTrue(name.equals("POCDMT000040Author"));
		name = this.extractNameFromPointer(1, par);
		assertTrue(name.equals("author"));
		DElement aa = (DElement)(((DElement)par).getFollowingAttributeOrElement());
		aa.setExtendedType("AAAA");
		name = this.extractNameFromPointer(1, aa);
		assertTrue(name.equals("assignedAuthor"));
		name = this.extractNameFromPointer(0, aa);
		assertTrue(name.equals("AAAA"));
	}

}
