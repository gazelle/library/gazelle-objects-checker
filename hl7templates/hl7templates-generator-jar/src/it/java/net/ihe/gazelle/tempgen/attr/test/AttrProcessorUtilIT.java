package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrProcessorUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class AttrProcessorUtilIT {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
	}

	@Test
	public void testGenerateCommentConstraintAttributeStringString() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		String comm = AttrProcessorUtil.generateCommentConstraint(attr, "code", "test");
		assertTrue(comm.equals("In Section Coded Social History, in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:code, "
				+ "the attribute code SHALL have the value 'test' if present"));
	}

	@Test
	public void testGenerateCommentConstraintAttributeStringBoolean() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		String comm = AttrProcessorUtil.generateCommentConstraint(attr, "code", true);
		assertTrue(comm.equals("In Section Coded Social History, in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:code, "
				+ "the attribute code SHALL have the value 'true' if present"));
	}

	@Test
	public void testGenerateOCLConstraintForEnumerationAttribute() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Attribute attr = firstRD.getAttribute().get(1);
		String comm = AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attr, "contextControlCode", "OP", null);
		assertEquals(comm, "self.contextControlCode.oclIsUndefined() or self.contextControlCode=NullFlavor::OP");
	}

	@Test
	public void testGenerateOCLConstraintFromBooleanOrIntegerAttribute() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/attr/decor_attr1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
		Attribute attr = entr.getAttribute().get(0);
		String comm = AttrProcessorUtil.generateOCLConstraintFromBooleanOrIntegerAttribute(attr, "contextConductionInd", "false");
		assertEquals(comm, "self.entry->forAll(contextConductionInd.oclIsUndefined() or contextConductionInd=false)");
	}

	@Test
	public void testGenerateOCLConstraintFromStringAttribute() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		String ocl = AttrProcessorUtil.generateOCLConstraintFromStringAttribute(attr, "code", "AA");
		assertTrue(ocl.equals("self.code->forAll(code.oclIsUndefined() or code='AA')"));
	}

	@Test
	public void testGenerateOCLConstraintFromNameAndValue1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		String ocl = AttrProcessorUtil.generateOCLConstraintFromNameAndValue(attr, "code", "AA", null);
		assertTrue(ocl.equals("self.code->forAll(code.oclIsUndefined() or code='AA')"));
	}
	
	@Test
	public void testGenerateOCLConstraintFromNameAndValue2() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/attr/decor_attr1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
		Attribute attr = entr.getAttribute().get(0);
		String comm = AttrProcessorUtil.generateOCLConstraintFromNameAndValue(attr, "contextConductionInd", "false", null);
		assertEquals(comm, "self.entry->forAll(contextConductionInd.oclIsUndefined() or contextConductionInd=false)");
	}

}
