package net.ihe.gazelle.tempgen.RDprocessors.test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.tempgen.action.RuleDefinitionAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class MultiplicityProcessorsIT extends RuleDefinitionAnalyzer {




    /**
     * Test with the min 0 and conformance R, expected to have a size()>1 but just a warning
     */
    @Covers(requirements = {"GOC-001","GOC-028"}, testType = TestType.INTEGRATION_TEST, comment = "RuleDefintions Coverage")
    @Test
    public void testProcessMinimumMultiplicity_0_R(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_multiplicity_0_n_R.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedRuleDefinition = entry;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMinimumMultiplicity(entry.getMinimumMultiplicity());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.entryRelationship" +
                "->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) " +
                "and observation.code.code='48766-0')->size()>0",
                ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.CARDINALITY,ownedRule.getOwnedRuleKind());
        assertEquals(OwnedRuleType.WARNING,ownedRule.getOwnedRuleType());

    }

    /**
     * Test with the min 1 (or >) and conformance R, expected to have a size()>1 with an a error
     */
    @Covers(requirements = {"GOC-009","GOC-028"}, testType = TestType.INTEGRATION_TEST, comment = "RuleDefintions Coverage")
    @Test
    public void testProcessMinimumMultiplicity_1_R(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_multiplicity_1_n_R.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedRuleDefinition = entry;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMinimumMultiplicity(entry.getMinimumMultiplicity());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.entryRelationship" +
                        "->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) " +
                        "and observation.code.code='48766-0')->size()>0",
                ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.CARDINALITY,ownedRule.getOwnedRuleKind());
        assertEquals(OwnedRuleType.ERROR,ownedRule.getOwnedRuleType());

    }

//    @Covers(requirements = {"GOC-009"}, testType = TestType.INTEGRATION_TEST)

    /**
     * The NP and C conformances are not processed by goc: GOC-010, GOC-011
     */
    @Ignore
    @Test
    public void testProcessMinimumMultiplicity_NP(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_multiplicity_1_n_R.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedRuleDefinition = entry;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMinimumMultiplicity(entry.getMinimumMultiplicity());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.entryRelationship" +
                        "->select((not observation.code.code.oclIsUndefined()) " +
                        "and observation.code.code='48766-0')->size()>0",
                ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.CARDINALITY,ownedRule.getOwnedRuleKind());
        assertEquals(OwnedRuleType.ERROR,ownedRule.getOwnedRuleType());

    }

    @Covers(requirements = {"GOC-004"}, testType = TestType.INTEGRATION_TEST, comment = "RuleDefintions Coverage")
    @Test
    public void testProcessMaximumMultiplicity_n(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_multiplicity_0_n_R.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedRuleDefinition = entry;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMaximumMultiplicity(entry.getMaximumMultiplicity());
        assertEquals(0,this.currentPackagedElement.getOwnedRule().size());

    }

    @Covers(requirements = {"GOC-003","GOC-026"}, testType = TestType.INTEGRATION_TEST, comment = "RuleDefintions Coverage")
    @Test
    public void testProcessMaximumMultiplicity_2(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_multiplicity_0_2_R.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        this.selectedRuleDefinition = entry;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMaximumMultiplicity(entry.getMaximumMultiplicity());
        assertEquals(1,this.currentPackagedElement.getOwnedRule().size());
        OwnedRule ownedRule = this.currentPackagedElement.getOwnedRule().get(0);
        assertEquals("self.entryRelationship->select((not (observation.oclIsUndefined() or observation.code.oclIsUndefined() or observation.code.code.oclIsUndefined())) " +
                "and observation.code.code='48766-0')->size()<3",ownedRule.getSpecification().getBody());
        assertEquals(OwnedRuleKind.CARDINALITY,ownedRule.getOwnedRuleKind());

    }






}
