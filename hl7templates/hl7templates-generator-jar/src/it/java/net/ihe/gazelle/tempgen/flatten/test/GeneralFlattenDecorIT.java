package net.ihe.gazelle.tempgen.flatten.test;

import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenDecor;
import net.ihe.gazelle.tempgen.flatten.action.RulesCleaner;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class GeneralFlattenDecorIT {

    Decor decorTemplates = null;
    Decor decorTemplatesBis = null;
    Decor decorTemplatesSequoia0 = null;
    Decor decorTemplatesSequoia1 = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain.xml");
        decorTemplatesBis = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_bis.xml");
        decorTemplatesSequoia0 = DecorMarshaller.loadDecor("src/test/resources/contain/flattenContainSequoia0.xml");
        decorTemplatesSequoia1 = DecorMarshaller.loadDecor("src/test/resources/contain/flattenContainSequoia1.xml");
    }

    @Test
    public void testGeneralFlattenDecor1() throws JAXBException {
        Decor res = GeneralFlattenDecor.generalFlattenDecor(decorTemplates);
        String strContent = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        strContent = baos.toString();
        assertFalse(strContent.contains("contains"));
        assertFalse(strContent.contains("<include"));
        assertEquals(3, StringUtils.countMatches(strContent, "<contain ref=\"1.3.6.1.4.1.19376.1.5.3.1.4.13.4\""));
    }

    @Test
    public void testGeneralFlattenDecor1Sequoia0() throws JAXBException {
        RulesCleaner.cleanRules(decorTemplatesSequoia0.getRules());
        Decor res = GeneralFlattenDecor.generalFlattenDecor(decorTemplatesSequoia0);
        String strContent = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        strContent = baos.toString();
        assertFalse(strContent.contains("contains="));
        assertFalse(strContent.contains("<include"));
        assertEquals(1, StringUtils.countMatches(strContent, "<contain ref=\"2.16.840.1.113883.10.20.22.4.61\""));
    }

    @Test
    public void testGeneralFlattenBisDecorSequoia1() throws JAXBException {
        Decor res = GeneralFlattenDecor.generalFlattenBisDecor(decorTemplatesSequoia1);
        String strContent = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        strContent = baos.toString();
        assertTrue(strContent.contains(
                "<contain ref=\"2.16.840.1.113883.10.20.22.4.61\" isMandatory=\"true\" minimumMultiplicity=\"1\" maximumMultiplicity=\"*\" " +
                        "contain=\"true\">"));
    }

    /**
     * Flatten contains from parent BBR
     *
     * @throws JAXBException
     * @throws FileNotFoundException
     */
    @Test
    @Ignore("This test is not working, depends on decor content)")
    public void testGeneralFlattenDecor5() throws JAXBException, FileNotFoundException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contains_bbr.xml");
        Decor res = GeneralFlattenDecor.generalFlattenDecor(dec);
        String strContent = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        strContent = baos.toString();
        assertFalse(strContent.contains("contains"));
        assertTrue(strContent.contains("<contain ref=\"2.16.840.1.113883.10.20.21.2.1\" minimumMultiplicity=\"0\" maximumMultiplicity=\"1\" contain" +
                "=\"true\"/>"));
    }

    @Test
    public void testGeneralFlattenBisDecor() throws JAXBException {
        Decor res = GeneralFlattenDecor.generalFlattenBisDecor(decorTemplatesBis);
        String strContent = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        strContent = baos.toString();
        assertTrue(strContent.contains(
                "<contain ref=\"1.3.6.1.4.1.19376.1.5.3.1.4.13.4\" minimumMultiplicity=\"1\" maximumMultiplicity=\"*\" contain=\"true\">"));
        assertEquals(4, StringUtils.countMatches(strContent, "1.3.6.1.4.1.19376.1.5.3.1.4.13.4"));
    }

    /**
     * test for flattening names
     *
     * @throws JAXBException
     * @throws FileNotFoundException
     */
    @Test
    public void testGeneralFlattenDecor2() throws JAXBException, FileNotFoundException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_name1.xml");
        Decor res = GeneralFlattenDecor.generalFlattenDecor(dec);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        String decs = baos.toString();
        assertFalse(decs.contains("hl7:entryRelationship[@typeCode='RSON']"));
    }

    /**
     * test for flattening names
     * DROPPED: Ignored due to the ignore of old name flatenning
     * @throws JAXBException
     * @throws FileNotFoundException
     */
    @Deprecated
    @Ignore
    @Test
    public void testGeneralFlattenDecor3() throws JAXBException, FileNotFoundException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_name2.xml");
        Decor res = GeneralFlattenDecor.generalFlattenDecor(dec);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        String decs = baos.toString();
        assertTrue(decs.contains("<attribute name=\"typeCode\" value=\"COMP\" isOptional=\"true\"/>"));
    }

    /**
     * test for flattening names
     * DROPPED: Ignored due to the ignore of old name flatenning
     * @throws JAXBException
     * @throws FileNotFoundException
     */
    @Deprecated
    @Ignore
    @Test
    public void testGeneralFlattenDecor4() throws JAXBException, FileNotFoundException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_name2.xml");
        Decor res = GeneralFlattenDecor.generalFlattenDecor(dec);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(res, baos);
        String decs = baos.toString();
        assertTrue(decs.contains("hl7:entryRelationship[aaaaaaa and []]"));
    }

    @Test
    public void testGeneralFlattenDecor() throws Exception {
        assertNull(GeneralFlattenDecor.generalFlattenDecor(null));
    }

    @Test
    public void testGeneralFlattenBisDecor2() throws Exception {
        assertNull(GeneralFlattenDecor.generalFlattenBisDecor(null));
    }

}
