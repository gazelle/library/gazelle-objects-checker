package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainMandatoryAnalyzerIT {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter_mandatory.xml");
    }

    @Test
    public void generateOCLConstraint() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");

        String gen = (new ContaintMandatoryAnalyzer()).generateOCLConstraint(entr);
        String expectedConstraint = "self.entry.observation.templateId->select(" +
                "(not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'" +
                ")->forAll(nullFlavor.oclIsUndefined())";

        assertEquals(expectedConstraint, gen);
    }

    @Test
    public void generateOCLConstraint2() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter_mandatory2.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entryRelationship");

        String gen = (new ContaintMandatoryAnalyzer()).generateOCLConstraint(entr);
        String expectedConstraint = "self.entryRelationship.act.templateId->select((not (root.oclIsUndefined())) and root='2.16.840.1.113883.10.20.22" +
                ".4.61')->forAll(nullFlavor.oclIsUndefined())";

        assertEquals(expectedConstraint, gen);
    }

    @Test
    public void generateCommentConstraint() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        String gen = (new ContaintMandatoryAnalyzer()).generateCommentConstraint(entr);
        assertEquals("In Section Coded Social History, in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16" +
                ".1']/hl7:entry/hl7:observation/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'], the element(s) hl7:templateId[@root='1.3" +
                ".6.1.4.1.19376.1.5.3.1.4.13.4'] SHALL not have nullFlavor (mandatory)", gen);
    }

}