package net.ihe.gazelle.tempgen.RDprocessors.test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.goctests.definitions.annotations.Covers;
import net.ihe.gazelle.goctests.definitions.annotations.TestType;
import net.ihe.gazelle.tempgen.action.RuleDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import net.ihe.gazelle.validation.Notification;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

public class ItemsProcessorIT extends RuleDefinitionAnalyzer {

    @Ignore
    @Test
    public void print(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_vocab_VS.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        RuleDefinition observation = RuleDefinitionUtil.getElementByName(code,"hl7:observation");
        this.selectedRuleDefinition = code;
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processVocabularys(code.getVocabulary());


        for(OwnedRule ownedRule : this.currentPackagedElement.getOwnedRule()){
            System.out.println(ownedRule.getSpecification().getBody());
            System.out.println(ownedRule.getOwnedRuleKind().getValue());
            System.out.println(ownedRule.getOwnedRuleType().getValue());
        }
        for(Notification notification : ProblemHandler.getDiagnostic()){
            System.out.println(notification.getDescription());
        }
    }

    @Covers(requirements = {"GOC-021"}, testType = TestType.INTEGRATION_TEST, comment = "RuleDefinitions Coverage")
    @Test
    public void testProcessItem(){
        Decor decor = DecorMarshaller.loadDecor("src/test/resources/processors/decor_item.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decor.getRules()).get(0));
        RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
        RuleDefinitionAnalyzer rda = new RuleDefinitionAnalyzer();
        PackagedElement pe = PackagedElementUtil.initPackagedElement();
        rda.process(value,pe);
        assertEquals(1,pe.getOwnedRule().size());
        assertTrue(pe.getOwnedRule().get(0).getOwnedComment().getBody().contains("(Item : CONF-test)"));
    }
}
