package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.GenerationProperties;
import net.ihe.gazelle.tempgen.action.HL7TemplatesConverter;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.NegativeDistinguisherList;

public class HL7TemplatesConverterIT {

	@Test
	public void testConvertArtDecorXMI1() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		NegativeDistinguisherList.setTreatNegativeDistinguisher(false);
		GenerationProperties.ignoreAssertionGeneration = true;
		String res = conv.convertArtDecorXMI("src/test/resources/decor_disting5.xml");
		NegativeDistinguisherList.setTreatNegativeDistinguisher(true);
		assertTrue(res.contains("<body>self.entry-&gt;select((not (typeCode.oclIsUndefined())) and typeCode=XActRelationshipEntry::COMP).act-&gt;"
				+ "forAll(nullFlavor.oclIsUndefined())</body>"));
		assertFalse(res.contains(">self.entry-&gt;select(templateId.root-&gt;exists(aa : String| aa = '1.2.3.4'))"));
		assertTrue(res.contains("<body>self.entry-&gt;select(observation.templateId.root-&gt;exists(aa : String| aa = '5.5.5'))"));
		assertFalse(res.contains("organizer"));
		assertTrue(res.contains("body>In Section Coded Social History, in "
				+ "/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry[@typeCode='COMP'], "
				+ "the element(s) hl7:act SHALL not have nullFlavor (mandatory)</body>"));
		assertFalse(res.contains("in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry[hl7:templateId/@root='1.2.3.4']"));
	}
	
	@Test
	public void testConvertArtDecorXMI2() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		GenerationProperties.ignoreAssertionGeneration = true;
		NegativeDistinguisherList.setTreatNegativeDistinguisher(true);
		String res = conv.convertArtDecorXMI("src/test/resources/decor_disting6.xml");
		assertTrue(res.contains("<body>self.templateId-&gt;select((not ((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1')))-&gt;"
				+ "forAll((not nullFlavor.oclIsUndefined()) or (not extension.oclIsUndefined()))</body>"));
		assertTrue(res.contains("<body>self.templateId-&gt;select((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1')-&gt;"
				+ "forAll(extension.oclIsUndefined() or extension='test1')</body>"));
	}
	
	@Test
	public void testConvertArtDecorXMI3() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		GenerationProperties.ignoreAssertionGeneration = true;
		String res = conv.convertArtDecorXMI("src/test/resources/decor_disting7.xml");
		assertTrue(res.contains("body>self.templateId-&gt;select((not ((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1')) "
				+ "and (not ((not (root.oclIsUndefined())) and root='1.2.3')))-&gt;forAll((not nullFlavor.oclIsUndefined()) or (not extension.oclIsUndefined()))</body>"));
		assertTrue(res.contains("<body>self.templateId-&gt;select((not (root.oclIsUndefined())) and root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1')-&gt;"
				+ "forAll(extension.oclIsUndefined() or extension='test1')</body>"));
	}
	
	@Test
	public void testConvertArtDecorXMI4() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		GenerationProperties.ignoreAssertionGeneration = true;
		String res = conv.convertArtDecorXMI("src/test/resources/decor_disting5.xml");
		assertTrue(res.contains("organizer"));
	}
	
	@Test
	public void testConvertArtDecorXMI5() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		GenerationProperties.ignoreAssertionGeneration = true;
		String res = conv.convertArtDecorXMI("src/test/resources/decor_dist_ignore.xml");
		assertFalse(res.toLowerCase().contains("owned"));
	}
	
	@Test
	public void testConvertArtDecorXMI6() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/decor_labeled3.xml", false, "1.0");
		assertTrue(res.contains("29762-2"));
		assertFalse(res.contains("entry"));
		assertTrue(res.contains("1.2.3.4.5"));
	}
	
	@Test
	public void testConvertArtDecorXMI7() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/decor_labeled3.xml", false, "1.1");
		assertFalse(res.contains("29762-2"));
		assertTrue(res.contains("entry"));
		assertTrue(res.contains("1.2.3.4.5"));
	}
	
	@Test
	public void testConvertArtDecorXMI8() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/decor_labeled3.xml", false, null);
		assertFalse(res.contains("29762-2"));
		assertTrue(res.contains("entry"));
		assertTrue(res.contains("1.2.3.4.5"));
	}

	@Test
	public void testConvertArtDecorXMITrueIgnoreTemplateRequirementForContains() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/contain/decor_contain_bis.xml", true, null);
		assertFalse(res.contains("SHALL contain at least ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertFalse(res.contains("SHALL contain at most ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
	}

	@Test
	public void testConvertArtDecorXMIFalseIgnoreTemplateRequirementForContains() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/contain/decor_contain_bis.xml", false, null);
		assertTrue(res.contains("SHALL contain at least ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertTrue(res.contains("SHALL contain at most ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
	}

	@Test
	public void testConvertArtDecorXMIFalseIgnoreTemplateRequirementForContainsMandatory() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/contain/decor_contain_ter_mandatory.xml", false, null);
		assertTrue(res.contains("SHALL contain at least ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertTrue(res.contains("SHALL contain at most ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertTrue(res.contains("the element(s) hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] SHALL not have nullFlavor (mandatory)"));
	}

	@Test
	public void testConvertArtDecorXMITrueIgnoreTemplateRequirementForContainsMandatory() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/contain/decor_contain_bis.xml", true, null);
		assertFalse(res.contains("SHALL contain at least ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertFalse(res.contains("SHALL contain at most ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
	}

	@Test
	public void testConvertArtDecorXMIFalseIgnoreTemplateRequirementForContainsInclude() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/contain/decor_contain.xml", false, null);
		assertTrue(res.contains("SHALL contain at least ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertTrue(res.contains("SHALL contain at most ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
	}

	@Test
	public void testConvertArtDecorXMITrueIgnoreTemplateRequirementForContainsInclude() throws FileNotFoundException, JAXBException {
		HL7TemplatesConverter conv = new HL7TemplatesConverter();
		String res = conv.convertArtDecorXMI("src/test/resources/contain/decor_contain_bis.xml", true, null);
		assertFalse(res.contains("SHALL contain at least ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
		assertFalse(res.contains("SHALL contain at most ONE hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']"));
	}
}
