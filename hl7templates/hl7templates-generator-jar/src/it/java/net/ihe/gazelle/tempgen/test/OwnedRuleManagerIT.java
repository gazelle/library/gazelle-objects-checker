package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempmodel.distinguisher.CDAElementsDistinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.CDAType;
import net.ihe.gazelle.tempmodel.distinguisher.Distinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class OwnedRuleManagerIT extends OwnedRuleManager {
	
	@Test
	public void testConvertMultiplicity() throws Exception {
		String res = OwnedRuleManager.convertMultiplicity(1);
		assertTrue(res.equals("ONE"));
		res = OwnedRuleManager.convertMultiplicity(0);
		assertTrue(res.equals("ZERO"));
		res = OwnedRuleManager.convertMultiplicity(10);
		assertTrue(res.equals("10"));
	}
	
	@Test
	public void testConvertMultiplicityString() throws Exception {
		String res = OwnedRuleManager.convertMultiplicity("1");
		assertTrue(res.equals("ONE"));
		res = OwnedRuleManager.convertMultiplicity("0");
		assertTrue(res.equals("ZERO"));
		res = OwnedRuleManager.convertMultiplicity("10");
		assertTrue(res.equals("10"));
	}
	
	@Test
	public void testGetDistinguisherCommentIfNeeded1() throws Exception {
		assertTrue(OwnedRuleManager.getDistinguisherCommentIfNeeded(null).equals(""));
		assertTrue(OwnedRuleManager.getDistinguisherCommentIfNeeded(new RuleDefinition()).equals(""));
	}
	
	@Test
	public void testGetDistinguisherCommentIfNeeded2() throws Exception {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition status = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
		assertTrue(OwnedRuleManager.getDistinguisherCommentIfNeeded(firstRD).equals("[/hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
		assertTrue(OwnedRuleManager.getDistinguisherCommentIfNeeded(entr).equals("[/hl7:observation/hl7:code/@code='48766-0']"));
		assertTrue(OwnedRuleManager.getDistinguisherCommentIfNeeded(obs).equals("[/hl7:code/@code='48766-0']"));
		assertTrue(OwnedRuleManager.getDistinguisherCommentIfNeeded(status).equals(""));
	}
	
	@Test
	public void testGetExtraDistinguisherCommentByDistinguisherForRuleDefinition1() throws Exception {
		assertTrue(OwnedRuleManager.getExtraDistinguisherCommentByDistinguisherForRuleDefinition(null, null) == null);
		assertTrue(OwnedRuleManager.getExtraDistinguisherCommentByDistinguisherForRuleDefinition(new RuleDefinition(), null) == null);
	}
	
	@Test
	public void testGetExtraDistinguisherCommentByDistinguisherForRuleDefinition2() throws Exception {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
		RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
		RuleDefinition code = RuleDefinitionUtil.getElementByName(obs, "hl7:code");
		CDAElementsDistinguisher dd = new CDAElementsDistinguisher();
		dd.getElementDiscriber().add(new ElementDiscriber());
		dd.getElementDiscriber().get(0).setIdentifier("hl7:act");
		dd.getElementDiscriber().get(0).setName("hl7:act");
		dd.getElementDiscriber().get(0).setType(CDAType.POCDMT_000040_ACT);
		dd.getElementDiscriber().get(0).getDistinguisher().add(new Distinguisher());
		dd.getElementDiscriber().get(0).getDistinguisher().get(0).setXpath("/hl7:templateId/@root");
		String aa = OwnedRuleManager.getExtraDistinguisherCommentByDistinguisherForRuleDefinition(firstRD, 
				dd.getElementDiscriber().get(0).getDistinguisher().get(0));
		assertTrue(aa.equals("/hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2'"));
		dd.getElementDiscriber().get(0).getDistinguisher().get(0).setXpath("/hl7:aaaa");
		aa = OwnedRuleManager.getExtraDistinguisherCommentByDistinguisherForRuleDefinition(firstRD, 
				dd.getElementDiscriber().get(0).getDistinguisher().get(0));
		assertNull(aa);
		dd.getElementDiscriber().get(0).getDistinguisher().get(0).setXpath("/hl7:entryRelationship/@typeCode");
		aa = OwnedRuleManager.getExtraDistinguisherCommentByDistinguisherForRuleDefinition(firstRD, 
				dd.getElementDiscriber().get(0).getDistinguisher().get(0));
		assertTrue(aa.equals("/hl7:entryRelationship/@typeCode='REFR'"));
		dd.getElementDiscriber().get(0).getDistinguisher().get(0).setXpath("/@code");
		aa = OwnedRuleManager.getExtraDistinguisherCommentByDistinguisherForRuleDefinition(code, 
				dd.getElementDiscriber().get(0).getDistinguisher().get(0));
		assertTrue(aa.equals("/@code='48766-0'"));
	}
	
}
