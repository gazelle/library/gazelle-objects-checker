package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.ItemCommentGenerator;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ItemCommentGeneratorTest {

	@Test
	public void testGenerateItemComment1() {
		RuleDefinition rd = new RuleDefinition();
		String comment = ItemCommentGenerator.generateItemComment(rd);
		assertTrue(comment.equals(""));
		rd.setItem(new Item());
		comment = ItemCommentGenerator.generateItemComment(rd);
		assertTrue(comment.equals(""));
		rd.getItem().setLabel("test");
		comment = ItemCommentGenerator.generateItemComment(rd);
		assertTrue(comment.equals(" (Item : test)"));
	}
	
	@Test
	public void testGenerateItemComment2() {
		String comment = ItemCommentGenerator.generateItemComment(null);
		assertTrue(comment.equals(""));
	}

}
