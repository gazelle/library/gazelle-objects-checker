package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import gnu.trove.map.hash.THashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.checker.IncCheckTemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncCheckTemplateDefinitionTest extends IncCheckTemplateDefinition {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_td.xml");
	}

	@Test
	public void testProcess() {
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		Map<String, Set<String>> mapIncludedTemplates = new THashMap<String, Set<String>>();
		this.process(td, mapIncludedTemplates);
		assertTrue(mapIncludedTemplates.size()>0);
	}

}
