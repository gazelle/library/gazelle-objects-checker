package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.ValueSetResponseType;
import net.ihe.gazelle.tempgen.valueset.action.ValueSetConceptListProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptListProcTest {

	@Test
	public void testProcess() {
		RetrieveValueSetResponseType rvsr = new RetrieveValueSetResponseType();
		rvsr.setValueSet(new ValueSetResponseType());
		ValueSetConceptList currentValueSetConceptList = new ValueSetConceptList();
		ValueSetConcept concept = new ValueSetConcept();
		currentValueSetConceptList.getConceptOrInclude().add(concept);
		(new ValueSetConceptListProc()).process(currentValueSetConceptList, new Object[]{rvsr});
		assertTrue(rvsr.getValueSet().getConceptList().size()>0);
	}

}
