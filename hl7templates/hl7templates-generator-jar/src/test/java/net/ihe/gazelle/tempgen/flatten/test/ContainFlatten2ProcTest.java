package net.ihe.gazelle.tempgen.flatten.test;

import net.ihe.gazelle.tempgen.flatten.action.ContainFlatten2Proc;
import net.ihe.gazelle.tempgen.flatten.action.IncludeDefinitionFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class ContainFlatten2ProcTest {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_bis.xml");
    }

    @Test
    public void testProcess() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        ContainDefinition containDefinition = RuleDefinitionUtil.getContains(entry).get(0);
        (new ContainFlatten2Proc()).process(containDefinition);
        assertTrue(entry.getLetOrAssertOrReport().contains(containDefinition));
        assertTrue(RuleDefinitionUtil.getContains(entry).size() == 1);
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(entry);
        RuleDefinition rdobs = ContainDefinitionUtil.getElementByName(contains.get(0), "hl7:observation");
        RuleDefinition templateId = RuleDefinitionUtil.getElementByName(rdobs, "hl7:templateId");
        assertTrue(templateId != null);
        assertTrue(templateId.getAttribute().get(0).getValue().equals("1.3.6.1.4.1.19376.1.5.3.1.4.13.4"));
        ContainDefinition res = (ContainDefinition) rdobs.getParentObject();
        assertEquals(res.getMinimumMultiplicity(), containDefinition.getMinimumMultiplicity());
        assertEquals(res.getMaximumMultiplicity(), containDefinition.getMaximumMultiplicity());

    }

    @Test
    public void testProcessContainInclude() {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(
                DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_contain_false.xml").getRules()).get(0));
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        ContainDefinition containDefinition = RuleDefinitionUtil.getContains(entry).get(0);
        assertEquals(1, RuleDefinitionUtil.getContains(entry).size());
        (new ContainFlatten2Proc()).process(containDefinition);
        assertFalse(entry.getLetOrAssertOrReport().contains(containDefinition));

        assertEquals(1, containDefinition.getMinimumMultiplicity().intValue());
        assertEquals("*", containDefinition.getMaximumMultiplicity());
        assertEquals(true, containDefinition.getIsMandatory());
        assertEquals(false, containDefinition.getContain());

        RuleDefinition rd = (RuleDefinition) entry.getLetOrAssertOrReport().get(0);
        assertEquals("hl7:observation", rd.getName());
        assertEquals("*", rd.getMaximumMultiplicity());
        assertEquals(1, rd.getMinimumMultiplicity().intValue());
        assertEquals(true, rd.getIsMandatory());

        assertEquals(0, RuleDefinitionUtil.getContains(entry).size());

    }

}
