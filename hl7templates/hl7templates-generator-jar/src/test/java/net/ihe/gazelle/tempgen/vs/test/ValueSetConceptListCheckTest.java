package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import gnu.trove.map.hash.THashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import net.ihe.gazelle.tempgen.valueset.checker.ValueSetConceptListCheck;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptListCheckTest extends ValueSetConceptListCheck {

	@Test
	public void testProcess1() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		ValueSetConceptList vscl = decor.getTerminology().getValueSet().get(0).getConceptList();
		Map<String, Set<String>> ref = new THashMap<String, Set<String>>();
		(new ValueSetConceptListCheck()).process(vscl, ref);
		assertTrue(ref.size()==0);
	}
	@Test
	public void testProcess2() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies2.xml");
		ValueSetConceptList vscl = decor.getTerminology().getValueSet().get(2).getConceptList();
		Map<String, Set<String>> ref = new THashMap<String, Set<String>>();
		(new ValueSetConceptListCheck()).process(vscl, ref);
		assertTrue(ref.size()>0);
		assertTrue(ref.keySet().contains("2.3.5"));
		
	}

	@Test
	public void testProcessIncludes() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies2.xml");
		this.currentValueSetConceptList = decor.getTerminology().getValueSet().get(1).getConceptList();
		this.vsReferences = new THashMap<String, Set<String>>();
		List<ValueSetRef> includes = new ArrayList<ValueSetRef>();
		this.processIncludes(includes);
		assertTrue(this.vsReferences.size()==0);
		includes.add(new ValueSetRef());
		includes.get(0).setRef("2.16.840.1.113883.1.11.12212");
		this.processIncludes(includes);
		assertTrue(this.vsReferences.size()>0);
		assertTrue(this.vsReferences.keySet().contains("2.3.4"));
	}

}
