package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrQualifierAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

public class AttrQualifierAnalyzerTest {

	@Test
	public void testGenerateOCLConstraint() {
		Attribute attr = new Attribute();
		attr.setQualifier("AA");
		String ocl = (new AttrQualifierAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("null.qualifier->forAll(aa | aa='AA')"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		Attribute attr = new Attribute();
		attr.setQualifier("AA");
		String comment = (new AttrQualifierAnalyzer()).generateCommentConstraint(attr);
		assertTrue(comment.equals("In --, in , the attribute qualifier SHALL have the value 'AA' if present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue((new AttrQualifierAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_QUALIFIER_PROCESS.getValue())));
	}
}
