package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;

import org.junit.Test;

import net.ihe.gazelle.tempgen.gui.view.UserQuestionerView;

public class UserQuestionerViewTest extends UserQuestionerView {

	@Test
	public void testTreatResp() {
		assertTrue(treatResp("y"));
		assertTrue(treatResp("y "));
		assertTrue(treatResp(" y"));
		assertTrue(treatResp(" y  "));
		assertTrue(treatResp(""));
		assertTrue(treatResp(null));
		assertFalse(treatResp("yy"));
		assertFalse(treatResp("n"));
		assertFalse(treatResp("nnn"));
		assertFalse(treatResp("n "));
	}
	
	@Test
	public void testGetAcceptanceFromUserForAnAction() throws Exception {
		System.setIn(new ByteArrayInputStream("y".getBytes()));
		assertTrue(UserQuestionerView.getAcceptanceFromUserForAnAction("ok?"));
		System.setIn(new ByteArrayInputStream("n".getBytes()));
		assertFalse(UserQuestionerView.getAcceptanceFromUserForAnAction("ok?"));
		System.setIn(new ByteArrayInputStream("aaa".getBytes()));
		assertFalse(UserQuestionerView.getAcceptanceFromUserForAnAction("ok?"));
		
	}

}
