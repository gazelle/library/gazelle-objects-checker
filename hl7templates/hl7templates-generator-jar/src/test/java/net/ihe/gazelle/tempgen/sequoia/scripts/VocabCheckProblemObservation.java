package net.ihe.gazelle.tempgen.sequoia.scripts;

import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.impl.ValueSetConceptListProcessorImpl;
import net.ihe.gazelle.tempapi.impl.VocabularyProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetRefUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VocabCheckProblemObservation extends VocabularyProcessorImpl {
	
	Vocabulary vocab;
	
	@Override
	public void process(Vocabulary t, Object... objects) {
		this.vocab = t;
		super.process(t, objects);
	}
	
	@Override
	public void processFlexibility(String flexibility) {
		if (this.vocab != null && this.vocab.getValueSet() != null && this.vocab.getValueSet().equals("2.16.840.1.113883.3.88.12.3221.7.4") && 
				this.vocab.getParentObject() instanceof RuleDefinition) {
			RuleDefinition rd = (RuleDefinition)this.vocab.getParentObject();
			TemplateDefinition td = RuleDefinitionUtil.getParentTemplateDefinition(rd);
			if (td.getId().equals("2.16.840.1.113883.10.20.22.4.4")) {
				this.vocab.setFlexibility(null);
			}
		}
	}
	
}
