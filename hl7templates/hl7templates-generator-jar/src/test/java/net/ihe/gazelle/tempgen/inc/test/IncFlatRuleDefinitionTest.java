package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.action.IncFlatRuleDefinition;
import net.ihe.gazelle.tempgen.inc.action.IncludeStats;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncFlatRuleDefinitionTest extends IncFlatRuleDefinition {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_rule.xml");
	}

	@Test
	public void testProcess() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorTemplates.getRules());
		List<TemplateDefinition> listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		IncludeStats incStats = new IncludeStats();
		this.process(firstRD, null, listCDATemplate, incStats);
		assertTrue(RuleDefinitionUtil.getIncludes(firstRD) == null || RuleDefinitionUtil.getIncludes(firstRD).size()==0);
	}

	@Test
	public void testProcessIncludes() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.incStats = new IncludeStats();
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorTemplates.getRules());
		this.listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		assertTrue(RuleDefinitionUtil.getElements(firstRD).size()==0);
		this.currentRuleDefinition = firstRD;
		this.processIncludes(RuleDefinitionUtil.getIncludes(firstRD));
		assertTrue(incStats.getNumberIntegrated() == 1);
		assertTrue(RuleDefinitionUtil.getElements(firstRD).size()==1);
	}

}
