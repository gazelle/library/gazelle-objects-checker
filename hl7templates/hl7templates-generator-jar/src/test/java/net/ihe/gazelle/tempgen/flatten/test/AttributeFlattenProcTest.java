package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import gnu.trove.map.hash.THashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.flatten.AttributeFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class AttributeFlattenProcTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
	}

	@Test
	public void testProcess() throws FileNotFoundException, JAXBException {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setRoot("eeeee");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		attr.setExtension("zz");
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==2);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().matches("root|extension"));
	}

	@Test
	public void testProcessClassCode() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setClassCode("eeeee");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("classCode"));
	}

	@Test
	public void testProcessContextConductionInd() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setContextConductionInd(true);
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("contextConductionInd"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("true"));
	}

	@Test
	public void testProcessContextControlCode() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setContextControlCode("aa");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("contextControlCode"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("aa"));
	}

	@Test
	public void testProcessDeterminerCode() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setDeterminerCode("cc");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("determinerCode"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("cc"));
	}

	@Test
	public void testProcessExtension() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setExtension("ss");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("extension"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("ss"));
	}

	@Test
	public void testProcessIndependentInd() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setIndependentInd(false);
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("independentInd"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("false"));
	}

	@Test
	public void testProcessInstitutionSpecified() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setInstitutionSpecified(true);
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("institutionSpecified"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("true"));
	}

	@Test
	public void testProcessInversionInd() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setInversionInd(true);
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("inversionInd"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("true"));
	}

	@Test
	public void testProcessMediaType() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setMediaType("image");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("mediaType"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("image"));
	}

	@Test
	public void testProcessMoodCode() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setMediaType("image");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("mediaType"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("image"));
	}

	@Test
	public void testProcessNegationInd() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setNegationInd("true");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("negationInd"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("true"));
	}

	@Test
	public void testProcessNullFlavor() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setNullFlavor("NA");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("nullFlavor"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("NA"));
	}

	@Test
	public void testProcessOperator() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setOperator("A");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("operator"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("A"));
	}

	@Test
	public void testProcessQualifier() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setQualifier("A");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("qualifier"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("A"));
	}

	@Test
	public void testProcessRepresentation() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setRepresentation("Q");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("representation"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("Q"));
	}

	@Test
	public void testProcessRoot() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setRoot("Q");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("root"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("Q"));
	}

	@Test
	public void testProcessTypeCode() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setTypeCode("Z");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("typeCode"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("Z"));
	}

	@Test
	public void testProcessUnit() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setUnit("kg");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("unit"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("kg"));
	}

	@Test
	public void testProcessUse() {
		AttributeFlattenProc attproc = new AttributeFlattenProc();
		Attribute attr = new Attribute();
		attr.setUse("f");
		attr.setParentObject(TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new THashMap<RuleDefinition, List<Attribute>>();
		attproc.process(attr, mapNewAttributes);
		assertTrue(mapNewAttributes.size()>0);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).size()==1);
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getName().equals("use"));
		assertTrue(mapNewAttributes.get(mapNewAttributes.keySet().iterator().next()).get(0).getValue().equals("f"));
	}

}
