package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.ElementClosed;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

public class ElementClosedTest {

	@Test
	public void testElementOrTemplateClosed1() throws Exception {
		RuleDefinition rd = new RuleDefinition();
		TemplateDefinition td = new TemplateDefinition();
		rd.setParentObject(td);
		td.getAttributeOrChoiceOrElement().add(rd);
		td.setIsClosed(true);
		assertTrue(ElementClosed.elementOrTemplateClosed(rd) == ElementClosed.TEMPLATE);
	}
	
	@Test
	public void testElementOrTemplateClosed2() throws Exception {
		RuleDefinition rd = new RuleDefinition();
		TemplateDefinition td = new TemplateDefinition();
		rd.setParentObject(td);
		td.getAttributeOrChoiceOrElement().add(rd);
		rd.setIsClosed(true);
		assertTrue(ElementClosed.elementOrTemplateClosed(rd) == ElementClosed.ELEMENT);
	}
	
	@Test
	public void testElementOrTemplateClosed3() throws Exception {
		RuleDefinition rd = new RuleDefinition();
		TemplateDefinition td = new TemplateDefinition();
		rd.setParentObject(td);
		td.getAttributeOrChoiceOrElement().add(rd);
		assertTrue(ElementClosed.elementOrTemplateClosed(rd) == null);
	}

}
