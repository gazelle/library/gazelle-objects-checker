package net.ihe.gazelle.tempgen.ehdsi.scripts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class CCDAExtractor {
	
	public static void main(String[] args) throws FileNotFoundException, JAXBException {
		String bbr = "/home/aboufahj/projects/sequoia/decor20180202/ccda-20180202T124318-en-US-decor-compiled.xml";
		String bbrout = "/home/aboufahj/projects/sequoia/decor20180202/ccda-20180202T124318-en-US-decor-compiled.xml-1.1.xml";
		String versionLabel = "1.1";
		extractTemplates(bbr, versionLabel, bbrout);
	}

	private static void extractTemplates(String bbr, String versionLabel, String bbrout) throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor(new FileInputStream(bbr));
		List<TemplateDefinition> temps = RulesUtil.getTemplates(dec.getRules());
		for (TemplateDefinition templateDefinition : temps) {
			if (!templateDefinition.getVersionLabel().equals(versionLabel)) {
				dec.getRules().getTemplateAssociationOrTemplate().remove(templateDefinition);
			}
		}
		DecorMarshaller.marshallDecor(dec, new FileOutputStream(bbrout));
	}

}
