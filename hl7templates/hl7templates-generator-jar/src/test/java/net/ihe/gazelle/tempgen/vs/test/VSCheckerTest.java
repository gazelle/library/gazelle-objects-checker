package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Set;

import org.junit.Test;

import net.ihe.gazelle.tempgen.valueset.checker.VSChecker;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VSCheckerTest {

	@Test
	public void testFindCircularValueSets1() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		Map<String, Set<String>> aa = VSChecker.findCircularValueSets(decor);
		assertTrue(aa.size() == 0);
	}
	
	@Test
	public void testFindCircularValueSets2() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies2.xml");
		Map<String, Set<String>> aa = VSChecker.findCircularValueSets(decor);
		assertTrue(aa.size()>0);
	}

}
