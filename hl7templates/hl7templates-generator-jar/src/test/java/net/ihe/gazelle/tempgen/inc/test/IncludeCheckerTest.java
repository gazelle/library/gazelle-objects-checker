package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.checker.IncludeChecker;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncludeCheckerTest {

	@Test
	public void testIncludesAreCircular1() throws FileNotFoundException, JAXBException {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/inc/test-decor.xml");
		assertFalse(IncludeChecker.includesAreCircular(decor));
	}
	
	@Test
	public void testIncludesAreCircular2() throws FileNotFoundException, JAXBException {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/inc/circ-decor.xml");
		assertTrue(IncludeChecker.includesAreCircular(decor));
	}

	@Test
	public void testMapCircularIncludes() throws FileNotFoundException, JAXBException {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/inc/circ-decor.xml");
		Map<String, Set<String>> map = IncludeChecker.mapCircularIncludes(decor);
		assertTrue(map.size()==3);
		assertTrue(map.keySet().containsAll(Arrays.asList(new String[]{"1.2.3.4", "1.2.3.5", "2.16.840.1.113883.2.4.3.11.60.22.10.134"})));
	}

}
