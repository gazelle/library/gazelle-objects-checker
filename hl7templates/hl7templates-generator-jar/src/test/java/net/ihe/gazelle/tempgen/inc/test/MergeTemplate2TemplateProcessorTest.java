package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.action.MergeTemplate2TemplateProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class MergeTemplate2TemplateProcessorTest {

	@Test
	public void testProcess() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_td.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getElementsByName(RulesUtil.getTemplates(decorTemplates.getRules()).get(0), "participant") != null);
	}

	@Test
	public void testProcess_asserts() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_assert.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getAsserts(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).size()==1);
	}

	@Test
	public void testProcessAttributes() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_attr.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getAttributes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).size()==1);
	}

	@Test
	public void testProcessChoices() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_choice2.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getChoices(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).size()==1);
	}

	@Test
	public void testProcessElements() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_td.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).size()==1);
	}

	@Test
	public void testProcessIncludes() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_attr.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).size()==1);
	}

	@Test
	public void testProcessItem() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_attr.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(RulesUtil.getTemplates(decorTemplates.getRules()).get(0).getItem().getLabel().equals("AA;BB"));
	}

	@Test
	public void testProcessLets() throws FileNotFoundException, JAXBException {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_attr.xml");
		(new MergeTemplate2TemplateProcessor()).process(RulesUtil.getTemplates(decorTemplates.getRules()).get(1), 
				RulesUtil.getTemplates(decorTemplates.getRules()).get(0), 
				TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0));
		assertTrue(TemplateDefinitionUtil.getIncludes(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).size()==1);
	}

}
