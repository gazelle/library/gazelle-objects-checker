package net.ihe.gazelle.tempgen.choice.test;

import static org.junit.Assert.*;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceInitTest {
	
	Decor decorTemplates = null;
	ChoiceDefinition currentChoiceDefinition = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(td), "hl7:participantRole");
		currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0);
	}
	
	@Test
	public void testName() throws Exception {
		assertTrue(currentChoiceDefinition != null);
	}

}
