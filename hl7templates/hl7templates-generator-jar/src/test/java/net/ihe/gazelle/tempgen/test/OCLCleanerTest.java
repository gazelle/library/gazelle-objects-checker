package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.*;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.OCLCleaner;

public class OCLCleanerTest {

	@Test
	public void testGenerateRejectOcl() {
		assertTrue(OCLCleaner.generateRejectOcl(true).equals("->reject(not nullFlavor.oclIsUndefined())"));
		assertTrue(OCLCleaner.generateRejectOcl(false).equals(""));
	}

}
