package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.ValueSetResponseType;
import net.ihe.gazelle.tempgen.valueset.action.ValueSetConceptProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptProcTest extends ValueSetConceptProc {
	
	@Before
	public void before(){
		this.concept = new CE();
	}

	@Test
	public void testProcess() {
		ValueSetConcept vsc = new ValueSetConcept();
		RetrieveValueSetResponseType currentRetrieveValueSetResponseType = new RetrieveValueSetResponseType();
		List<CE> conceptList = new ArrayList<>();
		currentRetrieveValueSetResponseType.setValueSet(new ValueSetResponseType());
		currentRetrieveValueSetResponseType.getValueSet().getConceptList().add(new ConceptListType());
		this.process(vsc, new Object[] {currentRetrieveValueSetResponseType, conceptList});
		assertTrue(this.conceptList!= null);
	}

	@Test
	public void testProcessCode() {
		this.processCode("code");
		assertTrue(this.concept.getCode().equals("code"));
	}

	@Test
	public void testProcessCodeSystem() {
		this.processCodeSystem("codeSystem");
		assertTrue(this.concept.getCodeSystem().equals("codeSystem"));
	}

	@Test
	public void testProcessCodeSystemName() {
		this.processCodeSystemName("codeSystemName");
		assertTrue(this.concept.getCodeSystemName().equals("codeSystemName"));
	}

	@Test
	public void testProcessCodeSystemVersion() {
		this.processCodeSystemVersion("csv");
		assertTrue(this.concept.getCodeSystemVersion().equals("csv"));
		this.processCodeSystemVersion(null);
		assertTrue(this.concept.getCodeSystemVersion() == null);
	}

	@Test
	public void testProcessDisplayName() {
		this.processDisplayName("dn");
		assertTrue(this.concept.getDisplayName().equals("dn"));
	}

}
