package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.tempgen.valueset.script.SVSConsumer;

public class SVSConsumerTest {

	@Test
	public void testGetConceptsListFromValueSet() {
		List<Concept> aa = (new SVSConsumer()).getConceptsListFromValueSet("1.3.6.1.4.1.12559.11.4.1.5", null);
		boolean containsPrescriptionCode = false;
		for (Concept concept : aa) {
			if (concept.getCode() != null && concept.getCode().equals("57833-6")) {
				containsPrescriptionCode = true;
			}
		}
		assertTrue(containsPrescriptionCode);
	}
	
	@Test
	public void testGetConceptsListFromValueSet2() {
		(new SVSConsumer()).getConceptsListFromValueSet("1.3.6.1.4.1.12559.11.4.1.5", null);
		List<Concept> aa = (new SVSConsumer()).getConceptsListFromValueSet("1.3.6.1.4.1.12559.11.4.1.5", null);
		boolean containsPrescriptionCode = false;
		for (Concept concept : aa) {
			if (concept.getCode() != null && concept.getCode().equals("57833-6")) {
				containsPrescriptionCode = true;
			}
		}
		assertTrue(containsPrescriptionCode);
	}

}
