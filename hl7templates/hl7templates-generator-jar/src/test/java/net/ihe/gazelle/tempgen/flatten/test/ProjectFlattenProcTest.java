package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.flatten.action.ProjectFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;

public class ProjectFlattenProcTest extends ProjectFlattenProc {
	
	@Before
	public void before(){
		this.currentProject = new Project();
		this.currentProject.setPrefix(null);
	}

	@Test
	public void testProcessPrefix1() {
		this.processPrefix("test-");
		assertTrue(this.currentProject.getPrefix().equals("test-"));
	}
	
	@Test
	public void testProcessPrefix2() {
		this.processPrefix("aa-test-");
		assertTrue(this.currentProject.getPrefix().equals("aatest-"));
	}
	
	@Test
	public void testProcessPrefix3() {
		this.processPrefix(null);
		assertTrue(this.currentProject.getPrefix() == null);
	}

	@Test
	public void testFlattenPrefix() {
		String aa = ProjectFlattenProc.flattenPrefix("IHE-EYE-");
		assertTrue(aa.equals("iheeye-"));
	}

}
