package net.ihe.gazelle.tempgen.sequoia.scripts;

import net.ihe.gazelle.common.marshaller.ObjectMarshallerImpl;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.ValueSetResponseType;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Some value set cannot be imported in SVS via the GUI because the xml file is too big (several Mo)
 * This class create sql scripts to import value set directly to the database
 */
public class GenerateSQLForBigValueSet {

    private static final Logger LOG = LoggerFactory.getLogger(GenerateSQLForBigValueSet.class);

    private static String[] valueSetFiles = {
            "/YOUR/PATH/TO/VS/2.16.840.1.113883.3.88.12.3221.7.4.xml",
            "/YOUR/PATH/TO/VS/2.16.840.1.113762.1.4.1010.2.xml",
            "/YOUR/PATH/TO/VS/2.16.840.1.113762.1.4.1010.4.xml",
            "/YOUR/PATH/TO/VS/2.16.840.1.113883.3.88.12.80.28.xml",
            "/YOUR/PATH/TO/VS/2.16.840.1.113762.1.4.1010.1.xml",
            "/YOUR/PATH/TO/VS/2.16.840.1.113883.3.88.12.3221.8.9.xml"
    };

    private static String outputDir = "/YOUR/PATH/TO/VS//sql/";

    public static void main(String[] args) {

        for (String valueSetFile : valueSetFiles) {
            LOG.info("***** Extracting file " + valueSetFile);
            System.out.println("***** Extracting file " + valueSetFile);

            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(valueSetFile);
                ObjectMarshallerImpl<RetrieveValueSetResponseType> om = new ObjectMarshallerImpl<>(
                        RetrieveValueSetResponseType.class);
                RetrieveValueSetResponseType rr = om.unmarshall(inputStream);

                ValueSetResponseType dvs = rr.getValueSet();

                LOG.info("Creating sql script");

                StringBuilder sql = new StringBuilder("INSERT INTO svs_value_set " +
                        "(id, displayname, vs_id_oid, version, binding, creation_date, definition, effective_date," +
                        " expiration_date, purpose, revision_date, source, sourceuri, status, type)" +
                        " VALUES (nextval('svs_value_set_id_seq'), '" + StringEscapeUtils.escapeSql(dvs.getDisplayName()) + "'," +
                        " '" + dvs.getId() + "', '" + dvs.getVersion() + "', 0," +
                        " NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', 0); " + System.lineSeparator());
                LOG.info("Value set " + dvs.getId());
                System.out.println("Value set " + dvs.getId());

                for (ConceptListType conceptListType : dvs.getConceptList()) {
                    sql.append("INSERT INTO svs_conceptlist (id, lang, main_list, value_set_id)").append(" VALUES (nextval('svs_conceptlist_id_seq'), '").append(conceptListType.getLang()).append("', true, currval('svs_value_set_id_seq'));").append(System.lineSeparator());
                    LOG.info("Concept list " + conceptListType.getLang());
                    System.out.println("Concept list " + conceptListType.getLang());

                    System.out.println("we have "+conceptListType.getConcept().size()+" concepts");
                    for (CE concept : conceptListType.getConcept()) {
                        sql.append("INSERT INTO svs_concept (id, code, codesystem, codesystemname, codesystemversion, displayname, concept_list_id)").append(" VALUES (nextval('om_container_id_seq'), '").append(concept.getCode()).append("', '").append(concept.getCodeSystem()).append("',").append(" '").append(concept.getCodeSystemName()).append("', '").append(concept.getCodeSystemVersion()).append("', ").append("'").append(StringEscapeUtils.escapeSql(concept.getDisplayName())).append("', currval('svs_conceptlist_id_seq'));").append(System.lineSeparator());
                        //LOG.info("Concept " + concept.getDisplayName());
//                        
                    }
                }
                System.out.println("done");
                String path = outputDir + "import" + dvs.getId() + ".sql";
                System.out.println(Paths.get(path));
                Files.write(Paths.get(path), sql.toString().getBytes());


            } catch (IOException | JAXBException e) {
                LOG.error("Unable to read/unmarshal multiple-value-set file", e);
                e.printStackTrace();
            } finally {
                IOUtils.closeQuietly(inputStream);
                System.out.println("connection closed");
            }

        }

    }
}
