package net.ihe.gazelle.tempgen.sequoia.scripts;

import net.ihe.gazelle.tempapi.interfaces.VocabularyProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VocabCheckImplProvider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(VocabularyProcessor.class)){
			return (T) new VocabCheckProblemObservation();
		}
		return null;
	}
	
}
