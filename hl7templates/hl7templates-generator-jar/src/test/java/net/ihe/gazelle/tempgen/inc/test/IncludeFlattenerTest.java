package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.inc.action.IncludeFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncludeFlattenerTest extends IncludeFlattener{

	@Test
	public void testFlattenIncludeInDecor() throws FileNotFoundException, JAXBException {
		Decor decorold = DecorMarshaller.loadDecor("src/test/resources/inc/test-decor.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decorold.getRules()).get(0);
		assertTrue(TemplateDefinitionUtil.getIncludes(td).size()==1);
		IncludeFlattener.flattenIncludeInDecor(decorold);
		assertTrue(TemplateDefinitionUtil.getIncludes(td).size()==0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(td);
		assertTrue(RuleDefinitionUtil.getIncludes(rd).size()==0);
	}
	
	@Test
	public void testHandleProblemsOfInclusions1() throws Exception {
		Map<String, Set<String>> mapCirc = new TreeMap<String, Set<String>>();
		mapCirc.put("aa", new TreeSet<String>());
		mapCirc.get("aa").add("test1");
		mapCirc.get("aa").add("test2");
		mapCirc.put("bb", new TreeSet<String>());
		mapCirc.get("bb").add("btest1");
		mapCirc.get("bb").add("btest2");
		handleProblemsOfInclusions(mapCirc);
		assertTrue(ProblemHandler.findNotification("Some circular includes will be ignored during") != null);
	}
	
	@Test
	public void testHandleProblemsOfInclusions2() throws Exception {
		Map<String, Set<String>> mapCirc = new TreeMap<String, Set<String>>();
		boolean alreadyExists = ProblemHandler.findNotification("Some circular includes will be ignored during") != null;
		handleProblemsOfInclusions(mapCirc);
		boolean newAdds = ProblemHandler.findNotification("Some circular includes will be ignored during") != null;
		assertTrue(alreadyExists || !newAdds);
	}
	
	@Test
	public void testHandleProblemsOfInclusions3() throws Exception {
		boolean alreadyExists = ProblemHandler.findNotification("Some circular includes will be ignored during") != null;
		handleProblemsOfInclusions(null);
		boolean newAdds = ProblemHandler.findNotification("Some circular includes will be ignored during") != null;
		assertTrue(alreadyExists || !newAdds);
	}

}
