package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.action.IncFlatUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncFlatUtilTest {
	
	Decor decorTemplates = null;
	
	Decor decorCust = null;
	
	ChoiceDefinition currentChoiceDefinition = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_choice.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
		currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0);
		decorCust = DecorMarshaller.loadDecor("src/test/resources/decor_custodian.xml");
}

	@Test
	public void testUpdateItemReferences() throws Exception {
		IncFlatUtil.updateItemReferences(ChoiceDefinitionUtil.getIncludes(currentChoiceDefinition).get(0), currentChoiceDefinition);
		assertTrue(this.currentChoiceDefinition.getItem().getLabel().contains("aa"));
	}

}
