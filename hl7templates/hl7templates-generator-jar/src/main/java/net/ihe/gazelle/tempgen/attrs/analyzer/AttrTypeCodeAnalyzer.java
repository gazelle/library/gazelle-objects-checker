package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrTypeCodeAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.typeCode not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "typeCode", attribute.getTypeCode(), getProcessIdentifier());
	}
	
	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "typeCode", attribute.getTypeCode());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_TYPE_CODE_PROCESS.getValue();
	}

}
