package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMinimumMultiplicityAnalyzer implements ConstraintGenerator {

	private final Boolean generateDTDistinguisher;

	public RDMinimumMultiplicityAnalyzer(Boolean generateDTDistinguisher) {
		this.generateDTDistinguisher = generateDTDistinguisher;
	}

	public RDMinimumMultiplicityAnalyzer(){
		this.generateDTDistinguisher = Boolean.FALSE;
	}

	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		OCLGenerator generator = new OCLGenerator();
		StringBuilder constraint = new StringBuilder(generator.generateRuleToParentFromPathNF(ruleDefinition));
		if (ruleDefinition.getMinimumMultiplicity().intValue() == 0 && ConformanceType.R.equals(ruleDefinition.getConformance())){
			ruleDefinition.setMinimumMultiplicity(Integer.valueOf(ruleDefinition.getMinimumMultiplicity() + 1));
			constraint.append(generator.getMinimumMultiplicityRuleForRD(constraint.toString(), ruleDefinition));
			ruleDefinition.setMinimumMultiplicity(Integer.valueOf(ruleDefinition.getMinimumMultiplicity() - 1));
		} else {
			constraint.append(generator.getMinimumMultiplicityRuleForRD(constraint.toString(), ruleDefinition));
		}
		if(this.generateDTDistinguisher == Boolean.TRUE){
			return generator.appendDTDistinguisher(ruleDefinition, constraint.toString());
		}
		return constraint.toString();
	}

	@Override
	public String generateCommentConstraint(RuleDefinition selectedRuleDefinition) {
		Integer minimumMultiplicity = selectedRuleDefinition.getMinimumMultiplicity();
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(selectedRuleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(selectedRuleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		DParent dparentRD = RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition);
		String elementName = DPathExtractor.createPathFromDParent(dparentRD.getTail()).substring(1);
		String verb = (minimumMultiplicity.intValue() == 0 && ConformanceType.R.equals(selectedRuleDefinition.getConformance()))?" SHOULD ":" SHALL ";
		minimumMultiplicity = Integer.valueOf((minimumMultiplicity == 0 && ConformanceType.R.equals(selectedRuleDefinition.getConformance())) ? minimumMultiplicity + 1 : minimumMultiplicity);
		return "In " + templateName + ", " + parentPath + verb + "contain at least " +
				OwnedRuleManager.convertMultiplicity(minimumMultiplicity) + " " + elementName;
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_MIN_MULTIPLICITY_PROCESS.getValue();
	}

}
