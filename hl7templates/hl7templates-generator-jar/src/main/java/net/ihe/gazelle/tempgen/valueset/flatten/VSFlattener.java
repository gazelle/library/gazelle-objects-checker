package net.ihe.gazelle.tempgen.valueset.flatten;

import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.valueset.checker.VSChecker;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class VSFlattener {
	
	private VSFlattener() {}
	
	public static Decor flattenDecorForValueSets(Decor decor) {
		if (decor != null) {
			Map<String, Set<String>> aa = VSChecker.findCircularValueSets(decor);
			if (aa != null && aa.keySet()!= null && !aa.keySet().isEmpty()) {
				ProblemHandler.handleError("There are circular referencing in value sets  : " + aa, AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
			}
			VSFlattenStats stats = new VSFlattenStats();
			int oldNumberIncluded;
			do {
				oldNumberIncluded = stats.getNumberIntegrated();
				DecorVisitor.INSTANCE.visitAndProcess(decor, new VSFlattenImplProvider(), aa, stats);
			}while (oldNumberIncluded<stats.getNumberIntegrated());
			return CloneUtil.cloneHL7TemplateEl(decor);
		}
		return null;
	} 

}
