package net.ihe.gazelle.tempgen.inc.action;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class MergeUtil {
	
	private MergeUtil() {}

	public static void updateCloneProperties(RuleDefinition clone,
			Integer minimumMultiplicity, String maximumMultiplicity,
			Boolean isMandatory, ConformanceType conformanceType) {
		if (clone != null){
			if(minimumMultiplicity != null){
				clone.setMinimumMultiplicity(minimumMultiplicity);
			}
			if (maximumMultiplicity != null && !maximumMultiplicity.trim().isEmpty()) {
				clone.setMaximumMultiplicity(maximumMultiplicity.trim());
			}
			if (isMandatory != null){
				clone.setIsMandatory(isMandatory);
			}
			if (conformanceType != null) {
				clone.setConformance(conformanceType);
			}
		}
	}

	public static void updateCloneProperties(IncludeDefinition clone,
			Integer minimumMultiplicity, String maximumMultiplicity,
			Boolean isMandatory, ConformanceType conformance) {
		if (clone != null){
			if(minimumMultiplicity != null){
				clone.setMinimumMultiplicity(minimumMultiplicity);
			}
			if (maximumMultiplicity != null && !maximumMultiplicity.trim().isEmpty()) {
				clone.setMaximumMultiplicity(maximumMultiplicity.trim());
			}
			if (isMandatory != null){
				clone.setIsMandatory(isMandatory);
			}
			if (conformance != null) {
				clone.setConformance(conformance);
			}
		}
	}

	public static void updateCloneProperties(ChoiceDefinition clone,
			Integer minimumMultiplicity, String maximumMultiplicity) {
		if (clone != null){
			if(minimumMultiplicity != null){
				clone.setMinimumMultiplicity(minimumMultiplicity);
			}
			if (maximumMultiplicity != null && !maximumMultiplicity.trim().isEmpty()) {
				clone.setMaximumMultiplicity(maximumMultiplicity.trim());
			}
		}
	}


}
