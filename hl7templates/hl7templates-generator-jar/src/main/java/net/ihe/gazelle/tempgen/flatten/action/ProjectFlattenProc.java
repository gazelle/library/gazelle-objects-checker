package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.impl.ProjectProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;

public class ProjectFlattenProc extends ProjectProcessorImpl {
	
	protected Project currentProject;
	
	@Override
	public void process(Project t, Object... objects) {
		this.currentProject = t;
		super.process(t, objects);
	}
	
	@Override
	public void processPrefix(String prefix) {
		if (this.currentProject != null) {
			this.currentProject.setPrefix(flattenPrefix(prefix));
		}
	}
	
	public static String flattenPrefix(String prefix) {
		if (prefix != null) {
			String res = prefix.replace("-", "");
			res = res.toLowerCase() + "-";
			return res;
		}
		return null;
	}

}
