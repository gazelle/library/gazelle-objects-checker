package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasParent;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public final class CDAPathGenerator {
	
	private CDAPathGenerator() {}

	public static String extractCDAPath(Object obj) {
		if (obj instanceof TemplateDefinition) {
			TemplateDefinition td = (TemplateDefinition)obj;
			DParent dp= RuleDefinitionUtil.getDParentOfRuleDefinition(TemplateDefinitionUtil.getFirstElement(td));
			return DPathExtractor.createPathFromDParent(dp);
		}
		else if (obj instanceof ChoiceDefinition) {
			ChoiceDefinition currentChoiceDefinition = (ChoiceDefinition)obj;
			return CDAPathGenerator.extractCDAPath(currentChoiceDefinition.getParentObject());
		}
		else if (obj instanceof RuleDefinition) {
			RuleDefinition ruleDefinition = (RuleDefinition)obj;
			DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
			return DPathExtractor.createPathFromDParent(dparent);
		}
		else if (obj instanceof Attribute) {
			Attribute attribute  = (Attribute)obj;
			DParent dparent = AttributeUtil.getDParentOfAttibute(attribute, AttributeUtil.getBetterMatchingName(attribute), 
					AttributeUtil.getBetterMatchingValue(attribute));
			return DPathExtractor.createPathFromDParent(dparent);
		}
		else if (obj instanceof HasParent) {
			HasParent hasParentObj = (HasParent)obj;
			if (hasParentObj.getParentObject() != null) {
				return CDAPathGenerator.extractCDAPath(hasParentObj.getParentObject());
			}
		}
		return null;
	}

}
