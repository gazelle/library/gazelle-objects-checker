package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.tempapi.impl.DecorProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DecorAnalyzer extends DecorProcessorImpl {
	
	protected Decor currentDecor;
	
	protected XMI xmi;
	
	protected Boolean ignoreTemplateIdRequirements = Boolean.FALSE;

	@Override
	public void process(Decor t, Object... objects) {
		this.currentDecor = t;
		this.xmi = (XMI) objects[0];
		if (objects.length>1) {
			this.ignoreTemplateIdRequirements = (Boolean) objects[1];
		}

		String prefix = DecorUtil.extractPrefixFromDecor(this.currentDecor);
		OwnedRuleUtil.setReqIdentifier(prefix);

		super.process(t, objects);

		ImportElementHandler.addElementsToBeImported(xmi);
		TamlHandler.setTargetIDScheme(DecorUtil.extractPrefixFromDecor(this.currentDecor));
		TamlHandler.addTamlToXMI(xmi);
	}
	
	@Override
	public void processRules(Rules rules) {
		(new RulesAnalyzer()).process(rules, this.xmi, this.ignoreTemplateIdRequirements);
	}

}
