package net.ihe.gazelle.tempgen.inc.action;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.inc.checker.IncludeChecker;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * cannot be set to final, extended by test class.
 *
 */
public class IncludeFlattener {
	
	public static Decor flattenIncludeInDecor(Decor decor){
		IncludeStats incStats = new IncludeStats();
		int oldNumberIntegrated = 0;
		do {
			oldNumberIntegrated = incStats.getNumberIntegrated();
			Map<String, Set<String>> mapCirc = IncludeChecker.mapCircularIncludes(decor);
			handleProblemsOfInclusions(mapCirc);
			List<TemplateDefinition> listCDATemplate = TemplateDefinitionUtil.extractListCDATemplateFromDecorTemplateRepository(decor);
			DecorVisitor.INSTANCE.visitAndProcess(decor, new IncludeFlattenerImplProvider(), mapCirc, listCDATemplate, incStats);
		} while (oldNumberIntegrated<incStats.getNumberIntegrated());
		return CloneUtil.cloneHL7TemplateEl(decor);
	}

	protected static void handleProblemsOfInclusions(Map<String, Set<String>> mapCirc) {
		if (mapCirc != null && mapCirc.size()>0) {
			StringBuilder sb = new StringBuilder();
			sb.append("Some circular includes will be ignored during the processing, which are : ");
			int i = 0;
			for (Entry<String, Set<String>> strAndSet : mapCirc.entrySet()) {
				if (i>0){
					sb.append(", ");
				}
				sb.append("[parent=").append(strAndSet.getKey()).append(", inclusion= ").append(strAndSet.getValue()).append("]");
			}
			ProblemHandler.handleError(sb.toString(), AnalyzerEnum.RD_INC_PROCESS.getValue());
		}
	}

}
