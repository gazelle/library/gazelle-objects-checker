package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrNegationIndAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.negationInd != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, "negationInd", String.valueOf(Boolean.valueOf(attribute.getNegationInd())));
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "negationInd", attribute.getNegationInd());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_NEGATION_IND_PROCESS.getValue();
	}

}
