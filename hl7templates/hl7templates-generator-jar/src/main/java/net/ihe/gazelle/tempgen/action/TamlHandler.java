package net.ihe.gazelle.tempgen.action;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.ihe.gazelle.goc.template.definer.model.Taml;
import net.ihe.gazelle.goc.uml.utils.TAMLUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class TamlHandler {
	
	private static Map<String, Set<String>> mapRulesToLabels = new LinkedHashMap<>();

	private static org.slf4j.Logger log = LoggerFactory.getLogger(ChoiceDefinitionAnalyzer.class);

	/**
	 * this value is initialized by the identifier of the decor document
	 */
	private static String targetIDScheme;
	
	private TamlHandler() {
		// private constructor
	}
	
	public static void setTargetIDScheme(String targetIDScheme) {
		TamlHandler.targetIDScheme = targetIDScheme;
	}

	public static void handleTAML(String label, OwnedRule or){
		if (label != null && !label.trim().isEmpty() && or != null && or.getId() != null){
			if (!mapRulesToLabels.containsKey(or.getId())){
				mapRulesToLabels.put(or.getId(), new LinkedHashSet<String>());
			}
			mapRulesToLabels.get(or.getId()).add(label.trim());
		}
	}
	
	public static void handleAllTAML(Collection<String> label, OwnedRule or){
		for (String string : label) {
			handleTAML(string, or);
		}
	}

	public static void addTamlToXMI(XMI xmi) {
		for (Entry<String, Set<String>> strAndSet : mapRulesToLabels.entrySet()) {
			String labelComplete = extractListLabelAsString(strAndSet.getValue());
			Taml taml = TAMLUtil.createTAML(strAndSet.getKey(), labelComplete, TamlHandler.targetIDScheme);
			xmi.getTaml().add(taml);
		}
	}

	private static String extractListLabelAsString(Set<String> listLabel) {
		String res = null;
		if (listLabel != null){
			StringBuilder sb = new StringBuilder("");
			int i = 0;
			for (String string : listLabel) {
				if (i>0){
					sb.append(";");
				}
				sb.append(string);
				i++;
			}
			res = sb.toString();
		}
		return res;
	}

	public static void handleAllTAML(Set<String> setItems, PackagedElement currentPackagedElement) {
		for (String string : setItems) {
			handleTAML(string, currentPackagedElement);
		}
	}

	private static void handleTAML(String label, PackagedElement currentPackagedElement) {
		if (label != null && !label.trim().isEmpty() && currentPackagedElement != null &&
				currentPackagedElement.getId() != null){
			if (!mapRulesToLabels.containsKey(currentPackagedElement.getId())){
				mapRulesToLabels.put(currentPackagedElement.getId(), new HashSet<String>());
			}
			mapRulesToLabels.get(currentPackagedElement.getId()).add(label.trim());
		}
	}


	public static void removeTAML(OwnedRule or){
		Set<String> removed = mapRulesToLabels.remove(or.getId());
		if(removed != null){
			log.info("removed {} element from the TAML", Integer.valueOf(removed.size()));
		}
		else{
			log.info("No TAML found for the provided OwnedRule");
		}
	}

}
