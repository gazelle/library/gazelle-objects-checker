package net.ihe.gazelle.tempgen.valueset.checker;

import gnu.trove.map.hash.THashMap;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempgen.nodes.check.NodesChecker;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class VSChecker {
	
	private VSChecker() {}
	
	public static Map<String, Set<String>> findCircularValueSets(Decor decor) {
		Map<String, Set<String>> vsReferences = new THashMap<>();
		Map<String, Set<String>> circularRefs = new THashMap<>();
		DecorVisitor.INSTANCE.visitAndProcess(decor, new VSCheckerImplProvider(), vsReferences);
		circularRefs = NodesChecker.extractCircularCall(vsReferences, circularRefs);
		return circularRefs;
	}

}
