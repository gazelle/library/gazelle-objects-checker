package net.ihe.gazelle.tempgen.ehdsi.scripts;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EHDSIFriendlyTransformer {
	
	public final static String ROOT = "1.3.6.1.4.1.12559.11.10.1.3.1.42";

	private static Logger log = LoggerFactory.getLogger(VocabularyFriendlyProcessorPivot.class);

	public static ArrayList<Object> ml_objectsWithVocabularyToDelete = new ArrayList<>();
	
	public static void main(String[] args) throws FileNotFoundException, JAXBException {

		String bbrPath = "DEFINE_YOUR_DEFAULT_PATH_HERE";
		String outputBbrPath = "DEFINE_YOUR_OUT_PATH_HERE";

		//Override input bbr path if exists as param
		if(args.length>0){
			bbrPath = args[0];
		}

		//Override output bbr path if exists as param
		if(args.length>1){
			outputBbrPath = args[1];
		}

		Decor dec = DecorMarshaller.loadDecor(bbrPath);
		DecorVisitor.INSTANCE.visitAndProcess(dec, new VSFriendlyProvider());

		for (Object lo_vocabularyParent : ml_objectsWithVocabularyToDelete){
			if (lo_vocabularyParent instanceof RuleDefinition){
				if (((RuleDefinition) lo_vocabularyParent).getVocabulary() != null &&
						!((RuleDefinition) lo_vocabularyParent).getVocabulary().isEmpty()){
					((RuleDefinition) lo_vocabularyParent).getVocabulary()
							.clear();
				}
			} else if (lo_vocabularyParent instanceof Attribute){
				if (((Attribute) lo_vocabularyParent).getVocabulary() != null &&
						!((Attribute) lo_vocabularyParent).getVocabulary().isEmpty()){
					((Attribute) lo_vocabularyParent).getVocabulary()
							.clear();
				}
			} else {
				log.error("Unchecked type of parent element : " + lo_vocabularyParent.getClass());
			}
		}
		DecorMarshaller.marshallDecor(dec, new FileOutputStream(outputBbrPath));
	}

}
