package net.ihe.gazelle.tempgen.valueset.action;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.tempapi.impl.ValueSetConceptProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;

import java.util.List;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptProc extends ValueSetConceptProcessorImpl {
	
	protected RetrieveValueSetResponseType currentRetrieveValueSetResponseType;
	
	protected net.ihe.gazelle.datatypes.CE concept;

	protected List<CE> conceptList;

	@Override
	public void process(ValueSetConcept t, Object... objects) {
		if (t == null) {
			return;
		}
		this.conceptList = (java.util.List<net.ihe.gazelle.datatypes.CE> ) objects[1];
		this.concept = new CE();
		super.process(t, objects);
		this.conceptList.add(concept);
	}
	
	@Override
	public void processCode(String code) {
		this.concept.setCode(code);
	}
	
	@Override
	public void processCodeSystem(String codeSystem) {
		this.concept.setCodeSystem(codeSystem);
	}
	
	@Override
	public void processCodeSystemName(String codeSystemName) {
		this.concept.setCodeSystemName(codeSystemName);
	}
	
	@Override
	public void processCodeSystemVersion(String codeSystemVersion) {
		this.concept.setCodeSystemVersion(codeSystemVersion);
	}

	@Override
	public void processDisplayName(String displayName) {
		this.concept.setDisplayName(displayName);
	}
	
}
