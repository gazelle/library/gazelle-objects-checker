package net.ihe.gazelle.tempgen.attrs.flatten;

import net.ihe.gazelle.tempapi.interfaces.AttributeProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrsFlattenImplProvider  implements ImplProvider {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(AttributeProcessor.class)){
			return (T) new AttributeFlattenProc();
		}
		return null;
	}

}
