package net.ihe.gazelle.tempgen.statistics;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ConverterStatistics {
	
	private ConverterStatistics() {}
	
	public static final List<TemplateDefinition> listCompleteTemplateDefinitions = new ArrayList<>();
	
	public static final List<TemplateDefinition> listCDATemplateDefinitions = new ArrayList<>();
	
	public static final List<TemplateDefinition> listIncludedTemplateDefinitions = new ArrayList<>();
	
	public static final List<RDNameAsXpath> listRDNameAsXpaths = new ArrayList<>();

}
