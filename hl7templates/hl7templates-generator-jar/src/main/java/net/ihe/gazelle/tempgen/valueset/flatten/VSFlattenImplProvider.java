package net.ihe.gazelle.tempgen.valueset.flatten;

import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptListProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VSFlattenImplProvider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(ValueSetConceptListProcessor.class)){
			return (T) new ValueSetConceptListFlatten();
		}
		return null;
	}
	
}
