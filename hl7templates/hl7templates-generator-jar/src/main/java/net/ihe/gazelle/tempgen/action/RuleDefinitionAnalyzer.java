package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.goc.uml.utils.*;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.annotation.UsedByArtDecor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.*;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTUtils;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasItem;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/**
 * @author Abderrazek Boufahja
 */
public class RuleDefinitionAnalyzer extends RuleDefinitionProcessorImpl {

    private static Logger log = LoggerFactory.getLogger(RuleDefinitionAnalyzer.class);

    protected RuleDefinition selectedRuleDefinition;

    protected String ruleContextDescription = "";

    protected PackagedElement currentPackagedElement;

    protected Boolean ignoreTemplateIdRequirements = Boolean.FALSE;

    private List<OwnedRule> listGeneratedOwnedRule = new ArrayList<>();

    private Boolean generateDTDistinguisher = Boolean.FALSE;


    private boolean containsError = false;

    protected static void updateListDistinguisherFollower(RuleDefinition ruleDefinition, Pair<String, String> pairDisting) {
        if (pairDisting != null && pairDisting.getObject1() != null && pairDisting.getObject2() != null) {
            DParent dElement = DPathExtractor.extractDElementFromDPath(pairDisting.getObject1());
            Object obj = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(dElement, ruleDefinition);
            if (obj instanceof RuleDefinition) {
                DParent ruleDefinitionDParent = RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition) obj);
                followDistinguisher(pairDisting.getObject2(), ruleDefinitionDParent);
                moveItemDistinguisher((HasItem) obj, ruleDefinition);
            } else if (obj instanceof Attribute) {
                DParent ruleDefinitionDParent = AttributeUtil.getDParentOfAttibute((Attribute) obj);
                followDistinguisher(pairDisting.getObject2(), ruleDefinitionDParent);
                moveItemDistinguisher((HasItem) obj, ruleDefinition);
            }
        }
    }

    protected static void moveItemDistinguisher(HasItem hasItem, RuleDefinition ruleDefinition) {
        if (hasItem != null && hasItem.getItem() != null && hasItem.getItem().getLabel() != null &&
                !hasItem.getItem().getLabel().trim().isEmpty() && ruleDefinition != null) {
            if (ruleDefinition.getItem() == null) {
                ruleDefinition.setItem(new Item());
            }
            if (ruleDefinition.getItem().getLabel() == null) {
                ruleDefinition.getItem().setLabel(hasItem.getItem().getLabel().trim());
            } else {
                ruleDefinition.getItem().setLabel(ruleDefinition.getItem().getLabel() + " / " +
                        hasItem.getItem().getLabel().trim());
            }
        }
    }

    protected static void followDistinguisher(String value, DParent ruleDefinitionDParent) {
        String path = DPathExtractor.createPathFromDParent(ruleDefinitionDParent);
        DistinguisherFollower.followDistinguisher(path, value);
    }

    protected static boolean checkIfConstraintCanBeGeneratedForMinimumMultiplicity(Integer minimumMultiplicity,
                                                                                   RuleDefinition selectedRuleDefinition2) {
        if (minimumMultiplicity == null || selectedRuleDefinition2 == null) {
            return false;
        }
        int elementMinMultiplicity =
                (new UMLElementsManager()).getMinAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition2), null);
        return (minimumMultiplicity.intValue() > elementMinMultiplicity) ||
                (minimumMultiplicity.intValue() > 0 &&
                        RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(selectedRuleDefinition2) ||
                        (minimumMultiplicity.intValue() == 0 && ConformanceType.R.equals(selectedRuleDefinition2.getConformance()))
                );
    }

    protected static boolean checkIfConstraintCanBeGeneratedForMaximumMultiplicity(String maximumMultiplicityString,
                                                                                   RuleDefinition selectedRuleDefinition2) {
        if (maximumMultiplicityString != null && !maximumMultiplicityString.isEmpty()) {
            if (!maximumMultiplicityString.equals("*")) {
                try {
                    int maximumMultiplicityInt = Integer.valueOf(maximumMultiplicityString).intValue();
                    return maximumMultiplicityInt >= 0;
                } catch (NumberFormatException e) {
                    ProblemHandler.handleError("Invalid maximum multiplicity value: " + maximumMultiplicityString,
                            AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getRuleContextDescription() {
        return ruleContextDescription;
    }

    public void setRuleContextDescription(String ruleContextDescription) {
        this.ruleContextDescription = ruleContextDescription;
    }

    public RuleDefinition getSelectedRuleDefinition() {
        return selectedRuleDefinition;
    }

    public void setSelectedRuleDefinition(RuleDefinition selectedRuleDefinition) {
        this.selectedRuleDefinition = selectedRuleDefinition;
    }

    @Override
    public void process(RuleDefinition ruleDefinition, Object... objects) {
        if (objects.length < 1 || !(objects[0] instanceof PackagedElement)) {
            ProblemHandler.handleError("The method RuleDefinitionAnalyzer::process is called with a bad objects attribute",
                    AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
            return;
        }
        if (objects.length > 1) {
            this.ignoreTemplateIdRequirements = (Boolean) objects[1];
        }
        this.selectedRuleDefinition = ruleDefinition;
        boolean isTemplateId = (this.selectedRuleDefinition.getName().matches(".*:templateId") || this.selectedRuleDefinition.getName().matches("templateId"));
        if (this.ignoreTemplateIdRequirements.booleanValue() && this.selectedRuleDefinition.getName() != null && isTemplateId) {
            return;
        }
        this.currentPackagedElement = (PackagedElement) objects[0];
        if(!isTemplateId){
            this.generateDTDistinguisher = (RuleDefinitionDistinguisherUtil.elementShouldBeDistinguisedByDT(this.selectedRuleDefinition, this.selectedRuleDefinition.getParentObject()));
        }
        // catch error in numbered predicates
        if (!RuleDefinitionProblemUtil.verifyIfRuleDefinitionIsTreatable(this.selectedRuleDefinition)) {
            String name = selectedRuleDefinition != null ? selectedRuleDefinition.getName() : null;
            ProblemHandler.handleRuleDefinitionError(selectedRuleDefinition,
                    "The ruleDefinition is not processed because it is not conform to the CDA model of reference, or there are multitude elements "
                            + "with the same name, which cannot be distinguished : " + name,
                    AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
            containsError = true;
            return;
        }
        updateDistinguisherFollowerIfNeeded();

        // Add a method that adds and 'AND' constraint based on datatype to the processing

        super.process(this.selectedRuleDefinition, objects);

//		-- still not done
//		processConformance(ruleDefinition.getConformance());
//		processName(ruleDefinition.getName());


//		--- already done :
//		processName(ruleDefinition.getName());
//		processDatatype(ruleDefinition.getDatatype());
//		processFlexibility(ruleDefinition.getFlexibility());
//		processIsMandatory(ruleDefinition.isIsMandatory());
//		processMaximumMultiplicity(ruleDefinition.getMaximumMultiplicity());
//		processMinimumMultiplicity(ruleDefinition.getMinimumMultiplicity());
//		processElements(RuleDefinitionUtil.getElements(ruleDefinition));
//		processConstraints(RuleDefinitionUtil.getConstraints(ruleDefinition));
//		processVocabularys(ruleDefinition.getVocabulary());
//		processPropertys(ruleDefinition.getProperty());
//		processAttributes(ruleDefinition.getAttribute());
//		processTexts(ruleDefinition.getText());
//		processItem(ruleDefinition.getItem());
//		processChoices(RuleDefinitionUtil.getChoiceDefinitions(ruleDefinition));
//		processIncludes : the includes are ignored
//		processIsClosed(ruleDefinition.isIsClosed());
//		processDefineVariables : nothing to do deprecated
//		processReports : nothing to do, the reports are not aimed to generate validation errors
//		processLets(RuleDefinitionUtil.getLets(ruleDefinition));
//		processAsserts(RuleDefinitionUtil.getAsserts(ruleDefinition));
//		processContains(ruleDefinition.getContains()); : nothing to do, deleted during the flatening
//		processContains : nothing to do, deleted during flatenning

    }

    /**
     *   the ruleDefinition is treatable
     */
    protected void updateDistinguisherFollowerIfNeeded() {
        if (RuleDefinitionProblemUtil.ruleDefinitionNeedsDistinguisher(selectedRuleDefinition)) {
            Pair<String, String> pairDisting = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(selectedRuleDefinition);
            updateListDistinguisherFollower(selectedRuleDefinition, pairDisting);
        }
    }

    /**
     * Take care of the contains attribute in BBR decor element
     * If flatenning took place, it should not enter in this method
     *
     * @param contains string
     */
    @Override
    public void processContains(String contains) {
        if (contains != null && contains.trim().isEmpty()) {
            ProblemHandler.handleRuleDefinitionWarning(this.selectedRuleDefinition, "There are no way to have such situation : "
                            + "the contains attribute are removed from ruleDefinitions during flatenning",
                    AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
        }
    }

    /**
     * Process the contain element to generate OCL constraint
     * At this stage at most one contain element should be remaining, because contain-includes (contain with attribute contain="false") have been
     * replaced by their targeted template during the second flattenization level.
     *
     * @param contains string
     */
    @Override
    public void processContainDefElements(List<ContainDefinition> contains) {
        if (contains != null && !contains.isEmpty() && !this.ignoreTemplateIdRequirements.booleanValue()) {
            // the list should have 0 or 1 element.
            for (ContainDefinition containDefinition : contains) {
                if (checkIfConstraintCanBeGeneratedForMinimumMultiplicity(containDefinition.getMinimumMultiplicity(), this.selectedRuleDefinition)) {
                    fulfillOwnedRuleForConstraintGenerator((new ContainMinimumMultiplicityAnalyzer()), this.currentPackagedElement,
                            OwnedRuleKind.CARDINALITY,
                            (containDefinition.getMinimumMultiplicity().intValue() == 0 && selectedRuleDefinition.getConformance().equals(ConformanceType.R))?
                                    OwnedRuleType.WARNING: OwnedRuleType.ERROR);
                }
                if (checkIfConstraintCanBeGeneratedForMaximumMultiplicity(containDefinition.getMaximumMultiplicity(),
                        this.selectedRuleDefinition)) {
                    fulfillOwnedRuleForConstraintGenerator((new ContainMaximumMultiplicityAnalyzer()), this.currentPackagedElement,
                            OwnedRuleKind.CARDINALITY);
                }
                if (containDefinition.getIsMandatory() != null && containDefinition.getIsMandatory().booleanValue()) {
                    fulfillOwnedRuleForConstraintGenerator((new ContaintMandatoryAnalyzer()), this.currentPackagedElement,
                            OwnedRuleKind.MANDATORY);
                }
            }
        }
    }

    @Override
    public void processLets(List<Let> lets) {
        // lets are included in the asserts when they exists
        // nothing to do more
    }

    @Override
    public void process_asserts(List<Assert> decorAsserts) {
        if (decorAsserts != null) {
            for (Assert assert1 : decorAsserts) {
                (new AssertAnalyzer()).process(assert1, this.currentPackagedElement, this.listGeneratedOwnedRule,this.generateDTDistinguisher);
            }
        }
    }

    @Override
    public void processIsClosed(Boolean isClosed) {
        boolean isCompletelyCosed = Boolean.valueOf(RuleDefinitionUtil.ruleDefinitionIsCompletelyClosed(this.selectedRuleDefinition));

        if (isCompletelyCosed && this.selectedRuleDefinition != null) {
            Set<String> listDefinedSubElements = RuleDefinitionUtil.getListAllowedSubElements(this.selectedRuleDefinition);
            Set<String> listCompleteElements = RuleDefinitionUtil.getListCompleteSubElements(this.selectedRuleDefinition);
            if (listCompleteElements != null) {
                for (String subElement : listCompleteElements) {
                    if (listDefinedSubElements != null && !listDefinedSubElements.contains(subElement)) {
                        IsClosedElementGenerator isgen = new IsClosedElementGenerator();
                        isgen.setSubElement(subElement);
                        fulfillOwnedRuleForConstraintGenerator(isgen, this.currentPackagedElement, OwnedRuleKind.CLOSED);
                    } else if (listDefinedSubElements != null && listDefinedSubElements.contains(subElement) &&
                            subElementHasUsefulDistinguisher(subElement, selectedRuleDefinition)) {
                        List<RuleDefinition> listSubElSames = RuleDefinitionUtil.getElementsByName(this.selectedRuleDefinition, subElement);
                        Set<String> setDistinguishers = new TreeSet<>();
                        for (RuleDefinition ruleDefinition : listSubElSames) {
                            String disct = OCLGenerator.getExtraDistinguisherIfNeeded(ruleDefinition);
                            setDistinguishers.add(disct);
                        }
                        IsClosedElWithDistGenerator isgen = new IsClosedElWithDistGenerator();
                        isgen.setSubElement(subElement);
                        isgen.setSetDistinguishers(setDistinguishers);
                        fulfillOwnedRuleForConstraintGenerator(isgen, this.currentPackagedElement, OwnedRuleKind.CLOSED);
                    }
                }
            }
        }
    }

    /**
     * if we have a list of distinguisher then we have a subelement that can be extracted,
     * and the other subelements with the same name can be deleted from isClosed ruleDefinition
     *
     * @param subElementName name of the sub element
     * @param parentRuleDefinition ruleDefinition of ther parent
     * @return boolean
     */
    protected boolean subElementHasUsefulDistinguisher(String subElementName, RuleDefinition parentRuleDefinition) {
        List<PathWithDT> lpwdt = RuleDefinitionUtil.getJavaPath(parentRuleDefinition);
        String path = PathWithDTUtil.extractTypedPathFromListPathWithDT(lpwdt);
        Integer max = Integer.valueOf(UMLLoader.getMaxAttribute(path, subElementName));
        if (max == null || max.intValue() == 0 || max.intValue() == 1) {
            return false;
        }
        List<RuleDefinition> listSubElSames = RuleDefinitionUtil.getElementsByName(parentRuleDefinition, subElementName);
        if (listSubElSames == null || listSubElSames.isEmpty()) {
            return false;
        }
        for (RuleDefinition ruleDefinition : listSubElSames) {
            Boolean hasDist = RuleDefinitionProblemUtil.verifyThatRuleDefinitionHasUniqueDistinguisher(ruleDefinition);
            if (!hasDist.booleanValue()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void processIncludes(List<IncludeDefinition> includes) {
        if (includes != null && !includes.isEmpty()) {
            ProblemHandler.handleRuleDefinitionWarning(this.selectedRuleDefinition, "There are no way to have such situation : "
                            + "the includes elements are ignored in ruleDefinitions",
                    AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
        }
    }

    @Override
    public void processItem(Item item) {
        Set<String> setItems = ItemUtil.extractRelatedItems(this.selectedRuleDefinition);
        if (setItems != null) {
            for (OwnedRule or : this.listGeneratedOwnedRule) {
                TamlHandler.handleAllTAML(setItems, or);
            }
        }
    }

    @Override
    public void processChoices(List<ChoiceDefinition> choices) {
        if (choices != null) {
            for (ChoiceDefinition choiceDefinition : choices) {
                (new ChoiceDefinitionAnalyzer()).process(choiceDefinition, this.currentPackagedElement, this.ignoreTemplateIdRequirements);
            }
        }
    }

    @UsedByArtDecor
    @Override
    public void processTexts(List<String> text) {
        if (text != null && !text.isEmpty()) {
            (new TextAnalyzer()).process(text, this.selectedRuleDefinition, this.currentPackagedElement,
                    this.listGeneratedOwnedRule);
        }
    }

    @UsedByArtDecor
    @Override
    public void processAttributes(List<Attribute> attributes) {
        if (attributes != null) {
            for (Attribute attribute : attributes) {
                (new AttributeAnalyzer()).process(attribute, this.currentPackagedElement);
            }
        }
    }

    @UsedByArtDecor // used in elga
    @Override
    public void processPropertys(List<Property> properties) {
        if (properties != null && !properties.isEmpty()) {
            (new PropertyAnalyzer()).process(properties, this.currentPackagedElement, this.listGeneratedOwnedRule);
        }
    }

    @UsedByArtDecor
    @Override
    public void processVocabularys(List<Vocabulary> vocabulary) {
        if (VocabularyUtil.isVocabularyListUseful(vocabulary,
                RuleDefinitionUtil.getParentTemplateDefinition(this.selectedRuleDefinition).getParentObject().getParentObject())) {
            if (RuleDefinitionUtil.verifyIfRDIsACodeDT(this.selectedRuleDefinition)) {
                RDVocabularyAnalyzer rdVocabularyAnalyzer = new RDVocabularyAnalyzer();
                if (rdVocabularyAnalyzer.generateConstraintOwnedRuleType(this.selectedRuleDefinition)!=null){
                    fulfillOwnedRuleForConstraintGenerator(rdVocabularyAnalyzer, this.currentPackagedElement, OwnedRuleKind.VOCABULARY,
                            rdVocabularyAnalyzer.generateConstraintOwnedRuleType(this.selectedRuleDefinition));
                    Decor dec = RuleDefinitionUtil.getParentTemplateDefinition(this.selectedRuleDefinition).getParentObject().getParentObject();
                    if (VocabularyUtil.hasExceptionValues(vocabulary, dec)) {
                        fulfillOwnedRuleForConstraintGenerator(new RDExceptionAnalyzer(), this.currentPackagedElement, OwnedRuleKind.VOCABULARY);
                    }
                }
            } else {
                ProblemHandler.handleRuleDefinitionError(this.selectedRuleDefinition,
                        "The type of the rule is not part of {CE, CD, CV, CS}", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
                containsError = true;
            }
        }
    }

    @UsedByArtDecor
    @Override
    public void processElements(List<RuleDefinition> elements) {
        if (elements != null) {
            for (RuleDefinition ruleDefinition : elements) {
                RuleDefinitionAnalyzer rda = new RuleDefinitionAnalyzer();
                rda.process(ruleDefinition, this.currentPackagedElement, this.ignoreTemplateIdRequirements);
            }
        }
    }

    @UsedByArtDecor
    @Override
    public void processMinimumMultiplicity(Integer minimumMultiplicity) {
        if (checkIfConstraintCanBeGeneratedForMinimumMultiplicity(minimumMultiplicity, this.selectedRuleDefinition)) {
            if (!(this.selectedRuleDefinition.getParentObject() instanceof TemplateDefinition)) {
                fulfillOwnedRuleForConstraintGenerator((new RDMinimumMultiplicityAnalyzer(this.generateDTDistinguisher)), this.currentPackagedElement, OwnedRuleKind.CARDINALITY,
                        (minimumMultiplicity.intValue() == 0 && selectedRuleDefinition.getConformance().equals(ConformanceType.R))?
                                OwnedRuleType.WARNING: OwnedRuleType.ERROR);
            } else {
                ProblemHandler.handleRuleDefinitionInfo(selectedRuleDefinition,
                        "The minimumMultiplicity attribute is not processed because the ruleDefinition is of the parent template",
                        (new RDMinimumMultiplicityAnalyzer()).getProcessIdentifier());
            }
        }
        else{
            ProblemHandler.handleRuleDefinitionInfo(selectedRuleDefinition,
                    "The minimumMultiplicity attribute doesn't match specifications",
                    (new RDMinimumMultiplicityAnalyzer()).getProcessIdentifier());
        }

    }

    @UsedByArtDecor
    @Override
    public void processMaximumMultiplicity(String maximumMultiplicityString) {
        if (this.selectedRuleDefinition.getParentObject() instanceof TemplateDefinition) {
            ProblemHandler.handleRuleDefinitionInfo(selectedRuleDefinition,
                    "The maximumMultiplicity attribute is not processed because the ruleDefinition is of the parent template",
                    (new RDMaximumMultiplicityAnalyzer()).getProcessIdentifier());
            return;
        }
        Integer maximumMultiplicity = MultiplicityUtil.extractMaximumMultiplicity(maximumMultiplicityString);
        if (maximumMultiplicity == null) {
            return;
        }
        int elementMaxMultiplicity = extractElementMaxMultiplicity(this.selectedRuleDefinition);
        boolean isPossibleToHaveConstraint = maximumMultiplicity.intValue() < elementMaxMultiplicity;
        if (isPossibleToHaveConstraint ||  (selectedRuleDefinition.getParentObject() instanceof RuleDefinition
                && ((RuleDefinition) selectedRuleDefinition.getParentObject()).getParentObject() instanceof ChoiceDefinition )) {
            fulfillOwnedRuleForConstraintGenerator((new RDMaximumMultiplicityAnalyzer(this.generateDTDistinguisher)), this.currentPackagedElement, OwnedRuleKind.CARDINALITY);
        }
    }

    @UsedByArtDecor
    @Override
    public void processIsMandatory(Boolean isMandatory) {
        if (isMandatory != null && isMandatory.booleanValue()) {
            // TODO problem when the template is a containment
            // think to update this later
            if (!(this.selectedRuleDefinition.getParentObject() instanceof TemplateDefinition)) {
                String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(this.selectedRuleDefinition);
                if (UMLLoader.isAnAttribute(typeName + ".nullFlavor").booleanValue()) {
                    fulfillOwnedRuleForConstraintGenerator(new RDMandatoryAnalyzer(), this.currentPackagedElement, OwnedRuleKind.MANDATORY);
                } else {
                    ProblemHandler.handleRuleDefinitionInfo(selectedRuleDefinition,
                            "The mandatory attribute is not processed because it has no nullFlavor attribute",
                            (new RDMandatoryAnalyzer()).getProcessIdentifier());
                }
            } else {
                ProblemHandler.handleRuleDefinitionInfo(selectedRuleDefinition,
                        "The mandatory attribute is not processed because it is of the parent template",
                        (new RDMandatoryAnalyzer()).getProcessIdentifier());
            }
        }
    }

    @UsedByArtDecor
    @Override
    public void processDatatype(QName datatype) {
        if (datatype == null) {
            return;
        }
        if(Boolean.TRUE.equals(this.generateDTDistinguisher)){
            ImportElementHandler.handleAddingElement(DTUtils.getUMLDatatype(datatype.getLocalPart()));
            return;
        }
        String dt = datatype.getLocalPart();
        String typeRD = RuleDefinitionUtil.getUMLTypeName(selectedRuleDefinition);
        if (dt != null) {

            if (DTUtils.datatypesContainDT(dt)) {
                if (DTUtils.constrainedDTIsAnExtension(dt, typeRD)) {
                    fulfillOwnedRuleForConstraintGenerator((new RDDatatypeAnalyzer()), this.currentPackagedElement, OwnedRuleKind.DATATYPE);
                    ImportElementHandler.handleAddingElement(DTUtils.getUMLDatatype(dt));
                } else {
                    String realDTConstrained = DTUtils.getUMLDatatype(dt);
                    if (realDTConstrained == null || (!realDTConstrained.equals(typeRD) && !DTUtils.ignoreDTExtensionError(dt))) {
                        ProblemHandler.handleRuleDefinitionError(selectedRuleDefinition,
                                "A DT extension has been done but no inheritence between the original one exists : " + realDTConstrained + " # " +
                                        typeRD,
                                (new RDDatatypeAnalyzer()).getProcessIdentifier());
                        containsError = true;
                    }
                }
            } else {
                ProblemHandler.handleRuleDefinitionError(selectedRuleDefinition,
                        "The datatypes specified is not part of possible datatypes (" + dt + ")",
                        (new RDDatatypeAnalyzer()).getProcessIdentifier());
                containsError = true;
            }
        }
    }

    protected void fulfillOwnedRuleForConstraintGenerator(ConstraintGenerator constraintGenerator, PackagedElement pe, OwnedRuleKind ownedRuleKind) {
        fulfillOwnedRuleForConstraintGenerator(constraintGenerator, pe, ownedRuleKind, null);
    }

    protected void fulfillOwnedRuleForConstraintGenerator(ConstraintGenerator constraintGenerator, PackagedElement pe, OwnedRuleKind ownedRuleKind, OwnedRuleType ownedRuleType) {
        if (constraintGenerator == null) {
            return;
        }
        OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
        ownedRule.setConstrainedElement(pe.getId());
        String specification = constraintGenerator.generateOCLConstraint(this.selectedRuleDefinition);
        String comment = constraintGenerator.generateCommentConstraint(this.selectedRuleDefinition) +
                ItemCommentGenerator.generateItemComment(selectedRuleDefinition);
        ownedRule.getSpecification().setBody(specification);
        ownedRule.getOwnedComment().setBody(comment);
        ownedRule.setOwnedRuleKind(ownedRuleKind);
        if (ownedRuleType != null){
            ownedRule.setOwnedRuleType(ownedRuleType);
        }
        pe.getOwnedRule().add(ownedRule);
        this.listGeneratedOwnedRule.add(ownedRule);

    }


    protected int extractElementMaxMultiplicity(RuleDefinition selectedRuleDefinition) {
        int elementMaxMultiplicity =
                (new UMLElementsManager()).getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition), null);
        if (elementMaxMultiplicity == -1) {
            elementMaxMultiplicity = MultiplicityUtil.MAX_INTEGER;
        }
        return elementMaxMultiplicity;
    }

    protected boolean ignoreMinCardinality() {
        return this.selectedRuleDefinition != null &&
                this.selectedRuleDefinition.getParentObject() != null &&
                this.selectedRuleDefinition.getParentObject() instanceof ChoiceDefinition;
    }

    /**
     * This method is created to generate OCL rules and returned instead of add it to the global package.
     * Mainly used with choices to construct a general rule
     * This method process all sub elements too, and clean the temporary used package, so it's "Stateless"
     * @param ruleDefinition the elements to generate self and childs rules
     * @param pe temporary package used to grab all sub elements rules
     * @return returns a list of ownedRules for the ruleDefinition itself and sub elements
     */
    @Deprecated(since = "3.1.0", forRemoval = true)
    public List<OwnedRule> processAndReturn(RuleDefinition ruleDefinition, PackagedElement pe){
        if(pe == null){
            pe = PackagedElementUtil.initPackagedElement();
        }
        this.currentPackagedElement = pe;
        this.selectedRuleDefinition = ruleDefinition;
        super.process(ruleDefinition,this.currentPackagedElement);

        if(pe.getOwnedRule().size() == 0){
            return null;
        }

        //Clean TAML
        for(OwnedRule or:pe.getOwnedRule()){
            TamlHandler.removeTAML(or);
        }

        // we need to clear TAML before exiting in case of error
        if(containsError){
            return null;
        }


        List<OwnedRule> clone = new ArrayList<>(this.currentPackagedElement.getOwnedRule());

        //Clean temporary used package
        this.listGeneratedOwnedRule.clear();
        currentPackagedElement.getPackagedElements().clear();
        currentPackagedElement.getOwnedRule().clear();
        pe.getPackagedElements().clear();
        pe.getOwnedRule().clear();

        return clone;

    }


}
