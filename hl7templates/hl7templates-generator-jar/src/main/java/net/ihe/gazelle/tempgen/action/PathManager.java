package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.temp.mapping.utils.TemplatesTypeMappingUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PathManager {
	
	
	public String findPathTypeName(DParent parentPath) {
		return findPathTypeName(parentPath, parentPath.size() - 1);
	}

	/**
	 * return the UML name of a path Example : //cda:observation/cda:code -&gt; CD
	 * 
	 * @param parentPath path to parent
	 * @param index index
	 * @return the UML name of a path Example : //cda:observation/cda:code -&gt; CD
	 */
	public String findPathTypeName(DParent parentPath, int index) {
		if (index == 0) {
			if (parentPath instanceof DElement &&((DElement)parentPath).getExtendedType() != null) {
				return ((DElement)parentPath).getExtendedType();
			}
			return TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists(parentPath.getName(), null);
		}

		StringBuilder sb = new StringBuilder("");
		int i = 0;
		DParent pointer = parentPath;
		while (i <= index && pointer != null) {
			String name = extractNameFromPointer(i++, pointer);
			if (sb.length()>0) {
				sb.append(".");
			}
			sb.append(name);
			if (pointer instanceof DElement) {
				pointer = ((DElement) pointer).getFollowingAttributeOrElement();
			}
		}
		return UMLLoader.getPropertyTypeName(sb.toString());
	}

	protected String extractNameFromPointer(int i, DParent pointer) {
		String name = pointer.getName();
		if (i == 0 && pointer instanceof DElement) {
			DElement elP = (DElement)pointer;
			if (elP.getExtendedType() != null && !elP.getExtendedType().trim().isEmpty()) {
				name = elP.getExtendedType();
			}
			else {
				name = TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists(name, null);
			}
		}
		return name;
	}

}
