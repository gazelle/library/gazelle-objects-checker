package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrDeterminerCodeAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.determinerCode != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "determinerCode", 
				attribute.getDeterminerCode(), getProcessIdentifier());
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "determinerCode", attribute.getDeterminerCode());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_DETERMINERCODE_PROCESS.getValue();
	}

}
