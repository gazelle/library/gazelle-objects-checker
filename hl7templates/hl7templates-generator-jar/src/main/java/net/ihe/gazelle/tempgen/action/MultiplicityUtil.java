package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class MultiplicityUtil {
	
	public static final int MAX_INTEGER = 1000000000;
	
	private MultiplicityUtil() {}
	
	public static Integer extractMaximumMultiplicity(String maximumMultiplicityString) {
		Integer maximumMultiplicity = null;
		if (maximumMultiplicityString != null){
			if (maximumMultiplicityString.equals("*")){
				maximumMultiplicity = Integer.valueOf(MAX_INTEGER);
			}
			else {
				try {
					maximumMultiplicity = Integer.valueOf(maximumMultiplicityString);
				}
				catch(NumberFormatException e) {
					ProblemHandler.handleError("One of the maximum multiplicity elements has a value which is "
							+ "not an integer or * : " + 
							maximumMultiplicityString, AnalyzerEnum.RD_MAX_MULTIPLICITY_PROCESS.getValue());
				}
			}
		}
		return maximumMultiplicity;
	}

}
