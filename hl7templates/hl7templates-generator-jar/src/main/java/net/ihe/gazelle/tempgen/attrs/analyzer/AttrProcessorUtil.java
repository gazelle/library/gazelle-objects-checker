package net.ihe.gazelle.tempgen.attrs.analyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class AttrProcessorUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(AttrProcessorUtil.class);
	
	private AttrProcessorUtil() {
		// private constructor
	}
	
	public static String generateCommentConstraint(Attribute attribute, String name, String value) {
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + parentPath + ", the attribute " + name + " SHALL have the value '" + 
					value + "' if present";
	}
	
	public static String generateCommentConstraint(Attribute attribute, String name, Boolean value) {
		return generateCommentConstraint(attribute, name, String.valueOf(value));
	}
	
	public static String generateOCLConstraintForEnumerationAttribute(Attribute attribute, String name, String value, String process){
		Integer max = AttributeUtil.getUMLMaxAttribute(attribute, name);
		return generateOCLConstraintForEnumerationAttribute(attribute, name, value, process, max);
	}
	
	private static String generateOCLConstraintForEnumerationAttribute(Attribute attribute, String name, String value, String process, Integer maxParam){
		Integer max = maxParam;
		if (max == null) {
			max = Integer.valueOf(1);
		}
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		String res = parentOCL;
		String type = AttributeUtil.getUMLTypeName(attribute, name);
		boolean valueIsIncludedInType = UMLLoader.checkIfEnumerationContainsValue(type, value);
		if (!valueIsIncludedInType){
			ProblemHandler.handleAttributeError(attribute, name, value, 
					"The value " + value + " is not part of the enumeration type " + type, process);
			return "true";
		}
		if (max.intValue() == 1){
			if (res.equals("self")){
				res = "self." + name + ".oclIsUndefined() or self." + name + "=" + type + "::" + 
						UMLLoader.getUMLNameForLitteral(type, value);
			}
			else {
				res = res + "->forAll(" + name + ".oclIsUndefined() or " + name + "=" + type + "::" + 
						UMLLoader.getUMLNameForLitteral(type, value) + ")";
			}
			ImportElementHandler.handleAddingElement(type);
		}
		else if (max.intValue() == -1){
			res = res + "." + name + OCLCleaner.generateRejectOcl(false) + "->forAll(aa | aa=" + type + "::" + 
					UMLLoader.getUMLNameForLitteral(type, value) + ")";
			ImportElementHandler.handleAddingElement(type);
		}
		ImportElementHandler.handleAddingElement(type);
		return res;
	}
	
	public static String generateOCLConstraintFromBooleanOrIntegerAttribute(Attribute attribute, String name, String value){
		Integer max = AttributeUtil.getUMLMaxAttribute(attribute, name);
		return generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, name, value, max);
	}
	
	private static String generateOCLConstraintFromBooleanOrIntegerAttribute(Attribute attribute, String name, String value, Integer maxParam){
		int max = maxParam == null?1: maxParam.intValue();
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		String res = parentOCL;
		if (max == 1){
			if (res.equals("self")){
				res = "self." + name + ".oclIsUndefined() or self." + name + "=" + value;
			}
			else {
				res = res + "->forAll(" + name + ".oclIsUndefined() or " + name + "=" + value + ")";
			}
		}
		else if (max == -1){
			res = res + "." + name + OCLCleaner.generateRejectOcl(false) + "->forAll(aa | aa=" + value + ")";
		}
		return res;
	}
	
	public static String generateOCLConstraintFromStringAttribute(Attribute attribute, String name, String value){
		Integer max = AttributeUtil.getUMLMaxAttribute(attribute, name);
		return generateOCLConstraintFromStringAttribute(attribute, name, value, max);
	}
	
	private static String generateOCLConstraintFromStringAttribute(Attribute attribute, String name, String value, Integer maxParam) {
		int max = maxParam == null?1: maxParam.intValue();
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		String res = parentOCL;
		if (max == 1){
			if (res.equals("self")){
				res = res + "." + name + ".oclIsUndefined() or self." + name + "='" + value + "'";
			}
			else {
				res = res + "->forAll(" + name + ".oclIsUndefined() or " + name + "='" + value + "')";
			}
		}
		else if (max == -1){
			res = res + "." + name + OCLCleaner.generateRejectOcl(false) + "->forAll(aa | aa='" + value + "')";
		}
		return res;
	}
	
	public static String generateOCLConstraintFromNameAndValue(Attribute attribute, String name, String value, String process) {
		String type = AttributeUtil.getUMLTypeName(attribute, name);
		Integer max = AttributeUtil.getUMLMaxAttribute(attribute, name);
		
		if (max.intValue() != 1 && max.intValue() != -1){
			log.error("The maximum attribute is not what is waited (AttrValueAnalyzer-generateOCLConstraintFromNameAndValue): " 
					+ AttributeUtil.getJavaPath(attribute, name));
		}
		if (type.equals(UMLPrimitiveType.STRING.getValue())){
			return generateOCLConstraintFromStringAttribute(attribute, name, value, max);
		}
		else if(type.equals(UMLPrimitiveType.BOOLEAN.getValue()) || 
				type.equals(UMLPrimitiveType.INTEGER.getValue()) ||
				type.equals(UMLPrimitiveType.DOUBLE.getValue())){
			 return generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, name, value, max);
		}
		else {
			return generateOCLConstraintForEnumerationAttribute(attribute, name, value, process, max);
		}
	}

}
