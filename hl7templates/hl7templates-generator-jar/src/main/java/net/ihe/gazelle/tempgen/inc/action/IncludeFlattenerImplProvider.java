package net.ihe.gazelle.tempgen.inc.action;

import net.ihe.gazelle.tempapi.interfaces.ChoiceDefinitionProcessor;
import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.interfaces.TemplateDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncludeFlattenerImplProvider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(RuleDefinitionProcessor.class)){
			return (T) new IncFlatRuleDefinition();
		}
		else if (t.equals(TemplateDefinitionProcessor.class)) {
			return (T) new IncFlatTemplateDefinition();
		}
		else if (t.equals(ChoiceDefinitionProcessor.class)) {
			return (T) new IncFlatChoiceDefinition();
		}
		return null;
	}
	
}
