package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrUnitAnalyzer implements ATConstraintGenerator {

	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintFromStringAttribute(attribute, "unit", attribute.getUnit());
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "unit", attribute.getUnit());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_UNIT_PROCESS.getValue();
	}

}
