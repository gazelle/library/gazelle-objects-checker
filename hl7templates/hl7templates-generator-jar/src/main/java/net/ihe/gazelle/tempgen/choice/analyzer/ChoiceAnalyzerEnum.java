package net.ihe.gazelle.tempgen.choice.analyzer;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public enum ChoiceAnalyzerEnum {
	
	CH_MAX_PROCESS("CH_MAX_PROCESS"), 
	CH_MIN_PROCESS("CH_MIN_PROCESS");
	
	private String value;

	private ChoiceAnalyzerEnum(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

}
