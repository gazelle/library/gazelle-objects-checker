package net.ihe.gazelle.tempgen.ehdsi.scripts;


import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

import java.util.Arrays;
import java.util.List;

public class TemplateL1Processor extends TemplateDefinitionProcessorImpl {

    private TemplateDefinition currentTemplateDefinition;

    //eHDSI ePrescription, eHDSI eDispensation and eHDSI Patient Summary are CDA L3 Templates:
    // they must be excluded from the L1 validators
    private List<String> templateOidToExclude = Arrays.asList("1.3.6.1.4.1.12559.11.10.1.3.1.1.1",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.2",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.3");

    @Override
    public void process(TemplateDefinition t, Object... objects) {
        this.currentTemplateDefinition = t;
        if (this.currentTemplateDefinition != null && this.currentTemplateDefinition.getId() != null
                && templateOidToExclude.contains(this.currentTemplateDefinition.getId())) {
            EHDSIL1Transformer.templatesToDelete.add(this.currentTemplateDefinition);
        }
    }
}
