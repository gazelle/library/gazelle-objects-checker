package net.ihe.gazelle.tempgen.inc.action;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncludeStats {
	
	private Set<String> listIncludedTemplates = new HashSet<>();
	
	private int numberIntegrated = 0;

	public Set<String> getListIncludedTemplates() {
		if (listIncludedTemplates == null) {
			listIncludedTemplates = new HashSet<>();
		}
		return listIncludedTemplates;
	}

	public void setListIncludedTemplates(Set<String> listIncludedTemplates) {
		this.listIncludedTemplates = listIncludedTemplates;
	}

	public int getNumberIntegrated() {
		return numberIntegrated;
	}

	public void setNumberIntegrated(int numberIntegrated) {
		this.numberIntegrated = numberIntegrated;
	} 

}
