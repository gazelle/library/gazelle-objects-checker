package net.ihe.gazelle.tempgen.inc.checker;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncCheckRuleDefinition extends RuleDefinitionProcessorImpl {
	
	private Map<String, Set<String>> mapIncludedTemplates;
	
	private RuleDefinition currentRuleDefinition;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(RuleDefinition t, Object... objects) {
		this.currentRuleDefinition = t;
		mapIncludedTemplates = (Map<String, Set<String>>) objects[0];
		super.process(t, objects);
	}

	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		super.processIncludes(includes);
		for (IncludeDefinition includeDefinition : includes) {
			TemplateDefinition td = RuleDefinitionUtil.getParentTemplateDefinition(this.currentRuleDefinition);
			Rules rules = td.getParentObject();
			String ref = RulesUtil.getReferenceTemplateIdentifier(rules, includeDefinition.getRef());
			if (ref != null) {
				if (this.mapIncludedTemplates.get(td.getId()) == null){
					this.mapIncludedTemplates.put(td.getId(), new  TreeSet<String>());
				}
				this.mapIncludedTemplates.get(td.getId()).add(ref);
			}
		}
	}

}
