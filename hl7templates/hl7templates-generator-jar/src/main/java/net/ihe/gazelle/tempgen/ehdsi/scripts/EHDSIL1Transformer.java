package net.ihe.gazelle.tempgen.ehdsi.scripts;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class EHDSIL1Transformer {

    private static Logger log = LoggerFactory.getLogger(EHDSIL1Transformer.class);

    public static ArrayList<TemplateDefinition> templatesToDelete = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException, JAXBException {

        String bbrPath = "DEFINE_YOUR_DEFAULT_PATH_HERE";
        String outputBbrPath = "DEFINE_YOUR_OUT_PATH_HERE";

        //Override input bbr path if exists as param
        if (args.length > 0) {
            bbrPath = args[0];
        }

        //Override output bbr path if exists as param
        if (args.length > 1) {
            outputBbrPath = args[1];
        }

        Decor dec = DecorMarshaller.loadDecor(bbrPath);
        DecorVisitor.INSTANCE.visitAndProcess(dec, new TemplateL1Provider());

        for (TemplateDefinition template : templatesToDelete) {
            if (template.getId() != null && !template.getId().isEmpty() && template.getParentObject() != null) {
                template.getParentObject().getTemplateAssociationOrTemplate().remove(template);
            }
        }

        DecorMarshaller.marshallDecor(dec, new FileOutputStream(outputBbrPath));
    }

}
