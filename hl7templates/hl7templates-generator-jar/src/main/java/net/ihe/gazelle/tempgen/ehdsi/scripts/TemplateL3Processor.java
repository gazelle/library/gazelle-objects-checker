package net.ihe.gazelle.tempgen.ehdsi.scripts;


import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

import java.util.Arrays;
import java.util.List;

public class TemplateL3Processor extends TemplateDefinitionProcessorImpl {

    private TemplateDefinition currentTemplateDefinition;

    //eHDSI ePrescription PDF embedded, eHDSI Patient Summary PDF embedded, eHDSI OrCD Laboratory Result,
    // eHDSI OrCD Hospital Discharge Report, eHDSI OrCD Medical Imaging Report
    // and eHDSI OrCD Medical Imaging Reportare CDA L1 Templates: they must be excluded from the L1 validators
    private List<String> templateOidToExclude = Arrays.asList("1.3.6.1.4.1.12559.11.10.1.3.1.1.6",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.7",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.8",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.9",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.10",
            "1.3.6.1.4.1.12559.11.10.1.3.1.1.11");

    @Override
    public void process(TemplateDefinition t, Object... objects) {
        this.currentTemplateDefinition = t;
        if (this.currentTemplateDefinition != null && this.currentTemplateDefinition.getId() != null
                && templateOidToExclude.contains(this.currentTemplateDefinition.getId())) {
            EHDSIL3Transformer.templatesToDelete.add(this.currentTemplateDefinition);
        }
    }
}
