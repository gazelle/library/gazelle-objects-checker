package net.ihe.gazelle.tempgen.ehdsi.scripts;

import net.ihe.gazelle.tempapi.impl.VocabularyProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VocabularyFriendlyProcessorPivot extends VocabularyProcessorImpl {

	private Vocabulary currentVocabulary;

	@Override
	public void process(Vocabulary t, Object... objects) {
		this.currentVocabulary = t;
		if (this.currentVocabulary != null && this.currentVocabulary.getValueSet() != null && this.currentVocabulary.getValueSet().contains(EHDSIPivotTransformer.ROOT)){
			// This is previous behavior of the transorfmer. After discussion on ticket DGSANTE-39, all vocabulary elements should be ignored in
			// friendly validators if a value-set inclusion is defined
//			) {
//			this.currentVocabulary.setFlexibility("dynamic");
			if (this.currentVocabulary.getParentObject() != null){
				EHDSIFriendlyTransformer.ml_objectsWithVocabularyToDelete.add(this.currentVocabulary.getParentObject());
			}
		}
	}

}
