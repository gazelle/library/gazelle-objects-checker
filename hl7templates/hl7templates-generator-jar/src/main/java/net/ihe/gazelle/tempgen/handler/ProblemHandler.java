package net.ihe.gazelle.tempgen.handler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ProblemHandler {
	
	private static Set<Notification> diagnostic = new HashSet<>();
	
	private ProblemHandler() {}
	
	public static Set<Notification> getDiagnostic() {
		return diagnostic;
	}
	
	private static void addNotificationToDiagnostic(Notification notif){
		if (notif != null){
			boolean exists = false;
			for (Notification not : diagnostic) {
				if (equalsNotif(notif, not)){
					exists = true;
				}
			}
			if (!exists){
				diagnostic.add(notif);
			}
		}
	}
	
	private static boolean equalsNotif(Notification notif, Notification other) {
		if (notif == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (notif.getDescription() == null) {
			if (other.getDescription() != null) {
				return false;
			}
		} else if (!notif.getDescription().equals(other.getDescription())) {
			return false;
		}
		if (notif.getLocation() == null) {
			if (other.getLocation() != null) {
				return false;
			}
		} else if (!notif.getLocation().equals(other.getLocation())) {
			return false;
		}
		if (notif.getTest() == null) {
			if (other.getTest() != null) {
				return false;
			}
		} else if (!notif.getTest().equals(other.getTest())) {
			return false;
		}
		return true;
	}
	
	public static void handleError(String message, String process){
		net.ihe.gazelle.validation.Error err = new Error(); 
		err.setDescription(message);
		err.setTest(process + " (" + findCallerMethod() + ")");
		addNotificationToDiagnostic(err);
	}
	
	public static void handleAttributeError(Attribute attribute, String attrName, String attrValue, String message, String process){
		net.ihe.gazelle.validation.Error err = new Error(); 
		createNotification(attribute, attrName, attrValue, message, process, err);
	}

	public static void handleRuleDefinitionError(RuleDefinition ruleDefinition, String message, String process){
		net.ihe.gazelle.validation.Error err = new Error(); 
		createNotification(ruleDefinition, message, process, err);
	}
	
	public static void handleRuleDefinitionWarning(RuleDefinition ruleDefinition, String message, String process){
		net.ihe.gazelle.validation.Warning err = new Warning(); 
		createNotification(ruleDefinition, message, process, err);
	}
	
	public static void handleRuleDefinitionInfo(RuleDefinition ruleDefinition, String message, String process){
		net.ihe.gazelle.validation.Info err = new Info(); 
		createNotification(ruleDefinition, message, process, err);
	}

	private static void createNotification(RuleDefinition ruleDefinition, String message, String process, net.ihe.gazelle.validation.Notification err) {
		if (ruleDefinition != null){
			DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
			String dpath = DPathExtractor.createPathFromDParent(dparent);
			err.setLocation(dpath);
		}
		err.setDescription(message);
		err.setTest(process + " (" + findCallerMethod() + ")");
		addNotificationToDiagnostic(err);
	}
	
	private static void createNotification(Attribute attribute, String attrName, String attrValue, String message, String process, net.ihe.gazelle.validation.Notification err) {
		if (attribute != null){
			DParent dparent = AttributeUtil.getDParentOfAttibute(attribute, attrName, attrValue);
			String dpath = DPathExtractor.createPathFromDParent(dparent);
			err.setLocation(dpath);
		}
		err.setDescription(message);
		err.setTest(process + " (" + findCallerMethod() + ")");
		addNotificationToDiagnostic(err);
	}
	
	public static Notification findNotification(String notif){
		for (Notification not : diagnostic) {
			if (not.getDescription().contains(notif)){
				return not;
			}
		}
		return null;
	}
	
	public static List<Notification> findNotifications(String notif){
		List<Notification> lnot = new ArrayList<>();
		if (notif != null) {
			for (Notification not : diagnostic) {
				if (not.getDescription().contains(notif)){
					lnot.add(not);
				}
			}
		}
		return lnot;
	}
	
	private static String findCallerMethod(){
		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		if (stackTraceElements.length>4){
			StackTraceElement method = stackTraceElements[4];
			String classeName = method.getClassName();
			String methodName = method.getMethodName();
			int lineNumber = method.getLineNumber();
			return classeName + "." + methodName + "( " + method.getFileName() + " : " + lineNumber + " )";
		}
		return null;
	}

}
