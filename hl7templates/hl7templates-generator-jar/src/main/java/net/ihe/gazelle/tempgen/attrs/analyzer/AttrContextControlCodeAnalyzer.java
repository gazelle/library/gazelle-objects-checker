package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrContextControlCodeAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.contextControlCode != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "contextControlCode",
					attribute.getContextControlCode(), getProcessIdentifier());
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "contextControlCode", attribute.getContextControlCode());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_CONTEXTCONTROLCODE_PROCESS.getValue();
	}

}
