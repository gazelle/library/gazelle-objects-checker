package net.ihe.gazelle.tempgen.action;

import java.util.List;

import net.ihe.gazelle.tempmodel.distinguisher.Distinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.distinguisher.utils.DistinguisherUtil;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * This class cannot be marked as final because extended by test class
 *
 */
public class OwnedRuleManager {
	
	public static String convertMultiplicity(Integer multi){
		if (multi.intValue() == 1){
			return "ONE";
		}
		else if (multi.intValue() == 0){
			return "ZERO";
		}
		else{
			return multi.toString();
		}
	}
	
	public static String convertMultiplicity(String multi){
		if (multi.equals("1")){
			return "ONE";
		}
		else if (multi.equals("0")){
			return "ZERO";
		}
		else{
			return multi;
		}
	}
	
	public static String getDistinguisherCommentIfNeeded(RuleDefinition ruleDefinition) {
		if (ruleDefinition == null) {
			return "";
		}
		ElementDiscriber eldisc = DistinguisherUtil.findElementDiscriber(ruleDefinition);
		List<Distinguisher> ldis = DistinguisherUtil.getRealDistinguisher(eldisc);
		if (eldisc != null && ldis != null && !ldis.isEmpty()){
			for (Distinguisher distinguisher : ldis) {
				String addenda = getExtraDistinguisherCommentByDistinguisherForRuleDefinition(ruleDefinition, distinguisher);
				if (addenda != null && !addenda.trim().isEmpty()){
					return "[" + addenda + "]";
				}
			}
		}
		return "";
	}

	protected static String getExtraDistinguisherCommentByDistinguisherForRuleDefinition(RuleDefinition ruleDefinition, Distinguisher distinguisher) {
		if (ruleDefinition == null || distinguisher == null) {
			return null;
		}
		DParent dElement = DPathExtractor.extractDElementFromDPath(distinguisher.getXpath());
		String valueDPath = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dElement, ruleDefinition);
		if (valueDPath != null){
			dElement.getTail().setValue(valueDPath);
			return DPathExtractor.createPathFromDParent(dElement);
		}
		return null;
	}

}
