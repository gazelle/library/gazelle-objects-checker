package net.ihe.gazelle.tempgen.gui.control;

import net.ihe.gazelle.tempgen.gui.view.UserQuestionerView;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.NegativeDistinguisherVerifier;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

public class ExtraNegativeDistinguisherControler implements NegativeDistinguisherVerifier {

	@Override
	public boolean acceptNegativeDistinguisher(RuleDefinition ruleDefinition) {
		return UserQuestionerView.getAcceptanceFromUserForAnAction(extractQuestion(ruleDefinition));
	}

	protected String extractQuestion(RuleDefinition ruleDefinition) {
		DParent del = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String path = DPathExtractor.createPathFromDParent(del);
		return "Do you accept a negative distinguisher for the RuleDefinition : " + path + "?";
	}

}
