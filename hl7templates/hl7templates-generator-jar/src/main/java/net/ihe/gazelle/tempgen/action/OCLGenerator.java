package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EDataType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Allow to generate the OCL for the root path
 * ie : for example : //hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson
 *
 * @author Abderrazek Boufahja
 */
public class OCLGenerator {

    private static final Logger log = Logger.getLogger(OCLGenerator.class);

    private static final String SIZE_GREATER = "->size()>";
    private static final String SIZE_EQUALS = "->size()=";
    private static final String OR = "or ";
    private static final String AND = "and ";

    public String generateRuleToParentFromPath(Attribute attribute) {
        DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
        if (dparent == null) {
            return null;
        }
        return this.generateRuleFromPath(dparent, attribute.getParentObject());

    }

    public String generateRuleContextToRuleDefinition(RuleDefinition ruleDefinition) {
        DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
        return this.generateRuleFromPath(dparent, ruleDefinition);

    }

    public String generateRuleToParentFromPath(RuleDefinition ruleDefinition) {
        DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(ruleDefinition);
        return this.generateRuleFromPath(dparent, ruleDefinition.getParentObject());

    }

    protected String generateRuleFromPath(DParent parentPath, Object ruleParent) {
        return this.generateContextRule(parentPath, Integer.valueOf(parentPath.size() - 1), ruleParent);
    }

    protected String generateContextRule(DParent parentPath, Integer index, Object ruleParent) {
        return generateContextRule(parentPath, index, null, ruleParent);
    }

    public String generateRuleToParentFromPathNF(RuleDefinition ruleDefinition){
        DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(ruleDefinition);
        List<String> rules = new ArrayList<>();
        Object parent = ruleDefinition.getParentObject();
        for (int i = dparent.size()-1 ; i >= 0 ; i--){
           rules.add(0, generateContextRule(dparent, Integer.valueOf(i), parent));
           parent = getParent(parent);
        }
        return addNullFlavorChecks(rules);
    }

    private Object getParent(Object object){
        Object parent = null;
        if (object instanceof RuleDefinition) {
            parent = ((RuleDefinition) object).getParentObject();
        } else if (object instanceof TemplateDefinition) {
            parent = ((TemplateDefinition) object).getParentObject();
        } else if (object instanceof ChoiceDefinition) {
            parent = ((ChoiceDefinition) object).getParentObject();
        }
        return parent;
    }

    public String getMinimumMultiplicityRuleForRD(String rule, RuleDefinition ruleDefinition){
        StringBuilder result = new StringBuilder();
        if (rule.equals("(not self.nullFlavor.oclIsUndefined()) or ")){
                result.append("self.");
                result.append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                result.append(OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition));
                result.append(SIZE_GREATER);
                result.append(ruleDefinition.getMinimumMultiplicity().intValue() -1);
        } else {
            int max = (new UMLElementsManager()).getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition), null);
            result.append("->forAll(");
            result.append(extractEliminationOfNullFlavor(ruleDefinition));
            if (max == 1){
                result.append("not ");
                result.append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                result.append(".oclIsUndefined())");
            }
            else{
                result.append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                result.append(OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition));
                result.append(SIZE_GREATER);
                result.append(ruleDefinition.getMinimumMultiplicity().intValue() -1);
                result.append(")");
            }
        }
        return result.toString();
    }

    private String addNullFlavorChecks(List<String> contextRules){
        StringBuilder result = new StringBuilder();
        int nbrRules = contextRules.size();
        for(int i = 0 ; i < nbrRules ; i++) {
            String contextRule = contextRules.get(i);
            if (contextRule.equals("self")){
                result.append("(not self.nullFlavor.oclIsUndefined()) or ");
            } else {
                if (i != nbrRules -1){
                    // Modify next rule to add rejection of nullFlavor
                    /////////////
                    StringBuilder modifiedNextRule = new StringBuilder();
                    modifiedNextRule.append(contextRule);
                    modifiedNextRule.append(OCLCleaner.generateRejectOcl(true));
                    /////////////

                    //Append next level rule to result and check size + or
                    ///////////////
                    result.append(modifiedNextRule);
                    listEmptyOrContinue(result);
                    //////////////

                    String nextContextRule = contextRules.get(i + 1);
                    modifiedNextRule.append(nextContextRule.replace(contextRule.replace(OCLCleaner.generateRejectOcl(true), ""), ""));
                    contextRules.set(i + 1, modifiedNextRule.toString());
                } else {
                    result.append(contextRule);
                }
            }
        }
        return result.toString();
    }

    /**
     * This methods modify the rule given as parameter to :
     * adds an OCL check that the list defined in the OCL rule string passed as parameter is empty
     * if not, continue the restriction from the same list.
     * @param listRuleBuilder: StringBuilder for the OCL rule that must define a list.
     * @return modified String builder for the rule.
     */
    private StringBuilder listEmptyOrContinue(StringBuilder listRuleBuilder){
        listRuleBuilder.append(SIZE_EQUALS);
        listRuleBuilder.append("0 ");
        listRuleBuilder.append(OR);
        return listRuleBuilder;
    }

    private String extractEliminationOfNullFlavor(RuleDefinition ruleDefinition) {
        RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition);
        Object parent = ruleDefinition.getParentObject();
        if (parent instanceof RuleDefinition) {
            RuleDefinition pRD = (RuleDefinition)parent;
            String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(pRD);
            typeName = typeName + ".nullFlavor";
            if (UMLLoader.isAnAttribute(typeName).booleanValue()) {
                return "(not nullFlavor.oclIsUndefined()) or ";
            }
        }
        else if (parent instanceof ChoiceDefinition) {
            ChoiceDefinition cd = (ChoiceDefinition) parent;
            if (cd.getParentObject() instanceof RuleDefinition) {
                RuleDefinition pRD = (RuleDefinition)cd.getParentObject();
                String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(pRD);
                typeName = typeName + ".nullFlavor";
                if (UMLLoader.isAnAttribute(typeName).booleanValue()) {
                    return "(not nullFlavor.oclIsUndefined()) or ";
                }
            }
        }
        return "";
    }

    /**
     * @param parentPath         : the DParent of the contextRule
     * @param indexParam         : the index of the element for which we are looking for the contextRule
     * @param stoppingIndexParam : the index of the element for which we will stop.
     * @param ruleParent         parent rule
     * @return index shall be greater than stoppingIndex. Here we extract the context : going from steppIngIndex to index
     * If index = stopingIndex, we return ""
     */
    protected String generateContextRule(DParent parentPath, Integer indexParam, Integer stoppingIndexParam, Object ruleParent) {
        int index = Integer.valueOf((indexParam == null) ? (parentPath.size() - 1) : indexParam);
        Integer stoppingIndex = Integer.valueOf((stoppingIndexParam == null) ? -1 : stoppingIndexParam);
        if ((int) index <= stoppingIndex.intValue()) {
            return "";
        }
        if (parentPath.size() <= (int) index) {
            ProblemHandler.handleError("OCLGenerator::generateContextRule is handling a non sense value", "OCLGenerator::generateContextRule");
            return "";
        }
        if ((int) index > 0) {
            String parent = generateParentContext(parentPath, index, stoppingIndex, ruleParent);
            String elementName = parentPath.get((int) index).getName() + addTypeConstrainedIfNeeded(parentPath.get((int) index));
            int max = (new UMLElementsManager()).getMaxAttribute(parentPath, index);
            String res = addElementNameToParentContext(parent, elementName, max);
            return processRuleParentForExtraSelectionIfNeeded(stoppingIndex, ruleParent, res);

        }
        return "self";
    }

    private String generateParentContext(DParent parentPath, Integer index, Integer stoppingIndex, Object ruleParent) {
        String parent = null;
        if (stoppingIndex.intValue() < index.intValue() - 1) {
            if (ruleParent instanceof RuleDefinition) {
                parent = this.generateContextRule(parentPath, Integer.valueOf(index - 1), stoppingIndex, ((RuleDefinition) ruleParent).getParentObject());
            } else if (ruleParent instanceof ChoiceDefinition) {
                parent = generateParentContextForChoiceDefinition(parentPath, index, stoppingIndex, (ChoiceDefinition) ruleParent);
            } else {
                parent = this.generateContextRule(parentPath, Integer.valueOf(index - 1), stoppingIndex, null);
            }
        }
        return parent;
    }

    private String generateParentContextForChoiceDefinition(DParent parentPath, Integer index, Integer stoppingIndex, ChoiceDefinition cdParent) {
        RuleDefinition parentRuleDefinition = (RuleDefinition) cdParent.getParentObject();
        return this.generateContextRule(parentPath, Integer.valueOf(index - 1), stoppingIndex, parentRuleDefinition.getParentObject());
    }

    private String addElementNameToParentContext(String parent, String elementName, int max) {
        String res;
        if (max == 1) {
            if (parent != null) {
                res = parent + "." + elementName;
            } else {
                res = elementName;
            }
        } else {
            res = processContextRuleWithRejectOCLForSetElement(parent, elementName);
        }
        return res;
    }

    private String processContextRuleWithRejectOCLForSetElement(String parent, String attrName) {
        String res;
        if (parent != null) {
            res = parent + "." + attrName + OCLCleaner.generateRejectOcl(false);
        } else {
            res = attrName + OCLCleaner.generateRejectOcl(false);
        }
        return res;
    }

    private String processRuleParentForExtraSelectionIfNeeded(Integer stoppingIndex, Object ruleParent, String resParam) {
        String res = resParam;
        if (stoppingIndex.intValue() == -1 && res.contains(".")) {
            if (ruleParent instanceof RuleDefinition) {
                res = res + getExtraSelectionIfNeeded((RuleDefinition) ruleParent);
            } else if (ruleParent instanceof ChoiceDefinition) {
                ChoiceDefinition cd = (ChoiceDefinition) ruleParent;
                res = res + getExtraSelectionIfNeeded((RuleDefinition) cd.getParentObject());
            } else {
                ProblemHandler.handleError("The kind of parent is unknown", AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
            }
        }
        return res;
    }

    public static String addTypeConstrainedIfNeeded(DParent dParent) {
        if (dParent instanceof DElement) {
            String res = "";
            DElement dElement = (DElement) dParent;
            if (dElement.getExtendedType() != null && !dElement.getExtendedType().trim().isEmpty()) {
                res = "->select(oclIsKindOf(" + dElement.getExtendedType() + ")).oclAsType(" + dElement.getExtendedType() + ")";
            }
            return res;
        }
        return "";
    }

    /**
     * return the max cardinality of a path :
     * example : act.templateId.root -&gt; -1
     * act.classCode -&gt; 1
     * @param parentPath path to parent
     * @param indexParam         : the index of the element for which we are looking for the contextRule
     * @param stoppingIndexParam : the index of the element for which we will stop.
     * @return int
     */
    protected int generateMaxCardinalityOfPath(DParent parentPath, Integer indexParam, Integer stoppingIndexParam) {
        int index = (indexParam == null) ? parentPath.size() - 1 : indexParam.intValue();
        int stoppingIndex = (stoppingIndexParam == null) ? -1 : stoppingIndexParam.intValue();
        if (index == stoppingIndex) {
            return 1;
        }
        if (index > 0) {
            int parent;
            parent = this.generateMaxCardinalityOfPath(parentPath, Integer.valueOf(index - 1), Integer.valueOf(stoppingIndex));
            int max = (new UMLElementsManager()).getMaxAttribute(parentPath, Integer.valueOf(index));
            return Math.min(parent, max);
        } else {
            return 1;
        }
    }

    /**
     * create a select for the attribute to be tested if there are a need to
     *
     * @param ruleDefinition rule
     * @return string
     */
    public static String getExtraSelectionIfNeeded(RuleDefinition ruleDefinition) {
        if (!RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(ruleDefinition)) {
            return "";
        }
        String addenda = getExtraDistinguisherIfNeeded(ruleDefinition);
        if (addenda != null && !addenda.trim().isEmpty()) {
            return "->select(" + addenda + ")";
        }
        return "";
    }

    /**
     * return the content of a distinguisher if exists for a RD
     *
     * @param ruleDefinition rule
     * @return string
     */
    public static String getExtraDistinguisherIfNeeded(RuleDefinition ruleDefinition) {
        if (ruleDefinition == null) {
            return "";
        }
        Pair<String, String> pairDisting = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(ruleDefinition);
        if (pairDisting != null) {
            String addenda = getExtraSelectionByDistinguisherForRuleDefinition(ruleDefinition, pairDisting);
            if (!StringUtils.isEmpty(addenda)) {
                return addenda;
            }
        } else if (RuleDefinitionProblemUtil.verifyThatRuleDefinitionCanHaveNegativeDistinguisher(ruleDefinition)) {
            Map<RuleDefinition, Pair<String, String>> ldis = RuleDefinitionDistinguisherUtil.extractListDistinguishersForBrodhers(ruleDefinition);
            StringBuilder sb = new StringBuilder("");
            for (Entry<RuleDefinition, Pair<String, String>> rdAndPair : ldis.entrySet()) {
                if (rdAndPair.getValue() != null) {
                    String addenda = getExtraSelectionByDistinguisherForRuleDefinition(rdAndPair.getKey(), rdAndPair.getValue());
                    sb = addANDIfStringNotEmpty(sb);
                    sb.append("(not (").append(addenda).append("))");
                }
            }
            return sb.toString();
        }
        return "";
    }

    private static StringBuilder addANDIfStringNotEmpty(StringBuilder sb) {
        if (sb != null && sb.length() > 0) {
            sb.append(" and ");
        }
        return sb;
    }

    /**
     * return a distinguisher string based on distinguisher description
     *
     * @param ruleDefinition rule
     * @param pairDisting pair of distinguisher
     * @return a distinguisher string based on distinguisher description
     */
    protected static String getExtraSelectionByDistinguisherForRuleDefinition(RuleDefinition ruleDefinition, Pair<String, String> pairDisting) {
        if (pairDisting != null && pairDisting.getObject1() != null && pairDisting.getObject2() != null) {
            DParent dElement = DPathExtractor.extractDElementFromDPath(pairDisting.getObject1());
            String valueDPath = pairDisting.getObject2();
            DParent ruleDefinitionDParent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
            int sizeoriginal = ruleDefinitionDParent.size();
            if (ruleDefinitionDParent.get(sizeoriginal - 1) instanceof DElement) {
                ((DElement) ruleDefinitionDParent.get(sizeoriginal - 1)).setFollowingAttributeOrElement(dElement);

                String typeName = (new PathManager()).findPathTypeName(ruleDefinitionDParent);
                String valueDPathModified = "'" + valueDPath + "'";
                if (typeName != null && UMLLoader.isEnumeration(typeName)) {
                    valueDPathModified = typeName + "::" + valueDPath;
                    ImportElementHandler.handleAddingElement(typeName);
                } else if (UMLPrimitiveType.BOOLEAN.getValue().equals(typeName) ||
                        UMLPrimitiveType.DOUBLE.getValue().equals(typeName) ||
                        UMLPrimitiveType.INTEGER.getValue().equals(typeName)) {
                    valueDPathModified = valueDPath;
                }

                return createDistinguisherString(ruleDefinition, ruleDefinitionDParent, sizeoriginal, typeName, valueDPathModified);
            }
        }
        return null;
    }

    private static String createDistinguisherString(RuleDefinition ruleDefinition, DParent ruleDefinitionDParent,
                                                    int sizeoriginal, String typeName, String valueDPathModified) {
        String res = (new OCLGenerator()).generateContextRule(ruleDefinitionDParent, null, Integer.valueOf(sizeoriginal - 1), ruleDefinition);
        int maxTotal = (new OCLGenerator()).generateMaxCardinalityOfPath(ruleDefinitionDParent, null, Integer.valueOf(sizeoriginal - 1));
        if (maxTotal == -1) {
            res = res + "->exists(aa : " + typeName + "| aa = " + valueDPathModified + ")";
            if (!UMLPrimitiveType.listUMLPrimitives().contains(typeName)) {
                ImportElementHandler.handleAddingElement(typeName);
            }
        } else {
            res = "(not (" + verifyAndModifyStructure(res) + ".oclIsUndefined())) and " + res + "=" + valueDPathModified;
        }
        return res;
    }

    //            res = "(not (" + verifyAndModifyStructure(res) + ".oclIsUndefined())) and " + res + "=" + valueDPathModified;
    // fix for the case where the path give a nullPointerException: observation.code.code
    public static String verifyAndModifyStructure(String input) {
        String[] attributeParts = input.split("\\.");
        if (attributeParts.length == 0) {
            return input;
        }
        StringBuilder modifiedStructure = new StringBuilder();
        String currentObject = "";
        for (int i = 0; i < attributeParts.length-1; i++) {
            currentObject += (i == 0 ? "" : ".") + attributeParts[i];
            modifiedStructure.append(currentObject).append(".oclIsUndefined()");
            if (i < attributeParts.length - 1) {
                modifiedStructure.append(" or ");
            }
        }

        return modifiedStructure + input ;
    }

    public String generateRuleToParentFromPath(ChoiceDefinition choiceDefinition) {
        DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition) choiceDefinition.getParentObject());
        return this.generateRuleFromPath(dparent, (RuleDefinition) choiceDefinition.getParentObject());
    }

    public String generateRuleToParentFromPathNF(ChoiceDefinition choiceDefinition) {
        RuleDefinition parent = new RuleDefinition();
        parent.setParentObject(choiceDefinition.getParentObject());
        return this.generateRuleToParentFromPathNF(parent);
    }

    public String generateRuleToParentFromPathNF(Attribute attribute){
        DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
        if (dparent == null) {
            return null;
        }
        List<String> rules = new ArrayList<>();
        Object parent = attribute.getParentObject();
        for (int i = dparent.size()-1 ; i >= 0 ; i--){
            rules.add(0, generateContextRule(dparent, Integer.valueOf(i), parent));
            parent = getParent(parent);
        }
        return addNullFlavorChecks(rules);
    }


    public String appendDTDistinguisher(RuleDefinition ruleDefinition, String constraint){
        if(constraint == null || ruleDefinition == null){
            return constraint;
        }
        String rdName = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
        StringBuilder selectionRule = new StringBuilder(rdName);
        if(ruleDefinition.getDatatype() == null){
            return constraint;
        }
        String datatype = ruleDefinition.getDatatype().getLocalPart();
        if(datatype == null){
            return constraint;
        }
        ImportElementHandler.handleAddingElement(datatype);
        String datatypeForOCL = datatype
                .replace("_","")
                .replace(".","_");
        String selectionStatement = "select(aa | CommonOperationsStatic::compareExactType(aa, '"+datatypeForOCL+"'))";
        selectionRule.append("->").append(selectionStatement);
        ImportElementHandler.handleAddingElement("CommonOperationsStatic");
        String newFullRule = constraint.replace(rdName, selectionRule);
        newFullRule = updateOcIsUndefinedRule(newFullRule, selectionStatement, rdName);
        return newFullRule;

    }

    private String updateOcIsUndefinedRule(String newFullRule, String selectionStatement, String rdName){
        String toReplace = selectionStatement+".oclIsUndefined()";
        if(newFullRule.contains(toReplace)){
            String sizeRule = "->size()<1)";
            String newRule = selectionStatement+sizeRule;
            String updated = newFullRule.replace(toReplace, newRule);
            String regex = "^(?!.*\\S\\s"+rdName+"\\b).*?(\\S+)(?=\\s*"+rdName+"\\b)";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(newFullRule);
            String beforeRd = "";
            if(matcher.find()){
                beforeRd = matcher.group(1);
                if(beforeRd != null){

                    log.info("found element: "+beforeRd+rdName);
                }
                else{
                    log.info("An Error has occured");
                }
            }
            else{
                log.info("No element found for compareExactType");
            }
            String newRdName = "("+beforeRd+rdName;
            updated = updated.replace(beforeRd+rdName, newRdName);
            return updated;
        }
        return newFullRule;
    }




}
