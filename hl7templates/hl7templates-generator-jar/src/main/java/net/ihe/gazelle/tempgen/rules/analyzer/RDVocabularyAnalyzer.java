package net.ihe.gazelle.tempgen.rules.analyzer;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodingStrengthType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.StringUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.VocabularyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDVocabularyAnalyzer implements ConstraintGenerator {

	private static final String DOT_CODE_SYSTEM = ".codeSystem";
	private static final String NOT = "(not ";
	private static final String POINT_CODE = ".code";

	/**
	 *   ruleDefinition.getVocabulary().size()&gt;0
	 */
	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		StringBuilder sb = new StringBuilder("");
		String type = RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition);
		boolean typeIsRestricted = !RuleDefinitionUtil.getUMLTypeName(ruleDefinition).equals(type);
		if (type != null){
			if (UMLLoader.isAnAttribute(type + POINT_CODE).booleanValue()){
				List<Vocabulary> lv = ruleDefinition.getVocabulary();
				List<String> lORs = new ArrayList<>();
				for (Vocabulary vocabulary : lv) {
					String validateContent = extractConstraintForVocabulary(vocabulary, ruleDefinition, type, typeIsRestricted);
					lORs.add(validateContent);
				}
				if (!lORs.isEmpty()){
					String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(ruleDefinition);
					sb.append(contextRule).append(".").append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)).append(OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)).append(OCLCleaner.generateRejectOcl(false)).append("->forAll( (not nullFlavor.oclIsUndefined()) or ");
					int i = 0;
					for (String validateContent : lORs) {
						if (i>0){
							sb.append(" or (").append(validateContent).append(")");
						}
						else {
							sb.append("(").append(validateContent).append(")");
						}
						i++;
					}
					sb.append(")");
				}
				else {
					ProblemHandler.handleRuleDefinitionError(ruleDefinition, 
							"The Vocabulary was not in the valid format", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
				}
			}
			else {
				ProblemHandler.handleRuleDefinitionError(ruleDefinition, 
						"The type of the rule is not part of {CE, CD, CV, CS}", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
			}
		}
		else {
			ProblemHandler.handleRuleDefinitionError(ruleDefinition, 
					"Weird : there are no type of the element", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
		}
		return sb.toString();
	}

	/**
	 * type != null and from {CE, CD, CV, CS}
	 * @param vocabulary voc
	 * @param ruleDefinition rd
	 * @param type type
	 * @param typeIsAny boolean
	 * @return string
	 */
	protected String extractConstraintForVocabulary(Vocabulary vocabulary, RuleDefinition ruleDefinition, String type, boolean typeIsAny) {
		String res = null;
		String cast = typeIsAny?"oclAsType(" + type + ").":"";
		if (typeIsAny){
			ImportElementHandler.handleAddingElement(type);
		}
		if (vocabulary.getValueSet() != null) {
			String oid = VocabularyUtil.getRealValueSetOid(vocabulary.getValueSet(), 
					RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getParentObject().getParentObject());
			if (oid == null){
				ProblemHandler.handleRuleDefinitionError(ruleDefinition, 
						"The value set provided in not a valid OID : " + vocabulary.getValueSet(), AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
				return null;
			}
			if (vocabulary.getFlexibility() != null && !vocabulary.getFlexibility().equals("dynamic")) {
				oid = oid + "&version=" + vocabulary.getFlexibility();
			}
			if (UMLLoader.isAnAttribute(type + POINT_CODE).booleanValue() && UMLLoader.isAnAttribute(type + DOT_CODE_SYSTEM).booleanValue() && !type.equals("CS")){
				res = NOT + cast + "code.oclIsUndefined()) and "
						+ "CommonOperationsStatic::matchesValueSet('" + oid + "', " + cast + "code, " + cast + "codeSystem, null, null)" ;
				ImportElementHandler.handleAddingElement("CommonOperationsStatic");
			}
			else if (UMLLoader.isAnAttribute(type + POINT_CODE).booleanValue()){
				res = NOT + cast + "code.oclIsUndefined()) and CommonOperationsStatic::matchesCodeToValueSet('" + oid + "', " + cast + "code)" ;
				ImportElementHandler.handleAddingElement("CommonOperationsStatic");
			}
			else {
				ProblemHandler.handleRuleDefinitionError(ruleDefinition, "Problem to handle type of element", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
			}
		}
		else if (vocabulary.getCode() != null){
			if (UMLLoader.isAnAttribute(type + POINT_CODE).booleanValue()) {
				res = NOT + cast + "code.oclIsUndefined()) and " + cast + "code='" + vocabulary.getCode() + "'";
				if (vocabulary.getCodeSystem() != null && UMLLoader.isAnAttribute(type + DOT_CODE_SYSTEM).booleanValue()) {
					res = res + " and ((" + cast + "codeSystem.oclIsUndefined()) or " + cast + "codeSystem='" + vocabulary.getCodeSystem() + "')";
				}
			}
		}
		else if (vocabulary.getCodeSystem() != null && UMLLoader.isAnAttribute(type + DOT_CODE_SYSTEM).booleanValue()){
			res = "((" + cast + "codeSystem.oclIsUndefined()) or " + cast + "codeSystem='" + vocabulary.getCodeSystem() + "')";
		}
		else {
			ProblemHandler.handleRuleDefinitionWarning(ruleDefinition, 
					"Not able to handle vocabulary (ValueSet, code, and codeSystem are null)", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
		}
		
		return res;
	}

	@Override
	public String generateCommentConstraint(RuleDefinition ruleDefinition) {
		StringBuilder sb = new StringBuilder();
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		sb.append("In ").append(templateName).append(", the code of ").append(parentPath);
		List<Vocabulary> lv = ruleDefinition.getVocabulary();
		int i=0;
		for (Vocabulary vocabulary : lv) {
			if (i>0){
				sb.append(" OR");
			}
			if (VocabularyUtil.isVocabularyListUseful(vocabulary, 
					RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getParentObject().getParentObject())){
				if (vocabulary.getValueSet() != null){
					sb.append(" ").append(getTypeKeyword(ruleDefinition)).append(" be from the valueSet ").append(vocabulary.getValueSet());
					if (!StringUtil.isOID(vocabulary.getValueSet())) {
						sb.append("(").append(VocabularyUtil.getRealValueSetOid(vocabulary.getValueSet(),
								RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getParentObject().getParentObject())).append(")");
					}
					String flexibility = vocabulary.getFlexibility();
					if (flexibility == null) {
						flexibility = "dynamic";
					}
					sb.append(" (flexibility : ").append(flexibility).append(")");
				}
				else if (vocabulary.getCode() != null){
					sb.append(" ").append(getTypeKeyword(ruleDefinition)).append(" have code='").append(vocabulary.getCode()).append("'");
					if (vocabulary.getCodeSystem() != null){
						sb.append("and codeSytem='").append(vocabulary.getCodeSystem()).append("'");
					}
				}
				else if (vocabulary.getCodeSystem() != null){
					sb.append(" ").append(getTypeKeyword(ruleDefinition)).append(" have codeSystem='").append(vocabulary.getCodeSystem()).append("'");
				}
				i++;
			}
		}
		return sb.toString();
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_VOCAB_PROCESS.getValue();
	}

	public OwnedRuleType generateConstraintOwnedRuleType(RuleDefinition ruleDefinition){
		String type = RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition);
		if (type != null){
			if (UMLLoader.isAnAttribute(type + POINT_CODE).booleanValue()){
				CodingStrengthType strength = ruleDefinition.getStrength();
				if (strength.equals(CodingStrengthType.required)){
					return OwnedRuleType.ERROR;
				}
				if (strength.equals(CodingStrengthType.extensible)){
					return OwnedRuleType.WARNING;
				} else if (strength.equals(CodingStrengthType.preferred)){
					return OwnedRuleType.INFO;
				}
			}
		}
		return null;
	}

	private String getTypeKeyword(RuleDefinition ruleDefinition){
		if(generateConstraintOwnedRuleType(ruleDefinition) == null) return  "";
		switch (generateConstraintOwnedRuleType(ruleDefinition)){
			case ERROR:
				return "SHALL";
			case INFO:
				return "MAY";
			case WARNING:
				return "SHOULD";
		}
		return "";
	}

}
