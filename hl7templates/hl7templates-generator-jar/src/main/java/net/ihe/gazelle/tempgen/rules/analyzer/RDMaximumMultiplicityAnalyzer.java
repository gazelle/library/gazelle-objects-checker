package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempgen.action.UMLElementsManager;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTUtils;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

import javax.xml.namespace.QName;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMaximumMultiplicityAnalyzer implements ConstraintGenerator{
	
	static final int MAX_INTEGER = 1000000000;

	private final Boolean generateDTDistinguisher;

	public RDMaximumMultiplicityAnalyzer(Boolean generateDTDistinguisher) {
		this.generateDTDistinguisher = generateDTDistinguisher;
	}

	public RDMaximumMultiplicityAnalyzer() {
		this.generateDTDistinguisher = Boolean.FALSE;
	}

	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		Integer maximumMultiplicity = null;
		String maximumMultiplicityString = ruleDefinition.getMaximumMultiplicity();
		if (maximumMultiplicityString  != null){
			if (maximumMultiplicityString.equals("*")){
				maximumMultiplicity = Integer.valueOf(MAX_INTEGER);
			}
			else {
				maximumMultiplicity = Integer.valueOf(maximumMultiplicityString);
			}
		}
		OCLGenerator generator = new OCLGenerator();
		String contextRule = generator.generateRuleToParentFromPath(ruleDefinition);
		String res = "";
		if (contextRule.equals("self")){
			res = contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
					+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
					+ OCLCleaner.generateRejectOcl(false) + "->size()<" + (maximumMultiplicity.intValue() +1);
		}
		else {
			res = contextRule + "->forAll(";
			int max = 0;
			 UMLElementsManager umlElementsManager = new UMLElementsManager();
			if(ruleDefinition.getParentObject() instanceof RuleDefinition &&
					((RuleDefinition) ruleDefinition.getParentObject()).getDatatype() != null){
				String parentDt = ((RuleDefinition) ruleDefinition.getParentObject()).getDatatype().getLocalPart();
				max = umlElementsManager.getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition), null, parentDt);
			}
			else{
				max = umlElementsManager.getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition), null);
			}
			if (max == 0){
				res = res + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition) + ".oclIsUndefined())";
			}
			else{
				res = res + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
						+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
						+ "->size()<" +  + (maximumMultiplicity.intValue() +1) + ")";
			}
		}
		if(generateDTDistinguisher == Boolean.TRUE){
			return generator.appendDTDistinguisher(ruleDefinition, res);
		}
		return res;
	}
	
	@Override
	public String generateCommentConstraint(RuleDefinition selectedRuleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(selectedRuleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(selectedRuleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		DParent dparentRD = RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition);
		String elementName = DPathExtractor.createPathFromDParent(dparentRD.getTail()).substring(1);
		return "In " + templateName + ", " + parentPath + " SHALL contain at most " + 
				OwnedRuleManager.convertMultiplicity(selectedRuleDefinition.getMaximumMultiplicity()) + " " + elementName;
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_MAX_MULTIPLICITY_PROCESS.getValue();
	}

}
