package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.impl.LetProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class LetFlattenProc extends LetProcessorImpl {
	
	@Override
	public void process(Let t, Object... objects) {
		Let currentLet = t;
		if (currentLet.getParentObject() instanceof TemplateDefinition) {
			TemplateDefinition parent  = (TemplateDefinition)currentLet.getParentObject();
			if (TemplateDefinitionUtil.templateDefinitionIsCDATemplate(parent)) {
				RuleDefinition first = TemplateDefinitionUtil.getFirstElement(parent);
				parent.getAttributeOrChoiceOrElement().remove(currentLet);
				first.getLetOrAssertOrReport().add(currentLet);
				currentLet.setParentObject(first);
			}
		}
	}

}
