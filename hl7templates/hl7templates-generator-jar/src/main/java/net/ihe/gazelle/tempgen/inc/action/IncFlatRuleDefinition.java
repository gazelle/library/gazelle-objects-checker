package net.ihe.gazelle.tempgen.inc.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncFlatRuleDefinition extends RuleDefinitionProcessorImpl {
	
	protected List<TemplateDefinition> listCDATemplate;
	
	protected IncludeStats incStats;
	
	protected RuleDefinition currentRuleDefinition;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(RuleDefinition t, Object... objects) {
		this.currentRuleDefinition = t;
		TemplateDefinition td = RuleDefinitionUtil.getParentTemplateDefinition(t);
		Map<String, Set<String>> mapCirc = (Map<String, Set<String>>) objects[0];
		listCDATemplate = (List<TemplateDefinition>) objects[1];
		incStats = (IncludeStats) objects[2];
		if (mapCirc!=null && mapCirc.containsKey(td.getId())){
			List<IncludeDefinition> lid = RuleDefinitionUtil.getIncludes(t);
			List<IncludeDefinition> lidToBeDeleted = new ArrayList<>();
			for (IncludeDefinition includeDefinition : lid) {
				String includeIdentifier = RulesUtil.getReferenceTemplateIdentifier(td.getParentObject(), includeDefinition.getRef());
				if (includeIdentifier!= null && mapCirc.get(td.getId()).contains(includeIdentifier)) {
					lidToBeDeleted.add(includeDefinition);
				}
			}
			t.getLetOrAssertOrReport().removeAll(lidToBeDeleted);
		}
		super.process(t, objects);
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		for (IncludeDefinition includeDefinition : includes) {
			TemplateDefinition tparent = RuleDefinitionUtil.getParentTemplateDefinition(currentRuleDefinition);
			String identifier = RulesUtil.getReferenceTemplateIdentifier(tparent.getParentObject(), includeDefinition.getRef());
			if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, this.listCDATemplate)) {
				ProblemHandler.handleRuleDefinitionError(currentRuleDefinition, "The included element is a real CDA template. You chould use a contains instead : " + 
						identifier + " in " + tparent.getId(), 
						AnalyzerEnum.RD_INC_PROCESS.getValue());
			}
			else {
				TemplateDefinition tdToBeMerged = RulesUtil.getTemplateDefinitionById(tparent.getParentObject(), identifier);
				IncFlatUtil.updateItemReferences(includeDefinition, this.currentRuleDefinition);
				(new MergeTemplate2RuleProcessor()).process(tdToBeMerged, currentRuleDefinition, includeDefinition);
				if (incStats != null && tdToBeMerged != null) {
					incStats.setNumberIntegrated(incStats.getNumberIntegrated()+1);
					incStats.getListIncludedTemplates().add(tdToBeMerged.getId());
				}
			}
		}
	}

}
