package net.ihe.gazelle.tempgen.nodes.check;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class NodeCirc {
	
	public enum Color {WHITE, GRAY, BLACK}
	
	private String nodeName;
	
	private Color color = Color.WHITE;
	
	private List<NodeCirc> listChildNodes = new ArrayList<>();

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public List<NodeCirc> getListChildNodes() {
		if (listChildNodes == null){
			listChildNodes = new ArrayList<>();
		}
		return listChildNodes;
	}

	public void setListChildNodes(List<NodeCirc> listChildNodes) {
		this.listChildNodes = listChildNodes;
	}
	
	
	

}
