package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTUtils;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDDatatypeAnalyzer implements ConstraintGenerator {
	
	/**
	 *   ruleDefinition.getDatatype() SHALL not be null
	 */
	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(ruleDefinition);
		return contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
				+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
				+ OCLCleaner.generateRejectOcl(false) + "->forAll(" 
				+ "oclIsKindOf(" + DTUtils.getUMLDatatype(ruleDefinition.getDatatype().getLocalPart()) + "))";
	}

	/**
	 *   ruleDefinition.getDatatype() SHALL not be null
	 */
	@Override
	public String generateCommentConstraint(RuleDefinition ruleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(ruleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		DParent dparentRD = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String elementName = DPathExtractor.createPathFromDParent(dparentRD.getTail()).substring(1);
		return "In " + templateName + ", in " + parentPath + ", the element(s) " + elementName + 
				" SHALL be from the datatype " + DTUtils.getUMLDatatype(ruleDefinition.getDatatype().getLocalPart());
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.DT_PROCESS.getValue();
	}

}
