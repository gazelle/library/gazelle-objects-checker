package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrNullFlavorAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.nullFlavor not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "nullFlavor", attribute.getNullFlavor(), getProcessIdentifier());
	}

	/**
	 *   attribute.nullFlavor not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "nullFlavor", attribute.getNullFlavor());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_NULLFLAVOR_PROCESS.getValue();
	}

}
