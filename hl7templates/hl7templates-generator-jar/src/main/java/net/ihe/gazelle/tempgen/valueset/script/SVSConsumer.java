package net.ihe.gazelle.tempgen.valueset.script;

import java.util.ArrayList;
import gnu.trove.map.hash.THashMap;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.XmlUtil;

public class SVSConsumer {
	
	private static Logger log = LoggerFactory.getLogger(SVSConsumer.class);

	private static Map<String, List<Concept>> listConcept = new THashMap<>(); 

	/**
	 * 
	 * @param valueSetId the valueSet Id
	 * @param lang Valueset language
	 * @return List of retrieved concepts
	 */
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
		if (listConcept.get(valueSetId + lang) != null) {
			return listConcept.get(valueSetId + lang);
		}
		String svsRepository = getSVSRepositoryUrl();
		ClientRequest request = new ClientRequest(svsRepository);
		request.followRedirects(true);
		request.queryParameter("id", valueSetId);
		if (lang != null && !lang.isEmpty()) {
			request.queryParameter("lang", lang);
		}
		try {
			ClientResponse<String> response = request.get(String.class);
			if (response.getStatus() == 200) {
				String xmlContent = response.getEntity();
				Document document = XmlUtil.parse(xmlContent);
				NodeList concepts = document.getElementsByTagName("Concept");
				if (concepts.getLength() > 0) {
					List<Concept> conceptsList = new ArrayList<>();
					for (int index = 0; index < concepts.getLength(); index++) {
						Node conceptNode = concepts.item(index);
						NamedNodeMap attributes = conceptNode.getAttributes();
						Concept aa = new Concept();
						if (attributes.getNamedItem("code") != null) {
							aa.setCode(attributes.getNamedItem("code").getTextContent());
						}
						if (attributes.getNamedItem("displayName") != null) {
							aa.setDisplayName(attributes.getNamedItem("displayName").getTextContent());
						}
						if (attributes.getNamedItem("codeSystem") != null) {
							aa.setCodeSystem(attributes.getNamedItem("codeSystem").getTextContent());
						}
						if (attributes.getNamedItem("codeSystemName") != null) {
							aa.setCodeSystemName(attributes.getNamedItem("codeSystemName").getTextContent());
						}

						conceptsList.add(aa);
					}
					listConcept.put(valueSetId + lang, conceptsList);
					return conceptsList;
				} else {
					listConcept.put(valueSetId + lang, null);
					return new ArrayList<>();
				}
			} else {
				log.error("svs endpoint responded with a bad response status : {}", Integer.valueOf(response.getStatus()));
				listConcept.put(valueSetId + lang, null);
				return new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Problem to process list of value sets", e);
		}
		listConcept.put(valueSetId + lang, null);
		return new ArrayList<>();
	}

	
	protected String getSVSRepositoryUrl() {
		return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
	}
}
