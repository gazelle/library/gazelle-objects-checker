package net.ihe.gazelle.tempgen.valueset.flatten;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VSFlattenStats {
	
	private Set<String> listVSFlattened = new HashSet<>();
	
	private int numberIntegrated = 0;

	public Set<String> getListVSFlattened() {
		return listVSFlattened;
	}

	public void setListVSFlattened(Set<String> listVSFlattened) {
		this.listVSFlattened = listVSFlattened;
	}

	public int getNumberIntegrated() {
		return numberIntegrated;
	}

	public void setNumberIntegrated(int numberIntegrated) {
		this.numberIntegrated = numberIntegrated;
	}

}
