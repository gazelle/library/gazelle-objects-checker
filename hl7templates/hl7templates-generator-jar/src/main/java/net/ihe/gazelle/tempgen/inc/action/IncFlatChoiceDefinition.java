package net.ihe.gazelle.tempgen.inc.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.impl.ChoiceDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncFlatChoiceDefinition extends ChoiceDefinitionProcessorImpl {
	
	protected List<TemplateDefinition> listCDATemplate;
	
	protected IncludeStats incStats;
	
	protected ChoiceDefinition currentChoiceDefinition;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(ChoiceDefinition t, Object... objects) {
		this.currentChoiceDefinition = t;
		TemplateDefinition td = ChoiceDefinitionUtil.getParentTemplateDefinition(t);
		Map<String, Set<String>> mapCirc = (Map<String, Set<String>>) objects[0];
		this.listCDATemplate = (List<TemplateDefinition>) objects[1];
		incStats = (IncludeStats) objects[2];
		if (mapCirc!= null && mapCirc.containsKey(td.getId())){
			List<IncludeDefinition> lid = ChoiceDefinitionUtil.getIncludes(t);
			List<IncludeDefinition> lidToBeDeleted = new ArrayList<>();
			for (IncludeDefinition includeDefinition : lid) {
				String includeIdentifier = RulesUtil.getReferenceTemplateIdentifier(td.getParentObject(),includeDefinition.getRef());
				if (includeIdentifier!= null && mapCirc.get(td.getId()).contains(includeIdentifier)) {
					lidToBeDeleted.add(includeDefinition);
				}
			}
			t.getIncludeOrElementOrConstraint().removeAll(lidToBeDeleted);
		}
		super.process(t, objects);
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		for (IncludeDefinition includeDefinition : includes) {
			TemplateDefinition tparent = ChoiceDefinitionUtil.getParentTemplateDefinition(this.currentChoiceDefinition);
			String identifier = RulesUtil.getReferenceTemplateIdentifier(tparent.getParentObject(), includeDefinition.getRef());
			if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, this.listCDATemplate)) {
				ProblemHandler.handleError("The included element is a real CDA template. You chould use a contains instead : " + 
						identifier + " in " + tparent.getId(), 
						AnalyzerEnum.RD_INC_PROCESS.getValue());
				return;
			}
			else {
				TemplateDefinition tdToBeMerged = RulesUtil.getTemplateDefinitionById(tparent.getParentObject(), identifier);
				List<Attribute> attrs = TemplateDefinitionUtil.getAttributes(tdToBeMerged);
				if (attrs != null && !attrs.isEmpty()){
					ProblemHandler.handleError("The included element is a taken as a choice, however it contains an attribute(s)! : " + 
							identifier + " in " + tparent.getId(), 
							AnalyzerEnum.RD_INC_PROCESS.getValue());
					return;
				}
				List<ChoiceDefinition> lcd = TemplateDefinitionUtil.getChoices(tdToBeMerged);
				if (lcd != null && !lcd.isEmpty()) {
					ProblemHandler.handleError("The included element is a taken as a choice, however it contains an choice(s) elements! : " + 
							identifier + " in " + tparent.getId(), 
							AnalyzerEnum.RD_INC_PROCESS.getValue());
					return;
				}
				IncFlatUtil.updateItemReferences(includeDefinition, this.currentChoiceDefinition);
				(new MergeTemplate2ChoiceProcessor()).process(tdToBeMerged, this.currentChoiceDefinition, includeDefinition);
				incStats.setNumberIntegrated(incStats.getNumberIntegrated()+1);
				if (tdToBeMerged != null) {
					incStats.getListIncludedTemplates().add(tdToBeMerged.getId());
				}
			}
		}
	}

}
