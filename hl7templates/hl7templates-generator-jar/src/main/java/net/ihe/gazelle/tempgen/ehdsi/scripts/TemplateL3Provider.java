package net.ihe.gazelle.tempgen.ehdsi.scripts;

import net.ihe.gazelle.tempapi.interfaces.TemplateDefinitionProcessor;
import net.ihe.gazelle.tempapi.interfaces.VocabularyProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TemplateL3Provider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(TemplateDefinitionProcessor.class)){
			return (T) new TemplateL3Processor();
		}
		return null;
	}
	
}
