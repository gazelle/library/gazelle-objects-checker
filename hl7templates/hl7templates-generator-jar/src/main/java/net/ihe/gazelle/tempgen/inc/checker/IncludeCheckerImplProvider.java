package net.ihe.gazelle.tempgen.inc.checker;

import net.ihe.gazelle.tempapi.interfaces.ChoiceDefinitionProcessor;
import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.interfaces.TemplateDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncludeCheckerImplProvider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(RuleDefinitionProcessor.class)){
			return (T) new IncCheckRuleDefinition();
		}
		else if (t.equals(TemplateDefinitionProcessor.class)){
			return (T) new IncCheckTemplateDefinition();
		}
		else if (t.equals(ChoiceDefinitionProcessor.class)){
			return (T) new IncCheckChoiceDefinition();
		}
		return null;
	}
	
}
