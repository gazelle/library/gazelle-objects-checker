package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrRepresentationAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.representation not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "representation", 
				attribute.getRepresentation(), getProcessIdentifier());
	}

	/**
	 *   attribute.representation not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "representation", attribute.getRepresentation());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_REPRESENTATION_PROCESS.getValue();
	}

}
