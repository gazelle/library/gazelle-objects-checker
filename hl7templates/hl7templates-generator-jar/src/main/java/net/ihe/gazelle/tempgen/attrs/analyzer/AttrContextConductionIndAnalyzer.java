package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrContextConductionIndAnalyzer implements ATConstraintGenerator {

	/**
	 *   contextConductionInd != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, "contextConductionInd", String.valueOf(attribute.isContextConductionInd()));
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "contextConductionInd", attribute.isContextConductionInd());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_CONTEXTCONDUCTIONIND_PROCESS.getValue();
	}

}
