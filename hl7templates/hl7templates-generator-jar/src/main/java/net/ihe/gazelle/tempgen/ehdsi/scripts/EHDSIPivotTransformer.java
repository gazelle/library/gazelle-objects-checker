package net.ihe.gazelle.tempgen.ehdsi.scripts;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

public class EHDSIPivotTransformer {
	
	public final static String ROOT = "1.3.6.1.4.1.12559.11.10.1.3.1.42";
	
	public static void main(String[] args) throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/Téléchargements/epsos-20171124T150657-en-US-decor-compiled.xml");
		DecorVisitor.INSTANCE.visitAndProcess(dec, new VSPivotImplProvider());
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/Téléchargements/epsosp-20171124T150657-en-US-decor-compiled.xml"));
	}

}
