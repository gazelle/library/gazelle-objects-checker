package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMandatoryAnalyzer implements ConstraintGenerator {
	
	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(ruleDefinition);
		return contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
				+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
				+ OCLCleaner.generateRejectOcl(false) + "->forAll(" 
				+ "nullFlavor.oclIsUndefined())";
	}

	@Override
	public String generateCommentConstraint(RuleDefinition ruleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(ruleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		DParent dparentRD = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String elementName = DPathExtractor.createPathFromDParent(dparentRD.getTail()).substring(1);
		return "In " + templateName + ", in " + parentPath + ", the element(s) " + elementName + 
				" SHALL not have nullFlavor (mandatory)";
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_MANDATORY_PROCESS.getValue();
	}

}
