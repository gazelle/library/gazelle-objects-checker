package net.ihe.gazelle.tempgen.inc.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncFlatTemplateDefinition extends TemplateDefinitionProcessorImpl {
	
	protected List<TemplateDefinition> listCDATemplate;
	
	protected IncludeStats incStats;
	
	private TemplateDefinition currentTemplateDefinition;
	
	public TemplateDefinition getCurrentTemplateDefinition() {
		return currentTemplateDefinition;
	}

	public void setCurrentTemplateDefinition(TemplateDefinition currentTemplateDefinition) {
		this.currentTemplateDefinition = currentTemplateDefinition;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process(TemplateDefinition t, Object... objects) {
		this.currentTemplateDefinition = t;
		Map<String, Set<String>> mapCirc = (Map<String, Set<String>>) objects[0];
		listCDATemplate = (List<TemplateDefinition>) objects[1];
		incStats = (IncludeStats) objects[2];
		if (mapCirc!=null && mapCirc.containsKey(t.getId())){
			List<IncludeDefinition> lid = TemplateDefinitionUtil.getIncludes(t);
			List<IncludeDefinition> lidToBeDeleted = new ArrayList<>();
			for (IncludeDefinition includeDefinition : lid) {
				String includeIdentifier = RulesUtil.getReferenceTemplateIdentifier(t.getParentObject(), includeDefinition.getRef());
				if (mapCirc.get(t.getId()).contains(includeIdentifier)) {
					lidToBeDeleted.add(includeDefinition);
				}
			}
			if (!lidToBeDeleted.isEmpty()){
				t.getAttributeOrChoiceOrElement().removeAll(lidToBeDeleted);
			}
		}
		super.process(t, objects);
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		for (IncludeDefinition includeDefinition : includes) {
			String identifier = RulesUtil.getReferenceTemplateIdentifier(this.currentTemplateDefinition.getParentObject(), includeDefinition.getRef());
			if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, this.listCDATemplate)) {
				ProblemHandler.handleError("The included element is a real CDA template. You chould use a contains instead : " + identifier + 
						" in " + this.currentTemplateDefinition.getId(), 
						AnalyzerEnum.RD_INC_PROCESS.getValue());
			}
			else {
				IncFlatUtil.updateItemReferences(includeDefinition, this.currentTemplateDefinition);
				TemplateDefinition tdToBeMerged = RulesUtil.getTemplateDefinitionById(this.currentTemplateDefinition.getParentObject(), identifier);
				(new MergeTemplate2TemplateProcessor()).process(tdToBeMerged, this.currentTemplateDefinition, includeDefinition);
				incStats.setNumberIntegrated(incStats.getNumberIntegrated()+1);
				incStats.getListIncludedTemplates().add(tdToBeMerged.getId());
			}
		}
	}

}
