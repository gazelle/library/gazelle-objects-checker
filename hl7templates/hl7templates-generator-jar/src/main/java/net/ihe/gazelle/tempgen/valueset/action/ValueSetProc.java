package net.ihe.gazelle.tempgen.valueset.action;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.ValueSetResponseType;
import net.ihe.gazelle.tempapi.impl.ValueSetProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetProc extends ValueSetProcessorImpl {
	
	private RetrieveValueSetResponseType currentRetrieveValueSetResponseType;
	
	@Override
	public void process(ValueSet t, Object... objects) {
		this.currentRetrieveValueSetResponseType = (RetrieveValueSetResponseType) objects[0];
		ValueSetResponseType vst = new ValueSetResponseType();
		this.currentRetrieveValueSetResponseType.setValueSet(vst);
		super.process(t, objects);
	}
	
	@Override
	public void processDisplayName(String displayName) {
		this.currentRetrieveValueSetResponseType.getValueSet().setDisplayName(displayName);
	}
	
	@Override
	public void processVersionLabel(String versionLabel) {
		if (this.currentRetrieveValueSetResponseType.getValueSet().getVersion() == null && versionLabel != null) {
			this.currentRetrieveValueSetResponseType.getValueSet().setVersion(versionLabel);
		}
	}
	
	@Override
	public void processId(String id) {
		this.currentRetrieveValueSetResponseType.getValueSet().setId(id);
	}
	
	@Override
	public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar) {
		if (this.currentRetrieveValueSetResponseType.getValueSet().getVersion() == null && xmlGregorianCalendar != null) {
			this.currentRetrieveValueSetResponseType.getValueSet().setVersion(xmlGregorianCalendar.toString());
		}
	}

}
