package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrUseAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.use not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		return parentOCL + ".use" + OCLCleaner.generateRejectOcl(false) + "->forAll(aa | aa='" + attribute.getUse() + "')";
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + parentPath + ", the attribute use SHALL have the value '" + 
				attribute.getUse() + "' if present";
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_USE_PROCESS.getValue();
	}

}
