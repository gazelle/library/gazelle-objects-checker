package net.ihe.gazelle.tempgen.statistics;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDNameAsXpath {
	
	private String value;
	
	private boolean isProcessed = false;
	
	private boolean pathIsIgnorable = false;
	
	private boolean pathIsConvertible = false;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public boolean isPathIsIgnorable() {
		return pathIsIgnorable;
	}

	public void setPathIsIgnorable(boolean pathIsIgnorable) {
		this.pathIsIgnorable = pathIsIgnorable;
	}

	public boolean isPathIsConvertible() {
		return pathIsConvertible;
	}

	public void setPathIsConvertible(boolean pathIsConvertible) {
		this.pathIsConvertible = pathIsConvertible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isProcessed ? 1231 : 1237);
		result = prime * result + (pathIsConvertible ? 1231 : 1237);
		result = prime * result + (pathIsIgnorable ? 1231 : 1237);
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RDNameAsXpath other = (RDNameAsXpath) obj;
		if (isProcessed != other.isProcessed) {
			return false;
		}
		if (pathIsConvertible != other.pathIsConvertible) {
			return false;
		}
		if (pathIsIgnorable != other.pathIsIgnorable) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}
	
	

}
