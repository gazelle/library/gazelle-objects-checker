package net.ihe.gazelle.tempgen.attrs.analyzer;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodingStrengthType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.StringUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.VocabularyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrVocabAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String attrName = AttributeUtil.getBetterMatchingName(attribute);
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		StringBuilder res = new StringBuilder(parentOCL);
		List<Vocabulary> lv = attribute.getVocabulary();
		List<String> lORs = new ArrayList<>();
		for (Vocabulary vocabulary : lv) {
			String validateContent = extractConstraintForVocabulary(vocabulary, attribute);
			lORs.add(validateContent);
		}
		if (!lORs.isEmpty()){
			String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
			res = new StringBuilder();
			res.append(contextRule).append(".").append(attrName).append(OCLCleaner.generateRejectOcl(false)).append("->forAll( aa | ");
			int i = 1;
			res.append("aa.oclIsUndefined()");
			for (String validateContent : lORs) {
				res.append(" or (").append(validateContent).append(")");
			}
			res.append(")");
		}
		else {
			ProblemHandler.handleAttributeError(attribute, attrName, null, 
					"The Vocabulary was not in the valid format", AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
		}
		return res.toString();
	}

	/**
	 * the attribute is of type string
	 * @param vocabulary voc
	 * @param attribute attr
	 * @return string
	 */
	protected String extractConstraintForVocabulary(Vocabulary vocabulary, Attribute attribute) {
		String attrName = AttributeUtil.getBetterMatchingName(attribute);
		String res = "";
		if (vocabulary.getValueSet() != null) {
			String oid = VocabularyUtil.getRealValueSetOid(vocabulary.getValueSet(), 
					AttributeUtil.getParentTemplateDefinition(attribute).getParentObject().getParentObject());
			if (oid == null){
				ProblemHandler.handleAttributeError(attribute, attrName, null, 
						"The value set provided in not a valid OID : " + vocabulary.getValueSet(), AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
				return null;
			}
			if (vocabulary.getFlexibility() != null && !vocabulary.getFlexibility().equals("dynamic")) {
				oid = oid + "&version=" + vocabulary.getFlexibility();
			}
			res = "CommonOperationsStatic::matchesCodeToValueSet('" + oid + "', aa)";
			ImportElementHandler.handleAddingElement("CommonOperationsStatic");
		}
		else if (vocabulary.getCode() != null){
			res = "aa='" + vocabulary.getCode() + "'";
		}
		else {
			ProblemHandler.handleAttributeError(attribute, attrName, null,
					"Not able to handle vocabulary (ValueSet and code are null)", AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
		}
		return res;
	}

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		String attrName = AttributeUtil.getBetterMatchingName(attribute);
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfAttibute(attribute, attrName, null);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		StringBuilder desc = new StringBuilder();
		desc.append("In ").append(templateName).append(", the code of ").append(parentPath);
		List<Vocabulary> lv = attribute.getVocabulary();
		int i=0;
		for (Vocabulary vocabulary : lv) {
			if (i>0){
				desc.append(" OR");
			}
			if (VocabularyUtil.isVocabularyListUseful(vocabulary, 
					AttributeUtil.getParentTemplateDefinition(attribute).getParentObject().getParentObject())){
				if (vocabulary.getValueSet() != null){
					desc.append(" ").append(getTypeKeyword(attribute)).append(" be from the valueSet ").append(vocabulary.getValueSet());
					addOIDIfTheVocabularyIsName(attribute, desc, vocabulary);
					addFlexibiliy(desc, vocabulary);
				}
				else if (vocabulary.getCode() != null){
					desc.append(" ").append(getTypeKeyword(attribute)).append(" have code='").append(vocabulary.getCode()).append("'");
				}
				i++;
			}
		}
		return desc.toString();
	}

	private void addFlexibiliy(StringBuilder desc, Vocabulary vocabulary) {
		String flexibility = vocabulary.getFlexibility();
		if (flexibility == null) {
			flexibility = "dynamic";
		}
		desc.append(" (flexibility : ").append(flexibility).append(")");
	}

	private void addOIDIfTheVocabularyIsName(Attribute attribute, StringBuilder desc, Vocabulary vocabulary) {
		if (!StringUtil.isOID(vocabulary.getValueSet())) {
			desc.append("(").append(VocabularyUtil.getRealValueSetOid(vocabulary.getValueSet(),
                    AttributeUtil.getParentTemplateDefinition(attribute).getParentObject().getParentObject())).append(")");
		}
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue();
	}


	public OwnedRuleType generateConstraintOwnedRuleType(Attribute attribute){
		CodingStrengthType strength = ((RuleDefinition) attribute.getParentObject()).getStrength();
		if (strength.equals(CodingStrengthType.required)){
			return OwnedRuleType.ERROR;
		}
		if (strength.equals(CodingStrengthType.extensible)){
			return OwnedRuleType.WARNING;
		} else if (strength.equals(CodingStrengthType.preferred)){
			return OwnedRuleType.INFO;
		}
		return null;
	}

	private String getTypeKeyword(Attribute attribute){
		switch (generateConstraintOwnedRuleType(attribute)){
			case ERROR:
				return "SHALL";
			case INFO:
				return "MAY";
			case WARNING:
				return "SHOULD";
		}
		return "";
	}
}
