package net.ihe.gazelle.tempgen.rules.analyzer;

import java.util.Set;

import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDExceptionAnalyzer implements ConstraintGenerator {
	
	/**
	 *   ruleDefinition.getVocabulary().size()&gt;0
	 */
	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		StringBuilder sb = new StringBuilder("");
		String type = RuleDefinitionUtil.getUMLTypeName(ruleDefinition);
		if (type != null){
			Set<ValueSetConcept> svsc = RuleDefinitionUtil.extractListRelatedException(ruleDefinition);
			if (svsc == null || svsc.isEmpty()) {
				ProblemHandler.handleRuleDefinitionError(ruleDefinition, "Problem to handle valueset exceptions", 
						getProcessIdentifier());
			}
			String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(ruleDefinition);
			sb.append(contextRule).append(".").append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)).append(OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)).append(OCLCleaner.generateRejectOcl(false)).append("->forAll(nullFlavor.oclIsUndefined()");
			for (ValueSetConcept valueSetConcept : svsc) {
				sb.append(" or nullFlavor=NullFlavor::").append(valueSetConcept.getCode());
			}
			sb.append(")");
			ImportElementHandler.handleAddingElement("NullFlavor");
		}
		else {
			ProblemHandler.handleRuleDefinitionError(ruleDefinition, 
					"Weird : there are no type of the element", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
		}
		return sb.toString();
	}

	@Override
	public String generateCommentConstraint(RuleDefinition ruleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		StringBuilder sb = new StringBuilder();
		sb.append("In ").append(templateName).append(", the nullFlavor attribute of ").append(parentPath).append(" SHALL have a value from : {");
		Set<ValueSetConcept> svsc = RuleDefinitionUtil.extractListRelatedException(ruleDefinition);
		int i=0;
		for (ValueSetConcept vsc : svsc) {
			if (i++>0){
				sb.append(" ,");
			}
			sb.append(vsc.getCode());
		}
		sb.append("}");
		return sb.toString();
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_VOCAB_PROCESS.getValue();
	}

}
