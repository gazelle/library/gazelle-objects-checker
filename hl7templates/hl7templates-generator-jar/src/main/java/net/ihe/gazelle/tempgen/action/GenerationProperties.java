package net.ihe.gazelle.tempgen.action;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class GenerationProperties {
	
	private GenerationProperties() {}
	
	public static boolean ignoreAssertionGeneration = false;
	
	public static boolean ignoreXPathAxes = false;
	
	/**
	 * if we want a user friendly interaction, when we have an assert, to
	 * confirm its use or not. Example you can declare like this :
	 * public static AssertVerifier userSupportForAssertClass = new AssertVerifierImpl(); .
	 */
	public static AssertVerifier userSupportForAssertClass = null;

}
