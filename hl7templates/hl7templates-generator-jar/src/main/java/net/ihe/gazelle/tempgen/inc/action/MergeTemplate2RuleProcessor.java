package net.ihe.gazelle.tempgen.inc.action;

import java.util.List;

import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class MergeTemplate2RuleProcessor extends TemplateDefinitionProcessorImpl {
	
	private RuleDefinition parentRuleDefinition;
	
	private IncludeDefinition currentIncludeDefinition;
	
	private Integer indexAdd;
	
	@Override
	public void process(TemplateDefinition t, Object... objects) {
		if (objects[0] instanceof RuleDefinition){
			this.parentRuleDefinition = (RuleDefinition)objects[0];
		}
		// objects[1] is a includeDefinition to be replaced
		this.currentIncludeDefinition = (IncludeDefinition) objects[1];
		this.indexAdd = Integer.valueOf(this.parentRuleDefinition.getLetOrAssertOrReport().indexOf(objects[1]));
		this.parentRuleDefinition.getLetOrAssertOrReport().remove(objects[1]);
		super.process(t, objects);
	}
	
	@Override
	public void processAttributes(List<Attribute> attributes) {
		if (attributes != null){
			for (Attribute attribute : attributes) {
				Attribute clone = CloneUtil.cloneHL7TemplateEl(attribute);
				clone.setParentObject(this.parentRuleDefinition);
				this.parentRuleDefinition.getAttribute().add(clone);
			}
		}
	}
	
	@Override
	public void processChoices(List<ChoiceDefinition> choices) {
		if (choices != null){
			for (ChoiceDefinition choiceDefinition : choices) {
				ChoiceDefinition clone = CloneUtil.cloneHL7TemplateEl(choiceDefinition);
				MergeUtil.updateCloneProperties(clone, this.currentIncludeDefinition.getMinimumMultiplicity(), 
						currentIncludeDefinition.getMaximumMultiplicity());
				clone.setParentObject(this.parentRuleDefinition);
				this.parentRuleDefinition.getLetOrAssertOrReport().add(this.indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void processElements(List<RuleDefinition> elements) {
		if (elements != null){
			for (RuleDefinition ruleDefinition : elements) {
				RuleDefinition clone = CloneUtil.cloneHL7TemplateEl(ruleDefinition);
				MergeUtil.updateCloneProperties(clone, this.currentIncludeDefinition.getMinimumMultiplicity(), 
						currentIncludeDefinition.getMaximumMultiplicity(), this.currentIncludeDefinition.getIsMandatory(), 
						this.currentIncludeDefinition.getConformance());
				clone.setParentObject(parentRuleDefinition);
				this.parentRuleDefinition.getLetOrAssertOrReport().add(this.indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		if (includes != null) {
			for (IncludeDefinition includeDefinition : includes) {
				IncludeDefinition clone = CloneUtil.cloneHL7TemplateEl(includeDefinition);
				MergeUtil.updateCloneProperties(clone, this.currentIncludeDefinition.getMinimumMultiplicity(), 
						currentIncludeDefinition.getMaximumMultiplicity(), this.currentIncludeDefinition.getIsMandatory(), 
						this.currentIncludeDefinition.getConformance());
				clone.setParentObject(this.parentRuleDefinition);
				this.parentRuleDefinition.getLetOrAssertOrReport().add(this.indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void processLets(List<Let> lets) {
		if (lets != null) {
			for (Let let : lets) {
				Let clone = CloneUtil.cloneHL7TemplateEl(let);
				clone.setParentObject(parentRuleDefinition);
				parentRuleDefinition.getLetOrAssertOrReport().add(indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void process_asserts(List<Assert> decorAsserts) {
		if(decorAsserts != null) {
			for (Assert assert1 : decorAsserts) {
				Assert clone = CloneUtil.cloneHL7TemplateEl(assert1);
				clone.setParentObject(parentRuleDefinition);
				parentRuleDefinition.getLetOrAssertOrReport().add(indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void processItem(Item item) {
		IncFlatUtil.updateItemReferences(item, this.parentRuleDefinition);
	}
	
}
