package net.ihe.gazelle.tempgen.action;


import net.ihe.gazelle.goc.template.definer.model.Taml;
import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenDecor;
import net.ihe.gazelle.tempgen.flatten.action.RulesCleaner;
import net.ihe.gazelle.tempgen.inc.action.IncludeFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.BBRResource;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Abderrazek Boufahja
 */
public class HL7TemplatesConverter {

    private static Logger log = LoggerFactory.getLogger(HL7TemplatesConverter.class);

    /**
     * Default conversion of BBR to UML. Base options are ignoreTemplateIdRequirements = true and versionLabel = null
     * @param bbrpath : path to the BBR to convert
     * @throws JAXBException if decor cannot be unmarshall
     * @return the XMI String
     */
    public String convertArtDecorXMI(String bbrpath) throws JAXBException {
        return convertArtDecorXMI(bbrpath, false, null);
    }

    /**
     * Conversion of BBR to UML.
     * @param decor : Decor to convert
     * @param ignoreTemplateIdRequirements : if true, no rule for templateId will be generated in the UML model
     * @param templateVersionFilter : only templates defining this version label will be kept in the model (ignored if null)
     * @throws JAXBException if decor cannot be unmarshall
     * @return The XMI String
     */
    public String convertArtDecorXMI(Decor decor, boolean ignoreTemplateIdRequirements, String templateVersionFilter)
            throws JAXBException {

        log.info("Converting BBR to UML Model");


        //Log used BBRs
        if(decor != null){
            for (BuildingBlockRepository bbr : decor.getProject().getBuildingBlockRepository()){
                log.info("used bbr: " + bbr.getIdent());
            }
        }
        else {
            log.warn("decor null");
            log.info("Couldn't list used BBRs");
        }

        if (templateVersionFilter != null) {
            log.warn("Clean-up for version label : " + templateVersionFilter);
            decor = DecorUtil.cleanupForVersionLabel(decor, templateVersionFilter);
        }

        log.warn("Remove older version of duplicated templates");
        decor = DecorUtil.cleanupForEffectiveDate(decor);



        log.warn("Start flattening decor process");
        decor = flattenDecor(decor);
        log.warn("End flattening decor process");


        log.warn("Converting Decor to XMI");
        //
        XMI xmi = convertFlattenedDecorToXMI(decor, ignoreTemplateIdRequirements);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMIMarshaller.printXMI(xmi, baos);
        String res = baos.toString();
        res = res.replaceAll("\\<xmi:XMI.*\\>", "<xmi:XMI xmi:version=\"2.1\" xmlns:xmi=\"http://schema.omg.org/spec/XMI/2.1\"\n" +
                "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "	xmlns:Ecore=\"http://www.eclipse.org/uml2/schemas/Ecore/5\"\n" +
                "	xmlns:TemplateDefiner=\"http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43\"\n" +
                "	xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\"\n" +
                "	xmlns:uml=\"http://www.eclipse.org/uml2/3.0.0/UML\"\n" +
                "	xsi:schemaLocation=\"http://www.eclipse.org/uml2/schemas/Ecore/5 " +
                "pathmap://UML_PROFILES/Ecore.profile.uml#_z1OFcHjqEdy8S4Cr8Rc_NA " +
                "http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43 ../../../hl7templates-resources/uml/profiles/common-profile" +
                ".uml#_L1HqYLk9EeadT_oVto_uPg\">");
        return res;
    }

    public String convertArtDecorXMI(String bbrpath, boolean ignoreTemplateIdRequirements, String templateVersionFilter)
            throws JAXBException {
            Decor decor = DecorMarshaller.loadDecor(bbrpath);
            return convertArtDecorXMI(decor, ignoreTemplateIdRequirements, templateVersionFilter);
    }

    public String convertArtDecorXMI(BBRResource bbrResource, Boolean ignoreTemplateIdRequirements, String templateVersionFilter)
            throws JAXBException {

        //TODO interface to hl7templates-model-jar
        Decor decor = DecorMarshaller.loadDecor(bbrResource);
        return convertArtDecorXMI(decor, ignoreTemplateIdRequirements, templateVersionFilter);
    }

    protected Decor flattenDecor(Decor decorParam) {
        Decor decor = decorParam;
        log.info("Clean rules of the decor to flatten");
        RulesCleaner.cleanRules(decor.getRules());
        //Transform include and contains to contain
        log.info("Transform include and contains to contain");
        decor = GeneralFlattenDecor.generalFlattenDecor(decor);
        //Transform contain to ContainsDefinitions
        log.info("Transform contain to ContainsDefinitions");
        decor = GeneralFlattenDecor.generalFlattenBisDecor(decor);
        // ? Includes are transformed to contains what does it do here ?
        // log.info("IncludeFlattener.flattenIncludeInDecor");
        decor = IncludeFlattener.flattenIncludeInDecor(decor);
        return decor;
    }


    protected XMI convertFlattenedDecorToXMI(Decor decor, boolean ignoreTemplateIdRequirements) {
        String prefix = DecorUtil.extractPrefixFromDecor(decor);
        XMI xmi = XMIUtil.createXMI(prefix);
        //The entrypoint to convert the decor to OCL!
        (new DecorAnalyzer()).process(decor, xmi, Boolean.valueOf(ignoreTemplateIdRequirements));
        //Skip incorrect OCL
        //cleanTAML(xmi);
        clearXMI(xmi);
        return xmi;
    }

    private void clearXMI(XMI xmi){
        ArrayList<String> removedOrIds = new ArrayList<>();
        for(Iterator<PackagedElement> itpe = xmi.getModel().getPackagedElement().listIterator(); itpe.hasNext();){
            PackagedElement currentPe = itpe.next();
            for(Iterator<OwnedRule> itor = currentPe.getOwnedRule().listIterator(); itor.hasNext();){
                OwnedRule currentOr = itor.next();
                if(!isOclClean(currentOr)){
                    log.info("Removing wrong OCL: {}",currentOr.getSpecification().getBody());
                    //TamlHandler.removeTAML(currentOr);
                    removedOrIds.add(currentOr.getId());
                    itor.remove();
                }
            }
        }
        for(Iterator<Taml> ittaml = xmi.getTaml().listIterator(); ittaml.hasNext();){
            Taml currentTaml = ittaml.next();
            if(removedOrIds.contains(currentTaml.getBaseConstraint())){
                ittaml.remove();
            }
        }
    }

    private boolean isOclClean(OwnedRule ownedRule){
        String mat= ownedRule.getSpecification().getBody();
        String matr= "\\)[A-Za-z]+";
        Pattern pattern = Pattern.compile(matr);
        Matcher matcher = pattern.matcher(ownedRule.getSpecification().getBody());
        if(!matcher.find()){
            return true;
        }
        return false;
    }

}
