package net.ihe.gazelle.tempgen.action;

import java.util.*;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.goc.common.utils.XPATHOCLGroup;
import net.ihe.gazelle.goc.common.utils.XPATHOCLRule;
import net.ihe.gazelle.goc.common.utils.XSITypeOCL;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTUtils;
import org.apache.commons.lang.StringUtils;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.AssertProcessorImpl;
import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempapi.visitor.RulesVisitor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AssertUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 *
 * @author Abderrazek Boufahja
 *
 */
public class AssertAnalyzer extends AssertProcessorImpl {

	private static final String CDA = "cda";

	private static final String HL7 = "hl7";

	protected Assert currentAssert;

	protected PackagedElement currentPackagedElement;

	protected List<OwnedRule> listGeneratedOwnedRule;

	protected Boolean generateDTDistinguisher = Boolean.FALSE;




	@SuppressWarnings("unchecked")
	@Override
	public void process(Assert t, Object... objects) {
		this.currentAssert = t;
		this.flattenAssert(currentAssert, 0);
		this.currentPackagedElement = (PackagedElement) objects[0];
		if (objects.length>1) {
			this.listGeneratedOwnedRule = (List<OwnedRule>) objects[1];
		}
		if(objects.length>2){
			this.generateDTDistinguisher = (Boolean) objects[2];
		}

		if (!GenerationProperties.ignoreAssertionGeneration && verifyThatAssertIsProcessable(this.currentAssert)){;
			super.process(t, objects);
		}
	}

	@Override
	public void processTest(String test) {
		if (test != null) {
			if (GenerationProperties.userSupportForAssertClass != null) {
				if (GenerationProperties.userSupportForAssertClass.acceptToUseAssert(this.currentAssert)) {
					this.fulfillOwnedRuleForConstraintGenerator();
				}
			}
			else {
				this.fulfillOwnedRuleForConstraintGenerator();
			}
		}
	}

	/**
	 *
	 * @param currentAssert2 an assert
	 * @param iParam : an index to search for circular let references
	 */
	public void flattenAssert(Assert currentAssert2, int iParam) {
		int i = iParam;
		if (currentAssert2 != null && currentAssert2.getTest() != null && currentAssert2.getParentObject() != null) {
			if (i>5) {
				ProblemHandler.handleError("there are a circular let references used by the assert : " + currentAssert2.getTest(),
						AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
				return;
			}
			if (currentAssert2.getTestFlatten() == null) {
				currentAssert2.setTestFlatten(currentAssert2.getTest());
			}
			if (currentAssert2.getParentObject() instanceof RuleDefinition) {
				RuleDefinition parent = (RuleDefinition)currentAssert2.getParentObject();
				List<Let> llets = RuleDefinitionUtil.getLets(parent);
				if (llets != null) {
					String oldTestFlattenValue = currentAssert2.getTestFlatten();
					for (Let let : llets) {
						if (let.getName() != null && let.getValue() != null) {
							currentAssert2.setTestFlatten(currentAssert2.getTestFlatten().replace("$" + let.getName(), let.getValue()));
						}
						else {
							ProblemHandler.handleRuleDefinitionError(parent, "there are a let element with a missing name/value attribute",
									AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
						}
					}
					if (!oldTestFlattenValue.equals(currentAssert2.getTestFlatten())) {
						flattenAssert(currentAssert2, ++i);
					}
				}
			}
			else if (currentAssert2.getParentObject() instanceof TemplateDefinition) {
				ProblemHandler.handleError("The flattened TemplateDefinition should not contains an assert element : " +
						((TemplateDefinition)currentAssert2.getParentObject()).getId(),
						AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
			}
		}

	}

	protected boolean verifyThatAssertIsProcessable(Assert currentAssert2) {
		if (currentAssert2 != null && currentAssert2.getTestFlatten() != null) {
			if (GenerationProperties.ignoreXPathAxes) {
				List<String> listProhibited = Arrays.asList("ancestor::", "ancestor-or-self::", "following::", "following-sibling::", "parent::",
						"preceding::", "preceding-sibling::");
				for (String proh : listProhibited) {
					if (currentAssert2.getTestFlatten().contains(proh)) {
						ProblemHandler.handleError("The flattened assert contrains reference to prohibited axis : '" + proh +
								"', for assert : " + currentAssert2.getTest(), AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
						return false;
					}
				}
			}
			Set<String> prefixes = getListNamespacesPrefix(AssertUtil.getParentTemplateDefinition(currentAssert2).getParentObject());
			// Uncomment nex line to enable prefixes (disabled for problems with SEQUOIA Assertions)
//			prefixes = cleanPrefixes(prefixes);
			for (String prefix : prefixes) {
				if (!prefix.equals(HL7) && !prefix.equals(CDA) && currentAssert2.getTestFlatten().contains(prefix + ":")) {
					ProblemHandler.handleError("The flattened assert contrains reference to unknown prefix : '" + prefix +
							"', for assert : " + currentAssert2.getTest(), AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
					return false;
				}
			}
			return true;
		}
		ProblemHandler.handleError("The flattened assert is null !", AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
		return false;
	}

	protected void fulfillOwnedRuleForConstraintGenerator(){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(this.currentPackagedElement.getId());

		String specification = this.generateOCLConstraint();
		String comment = this.generateCommentConstraint();
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(OwnedRuleKind.CONTEXT);
		ownedRule.setOwnedRuleType(extractOwnedRuleType());
		this.currentPackagedElement.getOwnedRule().add(ownedRule);
		if (this.listGeneratedOwnedRule != null) {
			this.listGeneratedOwnedRule.add(ownedRule);
		}
	}

	protected OwnedRuleType extractOwnedRuleType(){
		return this.extractOwnedRuleType(this.currentAssert);
	}

	protected OwnedRuleType extractOwnedRuleType(Assert assertion) {
		if (assertion != null && assertion.getRole() != null) {
			switch (assertion.getRole()) {
			case ERROR:
			case FATAL:
				return OwnedRuleType.ERROR;
			case WARNING:
				return OwnedRuleType.WARNING;
			case HINT:
				return OwnedRuleType.INFO;
			default:
				break;
			}
		}
		return null;
	}

	protected String generateCommentConstraint(){
		return this.generateCommentConstraint(this.currentAssert);
	}

	protected String generateCommentConstraint(Assert assertion) {
		String templateName = null;
		String path = null;
		String itemComment = "";
		if (assertion.getParentObject() instanceof RuleDefinition) {
			RuleDefinition constrainedRD = (RuleDefinition)assertion.getParentObject();
			templateName = RuleDefinitionUtil.getParentTemplateDefinition(constrainedRD).getDisplayName();
			DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(constrainedRD);
			path = DPathExtractor.createPathFromDParent(dparent);
			itemComment = ItemCommentGenerator.generateItemComment(constrainedRD);
		}
		else if (assertion.getParentObject() instanceof TemplateDefinition) {
			TemplateDefinition parent = (TemplateDefinition)assertion.getParentObject();
			templateName = parent.getDisplayName();
			DParent dp= RuleDefinitionUtil.getDParentOfRuleDefinition(TemplateDefinitionUtil.getFirstElement(parent));
			path = DPathExtractor.createPathFromDParent(dp);
			itemComment = ItemCommentGenerator.generateItemComment(parent);
		}

		String comment = AssertUtil.getStringValue(assertion);
		String tail;
		if (StringUtils.trimToNull(comment) != null) {
			tail = comment;
		}
		else {
			tail = assertion.getTest();
		}
		return "In " + templateName + ", the element defined by " + path + " shall verify this requirement : " + tail + itemComment;
	}

	protected String generateOCLConstraint(){
		return this.generateOCLConstraint(this.currentAssert);
	}

	public String generateOCLConstraintFromName(String test, RuleDefinition element){
		String res = "";
		String xpath = test.replace("hl7:", "cda:");
		xpath = xpath.replace("\\", "\\\\").replace("'", "\\\"");
		RuleDefinition parent = (RuleDefinition)(((ChoiceDefinition) element.getParentObject()).getParentObject());
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(element);
		String formattedXPATH = this.getFormatedXPATH(xpath);
		String name = RuleDefinitionUtil.getRealNameOfRuleDefinition(element);
		String prefix = getPrefixRuleWithName(contextRule,parent,name);
		res = getCleanedXPATHValidation(contextRule,element,formattedXPATH);
		if(formattedXPATH.contains("@xsi:type")){
			String extractedConstrains = extractConstraintFromXPATH(formattedXPATH,prefix);
			if(extractedConstrains != null){
				res = extractedConstrains;
			}
			else {
				res = prefix + "->forAll(aa | CommonOperationsStatic::validateByXPATHV2(aa, '"+formattedXPATH+ "'))";
			}
		}

		ImportElementHandler.handleAddingElement("CommonOperationsStatic");

		return res;
	}

	protected String generateOCLConstraint(Assert assertion) {
		String res = "";
		if (assertion.getParentObject() instanceof RuleDefinition) {
			RuleDefinition parent = (RuleDefinition)assertion.getParentObject();
			String xpath = assertion.getTestFlatten().replace("hl7:", "cda:");
			xpath = xpath.replace("\\", "\\\\").replace("'", "\\\"");
			String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(parent);
			if (parent.getParentObject() instanceof TemplateDefinition) {
				res = "CommonOperationsStatic::validateByXPATHV2(self, '" + this.getFormatedXPATH(xpath) + "')";
			}
			else if ((parent.getParentObject() instanceof RuleDefinition) ||
						(parent.getParentObject() instanceof ChoiceDefinition &&
							((ChoiceDefinition)parent.getParentObject()).getParentObject() instanceof RuleDefinition
						)
					){
				String formattedXPATH = this.getFormatedXPATH(xpath);
				String prefix = getPrefixRule(contextRule,parent);



				// replace invalid XSLT functions with a matchingValueSet()
				res = getCleanedXPATHValidation(contextRule,parent,formattedXPATH);

				// extract type constraint from xpath
				if(formattedXPATH.contains("@xsi:type")){
					String extractedConstrains = extractConstraintFromXPATH(formattedXPATH,prefix);
					if(extractedConstrains != null){
						res = extractedConstrains;
					}
					else {
						res = prefix + "->forAll(aa | CommonOperationsStatic::validateByXPATHV2(aa, '"+formattedXPATH+ "'))";
					}
				}

				ImportElementHandler.handleAddingElement("CommonOperationsStatic");
			}
		}
		if(this.generateDTDistinguisher && currentAssert.getParentObject() instanceof RuleDefinition){
			RuleDefinition parentRD = (RuleDefinition) currentAssert.getParentObject();
			OCLGenerator generator = new OCLGenerator();
			res =  generator.appendDTDistinguisher(parentRD, res);

		}

		return res;
	}

	public static Set<String> getListNamespacesPrefix(Rules t) {
		final Set<String> res = new HashSet<>();
		class RuleDefinitionNSParser extends RuleDefinitionProcessorImpl {
			@Override
			public void processName(String name) {
				String[] nn = name.split(":");
				if (nn.length>1) {
					res.add(nn[0]);
				}
			}
		}
		ImplProvider implProvider = new ImplProvider() {

			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public <T extends Processor> T provideImpl(Class<T> t) {
				if (t.equals(RuleDefinitionProcessor.class)){
					return (T) new RuleDefinitionNSParser();
				}
				return null;
			}
		};
		RulesVisitor.INSTANCE.visitAndProcess(t, implProvider);
		return res;
	}

	public String getFormatedXPATH(String xpath) {
		String res = xpath;
		if (xpath != null) {
			res = res.replace("\\\"", "\"");
			res = res.replace("\"", "\\u0027");
		}
		return res;
	}

	private Set<String> cleanPrefixes(Set<String> prefixes){
		Set<String> cleanedPrefixes = new HashSet<>();
		for(String prefix:prefixes){
			if(!prefix.isEmpty()){
				cleanedPrefixes.add(prefix);
			}
		}
		if(cleanedPrefixes.size() == 0){
			cleanedPrefixes.add("NO_PREFIX_FOUND");
		}
		return cleanedPrefixes;
	}

	public String getPrefixRuleNF(String contextRule, RuleDefinition parent){
		return contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(parent) +
				OCLGenerator.getExtraSelectionIfNeeded(parent) + OCLCleaner.generateRejectOcl(true);
	}

	public String getPrefixRule(String contextRule, RuleDefinition parent){
		return contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(parent) +
				OCLGenerator.getExtraSelectionIfNeeded(parent);
	}

	public String getPrefixRuleWithName(String contextRule, RuleDefinition parent, String name){
		return contextRule + "."+ name + OCLGenerator.getExtraSelectionIfNeeded(parent);
	}




	public Map<String,String> getCleanedRuleAndReplacedElement(String formatedXPATH){
		ArrayList<String> newOcls = new ArrayList<>();
		Map<String,String> results = new THashMap<>();
		if(formatedXPATH.contains("document(")){
			String[] ocls = formatedXPATH.split(" or ");
			for(String ocl : ocls){
				if(!ocl.contains("document(")){
					newOcls.add(ocl);
				}
				else {
					results.put("replacedElement",ocl);
				}
			}
			if(results.containsKey("replacedElement")){
				String cleanedRule = StringUtils.join(newOcls," or ");
				results.put("cleanedRule",cleanedRule);
				return results;
			}
		}
		results.put("cleanedRule",formatedXPATH);
		return results;
	}


	public String getCleanedXPATHValidation(String contextRule, RuleDefinition parent, String formatedXPATH){
		String res = "";
		Map<String, String> cleaning = getCleanedRuleAndReplacedElement(formatedXPATH);
		res = contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(parent) +
				OCLGenerator.getExtraSelectionIfNeeded(parent) + OCLCleaner.generateRejectOcl(true) +
				"->forAll(aa | " +
				"CommonOperationsStatic::validateByXPATHV2(aa, '" + cleaning.get("cleanedRule") + "')" +
				" )";
		if(cleaning.containsKey("replacedElement")){
			String extractVS = AssertUtil.extractVSFromIncludeRule(cleaning.get("replacedElement"));
			if(extractVS == null){
				ProblemHandler.handleError("Couldn't extract valueSet from assert, vocabulary has been ignored", AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
			}
			else{
				// TODO: 20/07/2021 Still needs improvement for generic attribute
				res += " or " + contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(parent) +
						OCLGenerator.getExtraSelectionIfNeeded(parent) +
						"->forAll(CommonOperationsStatic::matchesCodeToValueSet('"+extractVS+"', unit))";
			}
		}
		return res;
	}

	public static String extractConstraintFromXPATH(String xpath, String prefix){
		List<XPATHOCLGroup> groups = AssertUtil.extractGroupsFromXPATH(xpath);
		return constructOCLRule(groups,prefix);
	}

	public static String constructOCLRule(List<XPATHOCLGroup> groups, String prefix){
		if(groups == null) return null;
		StringBuilder rule = new StringBuilder("(");
		for(XPATHOCLGroup group:groups){
			StringBuilder subGroupRule = new StringBuilder("(");
			for(int i=0;i<group.getRules().size();i++){
				String subRule;
				XPATHOCLRule xpathoclRule = group.getRules().get(i);
				String stringOperation = xpathoclRule.getOperation().getValue().isEmpty() ?""
						:" "+xpathoclRule.getOperation().getValue()+" ";
				if(group.getRules().get(i) instanceof XSITypeOCL){
					XSITypeOCL xsiTypeOCL = (XSITypeOCL)xpathoclRule;
					String not = xsiTypeOCL.isNegativeOperation()?"not(":"";
					String closingNot = not.equals("not(")?")":"";
					String dt = DTUtils.getUMLDatatype(xsiTypeOCL.getType());

					if(dt==null){
						return null;
					}
					//String formattedType = xsiTypeOCL.getType().replace("_","");
					subRule = prefix+"->forAll("+not+"oclIsKindOf("+dt+")"
							+closingNot+")"+stringOperation;
					ImportElementHandler.handleAddingElement(dt);

				}
				else{
					subRule = prefix+"->forAll(aa | CommonOperationsStatic::validateByXPATHV2(aa, '"
							+xpathoclRule.getContent()+"') )"+stringOperation;
				}
				subGroupRule.append(subRule);
			}

			//closing a group
			String groupOperationString = group.getOperation().getValue().isEmpty() ?")"
					:") "+group.getOperation().getValue()+" ";
			subGroupRule.append(groupOperationString);

			//appending to general rule
			rule.append(subGroupRule);
		}
		rule.append(")");
		return rule.toString();
	}

	/**
	 * Process the XPATH assertion and return it as an OwnedRule
	 * Mainly used for choices handling
	 * @param test XPATH assertion (Ex: effectiveTime[@xsi:type='CD'] =&gt; test="@xsi:type='CD'")
	 * @param element element holding the test
	 * @param pe [NULLABLE] temporary used package for generation rules
	 * @return returns the generated ownedRule
	 */
	public OwnedRule processTestAndReturn(String test, RuleDefinition element, PackagedElement pe){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		if(pe != null){
			ownedRule.setConstrainedElement(this.currentPackagedElement.getId());
		}
		String specification = this.generateOCLConstraintFromName(test,element);
		ownedRule.getSpecification().setBody(specification);
		ownedRule.setOwnedRuleKind(OwnedRuleKind.CONTEXT);
		return ownedRule;
	}




}
