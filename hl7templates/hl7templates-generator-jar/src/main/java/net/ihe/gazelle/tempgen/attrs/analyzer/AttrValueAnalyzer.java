package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrValueAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.name and attribute.value not null
	 *   attribute.getParentObject() is of type RuleDefinition
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String name = attribute.getName();
		String value = attribute.getValue();
		return AttrProcessorUtil.generateOCLConstraintFromNameAndValue(attribute, name, value, this.getProcessIdentifier());
	}
	
	/**
	 *   attribute.name and attribute.value not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, attribute.getName(), attribute.getValue());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_VALUE_PROCESS.getValue();
	}

}
