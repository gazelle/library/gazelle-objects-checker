package net.ihe.gazelle.tempgen.inc.checker;

import gnu.trove.map.hash.THashMap;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempgen.nodes.check.NodesChecker;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;

/**
 * 
 * @author Abderrazek Boufahja
 * not final because extended by tests
 *
 */
public final class IncludeChecker {
	
	private IncludeChecker() {
		// private constructor
	}
	
	public static boolean includesAreCircular(Decor decor){
		return !mapCircularIncludes(decor).isEmpty();
	}
	
	public static Map<String, Set<String>> mapCircularIncludes(Decor decor){
		Map<String, Set<String>> mapIncludedTemplates = new THashMap<>();
		Map<String, Set<String>> mapCircularTemplates = new THashMap<>();
		DecorVisitor.INSTANCE.visitAndProcess(decor, new IncludeCheckerImplProvider(), mapIncludedTemplates);
		return NodesChecker.extractCircularCall(mapIncludedTemplates, mapCircularTemplates);
	}

}
