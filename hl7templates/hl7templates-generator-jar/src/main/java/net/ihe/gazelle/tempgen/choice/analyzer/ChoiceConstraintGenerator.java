package net.ihe.gazelle.tempgen.choice.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public interface ChoiceConstraintGenerator {
	
	String generateOCLConstraint(ChoiceDefinition choiceDefinition);
	
	String generateCommentConstraint(ChoiceDefinition choiceDefinition);
	
	String getProcessIdentifier();

}
