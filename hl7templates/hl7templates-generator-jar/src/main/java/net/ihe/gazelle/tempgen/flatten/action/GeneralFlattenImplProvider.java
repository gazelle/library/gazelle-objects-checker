package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.interfaces.AssertProcessor;
import net.ihe.gazelle.tempapi.interfaces.IncludeDefinitionProcessor;
import net.ihe.gazelle.tempapi.interfaces.LetProcessor;
import net.ihe.gazelle.tempapi.interfaces.ProjectProcessor;
import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class GeneralFlattenImplProvider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(AssertProcessor.class)){
			return (T) new AssertFlattenProc();
		}
		else if (t.equals(LetProcessor.class)){
			return (T) new LetFlattenProc();
		}
		else if (t.equals(RuleDefinitionProcessor.class)){
			return (T) new RuleDefinitionFlattenProc();
		}
		else if (t.equals(IncludeDefinitionProcessor.class)){
			return (T) new IncludeDefinitionFlattenProc();
		}
		else if (t.equals(ProjectProcessor.class)){
			return (T) new ProjectFlattenProc();
		}
		return null;
	}
	
}
