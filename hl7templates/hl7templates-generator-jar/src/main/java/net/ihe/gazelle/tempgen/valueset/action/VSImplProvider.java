package net.ihe.gazelle.tempgen.valueset.action;

import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptListProcessor;
import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptProcessor;
import net.ihe.gazelle.tempapi.interfaces.ValueSetProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VSImplProvider implements ImplProvider {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(ValueSetConceptListProcessor.class)){
			return (T) new ValueSetConceptListProc();
		}
		else if (t.equals(ValueSetConceptProcessor.class)){
			return (T) new ValueSetConceptProc();
		}
		else if (t.equals(ValueSetProcessor.class)){
			return (T) new ValueSetProc();
		}
		return null;
	}

}
