package net.ihe.gazelle.tempgen.action;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.ChoiceDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceConstraintGenerator;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMaximumMultiplicityANalyzer;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMinimumMultiplicityANalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.*;
import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.choices.utils.ChoiceDefaultUtil;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.handler.util.ChoiceDefinitionProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceDefinitionAnalyzer extends ChoiceDefinitionProcessorImpl {
	
	protected ChoiceDefinition currentChoiceDefinition;
	
	protected PackagedElement currentPackagedElement;
	
	protected Boolean ignoreTemplateIdRequirements = Boolean.FALSE;

	private List<OwnedRule> listGeneratedOwnedRule = new ArrayList<>();

	private static final String PATH_ELEM = "(([^/@]+?:)([^/\\[]+)(\\[(.+)])?)";
	private static final Pattern PATT_PATH_ELEM = Pattern.compile(PATH_ELEM);

	private static org.slf4j.Logger log = LoggerFactory.getLogger(ChoiceDefinitionAnalyzer.class);

	
	/**
	 *   objects[0] is the PackagedElement
	 */
	@Override
	public void process(ChoiceDefinition t, Object... objects) {
		if (objects == null || objects.length<1 || !(objects[0] instanceof PackagedElement)) {
			ProblemHandler.handleError("The method ChoiceDefinitionAnalyzer::process is called with a bad objects attribute", 
					AnalyzerEnum.CHOICE_PROCESS.getValue());
			return;
		}
		if (objects.length>1) {
			this.ignoreTemplateIdRequirements = (Boolean)objects[1];
		}
		this.currentChoiceDefinition = t;
		this.currentPackagedElement = (PackagedElement) objects[0];
		if (verifyThatChoiceIsProcessable(this.currentChoiceDefinition)){
			super.process(t, objects);
		}
		
//		-- to be done

//		-- processed
//		this.processConstraints
//		this.processDescs
//		this.processElements
//		this.processIncludes : if there are includes : abord the process
//		this.processMaximumMultiplicity
//		this.processMinimumMultiplicity
//		this.processItem
//		processContains : nothing to do, processed during flattening
	}
	
	@Override
	public void processItem(Item item) {
		Set<String> setItems = ItemUtil.extractRelatedItems(this.currentChoiceDefinition);
		if (setItems != null){
			for (OwnedRule or : this.listGeneratedOwnedRule) {
				TamlHandler.handleAllTAML(setItems, or);
			}
		}
	}
	
	@Override
	public void processMaximumMultiplicity(String maximumMultiplicityString) {
		Integer maximumMultiplicity = MultiplicityUtil.extractMaximumMultiplicity(maximumMultiplicityString);
		if (processingMaximumMultiplicityIsPossible(maximumMultiplicity)) {
			fulfillOwnedRuleForConstraintGenerator(new ChoiceMaximumMultiplicityANalyzer(), OwnedRuleKind.CARDINALITY);
		}
	}
	
	private boolean processingMaximumMultiplicityIsPossible(Integer maximumMultiplicity) {
		if (maximumMultiplicity == null || maximumMultiplicity.intValue() >=MultiplicityUtil.MAX_INTEGER) {
			return false;
		}
		ChoiceDefault choiceDefault = extractChoiceDefaultFromChoiceDefinition(this.currentChoiceDefinition);
		if (choiceDefault != null && choiceDefault.getMax() != null) {
			if (maximumMultiplicity.intValue() < choiceDefault.getMax().intValue()) {
				return true;
			}
			else if (maximumMultiplicity.intValue() > choiceDefault.getMax().intValue()) {
				ProblemHandler.handleError("The choice contains a problem, the maximum specified is bigger than "
						+ "the default maximum choice value : " + choiceDefault.getMax() + ", specified for the type " +
						choiceDefault.getType(), 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			return false;
		}
		return true;
	}

	@Override
	public void processMinimumMultiplicity(Integer minMultiplicity) {
		if (processingMinimumMultiplicityIsPossible(minMultiplicity)) {
			fulfillOwnedRuleForConstraintGenerator(new ChoiceMinimumMultiplicityANalyzer(), OwnedRuleKind.CARDINALITY);
		}
	}
	
	protected boolean processingMinimumMultiplicityIsPossible(Integer minMultiplicity) {
		if (minMultiplicity==null || minMultiplicity.intValue() <=0) {
			return false;
		}
		ChoiceDefault choiceDefault = extractChoiceDefaultFromChoiceDefinition(this.currentChoiceDefinition);
		if (choiceDefault != null && choiceDefault.getMin() != null) {
			if (minMultiplicity.intValue() > choiceDefault.getMin().intValue()) {
				return true;
			}
			else if (minMultiplicity.intValue() < choiceDefault.getMin().intValue()) {
				ProblemHandler.handleError("The choice contains a problem, the minimum specified is lower than "
						+ "the default minimum choice value : " + choiceDefault.getMin() + ", specified for the type " +
						choiceDefault.getType(), 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			return false;
		}
		return true;
	}

	protected void fulfillOwnedRuleForConstraintGenerator(ChoiceConstraintGenerator choiceConstraintGenerator, OwnedRuleKind ownedRuleKind){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(this.currentPackagedElement.getId());
		String specification = choiceConstraintGenerator.generateOCLConstraint(this.currentChoiceDefinition);
		String comment = choiceConstraintGenerator.generateCommentConstraint(this.currentChoiceDefinition) +
				ItemCommentGenerator.generateItemComment(currentChoiceDefinition);
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(ownedRuleKind);
		this.currentPackagedElement.getOwnedRule().add(ownedRule);
		this.listGeneratedOwnedRule.add(ownedRule);
	}
	
	@Override
	public void processElements(List<RuleDefinition> elements) {
		if (elements != null) {
			for (RuleDefinition ruleDefinition : elements) {
				(new RuleDefinitionAnalyzer()).process(ruleDefinition, this.currentPackagedElement, this.ignoreTemplateIdRequirements);
			}
		}
	}

	public static ChoiceDefault extractChoiceDefaultFromChoiceDefinition(ChoiceDefinition cd) {
		String type = null;
		if (cd.getParentObject() instanceof RuleDefinition) {
			type = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition)cd.getParentObject());
		}
		List<String> listElementsName = new ArrayList<>();
		for (RuleDefinition rd : ChoiceDefinitionUtil.getElements(cd)) {
			String name = RuleDefinitionUtil.getRealNameOfRuleDefinition(rd);
			if (name != null) {
				listElementsName.add(name);
			}
		}
		return ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements(type, listElementsName);
	}

	/**
	 * Process predicates provided in the name for choices
	 * Process elements normally when they are not identical and don't have predicates
	 * NOTICE: this method does not process numbered predicates due to a lack of functional specifications for the CDA
	 * @param elements elements in the ChoiceDefinition
	 */
	@Deprecated(since = "3.1.0", forRemoval = true)
	@Override
	public void processPredicates(List<RuleDefinition> elements){


		// TODO: 24/06/2021  check the homogeneity of predicates for performance
			if(elements != null) {
				if (choiceHasNumberedPredicates(elements)) {
					OwnedRule choiceOwnedRule = processNumberedPredicates(elements);
					this.listGeneratedOwnedRule.add(choiceOwnedRule);
					currentPackagedElement.getOwnedRule().add(choiceOwnedRule);
				} else if (choiceHasPredicates(elements) && !choiceHasNumberedPredicates(elements)) {
					List<List<OwnedRule>> rules = generateSubRuleOfAssertionElements(elements);
					if (rules != null && rules.size() > 0) {
						OwnedRule generalOwnedRule = generateGeneraleRuleForChoice(rules);
						this.listGeneratedOwnedRule.add(generalOwnedRule);
						currentPackagedElement.getOwnedRule().add(generalOwnedRule);
					}
					// don't process elements if there is an error in one sub element
					else {
						return;
					}
				}
				// no predicates
				else {
					//case 1: identical names
					if (hasSameNames(elements)) {
						List<List<OwnedRule>> rules = generateSubRuleOfElements(elements);
						if (rules != null && rules.size() > 0) {
							OwnedRule generalOwnedRule = generateGeneraleRuleForChoice(rules);
							this.listGeneratedOwnedRule.add(generalOwnedRule);
							currentPackagedElement.getOwnedRule().add(generalOwnedRule);
						}
						// don't process elements if there is an error in one sub element
						else {
							return;
						}
					}
				}
			}
			//case 2: different names (normal process)
			this.processElements(elements);
	}

	protected void ANDRules(OwnedRule ownedRule, OwnedRule subOwnedRule){
		String and = ownedRule.getSpecification().getBody().equals("(")?"":" and ";
		ownedRule.getSpecification().setBody(ownedRule.getSpecification().getBody()+and+"("+subOwnedRule.getSpecification().getBody()+")");
	}

	protected void ORRules(OwnedRule ownedRule, OwnedRule subOwnedRule){
		String or = ownedRule.getSpecification().getBody().equals("(")?"":" or ";
		ownedRule.getSpecification().setBody(ownedRule.getSpecification().getBody()+or+"("+subOwnedRule.getSpecification().getBody()+")");
	}


	protected boolean verifyThatChoiceIsProcessable(ChoiceDefinition cd) {
		if (cd != null) {
			List<IncludeDefinition> incs = ChoiceDefinitionUtil.getIncludes(cd);
			if (incs != null && !incs.isEmpty()) {
				ProblemHandler.handleError("The choice is not processable because it contains some includes : " + incs.size(), 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			if (!ChoiceDefinitionProblemUtil.choiceContainsValidsubElements(cd)) {
				ProblemHandler.handleRuleDefinitionError((RuleDefinition)cd.getParentObject(), 
						"There are a problem : the choice contains sub elements which are not from CDA", 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			return true;
		}
		return false;
	}

	protected OwnedRule getFulfilledOwnedRuleForConstraintGenerator(ConstraintGenerator constraintGenerator,RuleDefinition ruleDefinition, PackagedElement pe, OwnedRuleKind ownedRuleKind, OwnedRuleType ownedRuleType) {
		if (constraintGenerator == null) {
			return null;
		}
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(pe.getId());
		String specification = constraintGenerator.generateOCLConstraint(ruleDefinition);
		String comment = constraintGenerator.generateCommentConstraint(ruleDefinition) +
				ItemCommentGenerator.generateItemComment(ruleDefinition);
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(ownedRuleKind);
		if (ownedRuleType != null){
			ownedRule.setOwnedRuleType(ownedRuleType);
		}
		return ownedRule;

	}

	//TODO: Functional description needed to generalize numbered predicates handling
	//this numbered predicates process is specific to DGSante and similar structures
	protected OwnedRule processNumberedPredicates(List<RuleDefinition> elements){
		OwnedRule generalOwnedRule = OwnedRuleUtil.initOwnedRule();
		generalOwnedRule.setOwnedRuleKind(OwnedRuleKind.CHOICE);
		generalOwnedRule.getSpecification().setBody("(");

		for(int i=0;i<elements.size();i++){

			// for now this process just numbers
			if(elementHasNumberedPredicate(elements.get(i))){

				OwnedRule subGeneralOwnedRule = OwnedRuleUtil.initOwnedRule();
				subGeneralOwnedRule.setOwnedRuleKind(OwnedRuleKind.CHOICE);
				subGeneralOwnedRule.getSpecification().setBody("(");

				// Datatype
				if(elements.get(i).getDatatype() != null){
					OwnedRule ownedRule = getFulfilledOwnedRuleForConstraintGenerator(new RDDatatypeAnalyzer(),elements.get(i),currentPackagedElement,OwnedRuleKind.DATATYPE,null);
					ANDRules(subGeneralOwnedRule,ownedRule);
				}

				//Max multiplicity
				if(elements.get(i).getMaximumMultiplicity() != null){
					OwnedRule ownedRule = getFulfilledOwnedRuleForConstraintGenerator(new RDMaximumMultiplicityAnalyzer(),elements.get(i),currentPackagedElement,OwnedRuleKind.CARDINALITY,null);
					ANDRules(subGeneralOwnedRule,ownedRule);
				}

				//Min multiplicity
				if(elements.get(i).getMinimumMultiplicity() != null){
					OwnedRule ownedRule = getFulfilledOwnedRuleForConstraintGenerator(new RDMinimumMultiplicityAnalyzer(),elements.get(i), currentPackagedElement,OwnedRuleKind.CARDINALITY,null);
					ANDRules(subGeneralOwnedRule,ownedRule);
				}

				//Vocabulary
				if(elements.get(i).getVocabulary() != null && elements.get(i).getVocabulary().size()>0){
					OwnedRule ownedRule = getFulfilledOwnedRuleForConstraintGenerator(new RDVocabularyAnalyzer(),elements.get(i), currentPackagedElement,OwnedRuleKind.VOCABULARY,null);
					ANDRules(subGeneralOwnedRule,ownedRule);
				}

				//DGSante specification: Separate the <value /> element by code
				if(i == 0){
					String spec = "(not self.value->forAll( (not oclAsType(CD).codeSystem.oclIsUndefined())))";
					subGeneralOwnedRule.getSpecification().setBody(subGeneralOwnedRule.getSpecification().getBody()+" and ("+spec+")");

				}

				subGeneralOwnedRule.getSpecification().setBody(subGeneralOwnedRule.getSpecification().getBody()+")");
				ORRules(generalOwnedRule,subGeneralOwnedRule);
			}
		}
		generalOwnedRule.getSpecification().setBody(generalOwnedRule.getSpecification().getBody()+")");
		String comment = "In eHDSI Allergies And Intolerances, in /hl7:observation[hl7:templateId/@root='1.3.6.1.4.1.12559.11.10.1.3.1.3.17']" +
				"hl7:value element SHALL verify at most ONE constraint from the choices list";
		generalOwnedRule.getOwnedComment().setBody(comment);
		return generalOwnedRule;
	}

	protected boolean choiceHasNumberedPredicates(List<RuleDefinition> elements){
		for(RuleDefinition ruleDefinition:elements){
			if(elementHasNumberedPredicate(ruleDefinition)){
				return true;
			}
		}
		return false;
	}

	protected boolean choiceHasPredicates(List<RuleDefinition> elements){
		for(RuleDefinition ruleDefinition:elements){
			if(elementHasPredicate(ruleDefinition)){
				return true;
			}
		}
		return false;
	}

	protected boolean elementHasNumberedPredicate(RuleDefinition ruleDefinition){
		Matcher matcher = PATT_PATH_ELEM.matcher(ruleDefinition.getName());
		return (matcher.find() && matcher.group(5) != null && StringUtils.isNumeric(matcher.group(5)));
	}

	protected boolean elementHasPredicate(RuleDefinition ruleDefinition){
		Matcher matcher = PATT_PATH_ELEM.matcher(ruleDefinition.getName());
		return (matcher.find() && matcher.group(4)!=null);
	}

	public boolean hasSameNames(List<RuleDefinition> ruleDefinitions){
		Map<String,Integer> namesFreq = new THashMap<>();
		for(RuleDefinition ruleDefinition:ruleDefinitions){
			if(namesFreq.get(ruleDefinition.getName()) == null){
				namesFreq.put(ruleDefinition.getName(), Integer.valueOf(1));
			}
			else{
				return true;
			}
		}
		return false;

	}

	private List<List<OwnedRule>> generateSubRuleOfElements(List<RuleDefinition> elements){
		List<List<OwnedRule>> rules = new ArrayList<>();
		log.info("processing {} element in choices", Integer.valueOf(elements.size()));
		for(RuleDefinition element:elements){
			List<OwnedRule> ownedRules = (new RuleDefinitionAnalyzer()).processAndReturn(element, null);
			if(ownedRules != null){
				rules.add(ownedRules);
			}
			else{
				ProblemHandler.handleRuleDefinitionError(element,
						"There is a problem: One or more sub element of the choices contains an error",
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return null;
			}
		}
		return rules;
	}

	public List<List<OwnedRule>> generateSubRuleOfAssertionElements(List<RuleDefinition> elements){
		List<List<OwnedRule>> rules = new ArrayList<>();
		for(RuleDefinition element:elements){
			List<OwnedRule> ownedRules = generateRulesOfAssertionElements(element);
			if(ownedRules != null){
				rules.add(ownedRules);
			}
			else{
				ProblemHandler.handleRuleDefinitionError(element,
						"There is a problem: One or more sub element of the choices contains an error",
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return null;
			}
		}
		return rules;
	}

	private List<OwnedRule> generateRulesOfAssertionElements(RuleDefinition element){
		String test = AssertUtil.extractAssertFromName(element.getName());
		if(test == null){
			ProblemHandler.handleRuleDefinitionError((RuleDefinition)element.getParentObject(),
					"There is a problem: the assertion in the element name is not correct",
					AnalyzerEnum.CHOICE_PROCESS.getValue());
			return null;
		}
		List<OwnedRule> ownedRules = (new RuleDefinitionAnalyzer()).processAndReturn(element, null);
		if(!test.equals(element.getName()) && ownedRules != null){
			OwnedRule assertRule = (new AssertAnalyzer()).processTestAndReturn(test, element,null);
			ownedRules.add(assertRule);
		}
		return ownedRules;
	}

	public OwnedRule generateGeneraleRuleForChoice(List<List<OwnedRule>> rules){
		OwnedRule generalOwnedRule = OwnedRuleUtil.initOwnedRule();
		generalOwnedRule.setOwnedRuleKind(OwnedRuleKind.CHOICE);
		generalOwnedRule.getSpecification().setBody("(");
		for(List<OwnedRule> innerRules:rules){
			OwnedRule subGeneralOwnedRule = OwnedRuleUtil.initOwnedRule();
			subGeneralOwnedRule.setOwnedRuleKind(OwnedRuleKind.CHOICE);
			subGeneralOwnedRule.getSpecification().setBody("(");
			for(OwnedRule ownedRule:innerRules){
				ANDRules(subGeneralOwnedRule, ownedRule);
			}
			subGeneralOwnedRule.getSpecification().setBody(subGeneralOwnedRule.getSpecification().getBody()+")");
			ORRules(generalOwnedRule,subGeneralOwnedRule);
		}
		generalOwnedRule.getSpecification().setBody(generalOwnedRule.getSpecification().getBody()+")");
		String templateName = ChoiceDefinitionUtil.getParentTemplateDefinition(currentChoiceDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent((RuleDefinition)currentChoiceDefinition.getParentObject());
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		String desc = "In " + templateName + ", in " + parentPath + ", elements under the Choice tag SHALL verify at least ONE constraint from the choices list ";
		generalOwnedRule.getOwnedComment().setBody(desc);
		log.info("Constructed choice rule: {}",generalOwnedRule.getSpecification().getBody());
		return generalOwnedRule;
	}

}
