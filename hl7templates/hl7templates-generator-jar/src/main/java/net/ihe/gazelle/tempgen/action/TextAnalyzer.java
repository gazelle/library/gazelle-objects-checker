package net.ihe.gazelle.tempgen.action;

import java.util.List;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TextAnalyzer implements Processor<List<String>> {
	
	private RuleDefinition selectedRuleDefinition;
	
	public void setSelectedRuleDefinition(RuleDefinition selectedRuleDefinition) {
		this.selectedRuleDefinition = selectedRuleDefinition;
	}
	
	/**
	 *   objects[0] a ruleDefinition and objects[1] a PackagedElement
	 */
	@Override
	public void process(List<String> text, Object... objects) {
		if ((objects.length<2) || !(objects[0] instanceof RuleDefinition) || !(objects[1] instanceof PackagedElement)){
			ProblemHandler.handleError("The method TextAnalyzer::process is called with a bad objects attribute", 
					AnalyzerEnum.RD_TEXT_PROCESS.getValue());
			return;
		}
		RuleDefinition ruleDefinition = (RuleDefinition) objects[0];
		PackagedElement pe = (PackagedElement) objects[1];
		if (ruleDefinition == null || pe == null || text == null || text.isEmpty()){
			return;
		}
		this.selectedRuleDefinition = ruleDefinition;
		if (RuleDefinitionProblemUtil.isStringDatatype(this.selectedRuleDefinition)){
			OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
			ownedRule.setConstrainedElement(pe.getId());
			String specification = this.generateOCLConstraint(text);
			String comment = this.generateCommentConstraint(text) + ItemCommentGenerator.generateItemComment(selectedRuleDefinition);
			ownedRule.getSpecification().setBody(specification);
			ownedRule.getOwnedComment().setBody(comment);
			ownedRule.setOwnedRuleKind(OwnedRuleKind.FIXEDVAL);
			pe.getOwnedRule().add(ownedRule);
		}
		else {
			ProblemHandler.handleRuleDefinitionError(this.selectedRuleDefinition, 
					"The text element is provided, however the element is not of type ST or its inherited datatypes", 
					AnalyzerEnum.RD_TEXT_PROCESS.getValue());
		}
	}

	protected String generateOCLConstraint(List<String> text) {
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(selectedRuleDefinition);
		StringBuilder sb = new StringBuilder();
		sb.append(contextRule).append(".").append(RuleDefinitionUtil.getRealNameOfRuleDefinition(selectedRuleDefinition)).append(OCLGenerator.getExtraSelectionIfNeeded(selectedRuleDefinition)).append(OCLCleaner.generateRejectOcl(true)).append("->forAll(");
		int i = 0;
		for (String string : text) {
			if (i>0){
				sb.append(" or ");
			}
			sb.append("(").append(extractOCLFromProp(string)).append(")");
			i++;
		}
		sb.append(")");
		return sb.toString();
	}

	protected String extractOCLFromProp(String string) {
		return "getListStringValues()->forAll(st : String | st='" + string.replace("'", "\\u0027") + "')";
	}

	protected String generateCommentConstraint(List<String> text) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(selectedRuleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition);
		String path = DPathExtractor.createPathFromDParent(dparent);
		StringBuilder sb = new StringBuilder();
		sb.append("In ").append(templateName).append(", in ").append(path).append(", ");
		int i = 0;
		for (String string : text) {
			if (i>0){
				sb.append(" OR ");
			}
			int size = text.size();
			if (size>1){
				sb.append("(").append(generateDescrForProp(string)).append(")");
			}
			else {
				sb.append(generateDescrForProp(string));
			}
			i++;
		}
		return sb.toString();
	}

	protected String generateDescrForProp(String string) {
		return "the string value shall have the value '" + string + "'";
	}

}
