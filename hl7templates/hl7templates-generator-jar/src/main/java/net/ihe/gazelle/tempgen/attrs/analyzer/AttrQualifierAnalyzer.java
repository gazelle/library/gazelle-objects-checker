package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrQualifierAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.qualifier not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		return parentOCL + ".qualifier" + OCLCleaner.generateRejectOcl(false) + "->forAll(aa | aa='" + attribute.getQualifier() + "')";
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		TemplateDefinition temp = AttributeUtil.getParentTemplateDefinition(attribute);
		String templateName = "--";
		if (temp != null) {
			templateName = temp.getDisplayName();
		}
		DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + parentPath + ", the attribute qualifier SHALL have the value '" + 
				attribute.getQualifier() + "' if present";
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_QUALIFIER_PROCESS.getValue();
	}

}
