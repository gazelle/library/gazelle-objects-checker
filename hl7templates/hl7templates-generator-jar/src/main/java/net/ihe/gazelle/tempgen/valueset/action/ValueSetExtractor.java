package net.ihe.gazelle.tempgen.valueset.action;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.SVSMarshaller;
import net.ihe.gazelle.tempapi.visitor.ValueSetVisitor;
import net.ihe.gazelle.tempgen.action.FileReadWrite;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.valueset.flatten.VSFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ValueSetExtractor {
	
	
	private static Logger log = LoggerFactory.getLogger(ValueSetExtractor.class);
	
	private ValueSetExtractor() {}
	
	public static void flattenAndExtractValueSetsFromDecor(Decor decorParam, File outputFile) throws IOException {
		Decor decor = VSFlattener.flattenDecorForValueSets(decorParam);
		extractValueSetsFromDecor(decor, outputFile);
	}
	
	public static void extractValueSetsFromDecor(Decor decor, File outputFile) throws IOException {

		ArrayList<String> processedValueSets = new ArrayList<>();
		ArrayList<String> noConceptValueSets = new ArrayList<>();

		if (decor.getTerminology() != null && decor.getTerminology().getValueSet() != null ) {
			if (!outputFile.isDirectory()) {
				ProblemHandler.handleError("The outputFile is not a directory", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
				return;
			}
			for (ValueSet valueSet : decor.getTerminology().getValueSet()) {
				RetrieveValueSetResponseType resp = new RetrieveValueSetResponseType();
				List<net.ihe.gazelle.datatypes.CE> concepts = new ArrayList<>(10000);
				VSImplProvider vsImplProvider = new VSImplProvider();
				ValueSetVisitor.INSTANCE.visitAndProcess(valueSet, vsImplProvider, resp, concepts);
				if(resp.getValueSet().getConceptList() != null && !resp.getValueSet().getConceptList().isEmpty()){
					resp.getValueSet().getConceptList().get(0).getConcept().addAll(concepts);
				}
				String respString = SVSMarshaller.marshall(resp);
				if (respString.contains("Concept ")) {
					processedValueSets.add(valueSet.getId());
					String filepath = createFilePath(outputFile, valueSet);
					File out = new File(filepath);
					if (out.exists()) {
						log.info("{} already used !", valueSet.getId());
					}
					else{
						try(FileOutputStream f = new FileOutputStream(out)){
							BufferedOutputStream bos = new BufferedOutputStream(f);
							bos.write(respString.getBytes());
							bos.close();
						}
					}
				}
				else {
					ProblemHandler.handleError("There are no concept in the declared valueset : " + valueSet.getId(), 
							AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
					noConceptValueSets.add(valueSet.getId());
				}
			}

			StringBuilder processedValueSetsLog = new StringBuilder("[ ");
			for(String valueSet:processedValueSets){
				processedValueSetsLog.append(valueSet).append(", ");
			}
			processedValueSetsLog.append("]");

			StringBuilder noConceptValueSetsLog = new StringBuilder("[ ");
			for(String valueSet:noConceptValueSets){
				noConceptValueSetsLog.append(valueSet).append(", ");
			}
			noConceptValueSetsLog.append("]");

			log.info("Number of processed valueSets: "+processedValueSets.size());
			log.info("List of processed valueSets: "+processedValueSetsLog);

			log.info("Number of valueSet with no concept: "+noConceptValueSets.size());
			log.info("List of valueSet with no concept: "+noConceptValueSetsLog);
		}
	}

	private static String createFilePath(File outputFile, ValueSet valueSet) {
		StringBuilder sb = new StringBuilder();
		sb.append(outputFile.getAbsolutePath()).append(File.separator).append(valueSet.getId());
		if (valueSet.getEffectiveDate() != null) {
			sb.append("-").append(valueSet.getEffectiveDate().toString().replace(":", ""));
		}
		sb.append(".xml");
		return sb.toString();
	}
	
}
