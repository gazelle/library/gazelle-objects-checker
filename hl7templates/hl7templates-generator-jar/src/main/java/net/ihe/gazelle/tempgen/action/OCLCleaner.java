package net.ihe.gazelle.tempgen.action;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class OCLCleaner {
	
	private OCLCleaner() {
		// private constructor
	}
	
	public static String generateRejectOcl(boolean containNullFlavor){
		if (containNullFlavor){
			return "->reject(not nullFlavor.oclIsUndefined())";
		}
		else {
			return "";
			//return "->reject(oclIsUndefined())";
		}
	}

}
