package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptListConceptProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptListConcept;

public abstract class DataSetConceptListConceptProcessorImpl implements DataSetConceptListConceptProcessor {

	@Override
	public void process(DataSetConceptListConcept t, Object... objects) {
		if (t != null){
			this.processDescs(t.getDesc());
			this.processException(t.isException());
			this.processId(t.getId());
			this.processNames(t.getName());
			this.processSynonyms(t.getSynonym());
		}
	}

}
