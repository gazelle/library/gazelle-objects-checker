package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IdentifierAssociation;

public interface IdentifierAssociationProcessor extends Processor<IdentifierAssociation> {
	
		public void processConceptId(String conceptId);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processRef(String ref);
		public void processVersionLabel(String versionLabel);
	
}
