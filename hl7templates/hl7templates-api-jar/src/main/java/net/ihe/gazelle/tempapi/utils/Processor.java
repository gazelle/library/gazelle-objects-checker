package net.ihe.gazelle.tempapi.utils;

public interface Processor<T> {
	
	public void process(T t, Object...objects);

}
