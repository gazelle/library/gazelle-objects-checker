package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.AuthorityTypeProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AuthorityType;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AuthorityTypeUtil;

public class AuthorityTypeVisitor implements HL7TemplateVisitor<AuthorityType> {

	public static final AuthorityTypeVisitor INSTANCE = new AuthorityTypeVisitor();

	@Override
	public void visitAndProcess(AuthorityType t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		AuthorityTypeProcessor processor = implProvider.provideImpl(AuthorityTypeProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process addrLine
		if (AuthorityTypeUtil.getAddrLines(t) != null) {
			for(AddrLine _addrLine : AuthorityTypeUtil.getAddrLines(t)) {
				AddrLineVisitor.INSTANCE.visitAndProcess(_addrLine, implProvider, objects);
			}
		}
	}

}
