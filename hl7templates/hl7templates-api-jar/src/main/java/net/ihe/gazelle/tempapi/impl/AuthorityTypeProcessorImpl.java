package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.AuthorityTypeProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AuthorityType;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AuthorityTypeUtil;

public abstract class AuthorityTypeProcessorImpl implements AuthorityTypeProcessor {

	@Override
	public void process(AuthorityType t, Object... objects) {
		if (t != null){
			this.processAddrLines(AuthorityTypeUtil.getAddrLines(t));
			this.processId(t.getId());
			this.processName(t.getName());
		}
	}

}
