package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Context;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateStatusCodeLifeCycle;

public interface TemplateDefinitionProcessor extends Processor<TemplateDefinition> {
	
		public void process_asserts(List<Assert> _asserts);
		public void processAttributes(List<Attribute> attributes);
		public void processChoices(List<ChoiceDefinition> choices);
		public void processClassifications(List<TemplateProperties> classifications);
		public void processConstraints(List<FreeFormMarkupWithLanguage> constraints);
		public void processContext(Context context);
		public void processDefineVariables(List<DefineVariable> defineVariables);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processDisplayName(String displayName);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processElements(List<RuleDefinition> elements);
		public void processExamples(List<Example> examples);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processId(String id);
		public void processIdent(String ident);
		public void processIncludes(List<IncludeDefinition> includes);
		public void processIsClosed(Boolean b);
		public void processLets(List<Let> lets);
		public void processName(String name);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processRef(String ref);
		public void processReferencedFrom(String referencedFrom);
		public void processRelationships(List<TemplateRelationships> relationships);
		public void processReports(List<Report> reports);
		public void processStatusCode(TemplateStatusCodeLifeCycle statusCode);
		public void processUrl(String url);
		public void processVersionLabel(String versionLabel);
		public void processContains(List<ContainDefinition> contains);
		public void processItem(Item item);
	
}
