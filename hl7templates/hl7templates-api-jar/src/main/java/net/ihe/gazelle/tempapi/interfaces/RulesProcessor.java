package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

public interface RulesProcessor extends Processor<Rules> {
	
		public void processTemplates(List<TemplateDefinition> templates);
		public void processTemplateAssociations(List<TemplateAssociationDefinition> templateAssociations);
	
}
