package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TransactionTrigger;

public interface TransactionTriggerProcessor extends Processor<TransactionTrigger> {
	
		public void processId(String id);
	
}
