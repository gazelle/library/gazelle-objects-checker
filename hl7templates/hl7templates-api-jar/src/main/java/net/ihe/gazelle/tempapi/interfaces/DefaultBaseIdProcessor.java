package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DecorObjectType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultBaseId;

public interface DefaultBaseIdProcessor extends Processor<DefaultBaseId> {
	
		public void processId(String id);
		public void processType(DecorObjectType type);
	
}
