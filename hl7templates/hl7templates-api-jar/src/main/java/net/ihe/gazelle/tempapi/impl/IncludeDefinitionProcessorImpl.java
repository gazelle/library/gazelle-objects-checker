package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.IncludeDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

public abstract class IncludeDefinitionProcessorImpl implements IncludeDefinitionProcessor {

	@Override
	public void process(IncludeDefinition t, Object... objects) {
		if (t != null){
			this.processConformance(t.getConformance());
			this.processConstraints(t.getConstraint());
			this.processDescs(t.getDesc());
			this.processExamples(t.getExample());
			this.processFlexibility(t.getFlexibility());
			this.processIsMandatory(t.isIsMandatory());
			this.processItem(t.getItem());
			this.processMaximumMultiplicity(t.getMaximumMultiplicity());
			this.processMinimumMultiplicity(t.getMinimumMultiplicity());
			this.processRef(t.getRef());
		}
	}

	@Override
	public void processConformance(ConformanceType conformance) {
		// to be overrided if needed to
	}

	@Override
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints) {
		// to be overrided if needed to
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
	}

	@Override
	public void processExamples(List<Example> examples) {
		// to be overrided if needed to
	}

	@Override
	public void processFlexibility(String flexibility) {
		// to be overrided if needed to
	}

	@Override
	public void processIsMandatory(Boolean b) {
		// to be overrided if needed to
	}

	@Override
	public void processItem(Item item) {
		// to be overrided if needed to
	}

	@Override
	public void processMaximumMultiplicity(String maximumMultiplicity) {
		// to be overrided if needed to
	}

	@Override
	public void processMinimumMultiplicity(Integer integer) {
		// to be overrided if needed to
	}

	@Override
	public void processRef(String ref) {
		// to be overrided if needed to
	}

}
