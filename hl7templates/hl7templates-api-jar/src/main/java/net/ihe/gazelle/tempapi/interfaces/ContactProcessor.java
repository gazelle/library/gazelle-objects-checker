package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Contact;

public interface ContactProcessor extends Processor<Contact> {
	
		public void processEmail(String email);
	
}
