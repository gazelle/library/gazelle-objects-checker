package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.SourceCodeSystemProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet.SourceCodeSystem;

public class SourceCodeSystemVisitor implements HL7TemplateVisitor<SourceCodeSystem> {

	public static final SourceCodeSystemVisitor INSTANCE = new SourceCodeSystemVisitor();

	@Override
	public void visitAndProcess(SourceCodeSystem t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		SourceCodeSystemProcessor processor = implProvider.provideImpl(SourceCodeSystemProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
