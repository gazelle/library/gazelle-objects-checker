package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet.SourceCodeSystem;

public interface SourceCodeSystemProcessor extends Processor<SourceCodeSystem> {
	
	
}
