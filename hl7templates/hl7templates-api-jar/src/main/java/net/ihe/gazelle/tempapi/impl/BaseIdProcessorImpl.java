package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.BaseIdProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BaseId;

public abstract class BaseIdProcessorImpl implements BaseIdProcessor {

	@Override
	public void process(BaseId t, Object... objects) {
		if (t != null){
			this.process_default(t.isDefault());
			this.processId(t.getId());
			this.processPrefix(t.getPrefix());
			this.processType(t.getType());
		}
	}

}
