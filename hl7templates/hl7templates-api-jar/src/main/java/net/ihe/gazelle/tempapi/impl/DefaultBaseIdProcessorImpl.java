package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DefaultBaseIdProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultBaseId;

public abstract class DefaultBaseIdProcessorImpl implements DefaultBaseIdProcessor {

	@Override
	public void process(DefaultBaseId t, Object... objects) {
		if (t != null){
			this.processId(t.getId());
			this.processType(t.getType());
		}
	}

}
