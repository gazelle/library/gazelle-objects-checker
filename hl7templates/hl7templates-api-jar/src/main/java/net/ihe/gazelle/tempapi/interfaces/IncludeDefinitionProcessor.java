package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

public interface IncludeDefinitionProcessor extends Processor<IncludeDefinition> {
	
		public void processConformance(ConformanceType conformance);
		public void processConstraints(List<FreeFormMarkupWithLanguage> constraints);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processExamples(List<Example> examples);
		public void processFlexibility(String flexibility);
		public void processIsMandatory(Boolean b);
		public void processItem(Item item);
		public void processMaximumMultiplicity(String maximumMultiplicity);
		public void processMinimumMultiplicity(Integer integer);
		public void processRef(String ref);
	
}
