package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.VarUseProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarUse;

public class VarUseVisitor implements HL7TemplateVisitor<VarUse> {

	public static final VarUseVisitor INSTANCE = new VarUseVisitor();

	@Override
	public void visitAndProcess(VarUse t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		VarUseProcessor processor = implProvider.provideImpl(VarUseProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
