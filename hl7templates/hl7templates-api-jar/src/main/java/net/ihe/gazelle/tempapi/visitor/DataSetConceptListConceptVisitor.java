package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptListConceptProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptListConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public class DataSetConceptListConceptVisitor implements HL7TemplateVisitor<DataSetConceptListConcept> {

	public static final DataSetConceptListConceptVisitor INSTANCE = new DataSetConceptListConceptVisitor();

	@Override
	public void visitAndProcess(DataSetConceptListConcept t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DataSetConceptListConceptProcessor processor = implProvider.provideImpl(DataSetConceptListConceptProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
