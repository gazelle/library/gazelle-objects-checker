package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodedConcept;

public interface CodeSystemConceptListProcessor extends Processor<CodeSystemConceptList> {
	
		public void processCodedConcepts(List<CodedConcept> codedConcepts);
	
}
