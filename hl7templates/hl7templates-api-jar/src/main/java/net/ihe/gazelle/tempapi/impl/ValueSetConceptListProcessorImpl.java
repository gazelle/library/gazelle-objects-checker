package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptListProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptListUtil;

public abstract class ValueSetConceptListProcessorImpl implements ValueSetConceptListProcessor {

	@Override
	public void process(ValueSetConceptList t, Object... objects) {
		if (t != null){
			this.processConcepts(ValueSetConceptListUtil.getConcepts(t));
			this.processExceptions(t.getException());
			this.processIncludes(ValueSetConceptListUtil.getIncludes(t));
		}
	}

	@Override
	public void processConcepts(List<ValueSetConcept> concepts) {
		// to be overrided if needed to
	}

	@Override
	public void processExceptions(List<ValueSetConcept> exceptions) {
		// to be overrided if needed to
	}

	@Override
	public void processIncludes(List<ValueSetRef> includes) {
		// to be overrided if needed to
	}

}
