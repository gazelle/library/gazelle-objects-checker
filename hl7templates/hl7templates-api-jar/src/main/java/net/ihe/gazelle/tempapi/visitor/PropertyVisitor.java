package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.PropertyProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;

public class PropertyVisitor implements HL7TemplateVisitor<Property> {

	public static final PropertyVisitor INSTANCE = new PropertyVisitor();

	@Override
	public void visitAndProcess(Property t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		PropertyProcessor processor = implProvider.provideImpl(PropertyProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
