package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ContextProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Context;

public abstract class ContextProcessorImpl implements ContextProcessor {

	@Override
	public void process(Context t, Object... objects) {
		if (t != null){
			this.processId(t.getId());
			this.processPath(t.getPath());
		}
	}

}
