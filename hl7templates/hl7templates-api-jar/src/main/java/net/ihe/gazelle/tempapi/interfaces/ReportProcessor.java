package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;

public interface ReportProcessor extends Processor<Report> {
	
		public void processFlag(String flag);
		public void processRole(AssertRole role);
		public void processSee(String see);
		public void processTest(String test);
	
}
