package net.ihe.gazelle.tempapi.utils;

import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;

public class DefaultImplProvider implements ImplProvider {

	@SuppressWarnings({ "rawtypes" })
	public <T extends Processor> T provideImpl(Class<T> t) {
//		if (t.equals(RuleDefinitionProcessor.class)){
//			return (T) new RuleDefinitionProcessorImpl();
//		}
		return null;
	}
	
	public static void main(String[] args) {
		DefaultImplProvider dip = new DefaultImplProvider();
		RuleDefinitionProcessor aa = dip.provideImpl(RuleDefinitionProcessor.class);
		System.out.println(aa);
	}

}
