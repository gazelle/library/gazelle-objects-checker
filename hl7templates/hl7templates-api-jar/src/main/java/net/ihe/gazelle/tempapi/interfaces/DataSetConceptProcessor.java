package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptHistory;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptValue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.InheritDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept.Implementation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept.ValueSet;

public interface DataSetConceptProcessor extends Processor<DataSetConcept> {
	
		public void processComments(List<FreeFormMarkupWithLanguage> comments);
		public void processConcepts(List<DataSetConcept> concepts);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processHistorys(List<DataSetConceptHistory> historys);
		public void processId(String id);
		public void processImplementation(Implementation implementation);
		public void processInherit(InheritDefinition inherit);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processOperationalizations(List<FreeFormMarkupWithLanguage> operationalizations);
		public void processPropertys(List<ArbitraryPropertyType> propertys);
		public void processRationales(List<FreeFormMarkupWithLanguage> rationales);
		public void processRelationships(List<ObjectRelationships> relationships);
		public void processSources(List<FreeFormMarkupWithLanguage> sources);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
		public void processSynonyms(List<BusinessNameWithLanguage> list);
		public void processTerminologyAssociations(List<TerminologyAssociation> terminologyAssociations);
		public void processType(DataSetConceptType type);
		public void processValueDomains(List<DataSetConceptValue> valueDomains);
		public void processValueSets(List<ValueSet> valueSets);
		public void processVersionLabel(String versionLabel);
	
}
