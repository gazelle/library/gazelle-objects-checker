package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarCode;

public interface VarCodeProcessor extends Processor<VarCode> {
	
		public void processCode(String code);
		public void processCodeSystem(String codeSystem);
	
}
