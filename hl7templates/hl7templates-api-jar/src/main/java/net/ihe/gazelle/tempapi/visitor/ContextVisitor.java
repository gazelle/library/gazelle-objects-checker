package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ContextProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Context;

public class ContextVisitor implements HL7TemplateVisitor<Context> {

	public static final ContextVisitor INSTANCE = new ContextVisitor();

	@Override
	public void visitAndProcess(Context t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ContextProcessor processor = implProvider.provideImpl(ContextProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
