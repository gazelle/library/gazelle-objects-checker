package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.LetProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;

public class LetVisitor implements HL7TemplateVisitor<Let> {

	public static final LetVisitor INSTANCE = new LetVisitor();

	@Override
	public void visitAndProcess(Let t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		LetProcessor processor = implProvider.provideImpl(LetProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
