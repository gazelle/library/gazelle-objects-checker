package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TransactionTriggerProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TransactionTrigger;

public abstract class TransactionTriggerProcessorImpl implements TransactionTriggerProcessor {

	@Override
	public void process(TransactionTrigger t, Object... objects) {
		if (t != null){
			this.processId(t.getId());
		}
	}

}
