package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DatasetProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Dataset;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;

public class DatasetVisitor implements HL7TemplateVisitor<Dataset> {

	public static final DatasetVisitor INSTANCE = new DatasetVisitor();

	@Override
	public void visitAndProcess(Dataset t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DatasetProcessor processor = implProvider.provideImpl(DatasetProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process concept
		if (t.getConcept() != null) {
			for(DataSetConcept _dataSetConcept : t.getConcept()) {
				DataSetConceptVisitor.INSTANCE.visitAndProcess(_dataSetConcept, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process property
		if (t.getProperty() != null) {
			for(ArbitraryPropertyType _arbitraryPropertyType : t.getProperty()) {
				ArbitraryPropertyTypeVisitor.INSTANCE.visitAndProcess(_arbitraryPropertyType, implProvider, objects);
			}
		}
		// process relationship
		if (t.getRelationship() != null) {
			for(ObjectRelationships _objectRelationships : t.getRelationship()) {
				ObjectRelationshipsVisitor.INSTANCE.visitAndProcess(_objectRelationships, implProvider, objects);
			}
		}
	}

}
