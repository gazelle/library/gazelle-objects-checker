package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Contact;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Copyright;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultElementNamespace;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Reference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RestURI;

public interface ProjectProcessor extends Processor<Project> {
	
		public void processAuthors(List<Author> authors);
		public void processBuildingBlockRepositorys(List<BuildingBlockRepository> buildingBlockRepositorys);
		public void processContacts(List<Contact> contacts);
		public void processCopyrights(List<Copyright> copyrights);
		public void processDefaultElementNamespace(DefaultElementNamespace defaultElementNamespace);
		public void processDefaultLanguage(String defaultLanguage);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processExperimental(Boolean b);
		public void processId(String id);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processPrefix(String prefix);
		public void processReference(Reference reference);
		public void processReleases(List<ProjectRelease> releases);
		public void processRestURIs(List<RestURI> restURIs);
		public void processVersions(List<ProjectHistory> versions);
	
}
