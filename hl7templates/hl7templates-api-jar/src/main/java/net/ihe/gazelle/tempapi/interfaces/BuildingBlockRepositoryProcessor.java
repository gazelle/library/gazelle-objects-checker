package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;

public interface BuildingBlockRepositoryProcessor extends Processor<BuildingBlockRepository> {
	
		public void processFormat(String format);
		public void processIdent(String ident);
		public void processLicenseKey(String licenseKey);
		public void processUrl(String url);
	
}
