package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DecorObjectType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RestURI;

public interface RestURIProcessor extends Processor<RestURI> {
	
		public void process_for(DecorObjectType _for);
		public void processFormat(String format);
	
}
