package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ExampleType;

public interface ExampleProcessor extends Processor<Example> {
	
		public void processCaption(String caption);
		public void processType(ExampleType type);
	
}
