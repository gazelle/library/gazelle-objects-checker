package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ExampleProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;

public class ExampleVisitor implements HL7TemplateVisitor<Example> {

	public static final ExampleVisitor INSTANCE = new ExampleVisitor();

	@Override
	public void visitAndProcess(Example t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ExampleProcessor processor = implProvider.provideImpl(ExampleProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
