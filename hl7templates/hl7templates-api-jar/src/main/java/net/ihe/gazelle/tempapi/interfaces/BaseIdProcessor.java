package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BaseId;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DecorObjectType;

public interface BaseIdProcessor extends Processor<BaseId> {
	
		public void process_default(Boolean _default);
		public void processId(String id);
		public void processPrefix(String prefix);
		public void processType(DecorObjectType type);
	
}
