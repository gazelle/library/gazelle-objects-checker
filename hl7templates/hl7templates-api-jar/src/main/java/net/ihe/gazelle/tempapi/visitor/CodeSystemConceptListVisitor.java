package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.CodeSystemConceptListProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodedConcept;

public class CodeSystemConceptListVisitor implements HL7TemplateVisitor<CodeSystemConceptList> {

	public static final CodeSystemConceptListVisitor INSTANCE = new CodeSystemConceptListVisitor();

	@Override
	public void visitAndProcess(CodeSystemConceptList t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		CodeSystemConceptListProcessor processor = implProvider.provideImpl(CodeSystemConceptListProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process codedConcept
		if (t.getCodedConcept() != null) {
			for(CodedConcept _codedConcept : t.getCodedConcept()) {
				CodedConceptVisitor.INSTANCE.visitAndProcess(_codedConcept, implProvider, objects);
			}
		}
	}

}
