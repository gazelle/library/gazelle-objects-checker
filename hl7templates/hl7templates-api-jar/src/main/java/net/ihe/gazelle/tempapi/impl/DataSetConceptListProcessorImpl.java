package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptListProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptList;

public abstract class DataSetConceptListProcessorImpl implements DataSetConceptListProcessor {

	@Override
	public void process(DataSetConceptList t, Object... objects) {
		if (t != null){
			this.processConcepts(t.getConcept());
			this.processId(t.getId());
			this.processRef(t.getRef());
		}
	}

}
