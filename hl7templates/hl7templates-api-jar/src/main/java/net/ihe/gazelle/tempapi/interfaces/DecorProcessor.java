package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Datasets;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Ids;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issues;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenarios;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;

public interface DecorProcessor extends Processor<Decor> {
	
		public void processCompilationDate(String compilationDate);
		public void processDatasets(Datasets datasets);
		public void processDeeplinkprefix(String deeplinkprefix);
		public void processDeeplinkprefixservices(String deeplinkprefixservices);
		public void processIds(Ids ids);
		public void processIssues(Issues issues);
		public void processLanguage(String language);
		public void processProject(Project project);
		public void processRelease(Integer i);
		public void processRepository(Boolean boolean1);
		public void processRules(Rules rules);
		public void processScenarios(Scenarios scenarios);
		public void processTerminology(Terminology terminology);
		public void processVersionDate(String versionDate);
		public void processVersionLabel(String versionLabel);
	
}
