package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RelationshipTypes;

public interface ObjectRelationshipsProcessor extends Processor<ObjectRelationships> {
	
		public void processFlexibility(String flexibility);
		public void processRef(String ref);
		public void processType(RelationshipTypes type);
	
}
