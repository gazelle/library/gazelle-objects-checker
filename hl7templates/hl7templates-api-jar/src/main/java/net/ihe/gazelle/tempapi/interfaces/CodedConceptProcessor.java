package net.ihe.gazelle.tempapi.interfaces;

import java.math.BigInteger;
import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodedConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VocabType;

public interface CodedConceptProcessor extends Processor<CodedConcept> {
	
		public void processCode(String code);
		public void processDesignations(List<Designation> designations);
		public void processLevel(BigInteger bigInteger);
		public void processStatusCode(String statusCode);
		public void processType(VocabType type);
	
}
