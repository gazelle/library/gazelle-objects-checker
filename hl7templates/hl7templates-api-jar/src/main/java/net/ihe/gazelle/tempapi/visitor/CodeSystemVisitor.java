package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.CodeSystemProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystem;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public class CodeSystemVisitor implements HL7TemplateVisitor<CodeSystem> {

	public static final CodeSystemVisitor INSTANCE = new CodeSystemVisitor();

	@Override
	public void visitAndProcess(CodeSystem t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		CodeSystemProcessor processor = implProvider.provideImpl(CodeSystemProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process conceptList
		if (t.getConceptList() != null) {
			CodeSystemConceptListVisitor.INSTANCE.visitAndProcess(t.getConceptList(), implProvider, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
