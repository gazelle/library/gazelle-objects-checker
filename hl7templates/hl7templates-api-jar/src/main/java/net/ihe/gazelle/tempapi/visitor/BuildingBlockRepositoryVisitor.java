package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.BuildingBlockRepositoryProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;

public class BuildingBlockRepositoryVisitor implements HL7TemplateVisitor<BuildingBlockRepository> {

	public static final BuildingBlockRepositoryVisitor INSTANCE = new BuildingBlockRepositoryVisitor();

	@Override
	public void visitAndProcess(BuildingBlockRepository t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		BuildingBlockRepositoryProcessor processor = implProvider.provideImpl(BuildingBlockRepositoryProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
