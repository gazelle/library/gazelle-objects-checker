package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept.Implementation;

public interface ImplementationProcessor extends Processor<Implementation> {
	
	
}
