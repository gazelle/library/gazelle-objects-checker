package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.VariousMixedContentProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VariousMixedContent;

public class VariousMixedContentVisitor implements HL7TemplateVisitor<VariousMixedContent> {

	public static final VariousMixedContentVisitor INSTANCE = new VariousMixedContentVisitor();

	@Override
	public void visitAndProcess(VariousMixedContent t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		VariousMixedContentProcessor processor = implProvider.provideImpl(VariousMixedContentProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
