package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemReference;

public interface CodeSystemReferenceProcessor extends Processor<CodeSystemReference> {
	
		public void processCodeSystem(String codeSystem);
		public void processCodeSystemName(String codeSystemName);
		public void processCodeSystemVersion(String codeSystemVersion);
		public void processFlexibility(String flexibility);
	
}
