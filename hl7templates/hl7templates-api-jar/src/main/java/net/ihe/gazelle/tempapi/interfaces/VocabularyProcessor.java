package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public interface VocabularyProcessor extends Processor<Vocabulary> {
	
		public void processCode(String code);
		public void processCodeSystem(String codeSystem);
		public void processCodeSystemName(String codeSystemName);
		public void processDisplayName(String displayName);
		public void processDomain(String domain);
		public void processFlexibility(String flexibility);
		public void processValueSet(String valueSet);
	
}
