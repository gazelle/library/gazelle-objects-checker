package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TemplateAssociationConceptProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationConcept;

public class TemplateAssociationConceptVisitor implements HL7TemplateVisitor<TemplateAssociationConcept> {

	public static final TemplateAssociationConceptVisitor INSTANCE = new TemplateAssociationConceptVisitor();

	@Override
	public void visitAndProcess(TemplateAssociationConcept t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TemplateAssociationConceptProcessor processor = implProvider.provideImpl(TemplateAssociationConceptProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
