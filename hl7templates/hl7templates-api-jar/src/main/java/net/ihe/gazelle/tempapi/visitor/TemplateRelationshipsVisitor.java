package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TemplateRelationshipsProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;

public class TemplateRelationshipsVisitor implements HL7TemplateVisitor<TemplateRelationships> {

	public static final TemplateRelationshipsVisitor INSTANCE = new TemplateRelationshipsVisitor();

	@Override
	public void visitAndProcess(TemplateRelationships t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TemplateRelationshipsProcessor processor = implProvider.provideImpl(TemplateRelationshipsProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
