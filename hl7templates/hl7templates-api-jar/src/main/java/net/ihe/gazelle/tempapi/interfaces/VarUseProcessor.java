package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarUse;

public interface VarUseProcessor extends Processor<VarUse> {
	
		public void processAs(String as);
		public void processPath(String path);
	
}
