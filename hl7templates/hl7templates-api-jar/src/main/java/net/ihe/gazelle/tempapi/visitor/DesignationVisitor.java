package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DesignationProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;

public class DesignationVisitor implements HL7TemplateVisitor<Designation> {

	public static final DesignationVisitor INSTANCE = new DesignationVisitor();

	@Override
	public void visitAndProcess(Designation t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DesignationProcessor processor = implProvider.provideImpl(DesignationProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
