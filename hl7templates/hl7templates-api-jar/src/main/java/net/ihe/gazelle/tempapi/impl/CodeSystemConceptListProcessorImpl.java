package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.CodeSystemConceptListProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemConceptList;

public abstract class CodeSystemConceptListProcessorImpl implements CodeSystemConceptListProcessor {

	@Override
	public void process(CodeSystemConceptList t, Object... objects) {
		if (t != null){
			this.processCodedConcepts(t.getCodedConcept());
		}
	}

}
