package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DataSetValuePropertyProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueProperty;

public abstract class DataSetValuePropertyProcessorImpl implements DataSetValuePropertyProcessor {

	@Override
	public void process(DataSetValueProperty t, Object... objects) {
		if (t != null){
			this.process_default(t.getDefault());
			this.processCurrency(t.getCurrency());
			this.processFixed(t.getFixed());
			this.processFractionDigits(t.getFractionDigits());
			this.processMaxInclude(t.getMaxInclude());
			this.processMaxLength(t.getMaxLength());
			this.processMinInclude(t.getMinInclude());
			this.processMinLength(t.getMinLength());
			this.processTimeStampPrecision(t.getTimeStampPrecision());
			this.processUnit(t.getUnit());
		}
	}

}
