package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptValueProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptValue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueProperty;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DataSetConceptValueUtil;

public class DataSetConceptValueVisitor implements HL7TemplateVisitor<DataSetConceptValue> {

	public static final DataSetConceptValueVisitor INSTANCE = new DataSetConceptValueVisitor();

	@Override
	public void visitAndProcess(DataSetConceptValue t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DataSetConceptValueProcessor processor = implProvider.provideImpl(DataSetConceptValueProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process conceptList
		if (DataSetConceptValueUtil.getConceptLists(t) != null) {
			for(DataSetConceptList _dataSetConceptList : DataSetConceptValueUtil.getConceptLists(t)) {
				DataSetConceptListVisitor.INSTANCE.visitAndProcess(_dataSetConceptList, implProvider, objects);
			}
		}
		// process example
		if (DataSetConceptValueUtil.getExamples(t) != null) {
			for(Example _example : DataSetConceptValueUtil.getExamples(t)) {
				ExampleVisitor.INSTANCE.visitAndProcess(_example, implProvider, objects);
			}
		}
		// process property
		if (DataSetConceptValueUtil.getPropertys(t) != null) {
			for(DataSetValueProperty _dataSetValueProperty : DataSetConceptValueUtil.getPropertys(t)) {
				DataSetValuePropertyVisitor.INSTANCE.visitAndProcess(_dataSetValueProperty, implProvider, objects);
			}
		}
	}

}
