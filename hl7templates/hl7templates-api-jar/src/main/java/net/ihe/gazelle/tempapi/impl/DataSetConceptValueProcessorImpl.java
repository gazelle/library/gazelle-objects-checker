package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptValueProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptValue;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DataSetConceptValueUtil;

public abstract class DataSetConceptValueProcessorImpl implements DataSetConceptValueProcessor {

	@Override
	public void process(DataSetConceptValue t, Object... objects) {
		if (t != null){
			this.processConceptLists(DataSetConceptValueUtil.getConceptLists(t));
			this.processExamples(DataSetConceptValueUtil.getExamples(t));
			this.processPropertys(DataSetConceptValueUtil.getPropertys(t));
			this.processType(t.getType());
		}
	}

}
