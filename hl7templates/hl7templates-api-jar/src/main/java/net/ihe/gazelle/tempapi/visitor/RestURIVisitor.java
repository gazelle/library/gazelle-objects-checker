package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.RestURIProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RestURI;

public class RestURIVisitor implements HL7TemplateVisitor<RestURI> {

	public static final RestURIVisitor INSTANCE = new RestURIVisitor();

	@Override
	public void visitAndProcess(RestURI t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		RestURIProcessor processor = implProvider.provideImpl(RestURIProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
