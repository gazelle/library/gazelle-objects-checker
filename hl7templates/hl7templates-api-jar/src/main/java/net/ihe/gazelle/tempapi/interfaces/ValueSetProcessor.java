package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet.SourceCodeSystem;

public interface ValueSetProcessor extends Processor<ValueSet> {
	
		public void processCompleteCodeSystems(List<CodeSystemReference> completeCodeSystems);
		public void processConceptList(ValueSetConceptList conceptList);
		public void processCopyright(String copyright);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processDisplayName(String displayName);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processFlexibility(String flexibility);
		public void processId(String id);
		public void processIdent(String ident);
		public void processName(String name);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processRef(String ref);
		public void processReferencedFrom(String referencedFrom);
		public void processSourceCodeSystems(List<SourceCodeSystem> sourceCodeSystems);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
		public void processUrl(String url);
		public void processVersionLabel(String versionLabel);
	
}
