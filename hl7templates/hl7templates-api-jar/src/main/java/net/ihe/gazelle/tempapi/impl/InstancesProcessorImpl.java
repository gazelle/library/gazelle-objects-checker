package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.InstancesProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Instances;

public abstract class InstancesProcessorImpl implements InstancesProcessor {

	@Override
	public void process(Instances t, Object... objects) {
		if (t != null){
			this.processDisplayName(t.getDisplayName());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processProcess(t.getProcess());
			this.processRef(t.getRef());
			this.processStatusCode(t.getStatusCode());
		}
	}

}
