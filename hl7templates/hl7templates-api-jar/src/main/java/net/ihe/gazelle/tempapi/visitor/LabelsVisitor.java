package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.LabelsProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueLabelDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Labels;

public class LabelsVisitor implements HL7TemplateVisitor<Labels> {

	public static final LabelsVisitor INSTANCE = new LabelsVisitor();

	@Override
	public void visitAndProcess(Labels t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		LabelsProcessor processor = implProvider.provideImpl(LabelsProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process label
		if (t.getLabel() != null) {
			for(IssueLabelDefinition _issueLabelDefinition : t.getLabel()) {
				IssueLabelDefinitionVisitor.INSTANCE.visitAndProcess(_issueLabelDefinition, implProvider, objects);
			}
		}
	}

}
