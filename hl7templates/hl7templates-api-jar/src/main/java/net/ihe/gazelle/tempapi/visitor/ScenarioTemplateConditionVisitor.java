package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ScenarioTemplateConditionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateCondition;

public class ScenarioTemplateConditionVisitor implements HL7TemplateVisitor<ScenarioTemplateCondition> {

	public static final ScenarioTemplateConditionVisitor INSTANCE = new ScenarioTemplateConditionVisitor();

	@Override
	public void visitAndProcess(ScenarioTemplateCondition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ScenarioTemplateConditionProcessor processor = implProvider.provideImpl(ScenarioTemplateConditionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
