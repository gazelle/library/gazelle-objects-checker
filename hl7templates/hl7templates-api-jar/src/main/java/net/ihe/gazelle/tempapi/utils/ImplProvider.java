package net.ihe.gazelle.tempapi.utils;

public interface ImplProvider {
	
	@SuppressWarnings("rawtypes")
	public <T extends Processor> T provideImpl(Class<T> t);

}
