package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ActorDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorDefinition;

public abstract class ActorDefinitionProcessorImpl implements ActorDefinitionProcessor {

	@Override
	public void process(ActorDefinition t, Object... objects) {
		if (t != null){
			this.processDescs(t.getDesc());
			this.processId(t.getId());
			this.processNames(t.getName());
			this.processType(t.getType());
		}
	}

}
