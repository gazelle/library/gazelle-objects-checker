package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsDefinition;

public interface ActorsDefinitionProcessor extends Processor<ActorsDefinition> {
	
		public void processActors(List<ActorDefinition> actors);
	
}
