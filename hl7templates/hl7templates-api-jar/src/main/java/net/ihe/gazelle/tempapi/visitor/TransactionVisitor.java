package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TransactionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Transaction;

public class TransactionVisitor implements HL7TemplateVisitor<Transaction> {

	public static final TransactionVisitor INSTANCE = new TransactionVisitor();

	@Override
	public void visitAndProcess(Transaction t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TransactionProcessor processor = implProvider.provideImpl(TransactionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process actors
		if (t.getActors() != null) {
			ActorsReferenceVisitor.INSTANCE.visitAndProcess(t.getActors(), implProvider, objects);
		}
		// process condition
		if (t.getCondition() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getCondition()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process dependencies
		if (t.getDependencies() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDependencies()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process representingTemplate
		if (t.getRepresentingTemplate() != null) {
			RepresentingTemplateVisitor.INSTANCE.visitAndProcess(t.getRepresentingTemplate(), implProvider, objects);
		}
		// process transaction
		if (t.getTransactions() != null) {
			for(Transaction _transaction : t.getTransactions()) {
				TransactionVisitor.INSTANCE.visitAndProcess(_transaction, implProvider, objects);
			}
		}
		// process trigger
		if (t.getTrigger() != null) {
			TransactionTriggerVisitor.INSTANCE.visitAndProcess(t.getTrigger(), implProvider, objects);
		}
	}

}
