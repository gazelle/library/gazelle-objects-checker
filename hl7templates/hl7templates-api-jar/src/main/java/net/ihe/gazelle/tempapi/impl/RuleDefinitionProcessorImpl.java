package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.util.List;

public abstract class RuleDefinitionProcessorImpl implements RuleDefinitionProcessor {

	@Override
	public void process(RuleDefinition t, Object... objects) {
		if (t != null){
			this.process_asserts(RuleDefinitionUtil.getAsserts(t));
			this.processAttributes(t.getAttribute());
			this.processChoices(RuleDefinitionUtil.getChoices(t));
			this.processConformance(t.getConformance());
			this.processConstraints(RuleDefinitionUtil.getConstraints(t));
			this.processContains(t.getContains());
			this.processDatatype(t.getDatatype());
			this.processDefineVariables(RuleDefinitionUtil.getDefineVariables(t));
			this.processDescs(t.getDesc());
			this.processDisplayName(t.getDisplayName());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processElements(RuleDefinitionUtil.getElements(t));
			this.processExamples(t.getExample());
			this.processExpirationDate(t.getExpirationDate());
			this.processFlexibility(t.getFlexibility());
			this.processId(t.getId());
			this.processIncludes(RuleDefinitionUtil.getIncludes(t));
			this.processIsClosed(t.getIsClosed());
			this.processIsMandatory(t.isIsMandatory());
			this.processLets(RuleDefinitionUtil.getLets(t));
			this.processMaximumMultiplicity(t.getMaximumMultiplicity());
			this.processMinimumMultiplicity(t.getMinimumMultiplicity());
			this.processName(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processPropertys(t.getProperty());
			this.processReports(RuleDefinitionUtil.getReports(t));
			this.processStatusCode(t.getStatusCode());
			this.processStrength(t.getStrength());
			this.processTexts(t.getText());
			this.processUseWhere(t.isUseWhere());
			this.processVersionLabel(t.getVersionLabel());
			this.processVocabularys(t.getVocabulary());
			this.processContainDefElements(RuleDefinitionUtil.getContains(t));
			this.processItem(t.getItem());
		}
	}

	@Override
	public void process_asserts(List<Assert> _asserts) {
		// to be overrided if needed to
	}

	@Override
	public void processAttributes(List<Attribute> attributes) {
		// to be overrided if needed to		
	}

	@Override
	public void processChoices(List<ChoiceDefinition> choices) {
		// to be overrided if needed to		
	}

	@Override
	public void processConformance(ConformanceType conformance) {
		// to be overrided if needed to		
	}

	@Override
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints) {
		// to be overrided if needed to		
	}

	@Override
	public void processContains(String contains) {
		// to be overrided if needed to		
	}

	@Override
	public void processDatatype(QName qName) {
		// to be overrided if needed to		
	}

	@Override
	public void processDefineVariables(List<DefineVariable> defineVariables) {
		// to be overrided if needed to		
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to		
	}

	@Override
	public void processDisplayName(String displayName) {
		// to be overrided if needed to		
	}

	@Override
	public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to		
	}

	@Override
	public void processElements(List<RuleDefinition> elements) {
		// to be overrided if needed to		
	}

	@Override
	public void processExamples(List<Example> examples) {
		// to be overrided if needed to		
	}

	@Override
	public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to		
	}

	@Override
	public void processFlexibility(String flexibility) {
		// to be overrided if needed to		
	}

	@Override
	public void processId(String id) {
		// to be overrided if needed to		
	}

	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		// to be overrided if needed to		
	}

	@Override
	public void processIsClosed(Boolean isClosed) {
		// to be overrided if needed to		
	}

	@Override
	public void processIsMandatory(Boolean b) {
		// to be overrided if needed to		
	}

	@Override
	public void processItem(Item item) {
		// to be overrided if needed to		
	}

	@Override
	public void processLets(List<Let> lets) {
		// to be overrided if needed to		
	}

	@Override
	public void processMaximumMultiplicity(String maximumMultiplicity) {
		// to be overrided if needed to		
	}

	@Override
	public void processMinimumMultiplicity(Integer integer) {
		// to be overrided if needed to		
	}

	@Override
	public void processName(String name) {
		// to be overrided if needed to		
	}

	@Override
	public void processOfficialReleaseDate(
			XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to		
	}

	@Override
	public void processPropertys(List<Property> propertys) {
		// to be overrided if needed to		
	}

	@Override
	public void processReports(List<Report> reports) {
		// to be overrided if needed to		
	}

	@Override
	public void processStatusCode(ItemStatusCodeLifeCycle statusCode) {
		// to be overrided if needed to		
	}

	@Override
	public void processStrength(CodingStrengthType strength) {
		// to be overrided if needed to		
	}

	@Override
	public void processTexts(List<String> texts) {
		// to be overrided if needed to		
	}

	@Override
	public void processUseWhere(Boolean b) {
		// to be overrided if needed to		
	}

	@Override
	public void processVersionLabel(String versionLabel) {
		// to be overrided if needed to		
	}

	@Override
	public void processVocabularys(List<Vocabulary> vocabularys) {
		// to be overrided if needed to		
	}
	
	@Override
	public void processContainDefElements(List<ContainDefinition> contains) {
		// to be overrided if needed to
	}

}
