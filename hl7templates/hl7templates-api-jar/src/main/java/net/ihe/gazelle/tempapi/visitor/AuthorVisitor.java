package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.AuthorProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;

public class AuthorVisitor implements HL7TemplateVisitor<Author> {

	public static final AuthorVisitor INSTANCE = new AuthorVisitor();

	@Override
	public void visitAndProcess(Author t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		AuthorProcessor processor = implProvider.provideImpl(AuthorProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
