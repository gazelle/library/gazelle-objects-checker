package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Instances;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProcessCode;

public interface InstancesProcessor extends Processor<Instances> {
	
		public void processDisplayName(String displayName);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processProcess(ProcessCode process);
		public void processRef(String ref);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
	
}
