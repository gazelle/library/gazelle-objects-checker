package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IdsProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BaseId;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultBaseId;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Id;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IdentifierAssociation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Ids;

public class IdsVisitor implements HL7TemplateVisitor<Ids> {

	public static final IdsVisitor INSTANCE = new IdsVisitor();

	@Override
	public void visitAndProcess(Ids t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IdsProcessor processor = implProvider.provideImpl(IdsProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process baseId
		if (t.getBaseId() != null) {
			for(BaseId _baseId : t.getBaseId()) {
				BaseIdVisitor.INSTANCE.visitAndProcess(_baseId, implProvider, objects);
			}
		}
		// process defaultBaseId
		if (t.getDefaultBaseId() != null) {
			for(DefaultBaseId _defaultBaseId : t.getDefaultBaseId()) {
				DefaultBaseIdVisitor.INSTANCE.visitAndProcess(_defaultBaseId, implProvider, objects);
			}
		}
		// process id
		if (t.getId() != null) {
			for(Id _id : t.getId()) {
				IdVisitor.INSTANCE.visitAndProcess(_id, implProvider, objects);
			}
		}
		// process identifierAssociation
		if (t.getIdentifierAssociation() != null) {
			for(IdentifierAssociation _identifierAssociation : t.getIdentifierAssociation()) {
				IdentifierAssociationVisitor.INSTANCE.visitAndProcess(_identifierAssociation, implProvider, objects);
			}
		}
	}

}
