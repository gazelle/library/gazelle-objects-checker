package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TemplateRelationshipsProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;

public abstract class TemplateRelationshipsProcessorImpl implements TemplateRelationshipsProcessor {

	@Override
	public void process(TemplateRelationships t, Object... objects) {
		if (t != null){
			this.processFlexibility(t.getFlexibility());
			this.processModel(t.getModel());
			this.processTemplate(t.getTemplate());
			this.processType(t.getType());
		}
	}

}
