package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateFormats;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateTypes;

public interface TemplatePropertiesProcessor extends Processor<TemplateProperties> {
	
		public void processFormat(TemplateFormats format);
		public void processPropertys(List<String> propertys);
		public void processTags(List<String> tags);
		public void processType(TemplateTypes type);
	
}
