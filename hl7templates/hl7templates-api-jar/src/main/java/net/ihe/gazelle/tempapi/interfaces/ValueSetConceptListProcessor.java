package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

public interface ValueSetConceptListProcessor extends Processor<ValueSetConceptList> {
	
		public void processConcepts(List<ValueSetConcept> concepts);
		public void processExceptions(List<ValueSetConcept> exceptions);
		public void processIncludes(List<ValueSetRef> includes);
	
}
