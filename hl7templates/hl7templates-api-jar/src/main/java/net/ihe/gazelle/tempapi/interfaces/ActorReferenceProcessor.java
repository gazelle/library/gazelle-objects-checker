package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorType;

public interface ActorReferenceProcessor extends Processor<ActorReference> {
	
		public void processId(String id);
		public void processRole(ActorType role);
	
}
