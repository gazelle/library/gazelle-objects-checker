package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.BaseIdProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BaseId;

public class BaseIdVisitor implements HL7TemplateVisitor<BaseId> {

	public static final BaseIdVisitor INSTANCE = new BaseIdVisitor();

	@Override
	public void visitAndProcess(BaseId t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		BaseIdProcessor processor = implProvider.provideImpl(BaseIdProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
