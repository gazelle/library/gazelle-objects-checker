package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TransactionTriggerProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TransactionTrigger;

public class TransactionTriggerVisitor implements HL7TemplateVisitor<TransactionTrigger> {

	public static final TransactionTriggerVisitor INSTANCE = new TransactionTriggerVisitor();

	@Override
	public void visitAndProcess(TransactionTrigger t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TransactionTriggerProcessor processor = implProvider.provideImpl(TransactionTriggerProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
