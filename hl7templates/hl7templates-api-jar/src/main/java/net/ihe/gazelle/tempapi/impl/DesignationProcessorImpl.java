package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DesignationProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;

public abstract class DesignationProcessorImpl implements DesignationProcessor {

	@Override
	public void process(Designation t, Object... objects) {
		if (t != null){
			this.processDisplayName(t.getDisplayName());
			this.processType(t.getType());
		}
	}

}
