package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IssueObjectProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueObject;

public class IssueObjectVisitor implements HL7TemplateVisitor<IssueObject> {

	public static final IssueObjectVisitor INSTANCE = new IssueObjectVisitor();

	@Override
	public void visitAndProcess(IssueObject t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IssueObjectProcessor processor = implProvider.provideImpl(IssueObjectProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
