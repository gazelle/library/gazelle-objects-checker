package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.InheritDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.InheritDefinition;

public abstract class InheritDefinitionProcessorImpl implements InheritDefinitionProcessor {

	@Override
	public void process(InheritDefinition t, Object... objects) {
		if (t != null){
			this.processEffectiveDate(t.getEffectiveDate());
			this.processRef(t.getRef());
		}
	}

}
