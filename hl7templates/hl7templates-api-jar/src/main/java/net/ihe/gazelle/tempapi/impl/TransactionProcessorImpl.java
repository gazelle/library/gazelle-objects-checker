package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TransactionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Transaction;

public abstract class TransactionProcessorImpl implements TransactionProcessor {

	@Override
	public void process(Transaction t, Object... objects) {
		if (t != null){
			this.processActors(t.getActors());
			this.processConditions(t.getCondition());
			this.processDependenciess(t.getDependencies());
			this.processDescs(t.getDesc());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processId(t.getId());
			this.processLabel(t.getLabel());
			this.processModel(t.getModel());
			this.processNames(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processRepresentingTemplate(t.getRepresentingTemplate());
			this.processStatusCode(t.getStatusCode());
			this.processTransactions(t.getTransactions());
			this.processTrigger(t.getTrigger());
			this.processType(t.getType());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
