package net.ihe.gazelle.tempapi.interfaces;

import java.math.BigInteger;
import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VocabType;

public interface ValueSetConceptProcessor extends Processor<ValueSetConcept> {
	
		public void processCode(String code);
		public void processCodeSystem(String codeSystem);
		public void processCodeSystemName(String codeSystemName);
		public void processCodeSystemVersion(String codeSystemVersion);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processDisplayName(String displayName);
		public void processLevel(BigInteger bigInteger);
		public void processType(VocabType type);
	
}
