package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ScenarioProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenario;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Transaction;

public class ScenarioVisitor implements HL7TemplateVisitor<Scenario> {

	public static final ScenarioVisitor INSTANCE = new ScenarioVisitor();

	@Override
	public void visitAndProcess(Scenario t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ScenarioProcessor processor = implProvider.provideImpl(ScenarioProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process condition
		if (t.getCondition() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getCondition()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process transaction
		if (t.getTransaction() != null) {
			for(Transaction _transaction : t.getTransaction()) {
				TransactionVisitor.INSTANCE.visitAndProcess(_transaction, implProvider, objects);
			}
		}
		// process trigger
		if (t.getTrigger() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getTrigger()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
