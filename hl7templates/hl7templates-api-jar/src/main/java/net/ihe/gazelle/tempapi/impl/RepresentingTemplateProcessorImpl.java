package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.RepresentingTemplateProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RepresentingTemplate;

public abstract class RepresentingTemplateProcessorImpl implements RepresentingTemplateProcessor {

	@Override
	public void process(RepresentingTemplate t, Object... objects) {
		if (t != null){
			this.processConcepts(t.getConcept());
			this.processDisplayName(t.getDisplayName());
			this.processFlexibility(t.getFlexibility());
			this.processRef(t.getRef());
			this.processSourceDataset(t.getSourceDataset());
			this.processSourceDatasetFlexibility(t.getSourceDatasetFlexibility());
		}
	}

}
