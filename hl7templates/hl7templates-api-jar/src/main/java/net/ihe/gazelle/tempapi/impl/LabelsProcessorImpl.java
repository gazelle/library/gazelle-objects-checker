package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.LabelsProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Labels;

public abstract class LabelsProcessorImpl implements LabelsProcessor {

	@Override
	public void process(Labels t, Object... objects) {
		if (t != null){
			this.processLabels(t.getLabel());
		}
	}

}
