package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IssueTrackingProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueTracking;

public class IssueTrackingVisitor implements HL7TemplateVisitor<IssueTracking> {

	public static final IssueTrackingVisitor INSTANCE = new IssueTrackingVisitor();

	@Override
	public void visitAndProcess(IssueTracking t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IssueTrackingProcessor processor = implProvider.provideImpl(IssueTrackingProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process author
		if (t.getAuthor() != null) {
			AuthorVisitor.INSTANCE.visitAndProcess(t.getAuthor(), implProvider, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
