package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.VarUseProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarUse;

public abstract class VarUseProcessorImpl implements VarUseProcessor {

	@Override
	public void process(VarUse t, Object... objects) {
		if (t != null){
			this.processAs(t.getAs());
			this.processPath(t.getPath());
		}
	}

}
