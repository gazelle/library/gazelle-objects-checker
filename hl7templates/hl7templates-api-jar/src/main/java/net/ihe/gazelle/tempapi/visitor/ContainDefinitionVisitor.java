package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ContainDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;

public class ContainDefinitionVisitor implements HL7TemplateVisitor<ContainDefinition> {

	public static final ContainDefinitionVisitor INSTANCE = new ContainDefinitionVisitor();

	@Override
	public void visitAndProcess(ContainDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ContainDefinitionProcessor processor = implProvider.provideImpl(ContainDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process item
		if (t.getItem() != null) {
			ItemVisitor.INSTANCE.visitAndProcess(t.getItem(), implProvider, objects);
		}
	}

}
