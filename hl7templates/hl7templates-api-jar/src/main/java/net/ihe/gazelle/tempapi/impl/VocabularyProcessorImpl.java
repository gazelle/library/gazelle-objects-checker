package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.VocabularyProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public abstract class VocabularyProcessorImpl implements VocabularyProcessor {

	@Override
	public void process(Vocabulary t, Object... objects) {
		if (t != null){
			this.processCode(t.getCode());
			this.processCodeSystem(t.getCodeSystem());
			this.processCodeSystemName(t.getCodeSystemName());
			this.processDisplayName(t.getDisplayName());
			this.processDomain(t.getDomain());
			this.processFlexibility(t.getFlexibility());
			this.processValueSet(t.getValueSet());
		}
	}

	@Override
	public void processCode(String code) {
		// nothing to do
		
	}

	@Override
	public void processCodeSystem(String codeSystem) {
		// nothing to do
		
	}

	@Override
	public void processCodeSystemName(String codeSystemName) {
		// nothing to do
		
	}

	@Override
	public void processDisplayName(String displayName) {
		// nothing to do
		
	}

	@Override
	public void processDomain(String domain) {
		// nothing to do
		
	}

	@Override
	public void processFlexibility(String flexibility) {
		// nothing to do
		
	}

	@Override
	public void processValueSet(String valueSet) {
		// nothing to do
		
	}
	
	

}
