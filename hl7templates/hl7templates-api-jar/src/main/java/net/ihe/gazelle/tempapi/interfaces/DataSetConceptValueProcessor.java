package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptValue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueProperty;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;

public interface DataSetConceptValueProcessor extends Processor<DataSetConceptValue> {
	
		public void processConceptLists(List<DataSetConceptList> conceptLists);
		public void processExamples(List<Example> examples);
		public void processPropertys(List<DataSetValueProperty> propertys);
		public void processType(DataSetValueType type);
	
}
