package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RelationshipTypes;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;

public interface TemplateRelationshipsProcessor extends Processor<TemplateRelationships> {
	
		public void processFlexibility(String flexibility);
		public void processModel(String model);
		public void processTemplate(String template);
		public void processType(RelationshipTypes type);
	
}
