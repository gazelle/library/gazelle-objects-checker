package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;

public abstract class DataSetConceptProcessorImpl implements DataSetConceptProcessor {

	@Override
	public void process(DataSetConcept t, Object... objects) {
		if (t != null){
			this.processComments(t.getComment());
			this.processConcepts(t.getConcept());
			this.processDescs(t.getDesc());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processHistorys(t.getHistory());
			this.processId(t.getId());
			this.processImplementation(t.getImplementation());
			this.processInherit(t.getInherit());
			this.processNames(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processOperationalizations(t.getOperationalization());
			this.processPropertys(t.getProperty());
			this.processRationales(t.getRationale());
			this.processRelationships(t.getRelationship());
			this.processSources(t.getSource());
			this.processStatusCode(t.getStatusCode());
			this.processSynonyms(t.getSynonym());
			this.processTerminologyAssociations(t.getTerminologyAssociation());
			this.processType(t.getType());
			this.processValueDomains(t.getValueDomain());
			this.processValueSets(t.getValueSet());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
