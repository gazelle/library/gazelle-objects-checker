package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.PathnameProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Pathname;

public abstract class PathnameProcessorImpl implements PathnameProcessor {

	@Override
	public void process(Pathname t, Object... objects) {
		if (t != null){
			this.processPath(t.getPath());
		}
	}

}
