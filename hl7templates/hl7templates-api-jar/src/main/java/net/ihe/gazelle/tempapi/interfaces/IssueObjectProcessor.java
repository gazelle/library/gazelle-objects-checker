package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueObject;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueObjectType;

public interface IssueObjectProcessor extends Processor<IssueObject> {
	
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processId(String id);
		public void processName(String name);
		public void processType(IssueObjectType type);
	
}
