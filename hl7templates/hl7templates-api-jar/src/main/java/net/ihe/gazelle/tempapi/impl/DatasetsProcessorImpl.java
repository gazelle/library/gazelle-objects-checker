package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DatasetsProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Datasets;

public abstract class DatasetsProcessorImpl implements DatasetsProcessor {

	@Override
	public void process(Datasets t, Object... objects) {
		if (t != null){
			this.processDatasets(t.getDataset());
		}
	}

}
