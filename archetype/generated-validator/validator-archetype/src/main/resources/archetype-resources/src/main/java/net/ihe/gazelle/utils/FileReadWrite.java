package net.ihe.gazelle.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class FileReadWrite {

	public static void printDoc(String doc, String name) throws IOException {
		FileWriter fw = new FileWriter(new File(name));
		fw.append(doc);
		fw.close();
	}

	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name), StandardCharsets.UTF_8));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
