package net.ihe.gazelle.assembler.${projectName};

import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class NullFlavorCheckerTest {

    @Test
    public void testValidate(){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Node node;
        Document doc;
        NullFlavorChecker nullFlavorChecker = new NullFlavorChecker();
        List<Notification> notifications = new ArrayList<>();

        try {
            String xmlString = FileUtils.readFileToString(new File("src/test/resource/testNullFlavor.xml"), StandardCharsets.UTF_8.toString());
            String xmlExpectedResult = FileUtils.readFileToString(new File("src/test/resource/checkedNullFlavorResult.xml"), StandardCharsets.UTF_8.toString());
            //Create DocumentBuilder with default configuration
            builder = dbf.newDocumentBuilder();

            //Parse the content to Document object
            doc = builder.parse(new InputSource(new StringReader(xmlString)));
            node = doc.getFirstChild();
            String xmlResult = nullFlavorChecker.flatten(node, "/ClinicalDocument", notifications);
            Assert.assertEquals(xmlExpectedResult, xmlResult);
            Assert.assertEquals(2, notifications.size());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testParse(){

        net.ihe.gazelle.cda.POCDMT000040ClinicalDocument clin = null;
        List<Notification> ln = new ArrayList<Notification>();
        try {
            clin = CDATemplateParser.loadBasic(new FileInputStream(new File("src/test/resource/testParse.xml").getAbsolutePath()));

            // Null Flavor check implemented here //
            Node clinNode = clin.get_xmlNodePresentation();
            NullFlavorChecker nullFlavorChecker = new NullFlavorChecker();
            nullFlavorChecker.flatten(clinNode, "/ClinicalDocument", ln);

            //Assert.assertEquals(FileUtils.readFileToString(new File("src/test/resource/testParse.xml"), StandardCharsets.UTF_8.toString()), NullFlavorChecker.nodeToString(clinNode));
            net.ihe.gazelle.cda.POCDMT000040ClinicalDocument clinNullFlavorChecked =
                    CDATemplateParser.loadBasic(new ByteArrayInputStream(NullFlavorChecker.nodeToString(clinNode).getBytes()));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

}
