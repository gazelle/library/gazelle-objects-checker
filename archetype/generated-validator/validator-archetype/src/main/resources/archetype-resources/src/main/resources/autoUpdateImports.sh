#!/bin/bash

echo "Updating imports..."
dir=`pwd`
cd ..
projectDir=`pwd`

echo "from autoUpdateImports: {$1} {$2}"

java -jar "${dir}/UpdateImports-1.0.jar" -path $dir -props $1 -project $2