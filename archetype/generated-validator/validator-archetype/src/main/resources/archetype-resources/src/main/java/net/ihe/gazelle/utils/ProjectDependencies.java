package net.ihe.gazelle.utils;

public class ProjectDependencies {

	public static String CDA_XSD =  "resources/xsd/CDA.xsd";;
	
	public static String CDA_EPSOS_XSD = "resources/xsd/CDA_extended.xsd";
	
	public static String CDA_XSL_TRANSFORMER= "resources/mbvalidatorDetailedResult.xsl";
	
	public static String VALUE_SET_REPOSITORY= "resources/valueSets/";
	
}
