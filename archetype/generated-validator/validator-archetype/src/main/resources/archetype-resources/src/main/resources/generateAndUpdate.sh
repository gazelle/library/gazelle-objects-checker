#!/bin/bash

echo "from generateAndUpdate: {$1} {$2} {$3}"

if [[ $# -ne 3 ]]; then
  echo "invalid number of parameters, required 3"
  echo "usage: $0 <cdaProps> <projectName> <mvnPath>"
  exit 1
fi


if [[ ! -f $3 ]]; then
  echo "invalid binary path to maven"
  exit 1
fi

$3 mbgen:genvalidator

$3 exec:exec@UpdateImports -DcdaProps="${1}" -DprojectName="${2}"