package net.ihe.gazelle.goc;

public class ValidatorInstantiator {

    private static final String CD_COMMAND = "cd \"";

    public static String generateArchetype(String workspacePath, String mvnExecPath, String gocVersion, String projectName, String projectNameCapitalyzed,
                                           String configuration, String projectNameCapitalyzedFirst, String projectServiceName, String folderOutput,
                                           String ignoreCdaBasic, String javaHome) {
        String loadMethod;
        String validatorRootClass;
        if ("cdaepsos".equals(configuration)) {
            validatorRootClass = "net.ihe.gazelle.cdaepsos4.POCDMT000040ClinicalDocument";
            loadMethod="load";
        } else if ("cdaxehealth".equals(configuration)){
            validatorRootClass = "net.ihe.gazelle.cdaxehealth.POCDMT000040ClinicalDocument";
            loadMethod="load";
        }
        else {
            validatorRootClass = "net.ihe.gazelle.cda.POCDMT000040ClinicalDocument";
            loadMethod = "loadBasic";
        }
        String javaEnv = "";
        if(javaHome != null && !javaHome.isEmpty()){
            javaEnv = "env $JAVA_HOME=\""+javaHome+"\" ";
        }
        String version = getVersion();
        version = version==null?gocVersion:version;
        return CD_COMMAND + workspacePath + "\"\n" + javaEnv + mvnExecPath + " org.apache.maven.plugins:maven-archetype-plugin:2.3:generate -B " +
                "-DarchetypeGroupId=net.ihe.gazelle.goc " +
                "-DarchetypeArtifactId=validator-archetype " +
                "-DarchetypeVersion=" + version + " " +
                "-DprojectName=" + projectName + " " +
                "-DgroupId=net.ihe.gazelle.goc " +
                "-DartifactId=" + projectName + "-validator-jar " +
                "-Dversion=" + gocVersion + " " +
                "-DprojectNameCapitalyzed=" + projectNameCapitalyzed + " " +
                "-DvalidatorRootClass=" + validatorRootClass + " " +
                "-DloadMethod=" + loadMethod + " " +
                "-DprojectNameCapitalyzedFirst=" + projectNameCapitalyzedFirst + " " +
                "-DignoreCdaBasic=" + ignoreCdaBasic + " " +
                "-DfolderOutput=\"" + folderOutput + "\" " +
                "-DprojectServiceName=\"" + projectServiceName + "\"";
    }

    public static String getVersion(){
        return ValidatorInstantiator.class.getPackage().getImplementationVersion();
    }
}
