---
title:  Installation & user manual
subtitle: Installation Guide of Gazelle Objects Checker
authors: Achraf Achkari
date: 19/10/2021
---

# Installation Guide of Gazelle Objects Checker

### Table of Content
1. ##### [Introduction](#introduction)
2. ##### [Clone Project](#cloneProject)
3. ##### [Run Tests (Optional)](#runTests)
4. ##### [Prepare BBR](#prepareBBR)
5. ##### [Generate Artifact](#generateArtifact)
6. ##### [Generate Validator](#generateValidator)
7. ##### [Generation Validator form UML (Alternative)](#generateValidatorFromUML)
8. ##### [Validate CDA](#validateCDA)
9. ##### [Utility Scripts](#utilityScripts)
10. ##### [System Testing](#systemTesting)
11. ##### [Extras](#extras)

<div id='introduction'></div>

## 1. Introduction
In this guide, we will show how to retrieve the new version of **Gazelle Objects Cheker**,
build it and generate a CDA Validator in a generic case, and client specific case

**Requirements:**
- **JDK 11** or higher
- Maven **3.5.x** /!\ You need to respect this version, or the tool won't work
  - Thanks to maven wrapper, this version is packaged now with GOC, you can find it in `gazelle-objects-checker/hl7templates/hl7templates-packager-jar/src/main/resources/maven-wrapper`
- Git

<div id='cloneProject'></div>

## 2. Clone Project
the project is not maintained by the _forge_ anymore.<br>
Gazelle Objects Checker has been migrated to **[Gitlab](https://gitlab.inria.fr)** and packaged under one module called **gazelle-objects-checker**<br>
If you worked already with the previous versions of GOC, the following structure could be familiar to you:
- Archetypes
  - generated-validator
- Hl7templates
  - goc-tests
  - gocmodel-jar
  - hl7templates-model-jar
  - hl7templates-api-jar
  - hl7templates-generator-jar
  - hl7templates-packager-jar
- Models
  - cda-models
  - cdaepsos-models
  - datatypes-models
  - common-models
  - infr-model
  - model-resources
  - nblock-model
  - voc-model

In the current version `3.x.x`, the `Archtypes` and `Hl7templates` modules _(with all their submodules)_ are packaged in one module `Gazelle Objects Checker` <br>
The resources models are packaged in a module called `UML Models`.

<table>
  <thead>
    <th>Project</th>
    <th>Link Gitlab</th>
  </thead>
  <tr>
    <td>Gazelle Objects Checker</td>
    <td><a href="https://gitlab.inria.fr/gazelle/library/gazelle-objects-checker">https://gitlab.inria.fr/gazelle/library/gazelle-objects-checker</a></td>
  </tr>
  <tr>
    <td>UML Models</td>
    <td><a href="https://gitlab.inria.fr/gazelle/library/uml-models">https://gitlab.inria.fr/gazelle/library/uml-models</a></td>
  </tr>
</table>

<br>

##### Cloning gazelle-objects-checker:

you need to have `git` and `maven` installed<br><br>
Run the following command:
```bash
> git clone https://gitlab.inria.fr/gazelle/library/gazelle-objects-checker.git
> cd gazelle-objects-checker
```

> Notice: the installation of gazelle-objects-checker will be done in the [Generate Artifact](#generateArtifact) section.<br>
You can skip tests if you want, but it's not recommend.
>
> You don't have to install the `uml-models` module, it's already released




<div id='runTests'></div>

## 3. Run Tests (Optional)

_/!\ You can skip this section, but it's recommended to have all tests passed before continue_

GOC provides two types of tests, unit tests and integration tests.<br>
The unit tests are _environment independent_.
The integration tests create some temporary resources (UML Models) using the `uml-modes` (no need to have it installed locally, it has been packaged in [Nexus](https://gazelle.ihe.net/nexus/#nexus-search;quick~uml-models))
<br>
**Run Unit tests**
```bash
> cd gazelle-objects-checker

> mvn test
```

**Run Integration Tests**
```bash
> cd gazelle-objects-checker

> mvn verify
```

If Something went wrong, make sure you have mentioned in maven logs:
- Resources created successfully
- Resources deleted successfully

If not, try to run the creation resources script `CreateResourcesForTest.java` in:
- _gazelle-objects-checker/gocmodel-jar/src/main/java/net.ihe.gazelle.goc.script_

<div id='prepareBBR'></div>

## 4. Prepare BBR
BBR is an abbreviation of Building Block Repository, a large XML file containing all the functional specifications,
that will be used by GOC to generate OCL which is converted finally to a Java validator.<br>
So Basically, the BBR is considered as **the main input for GOC**

Each client provide his own BBR, and we can retrieve it from the ART-DECOR platform that belong to this client.<br>
We'll take the example of a BBR in SEQUOIA's ART-DECOR.

To retrieve the project follow the link:
>[https://gazellecontent.sequoiaproject.org/apps/decor/services/RetrieveProject](https://gazellecontent.sequoiaproject.org/apps/decor/services/RetrieveProject)

Choose project as _**(BBR) Consolidated CDA (en-US)**_.<br>
Make sure to select the **_Compiled_** version. <br>
Then check the **_Download_** radio.<br>
Press **_send_** button.<br>

Once you download it, you need to adapt it before using it in the next section of generation.<br>

You first need to open it _(as it is a very large file, I recommend you to use VSCode as it's optimized for that )_
then change **ccda-** by **ccda(version)-** for example for version 2.1:
**ccda21-** (without the '.')

NOTICE: A script could be released for this, due to performance issues

<div id='generateArtifact'></div>

## 5. Generate Artifact

After having all tests working _(not mandatory but recommended)_ we can now install the `gazelle-objects-checker` module and generate the artifact.
<br>
The Artifact generated is a `.jar` suffixed by `jar-with-dependencies` as it's created as a **standalone** application.
<br><br>
To do we run the following command:
```bash
> mvn clean install
or
> mvn clean install -DskipTests=true #(to skip tests)
```


The Artifact is generated at _**gazelle-objects-checker/hl7templates-packager-jar/target/hl7templates-packager-jar-X.X.X-SNAPSHOT-jar-with-dependencies.jar**_

> The X.X.X refers to the current SNAPSHOT version
>

> _**HINT**_: if you still have compile errors, you can install just the archetypes module (as it's required at runtime) with:
>
> ```bash
> cd gazelle-objects-checker/archetype/generated-validator
> 
> mvn clean install
> ```
>
> And retrieve GOC Artifact from nexus.
>
> here: [GOC Artifact Nexus](https://gazelle.ihe.net/nexus/#nexus-search;quick~hl7templates-packager)
> Choose the latest version, and download the: **hl7templates-packager-jar-X.X.X-jar-with-dependencies.jar** artifact

### Generate tests coverage report: 

#### Overview:
Some Tests methods in GOC, are annotated with `@Covers` annotation (GOC Specific), 
these methods tests a given HL7 Specification, these annotations are used later to generate a coverd 
report if requested.

#### Usage
You can generate a coverage report of all covered HL7 specifications by GOC, with passing the 
following parameters during install process:
- `-Dcsv`: Path to a CSV that will aggregate all
- `-Ddest`: Destination path to generated XML report
- `-Dtype`: (OPTIONAL) Report type (Only XML is available so far, which is the default one)

> Notice: if one of the first parameters is missing, coverage report not generated, but packagin
> should be done succcessfully

/!\ Be careful to not generate the coverage with same csv file, because it will aggregate with old results

<div id='generateValidator'></div>

## 6. Generate Validator
This section remains to **run** Gazelle Objects Checker and generate the CDA MBVal validator.

we will need the following parameters to run GOC and generate the validator:



<table>
  <thead>
    <th>Parameters</th>
    <th>Description</th>
    <th>Type</th>
    <th>Example</th>
    <th>is Required</th>
  </thead>
  <tr>
    <td>-bbr</td>
    <td>Path to the BBR file (provided by ART-DECOR)</td>
    <td>XML File</td>
    <td>/home/mylap/Documents/bbr-eHDSI-5.0.1.xml</td>
    <td><b>Required</b></td>
  </tr>
  <tr>
    <td>-out</td>
    <td>Path where to generate the validator</td>
    <td>Directory</td>
    <td>/home/mylap/Documents/CDA-Validators/output-pivot-19-10-2021</td>
    <td><b>Required</b></td>
  </tr>
  <tr>
    <td>-mvn</td>
    <td>Path to maven <b>binary</b></td>
    <td>Binary (executable)</td>
    <td>/opt/apache-maven-3.5.4/bin/mvn</td>
    <td><b>Required</b></td>
  </tr>
  <tr>
    <td>-serviceName</td>
    <td>Validation Service Name</td>
    <td>String</td>
    <td>Pivot (for dgsante)</td>
    <td>not Required</td>
  </tr>
  <tr>
    <td>-HL7TEMP_CDACONFFOLDERNAME </td>
    <td>HL7template CDA config folder name</td>
    <td>cdabasic or cdaepsos</td>
    <td>cdaepsos (for dgsante)</td>
    <td>not Required (default=cdabasic)</td>
  </tr>
  <tr>
    <td>-ignoreTemplateIdRequirements</td>
    <td>ignore TemplateId Requirements when generating the constraints (useful for C-CDA validation tools) name</td>
    <td>Boolean</td>
    <td>VOID, parameter present ==> it's true, not present ==> it's false</td>
    <td>not Required (default=false [Not present])</td>
  </tr>
  <tr>
    <td>-ignoreCdaBasicRequirements</td>
    <td>ignore TemplateId Requirements when generating the constraints (useful for C-CDA validation tools) name</td>
    <td>Boolean</td>
    <td>VOID, parameter present ==> it's true, not present ==> it's false</td>
    <td>not Required (default=false [Not present])</td>
  </tr>
  <tr>
    <td>-versionLabel</td>
    <td>VersionLabel that will be used during the generation</td>
    <td>String</td>
    <td>2.1 or 1.1 (for SEQUOIA)</td>
    <td>not Required</td>
  </tr>
</table>

Move to the hl7templates-packager-jar/target directory and run the following command with java 11:
```bash
>cd gazelle-objects-checker/hl7templates/hl7templates-packager-jar/target
```

Generic command with all possible parameters: <br>

```bash
> java -jar hl7templates-packager-jar-X.X.X[-SNAPSHOT]-jar-with-dependencies.jar -bbr <Path_To_BBR> 
-out <Path_To_Output> -mvn <Path_To_MVN> [-serviceName <Service_Name>] [-HL7TEMP_CDACONFFOLDERNAME <cdabasic|cdaepsos>] 
 [-ignoreTemplateIdRequirements <true|false>] [-ignoreCdaBasicRequirements <true|false>]
[-versionLabel <version>] [-rootClassName <Root_ClassName>] [-HL7TEMP_RESOURCES_PATH <Path_To_Resources>]
```

_The [X.X.X] refers to the current used version of GOC, and it's not present if we retrieve a release from Nexus._

Example with SEQUOIA:
```bash
> java -jar hl7templates-packager-jar-3.0.0-SNAPSHOT-jar-with-dependencies.jar -bbr ~/Documents/BBR-SEQUOIA.xml
 -out home/mylap/Documents/GOC-OUTPUT/output-sequoia-mu3 -mvn /opt/apache-maven-3.5.4/bin/mvn
 -serviceName "HL7 - C-CDA R2.1 - Meaningful Use Stage 3" -ignoreTemplateIdRequirements 
 -ignoreCdaBasicRequirements  -versionLabel 2.1
```

**For logs:**
> Go to the root folder of the provided output path, then logs/logs-[generation_date_time]
>
> `cd /home/mylap/Documents/GOC-OUTPUT/logs/logs-[generation_date_time]`

**Logs are separated as follows:**
- **generated_report.log:** contains synthetic report about the generation process (`Warnings` and `Errors`)
- **detailed_report.log:** contains all the logging information from the generation process (`Infos`, `Warnings`, `Errors`)
- **cmdOutput.log:** contains the Standard Output of executed commands during generation (maven command to init project with archetype, generate code from OLC, packaging...)
- **cdeModelsErrors.txt:** contains all detected inconsistencies between the provided BBR and pre-defined models used by GOC

![Logs Conception](Improved_Logging_Conception_v2.jpg "LogsConception")

<div id='generateValidatorFromUML'></div>

## 7. Generate Validator from UML (Alternative)

This section is just an alternative way to regenerate the validator when the functional specifications change
without re-running GOC, but just from the generated ```model.uml``` file.

```bash
> cd /home/mylap/Documents/GOC-OUTPUT/validator/[validator-name]-validator-jar/model/[validator-name].uml
```
for example for epsos5

```bash
> cd /home/mylap/Documents/GOC-OUTPUT/validator/epsos5-validator-jar/model/epsos5.uml
```

Then you can edit it with a Text Editor, or a dedicated modeling tool (ex: TopCased).

**After saving your model, you need to:**

Go to GOC-OUTPUT/epsos5-validator-jar and do the following:
```bash
> cd /home/mylap/Documents/GOC-OUTPUT/validator/epsos5-validator-jar
> mvn exec:exec@generateValidator -DmvnPath="/opt/apache-maven-3.5.4/bin/mvn" -DcdaProps="cdaepsos" -DprojectName="epsos5"
> mvn clean package
```

The generic form of the command is:

```bash
> mvn exec:exec@generateValidator -DmvnPath="[maven-path]" -DcdaProps="[cdaepsos or cdabasic]" -DprojectName="[validator-name]"
```

Your validator will be available at: GOC-OUTPUT/epsos5-validator-jar/target/appassembler/bin
and do the following:

```bash
> cd /home/mylap/Documents/GOC-OUTPUT/epsos5-validator-jar/target/appassembler/bin
> chmod +x validator.sh
```

Now you can refer to [Section 8](#validateCDA) to Validate a CDA document with your new validator.




<div id='validateCDA'></div>

## 8. Validate CDA

Now we are going to use the generated validator.<br>

We will suppose we generated a validator with a BBR containing the following version: epsos5-.<br>

Open the following directory:
```bash
> cd <Path_To_Output>/validator/epsos-validator-app/bin

> chmod +x validator.sh  #if on linux 

> ./validator.sh -path <Path_to_CDA.xml> -out <Path_To_output_result.xml>
```

For SEQUOIA with version ccda21, you need to adapt it to SVS first, and for that we have a generation script that handle all of that automatically:
```bash
> cd gazelle-objects-checker/hl7templates/hl7templates-packager-jar/target
> ./SEQUOIAGeneration.sh
# or just for adapting SVS you can use the SEQUOIASVSAdaptation.sh
```

Then:

```bash
> cd <Path_To_Output>/ccda21-validator-jar/target/appassembler/bin

> chmod +x validator.sh  #if on linux 

> ./validator.sh -path <Path_to_CDA.xml> -out <Path_To_output_result.xml>
```

Here is a summary of possible parameters for the validator:
<table>
  <thead>
    <th>Parameters</th>
    <th>Description</th>
    <th>Type</th>
    <th>Example</th>
    <th>is Required</th>
  </thead>
  <tr>
    <td>-path</td>
    <td>Path to the CDA Document we want to validate</td>
    <td>Path to an XML file</td>
    <td>/home/mylap/Documents/eHDSI_CDA.xml</td>
    <td><b>Required</b></td>
  </tr>
  <tr>
    <td>-out</td>
    <td>Path to save the generated results</td>
    <td>Path to an XML file</td>
    <td>../../results1.xml</td>
    <td><b>Recommended: </b>If not provided, output is redirected to STDOUT</td>
  </tr>
  <tr>
    <td>-b64</td>
    <td>Provide a path to a document in Base64</td>
    <td>Path to an XML file</td>
    <td>/home/mylap/Documents/eHDSI_CDA_B64.xml</td>
    <td>Don't use it if you use the -path parameter</td>
  </tr>
  <tr>
    <td>-html</td>
    <td>Generate html as output of validation</td>
    <td>Boolean</td>
    <td>VOID, parameter present ==> it's true, not present ==> it's false</td>
    <td>Not Required</td>
  </tr>
  <tr>
    <td>-resources</td>
    <td>Full path to the resources folder (XSD schema + valueSets) </td>
    <td>Path to directory</td>
    <td>/home/mylap/resources/</td>
    <td>Not Required</td>
  </tr>

</table>

Here is an example to validate a document with normal case:

```bash
> ./validator.sh -path /home/mylap/eHDSI_CDA.xml -out ../../results1.xml
```

**HINT**
If you got some errors related to the XSD Validation, you can adapt your XSD at:<br>
```bash
cd <Path_To_Output>/epsos5-validator-app/bin/resources/valresources/xsd.<br>
```

If you want to test the generated validator, you can use the provided SoapUI project provided in:

> gazelle-objects-checker/testing/MBValGOC-soapui-project.xml

Just import it to SoapUI and run the test cases.



<div id='utilityScripts'></div>

## 9. Test GOC with default BBR and CDA

If you want to test goc with a default bbr and cda:
Path to bbr file:
```bash 
gazelle-objects-checker/hl7templates/goc-tests/goc-tests-runner/src/main/resources/global/decor_global.xml
```
Path to cda file:
```bash 
gazelle-objects-checker/hl7templates/goc-tests/goc-tests-runner/src/main/resources/global/cda_global.xml
```

## 10. Utility Scripts

As we saw in the previous section: [Generate Validator](#generateValidator), there is too many options to remember for generating a validator.<br>
Therefore, that decreases the usability of the tool, so we created some utility scripts for both common generation types:
- CDA Epsos Generation (for eHDSI)
- CDA Basic Generation (for Sequoia Project)

> /!\ You need to set **$JAVA_HOME** before running the script, or override it inside.

You can find the scripts directly in the **target** directory:

```bash
> cd gazelle-objects-checker/hl7templates/hl7templates-packager-jar/target
> ./eHDSIGeneration.sh
```
And just follow the instructions shown by the interactive script.

>HINT: if the script doesn't work, try:
> ```bash
> chmod +x eHDSIGeneration.sh
> ```
> **And re-run it**

For SEQUOIA:
```bash
> cd gazelle-objects-checker/hl7templates/hl7templates-packager-jar/target
> ./SEQUOIAGeneration.sh
```

<div id='systemTesting'></div>

## 11. System Testing

### Overview: 
Due to the variety of the HL7 specifications handled by GOC, and the Conversion & Generation processes 
that it executed, we created a new module to perform a System Test for all known (i.e main) rules by GOC.
Each specification has its own _DECOR_, _CDA_ and _Expected Results_  for comparaison.<br>
The following specifications are supported so far by GOC System Testing module:
- Cardinality
- Mandatory
- Vocabulary
- Context
- Datatype
- Fixed Value
- **GLOBAL****

** The **_Global_** type is not a specification, but a pre-defined profile with _DECOR_, _CDA_ and _Expected Results_
that contains all other specifications, for a general system testing.

### Configuration
Before using the provided APIs, you need to make a basic configuration in:
- `application.properties`<br>

Required properties to be set:
- `maven.path`: The binary path to your maven installation (you can reference the wrapped one)
- `result.type`: **xml** for xml generated report or **standard** for console output
- `result.type.xmlpath`: You need to set a path if the `result.type` is set to XML

Optional properties (could keep the default value):
- `goc.persistValidator`: set to true if you want to keep the generated validator during the test
- `goc.persistResult`: set to true if you want to keep the validation result during test

**NOT RECOMMENDED** to change: 
- `goc.outputFolderPrefix`: prefix of the generation folder
- `goc.resultFolderPrefix`: prefix for the results folder

### Usage:
####11.1 From IDE with JUnit

The module have an integrated interface with JUnit annotations, with pre-defined assertions (that could be overriden
by the use), so could easly be executed from your IDE.
The main Test Class called `SystemTest` at
`hl7templates/goc-tests/goc-tests-runner/src/test/java/net/ihe/gazelle/goctests/SystemTests.java`

####11.2 From Command Line Interface
The module provides a user-friendly CLI with one parameter `-S` or `--spec-type` 
The executable jar in `hl7templates/goc-tests/goc-tests-runner/target/goc-tests-runner-[X.X.X]-jar-with-dependencies.jar`

```bash
> java -jar goc-tests-runner-[X.X.X]-jar-with-dependencies.jar --spec-type <SPECIFICATION>
```

Available options for `SPECIFICATION` are: 
- cardinality
- mandatory
- vocabulary
- context
- datatype
- "fixed value"
- global

<div id='extras'></div>

####11.3 GOC System Testing guide for developers

You can find more details about System Testing:
- [GOC System Testing guide for developers](https://saskereval-my.sharepoint.com/:w:/g/personal/nabila_aazibou-el-gerrab_kereval_com/EfRES8rEmoVEt0Ty2NPnljMBPfmnWWdJ02YjJw_x-xTPWA?e=wk0EJM)


## 12. Extras

You can find some previous made documentation here _(belong the old GOC, and could probably not represent the actual version)_:
- [Gazelle Object Checker General Public info - GOC](https://docs.google.com/document/d/1265YcvKk983Fna2Y8TI6V9O1wnKqERFAibIlVGYgW20/edit?usp=sharing)
- [GOC Installation guide for developers](https://docs.google.com/document/d/1G5bmLhFKNjOvg_rxEllidTofYrwxBeZG_kigTM0pqXM/edit?usp=sharing)

#### Next released documentation

- Functional description of GOC


